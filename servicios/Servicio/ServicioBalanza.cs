﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Repositorio;
using log4net;

namespace Balanza.Servicio
{
    public class ServicioBalanza : IServicioBalanza
    {
        protected readonly IRepositorio repositorio;
        protected ILog logger;

        public ServicioBalanza(IRepositorio _repositorio)
        {
            repositorio = _repositorio;
            log4net.Config.XmlConfigurator.Configure();
            ILog Log = LogManager.GetLogger("Liquidacion");
            logger = Log;
        }
        public ServicioBalanza() { }

        #region [Accesos]
        public CollectionResponse<AccesoModel> GetAllAccesos()
        {
            logger.InfoFormat("Servicio, Accesos:   GetAllAccesos()");

            IEnumerable<AccesoModel> lstListaGeneral = new List<AccesoModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<AccesoModel>("dbo.[spAccesos_GetAll]").ToList();
                return new CollectionResponse<AccesoModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Accesos:  Error en el metodo GetAllAccesos()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<AccesoModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [RolAcceso]
        public CollectionResponse<RolAccesoModel> GetRolAcceso(int? intIdRol)
        {
            logger.InfoFormat("Servicio, RolAcceso: GetRolAcceso(int? intIdRol =: {0})", intIdRol);

            IEnumerable<RolAccesoModel> lstListaGeneral = new List<RolAccesoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intCodRol", intIdRol));
                lstListaGeneral = repositorio.ExecuteSP<RolAccesoModel>("dbo.[spRolAcceso_Get]", lstParametros).ToList();
                return new CollectionResponse<RolAccesoModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, RolAcceso:    Error en el metodo GetRolAcceso(int? intIdRol =: {0})", intIdRol);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RolAccesoModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Rol]
        public CollectionResponse<RolModel> GetAllRoles()
        {
            logger.InfoFormat("Servicio, Rol:   GetAllRoles()");

            IEnumerable<RolModel> lstListaGeneral = new List<RolModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<RolModel>("dbo.[spRol_GetAll]").ToList();
                return new CollectionResponse<RolModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo GetAllRoles()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RolModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public int PutRol(RolModel objRol)
        {
            logger.InfoFormat("Servicio, Rol:   PutRol(RolModel objRol=: <strDescripcion=:{0}, strUsuarioCrea=:{1}>)", objRol.strDescripcion, objRol.strUsuarioCrea);

            try
            {
                SqlParameter parAccesos = GetAccesos("listaAccesos", objRol.lstAccesos);

                IList <SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strDescripcion", objRol.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objRol.strUsuarioCrea));
                lstParametros.Add(parAccesos);

                return repositorio.ExecuteSP<int>("dbo.[spRol_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo PutRol(RolModel objRol=: <strDescripcion=:{0}, strUsuarioCrea=:{1}>)", objRol.strDescripcion, objRol.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        private SqlParameter GetAccesos(string name, List<AccesoModel> lstListaAcc)
        {
            logger.InfoFormat("Servicio, Rol:   GetAccesos(string name=:{0}, List<AccesoModel> lstListaAcc)", name);

            try
            {
                DataTable table = new DataTable("dbo.lstAccesos");
                table.Columns.Add("intAcceso", typeof(int));
                table.Columns.Add("strNombre", typeof(string));

                foreach (AccesoModel acceso in lstListaAcc)
                    table.Rows.Add(new object[] { acceso.intCodAcceso, acceso.strTipoAcceso });

                SqlParameter parameter = new SqlParameter(name, table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.lstAccesos";

                return parameter;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo GetAccesos(string name=:{0}, List<AccesoModel> lstListaAcc)", name);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        public int UpdateRol(RolModel objRol)
        {
            logger.InfoFormat("Servicio, Rol:   UpdateRol(RolModel objRol=: <intCodRol=:{0}, strDescripcion=:{1}, strUsuarioModif=:{2}>)", objRol.intCodRol, objRol.strDescripcion, objRol.strUsuarioModif);

            try
            {
                SqlParameter parAccesos = GetAccesos("listaAccesos", objRol.lstAccesos);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intCodRol", objRol.intCodRol));
                lstParametros.Add(new SqlParameter("strDescripcion", objRol.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objRol.strUsuarioModif));
                lstParametros.Add(parAccesos);

                return repositorio.ExecuteSP<int>("dbo.[spRol_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo UpdateRol(RolModel objRol=: <intCodRol=:{0}, strDescripcion=:{1}, strUsuarioModif=:{2}>)", objRol.intCodRol, objRol.strDescripcion, objRol.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteRol(int? intIdRol, string strUsuario)
        {
            logger.InfoFormat("Servicio, Rol:   DeleteRol(int? intIdRol=:{0}, string strUsuario=:{1})", intIdRol, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intCodRol", intIdRol));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spRol_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo DeleteRol(int? intIdRol=:{0}, string strUsuario=:{1})", intIdRol, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region[Usuarios]
        public CollectionResponse<UsuarioModel> GetAllUsuarios()
        {
            logger.InfoFormat("Servicio, Usuarios:  GetAllUsuarios()");

            IEnumerable<UsuarioModel> lstListaGeneral = new List<UsuarioModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<UsuarioModel>("dbo.[spUsuarios_GetAll]").ToList();
                return new CollectionResponse<UsuarioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo GetAllUsuarios()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<UsuarioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public string PutUsuario(UsuarioModel objUsuario)
        {
            logger.InfoFormat("Servicio, Usuarios:  PutUsuario(UsuarioModel objUsuario=: <strUsuario=:{0}, strUsuarioCrea=:{1}>)", objUsuario.strUsuario, objUsuario.strUsuarioCrea);

            try
            {
                string strEncode = Security.Encriptar(objUsuario.strPassword);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombres", objUsuario.strNombres));
                lstParametros.Add(new SqlParameter("strApellidoPat", objUsuario.strApellidoPat));
                lstParametros.Add(new SqlParameter("strApellidoMat", objUsuario.strApellidoMat));
                lstParametros.Add(new SqlParameter("strEmail", objUsuario.strEmail));
                lstParametros.Add(new SqlParameter("strUsuario", objUsuario.strUsuario));
                lstParametros.Add(new SqlParameter("strPassword", strEncode));
                lstParametros.Add(new SqlParameter("strCargo", objUsuario.strCargo));
                lstParametros.Add(new SqlParameter("strSuperAdmin", objUsuario.strSuperAdmin));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objUsuario.strUsuarioCrea));

                string result =  repositorio.ExecuteSP<string>("dbo.[spUsuarios_Put]", lstParametros).First();
                return "Usuario guardado correctamente.";
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo PutUsuario(UsuarioModel objUsuario=: <strUsuario=:{0}, strUsuarioCrea=:{1}>)", objUsuario.strUsuario, objUsuario.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return e.ToString();
                throw e;
            }
        }

        public string UpdateUsuario(UsuarioModel objUsuario)
        {
            logger.InfoFormat("Servicio, Usuarios:  UpdateUsuario(UsuarioModel objUsuario=: <strCodUsuario=:{0}, strUsuario=:{1}, strUsuarioModif=:{2}>)", objUsuario.strCodUsuario, objUsuario.strUsuario, objUsuario.strUsuarioModif);

            try
            {
                UsuarioModel objUser = getUserS(objUsuario.strCodUsuario);
                string strEncode = "";
                if (objUser.strPassword == objUsuario.strPassword)
                {
                    strEncode = objUsuario.strPassword;
                }
                else {
                    strEncode = Security.Encriptar(objUsuario.strPassword);
                }

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodUsuario", objUsuario.strCodUsuario));
                lstParametros.Add(new SqlParameter("strCodPersona", objUsuario.strCodPersona));
                lstParametros.Add(new SqlParameter("strNombres", objUsuario.strNombres));
                lstParametros.Add(new SqlParameter("strApellidoPat", objUsuario.strApellidoPat));
                lstParametros.Add(new SqlParameter("strApellidoMat", objUsuario.strApellidoMat));
                lstParametros.Add(new SqlParameter("strEmail", objUsuario.strEmail));
                lstParametros.Add(new SqlParameter("strUsuario", objUsuario.strUsuario));
                lstParametros.Add(new SqlParameter("strPassword", strEncode));
                lstParametros.Add(new SqlParameter("strCargo", objUsuario.strCargo));
                lstParametros.Add(new SqlParameter("strSuperAdmin", objUsuario.strSuperAdmin));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objUsuario.strUsuarioModif));

                string result = repositorio.ExecuteSP<string>("dbo.[spUsuarios_Update]", lstParametros).First();
                return "Usuario modificado correctamente.";
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo UpdateUsuario(UsuarioModel objUsuario=: <strCodUsuario=:{0}, strUsuario=:{1}, strUsuarioModif=:{2}>)", objUsuario.strCodUsuario, objUsuario.strUsuario, objUsuario.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return e.ToString();
                throw e;
            }
        }

        public string DeleteUsuario(string strIdUsuario, string strUsuario)
        {
            logger.InfoFormat("Servicio, Usuarios:  DeleteUsuario(string strIdUsuario=:{0}, string strUsuario=:{1})", strIdUsuario, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodUsuario", strIdUsuario));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                string result = repositorio.ExecuteSP<string>("dbo.[spUsuarios_Delete]", lstParametros).First();
                return "Usuario eliminado correctamente";
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo DeleteUsuario(string strIdUsuario=:{0}, string strUsuario=:{1})", strIdUsuario, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return e.ToString();
                throw e;
            }
        }
        private UsuarioModel getUserS(string strCodUsuario)
        {
            IEnumerable<UsuarioModel> lstListaGeneral = new List<UsuarioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodUsuario", strCodUsuario));

                return repositorio.ExecuteSP<UsuarioModel>("dbo.[spUsuarios_GetUser]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo getUserS(string strCodUsuario=:{0})", strCodUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public UsuarioModel LoginUsuario(string strUsuario, string strPassword)
        {
            logger.InfoFormat("Servicio, Usuarios:  LoginUsuario(string strUsuario=:{0}, string strPassword=:******)", strUsuario);

            try
            {
                string strEncode = Security.Encriptar(strPassword);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));
                lstParametros.Add(new SqlParameter("strPassword", strEncode));

                UsuarioModel usuario = repositorio.ExecuteSP<UsuarioModel>("dbo.[spUsuarios_Login]", lstParametros).First();
                return usuario;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo LoginUsuario(string strUsuario=:{0}, string strPassword=:*****)", strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public CollectionResponse<UsuarioModel> SearchUsuarios(UsuarioModel objQuery)
        {
            logger.InfoFormat("Servicio, Usuarios:  SearchUsuarios(string strCodUsuario=:{0}, string strUsuario=:{1}, string strCargo=:{2}, string strNombres=:{3}, string strApellidoPat=:{4}, string strApellidoMat=:{5})",
                objQuery.strCodUsuario, objQuery.strUsuario, objQuery.strCargo, objQuery.strNombres, objQuery.strApellidoPat, objQuery.strApellidoMat);

            IEnumerable<UsuarioModel> lstListaGeneral = new List<UsuarioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodUsuario", objQuery.strCodUsuario));
                lstParametros.Add(new SqlParameter("strUsuario", objQuery.strUsuario)); 
                lstParametros.Add(new SqlParameter("strCargo", objQuery.strCargo));
                lstParametros.Add(new SqlParameter("strNombre", objQuery.strNombres));
                lstParametros.Add(new SqlParameter("strApellidoPat", objQuery.strApellidoPat));
                lstParametros.Add(new SqlParameter("strApellidoMat", objQuery.strApellidoMat));

                lstListaGeneral = repositorio.ExecuteSP<UsuarioModel>("dbo.[spUsuarios_Search]", lstParametros).ToList();
                return new CollectionResponse<UsuarioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo SearchUsuarios(string strCodUsuario=:{0}, string strUsuario=:{1}, string strNombres=:{2}, string strApellidoPat=:{3}, string strApellidoMat=:{4})",
                    objQuery.strCodUsuario, objQuery.strUsuario, objQuery.strNombres, objQuery.strApellidoPat, objQuery.strApellidoMat);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<UsuarioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<RolAccesoModel> GetUsuarioAccesos(string strCargo)
        {
            logger.InfoFormat("Servicio, Usuarios:  GetUsuarioAccesos(string strCargo=:{0})", strCargo);

            List<RolAccesoModel> lstListaGeneral = new List<RolAccesoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCargo", strCargo));

                lstListaGeneral = repositorio.ExecuteSP<RolAccesoModel>("dbo.[spUsuario_GetAccesos]", lstParametros).ToList();
                return new CollectionResponse<RolAccesoModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo GetUsuarioAccesos(string strCargo=:{0})", strCargo);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public UsuarioModel GetPersona(string strIdPersona)
        {
            logger.InfoFormat("Servicio, Usuarios:  GetPersona(string idPersona=:{0})", strIdPersona);

            List<RolAccesoModel> lstListaGeneral = new List<RolAccesoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodPersona", strIdPersona));

                UsuarioModel persona = repositorio.ExecuteSP<UsuarioModel>("dbo.[spPersona_Get]", lstParametros).First();
                return persona;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo GetPersona(string idPersona=:{0})", strIdPersona);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public string UpdatePersona(UsuarioModel objPersona)
        {
            logger.InfoFormat("Servicio, Usuarios:  UpdatePersona(UsuarioModel objPersona=: <strCodPersona=:{0}>)", objPersona.strCodPersona);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodPersona", objPersona.strCodPersona));
                lstParametros.Add(new SqlParameter("strNombres", objPersona.strNombres));
                lstParametros.Add(new SqlParameter("strApellidoPat", objPersona.strApellidoPat));
                lstParametros.Add(new SqlParameter("strApellidoMat", objPersona.strApellidoMat));
                lstParametros.Add(new SqlParameter("strEmail", objPersona.strEmail));
                lstParametros.Add(new SqlParameter("strTelefono", objPersona.strTelefono));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objPersona.strUsuarioModif));

                string success = repositorio.ExecuteSP<string>("dbo.[spPersona_Update]", lstParametros).First();
                return "Se guardaron los cambios correctamente.";
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo UpdatePersona(UsuarioModel objPersona=: <strCodPersona=:{0}>)", objPersona.strCodPersona);
                logger.ErrorFormat("Exception - {0}", e);
                return e.ToString();
                throw e;
            }
        }

        public string ChangeUsuario(UsuarioModel objUsuario)
        {
            logger.InfoFormat("Servicio, Usuarios:  ChangeUsuario(UsuarioModel objUsuario=: <strCodUsuario=:{0}>)", objUsuario.strCodUsuario);

            try
            {
                string strEncode = Security.Encriptar(objUsuario.strPassword);
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodUsuario", objUsuario.strCodUsuario));
                lstParametros.Add(new SqlParameter("strUsuarioNew", objUsuario.strUsuario));
                lstParametros.Add(new SqlParameter("strPasswordNew", strEncode));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objUsuario.strUsuarioModif));

                return repositorio.ExecuteSP<string>("dbo.[spUsuario_Change]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Usuarios: Error en el metodo ChangeUsuario(UsuarioModel objUsuario=: <strCodUsuario=:{0}>)", objUsuario.strCodUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return e.ToString();
                throw e;
            }
        }
        #endregion

        #region [Material]
        public CollectionResponse<MaterialModel> GetAllMaterial()
        {
            logger.InfoFormat("Servicio, Material:  GetAllMaterial()");

            IEnumerable<MaterialModel> lstListaGeneral = new List<MaterialModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<MaterialModel>("dbo.[spMaterial_GetAll]").ToList();
                return new CollectionResponse<MaterialModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Material: Error en el metodo GetAllMaterial()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<MaterialModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutMaterial(MaterialModel objMaterial)
        {
            logger.InfoFormat("Servicio, Material:  PutMaterial(MaterialModel objMaterial=: <strDescripcion=:{0}, strUsuarioCrea=:{1}>)", objMaterial.strDescripcion, objMaterial.strUsuarioCrea);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strDescripcion", objMaterial.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objMaterial.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spMaterial_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Material: Error en el metodo PutMaterial(MaterialModel objMaterial=: <strDescripcion=:{0}, strUsuarioCrea=:{1}>)", objMaterial.strDescripcion, objMaterial.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateMaterial(MaterialModel objMaterial)
        {
            logger.InfoFormat("Servicio, Material:  UpdateMaterial(MaterialModel objMaterial=: <intIdMaterial=:{0}, strDescripcion=:{1}, strUsuarioModif=:{2}>)", objMaterial.intIdMaterial, objMaterial.strDescripcion, objMaterial.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdMaterial", objMaterial.intIdMaterial));
                lstParametros.Add(new SqlParameter("strDescripcion", objMaterial.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objMaterial.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spMaterial_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Material: Error en el metodo UpdateMaterial(MaterialModel objMaterial=: <intIdMaterial=:{0}, strDescripcion=:{1}, strUsuarioModif=:{2}>)", objMaterial.intIdMaterial, objMaterial.strDescripcion, objMaterial.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteMaterial(int? intIdMaterial, string strUsuario)
        {
            logger.InfoFormat("Servicio, Material:  DeleteMaterial(int? intIdMaterial=:{0}, string strUsuario=:{1})", intIdMaterial, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdMaterial", intIdMaterial));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spMaterial_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Material: Error en el metodo DeleteMaterial(int? intIdMaterial=:{0}, string strUsuario=:{1})", intIdMaterial, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Registro]
        public CollectionResponse<RegistroModel> GetRegistros()
        {
            throw new NotImplementedException();
        }

        public int PutRegistro(RegistroModel objRegistro)
        {
            //logger.InfoFormat("Servicio, Registro:  PutRegistro(RegistroModel objRegistro=: <strLote=:{0}, strUsuarioCrea=:{1}>)", objRegistro.strLote, objRegistro.strUsuarioCrea);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objRegistro.strLote));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objRegistro.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strProcedenciaLg", objRegistro.strProcedenciaLg));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objRegistro.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strRucCliente", objRegistro.strClienteRuc));
                lstParametros.Add(new SqlParameter("strClienteRazonS", objRegistro.strClienteRazonS));
                lstParametros.Add(new SqlParameter("strCodConcesion", objRegistro.strCodConcesion));
                lstParametros.Add(new SqlParameter("strNombreConcesion", objRegistro.strNombreConcesion));
                lstParametros.Add(new SqlParameter("strGuiaRemitente", objRegistro.strGuiaRemitente));
                lstParametros.Add(new SqlParameter("strCodCompromiso", objRegistro.strCodCompromiso));
                lstParametros.Add(new SqlParameter("strMaterial", objRegistro.strMaterial));
                lstParametros.Add(new SqlParameter("intNroSacos", objRegistro.intNroSacos));
                lstParametros.Add(new SqlParameter("fltPesoTmh", objRegistro.fltPesoTmh));
                lstParametros.Add(new SqlParameter("strHoraIngreso", objRegistro.strHoraIngreso));
                lstParametros.Add(new SqlParameter("strHoraSalida", objRegistro.strHoraSalida));
                lstParametros.Add(new SqlParameter("strTiempoDescarga", objRegistro.strTiempoDescarga));
                lstParametros.Add(new SqlParameter("strDuenioMineral", objRegistro.strDuenioMineral));
                lstParametros.Add(new SqlParameter("strFechaMuestreo", objRegistro.strFechaMuestreo));
                lstParametros.Add(new SqlParameter("strTurnoMuestreo", objRegistro.strTurnoMuestreo));
                lstParametros.Add(new SqlParameter("strFechaRetiro", objRegistro.strFechaRetiro));
                lstParametros.Add(new SqlParameter("strNombreTransportista", objRegistro.strNombreTransportista));
                lstParametros.Add(new SqlParameter("strNroPlaca", objRegistro.strNroPlaca));
                lstParametros.Add(new SqlParameter("strGuiaTransport", objRegistro.strGuiaTransport));
                lstParametros.Add(new SqlParameter("strDniAcopiador", objRegistro.strDniAcopiador));
                lstParametros.Add(new SqlParameter("strNombreAcopiador", objRegistro.strNombreAcopiador));
                lstParametros.Add(new SqlParameter("strTurnoRegistro", objRegistro.strTurnoRegistro));
                lstParametros.Add(new SqlParameter("strObservacion", objRegistro.strObservacion));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objRegistro.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo PutRegistro(RegistroModel objRegistro=: <strLote=:{0}, strUsuarioCrea=:{1}>)", objRegistro.strLote, objRegistro.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateRegistroBalanza(RegistroModel objRegistro)
        {
            logger.InfoFormat("Servicio, Registro:  UpdateRegistroBal(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strSubLote=:{2}, fltPesoTmh=:{3}, strUsuarioModif=:{4}>)", objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strSubLote, objRegistro.fltPesoTmh, objRegistro.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objRegistro.intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", objRegistro.strLote));
                lstParametros.Add(new SqlParameter("strSubLote", objRegistro.strSubLote));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objRegistro.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strProcedenciaLg", objRegistro.strProcedenciaLg));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objRegistro.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strRegion", objRegistro.strRegion));
                lstParametros.Add(new SqlParameter("strRucCliente", objRegistro.strClienteRuc));
                lstParametros.Add(new SqlParameter("strClienteRazonS", objRegistro.strClienteRazonS));
                lstParametros.Add(new SqlParameter("strCodConcesion", objRegistro.strCodConcesion));
                lstParametros.Add(new SqlParameter("strNombreConcesion", objRegistro.strNombreConcesion));
                lstParametros.Add(new SqlParameter("strGuiaRemitente", objRegistro.strGuiaRemitente));
                lstParametros.Add(new SqlParameter("strCodCompromiso", objRegistro.strCodCompromiso));
                lstParametros.Add(new SqlParameter("strMaterial", objRegistro.strMaterial));
                lstParametros.Add(new SqlParameter("intNroSacos", objRegistro.intNroSacos));
                lstParametros.Add(new SqlParameter("fltPesoTmh", objRegistro.fltPesoTmh));
                lstParametros.Add(new SqlParameter("strHoraIngreso", objRegistro.strHoraIngreso));
                lstParametros.Add(new SqlParameter("strHoraSalida", objRegistro.strHoraSalida));
                lstParametros.Add(new SqlParameter("strTiempoDescarga", objRegistro.strTiempoDescarga));
                lstParametros.Add(new SqlParameter("strDuenioMineral", objRegistro.strDuenioMineral));
                lstParametros.Add(new SqlParameter("strFechaMuestreo", objRegistro.strFechaMuestreo));
                lstParametros.Add(new SqlParameter("strTurnoMuestreo", objRegistro.strTurnoMuestreo));
                lstParametros.Add(new SqlParameter("strFechaRetiro", objRegistro.strFechaRetiro));
                lstParametros.Add(new SqlParameter("strNombreTransportista", objRegistro.strNombreTransportista));
                lstParametros.Add(new SqlParameter("strNroPlaca", objRegistro.strNroPlaca));
                lstParametros.Add(new SqlParameter("strGuiaTransport", objRegistro.strGuiaTransport));
                lstParametros.Add(new SqlParameter("strNombreAcopiador", objRegistro.strNombreAcopiador));
                lstParametros.Add(new SqlParameter("strTurnoRegistro", objRegistro.strTurnoRegistro));
                lstParametros.Add(new SqlParameter("strObservacion", objRegistro.strObservacion));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objRegistro.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_UpdateBal]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo UpdateRegistroBal(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strUsuarioModif=:{2}>)", objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateRegistroLaboratorio(RegistroModel objRegistro)
        {
            logger.InfoFormat("Servicio, Registro:  UpdateRegistroLab(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strPorcH2O=:{2}, strUsuarioModif=:{3}>)",
                objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strPorcH2O, objRegistro.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objRegistro.intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", objRegistro.strLote));
                lstParametros.Add(new SqlParameter("strPorcH2O", objRegistro.strPorcH2O));
                lstParametros.Add(new SqlParameter("strCheckH2O", objRegistro.strCheckH2O));
                lstParametros.Add(new SqlParameter("strPSecoTM", objRegistro.strPSecoTM));

                if (objRegistro.dtmFechaHoraMuestreo == null) {
                    lstParametros.Add(new SqlParameter("dtmFechaHoraMuestreo", DBNull.Value));
                }
                else {
                    lstParametros.Add(new SqlParameter("dtmFechaHoraMuestreo", objRegistro.dtmFechaHoraMuestreo));
                }

                lstParametros.Add(new SqlParameter("strPorcRCPRef12hrs", objRegistro.strPorcRCPRef12hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu30hrs", objRegistro.strPorcRCPAu30hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu70hrs", objRegistro.strPorcRCPAu70hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPReeAu72hrs", objRegistro.strPorcRCPReeAu72hrs));
                lstParametros.Add(new SqlParameter("strLeyAuOzTc", objRegistro.strLeyAuOzTc));
                lstParametros.Add(new SqlParameter("strLeyAuREE", objRegistro.strLeyAuREE));
                lstParametros.Add(new SqlParameter("strLeyAuGrTm", objRegistro.strLeyAuGrTm));
                lstParametros.Add(new SqlParameter("strFinoAuGr", objRegistro.strFinoAuGr));
                lstParametros.Add(new SqlParameter("strPorcRCPAg30hrs", objRegistro.strPorcRCPAg30hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAg70hrs", objRegistro.strPorcRCPAg70hrs));
                lstParametros.Add(new SqlParameter("strLeyAgOzTc", objRegistro.strLeyAgOzTc));
                lstParametros.Add(new SqlParameter("strLeyAgREE", objRegistro.strLeyAgREE));
                lstParametros.Add(new SqlParameter("strPorcCobre", objRegistro.strPorcCobre));
                lstParametros.Add(new SqlParameter("strNaOHKgTM", objRegistro.strNaOHKgTM));
                lstParametros.Add(new SqlParameter("strNaCNKgTM", objRegistro.strNaCNKgTM));
                lstParametros.Add(new SqlParameter("strHorasAgitacion", objRegistro.strHorasAgitacion));
                lstParametros.Add(new SqlParameter("strPenalidad", objRegistro.strPenalidad));
                lstParametros.Add(new SqlParameter("strLeyAuComercial", objRegistro.strLeyAuComercial));
                lstParametros.Add(new SqlParameter("strLeyAgComercial", objRegistro.strLeyAgComercial));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objRegistro.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_UpdateLab]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo UpdateRegistroLab(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strPorcH2O=:{2}, strUsuarioModif=:{3}>)",
                    objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strPorcH2O, objRegistro.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateRegistroComercial(RegistroModel objRegistro)
        {
            logger.InfoFormat("Servicio, Registro:  UpdateRegistroCom(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strUsuarioModif=:{2}>)", objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objRegistro.intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", objRegistro.strLote));
                lstParametros.Add(new SqlParameter("strLeyAuComercial", objRegistro.strLeyAuComercial));
                lstParametros.Add(new SqlParameter("strLeyAgComercial", objRegistro.strLeyAgComercial));
                lstParametros.Add(new SqlParameter("strEstadoLote", objRegistro.strEstadoLote));
                lstParametros.Add(new SqlParameter("strLeyCertificado", objRegistro.strLeyCertificado));
                lstParametros.Add(new SqlParameter("strLeyDirimencia", objRegistro.strLeyDirimencia));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objRegistro.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_UpdateCom]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo UpdateRegistroLab(RegistroModel objRegistro=: <intIdRegistro=:{0}, strLote=:{1}, strUsuarioModif=:{2}>)", objRegistro.intIdRegistro, objRegistro.strLote, objRegistro.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteRegistro(int? intIdRegistro, string strUsuario)
        {
            logger.InfoFormat("Servicio, Registro:  DeleteRegistro(int? intIdRegistro=:{0}, string strUsuario=:{1})", intIdRegistro, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", intIdRegistro));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Material: Error en el metodo DeleteRegistro(int? intIdRegistro=:{0}, string strUsuario=:{1})", intIdRegistro, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public CollectionResponse<RegistroModel> SearchRegistros(RegistroSearchModel objQuery)
        {
            //logger.InfoFormat("Servicio, Registro:  SearchRegistros(RegistroModel objQuery)");

            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
	            lstParametros.Add(new SqlParameter("strProcedencia", objQuery.strProcedencia));
                lstParametros.Add(new SqlParameter("strAcopiador", objQuery.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objQuery.strProveedor));
                lstParametros.Add(new SqlParameter("strMaterial", objQuery.strMaterial));
                lstParametros.Add(new SqlParameter("strEstadoLote", objQuery.strEstadoLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objQuery.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objQuery.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<RegistroModel>("dbo.[spRegistro_Get]", lstParametros).ToList();
                return new CollectionResponse<RegistroModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo SearchRegistros(RegistroModel objQuery)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RegistroModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<RegistroModel> SearchRegistrosComercial(RegistroSearchModel objQuery)
        {
            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
                lstParametros.Add(new SqlParameter("strSubLote", objQuery.strSubLote));
                lstParametros.Add(new SqlParameter("strProcedencia", objQuery.strProcedencia));
                lstParametros.Add(new SqlParameter("strAcopiador", objQuery.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objQuery.strProveedor));
                lstParametros.Add(new SqlParameter("strMaterial", objQuery.strMaterial));
                lstParametros.Add(new SqlParameter("strEstadoLote", objQuery.strEstadoLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objQuery.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objQuery.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<RegistroModel>("dbo.[spRegistro_GetComercial]", lstParametros).ToList();
                return new CollectionResponse<RegistroModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo SearchRegistrosComercial(RegistroModel objQuery)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RegistroModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int GetLastRegistro()
        {
            logger.InfoFormat("Servicio, Registro:  GetLastRegistro()");

            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                string strLastLote = repositorio.ExecuteSPGET<string>("dbo.[spRegistro_GetLast]").First();
                string re = strLastLote.Replace("LJ-","");
                return Convert.ToInt32(re) - 10;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo GetLastRegistro()");
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public int DuplicateRegistro(int? intIdRegistro, string strLote)
        {
            logger.InfoFormat("Servicio, Registro:  DuplicateRegistro(int? intIdRegistro=:{0}, string strLote=:{1})", intIdRegistro, strLote);
            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", strLote));

                return repositorio.ExecuteSP<int>("dbo.[spRegistro_Duplicate]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo DuplicateRegistro(int? intIdRegistro=:{0}, string strLote=:{1})", intIdRegistro, strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Maestro]
        public MaestroModel SearchMaestro(string strNombre)
        {
            logger.InfoFormat("Servicio, Maestro:   SearchMaestro(string strNombre=:{0})", strNombre);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombre", strNombre));

                MaestroModel objMaestro = repositorio.ExecuteSP<MaestroModel>("dbo.[spMaestro_Search]", lstParametros).First();
                return objMaestro;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Maestro: Error en el metodo SearchMaestro(string strNombre=:{0})", strNombre);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        #endregion

        #region [Acopiador]
        public CollectionResponse<AcopiadorModel> GetAllAcopiador()
        {
            logger.InfoFormat("Servicio, Acopiador: GetAllAcopiador()");

            IEnumerable<AcopiadorModel> lstListaGeneral = new List<AcopiadorModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<AcopiadorModel>("dbo.[spAcopiador_GetAll]").ToList();
                return new CollectionResponse<AcopiadorModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Acopiador:    Error en el metodo GetAllAcopiador()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<AcopiadorModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutAcopiador(AcopiadorModel objAcopiador)
        {
            logger.InfoFormat("Servicio, Acopiador: PutAcopiador(AcopiadorModel objAcopiador=: <strDni=:{0}, strNombres=:{1}, strApellidos=:{2}, strUsuarioCrea=:{3}>)", objAcopiador.strDni, objAcopiador.strNombres, objAcopiador.strApellidos, objAcopiador.strUsuarioCrea);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strDni", objAcopiador.strDni));
                lstParametros.Add(new SqlParameter("strRuc", objAcopiador.strRuc));
                lstParametros.Add(new SqlParameter("strNombres", objAcopiador.strNombres));
                lstParametros.Add(new SqlParameter("strApellidos", objAcopiador.strApellidos));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objAcopiador.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spAcopiador_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Acopiador:    Error en el metodo PutAcopiador(AcopiadorModel objAcopiador=: <strDni=:{0}, strNombres=:{1}, strApellidos=:{2} strUsuarioCrea=:{3}>)", objAcopiador.strDni, objAcopiador.strNombres, objAcopiador.strApellidos, objAcopiador.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateAcopiador(AcopiadorModel objAcopiador)
        {
            logger.InfoFormat("Servicio, Acopiador: UpdateAcopiador(AcopiadorModel objAcopiador=: <intIdAcopiador=:{0}, strDni=:{1}, strNombres=:{2}, strApellidos=:{3}, strUsuarioModif=:{4}>)", objAcopiador.intIdAcopiador, objAcopiador.strDni, objAcopiador.strNombres, objAcopiador.strApellidos, objAcopiador.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdAcopiador", objAcopiador.intIdAcopiador));
                lstParametros.Add(new SqlParameter("strDni", objAcopiador.strDni));
                lstParametros.Add(new SqlParameter("strRuc", objAcopiador.strRuc));
                lstParametros.Add(new SqlParameter("strNombres", objAcopiador.strNombres));
                lstParametros.Add(new SqlParameter("strApellidos", objAcopiador.strApellidos));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objAcopiador.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spAcopiador_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Acopiador:    Error en el metodo UpdateAcopiador(AcopiadorModel objAcopiador=: <intIdAcopiador=:{0}, strDni=:{1}, strNombres=:{2}, strApellidos=:{3}, strUsuarioModif=:{4}>)", objAcopiador.intIdAcopiador, objAcopiador.strDni, objAcopiador.strNombres, objAcopiador.strApellidos, objAcopiador.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteAcopiador(int? intIdAcopiador, string strUsuario)
        {
            logger.InfoFormat("Servicio, Acopiador: DeleteAcopiador(int? intIdAcopiador=:{0}, string strUsuario=:{1})", intIdAcopiador, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdAcopiador", intIdAcopiador));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spAcopiador_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Acopiador:    Error en el metodo DeleteAcopiador(int? intIdAcopiador=:{0}, string strUsuario=:{1})", intIdAcopiador, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Zona]
        public CollectionResponse<ZonaModel> GetAllZonas()
        {
            logger.InfoFormat("Servicio, Zona:  GetAllZonas()");

            IEnumerable<ZonaModel> lstListaGeneral = new List<ZonaModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<ZonaModel>("dbo.[spZona_GetAll]").ToList();
                return new CollectionResponse<ZonaModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Zona: Error en el metodo GetAllZonas()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ZonaModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutZona(ZonaModel objZona)
        {
            logger.InfoFormat("Servicio, Zona:  PutZona(ZonaModel objZona=: <strCodZona=:{0}, strZona=:{1}, strProcedencia=:{2}, strRegion=:{3}, strCalculoEspecial=:{4}, strUsuarioCrea=:{5}>)", objZona.strCodZona, objZona.strZona, objZona.strProcedencia, objZona.strZona, objZona.strCalculoEspecial, objZona.strUsuarioCrea);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCodZona", objZona.strCodZona));
                lstParametros.Add(new SqlParameter("strZona", objZona.strZona));
                lstParametros.Add(new SqlParameter("strProcedencia", objZona.strProcedencia));
                lstParametros.Add(new SqlParameter("strRegion", objZona.strRegion));
                lstParametros.Add(new SqlParameter("strCalculoEspecial", objZona.strCalculoEspecial));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objZona.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spZona_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Zona: Error en el metodo PutZona(ZonaModel objZona=: <strCodZona=:{0}, strZona=:{1}, strProcedencia=:{2}, strRegion=:{3}, strCalculoEspecial=:{4}, strUsuarioCrea=:{5}>)", objZona.strCodZona, objZona.strZona, objZona.strProcedencia, objZona.strZona, objZona.strCalculoEspecial, objZona.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateZona(ZonaModel objZona)
        {
            logger.InfoFormat("Servicio, Zona:  UpdateZona(ZonaModel objZona=: <intIdZona=:{0}, strZona=:{1}, strProcedencia=:{2}, strRegion=:{3}, strCalculoEspecial=:{4}, strUsuarioModif=:{5}>)",
                objZona.intIdZona, objZona.strZona, objZona.strProcedencia, objZona.strRegion, objZona.strCalculoEspecial, objZona.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdZona", objZona.intIdZona));
                lstParametros.Add(new SqlParameter("strCodZona", objZona.strCodZona));
                lstParametros.Add(new SqlParameter("strZona", objZona.strZona));
                lstParametros.Add(new SqlParameter("strProcedencia", objZona.strProcedencia));
                lstParametros.Add(new SqlParameter("strRegion", objZona.strRegion));
                lstParametros.Add(new SqlParameter("strCalculoEspecial", objZona.strCalculoEspecial));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objZona.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spZona_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Zona: Error en el metodo UpdateZona(ZonaModel objZona=: <intIdZona=:{0}, strZona=:{1}, strProcedencia=:{2}, strRegion=:{3}, strCalculoEspecial=:{4}, strUsuarioModif=:{5}>)",
                    objZona.intIdZona, objZona.strZona, objZona.strProcedencia, objZona.strRegion, objZona.strCalculoEspecial, objZona.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteZona(int? intIdZona, string strUsuario)
        {
            logger.InfoFormat("Servicio, Zona:  DeleteZona(int? intIdZona=:{0}, string strUsuario=:{1})", intIdZona, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdZona", intIdZona));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spZona_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Zona: Error en el metodo DeleteZona(int? intIdZona=:{0}, string strUsuario=:{1})", intIdZona, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public ZonaModel SearchZona(string strZona)
        {
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", strZona));

                return repositorio.ExecuteSP<ZonaModel>("dbo.[spZona_Search]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Zona: Error en el metodo SearchZona(string strZona=:{0})", strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        #endregion

        #region [Cuota]
        public CollectionResponse<CuotaModel> GetAllCuotas()
        {
            logger.InfoFormat("Servicio, Cuota: GetAllCuotas()");

            IEnumerable<CuotaModel> lstListaGeneral = new List<CuotaModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<CuotaModel>("dbo.[spCuota_GetAll]").ToList();
                return new CollectionResponse<CuotaModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cuota:    Error en el metodo GetAllCuotas()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<CuotaModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutCuota(CuotaModel objCuota)
        {
            logger.InfoFormat("Servicio, Cuota: PutCuota(CuotaModel objCuota=: <intIdAcopiador=:{0}, intIdZona=:{1}, strAnio=:{2}, strMes=:{3}, strUsuarioCrea=:{4}>)", objCuota.intIdAcopiador, objCuota.intIdZona, objCuota.strAnio, objCuota.strMes, objCuota.strUsuarioCrea);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdAcopiador", objCuota.intIdAcopiador));
                lstParametros.Add(new SqlParameter("intIdZona", objCuota.intIdZona));
                lstParametros.Add(new SqlParameter("fltCuota", objCuota.fltCuota));
                lstParametros.Add(new SqlParameter("strLeyAu", objCuota.strLeyAu));
                lstParametros.Add(new SqlParameter("strFinoAu", objCuota.strFinoAu));
                lstParametros.Add(new SqlParameter("strUnidadMedida", objCuota.strUnidadMedida));
                lstParametros.Add(new SqlParameter("strAnio", objCuota.strAnio));
                lstParametros.Add(new SqlParameter("strMes", objCuota.strMes));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objCuota.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spCuota_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cuota:    Error en el metodo PutCuota(CuotaModel objCuota=: <intIdAcopiador=:{0}, intIdZona=:{1}, strAnio=:{2}, strMes=:{3}, strUsuarioCrea=:{4}>)", objCuota.intIdAcopiador, objCuota.intIdZona, objCuota.strAnio, objCuota.strMes, objCuota.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateCuota(CuotaModel objCuota)
        {
            logger.InfoFormat("Servicio, Cuota: UpdateCuota(CuotaModel objCuota=: <intIdCuota=:{0}, intIdAcopiador=:{0}, intIdZona=:{2}, strAnio=:{3}, strMes=:{4}, strUsuarioModif=:{5}>)", objCuota.intIdCuota, objCuota.intIdAcopiador, objCuota.intIdZona, objCuota.strAnio, objCuota.strMes, objCuota.strUsuarioModif);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdCuota", objCuota.intIdCuota));
                lstParametros.Add(new SqlParameter("intIdAcopiador", objCuota.intIdAcopiador));
                lstParametros.Add(new SqlParameter("intIdZona", objCuota.intIdZona));
                lstParametros.Add(new SqlParameter("fltCuota", objCuota.fltCuota));
                lstParametros.Add(new SqlParameter("strLeyAu", objCuota.strLeyAu));
                lstParametros.Add(new SqlParameter("strFinoAu", objCuota.strFinoAu));
                lstParametros.Add(new SqlParameter("strUnidadMedida", objCuota.strUnidadMedida));
                lstParametros.Add(new SqlParameter("strAnio", objCuota.strAnio));
                lstParametros.Add(new SqlParameter("strMes", objCuota.strMes));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objCuota.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spCuota_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cuota:    Error en el metodo PutCuota(CuotaModel objCuota=: <intIdAcopiador=:{0}, intIdZona=:{1}, strAnio=:{2}, strMes=:{3}, strUsuarioCrea=:{4}>)", objCuota.intIdAcopiador, objCuota.intIdZona, objCuota.strAnio, objCuota.strMes, objCuota.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteCuota(int? intIdCuota, string strUsuario)
        {
            logger.InfoFormat("Servicio, Cuota: DeleteCuota(int? intIdCuota=:{0}, string strUsuario=:{1})", intIdCuota, strUsuario);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdCuota", intIdCuota));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spCuota_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cuota:    Error en el metodo DeleteCuota(int? intIdCuota=:{0}, string strUsuario=:{1})", intIdCuota, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public CollectionResponse<CuotaModel> SearchCuotas(CuotaModel objSearch)
        {
            IEnumerable<CuotaModel> lstListaGeneral = new List<CuotaModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strMes", objSearch.strMes));
                lstListaGeneral = repositorio.ExecuteSP<CuotaModel>("dbo.[spCuota_Search]", lstParametros).ToList();
                return new CollectionResponse<CuotaModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cuota:    Error en el metodo SearchCuotas(CuotaModel objSearch=: <strMes=:{0}>)", objSearch.strMes);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<CuotaModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        #endregion

        #region [Reportes]
        public CollectionResponse<ReporteAcopioMineralModel> getReporteAcopioMineral(ReporteAcopioMineralModel objSearch)
        {
            //logger.InfoFormat("Servicio, Reportes:  getReporteAcopioMineral(ReporteAcopioMineralModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}, strAcopiador=:{2}>)", objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strAcopiador);

            IEnumerable<ReporteAcopioMineralModel> lstListaGeneral = new List<ReporteAcopioMineralModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objSearch.strProveedor));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));
                lstParametros.Add(new SqlParameter("strEstadoLote", objSearch.strEstadoLote));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMineralModel>("dbo.[spReporte_AcopioMineral]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMineralModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reportes: Error en el metodo getReporteAcopioMineral(ReporteAcopioMineralModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}, strAcopiador=:{2}>)", objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strAcopiador);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMineralModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteControlCuotasModel> getReporteControlCuotas(ReporteControlCuotasModel objSearch)
        {
            //logger.InfoFormat("Servicio, Reportes:  getReporteControlCuotas(ReporteControlCuotasModel objSearch=: <strMesCuota=:{0}, strAcopiador=:{1}, strZona=:{2}>)", objSearch.strMesCuota, objSearch.strAcopiador, objSearch.strZona);

            IEnumerable<ReporteControlCuotasModel> lstListaGeneral = new List<ReporteControlCuotasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strMesCuota", objSearch.strMesCuota));
                lstParametros.Add(new SqlParameter("strAnioCuota", objSearch.strAnioCuota));
                lstParametros.Add(new SqlParameter("strFecha", objSearch.strFecha));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteControlCuotasModel>("dbo.[spReporte_ControlCuotas]", lstParametros).ToList();
                return new CollectionResponse<ReporteControlCuotasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reportes: Error en el metodo getReporteControlCuotas(ReporteControlCuotasModel objSearch=: <strMesCuota=:{0}, strAcopiador=:{1}, strZona=:{2}>)", objSearch.strMesCuota, objSearch.strAcopiador, objSearch.strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteControlCuotasModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteDetAcopioModel> getDetalleReporteAcopio(ReporteDetAcopioModel objSearch)
        {
            IEnumerable<ReporteDetAcopioModel> lstListaGeneral = new List<ReporteDetAcopioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objSearch.strProveedor));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteDetAcopioModel>("dbo.[spReporte_DetAcopio]", lstParametros).ToList();
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte: Error en el metodo getDetalleReporteAcopio(ReporteDetAcopioModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}, strAcopiador=:{2}, strProveedor=:{3}, strZona=:{4}>)", 
                    objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strAcopiador, objSearch.strProveedor, objSearch.strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteDetAcopioModel> getReporteAcopioMRL_Detallado(ReporteDetAcopioModel objSearch)
        {
            IEnumerable<ReporteDetAcopioModel> lstListaGeneral = new List<ReporteDetAcopioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objSearch.strProveedor));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteDetAcopioModel>("dbo.[spReporte_AcopioMRL_Detallado]", lstParametros).ToList();
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte: Error en el metodo getReporteAcopioMRL_Detallado(ReporteDetAcopioModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}, strLote=:{2}, strAcopiador=:{3}, strProveedor=:{4}, strZona=:{5}>)",
                    objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strLote, objSearch.strAcopiador, objSearch.strProveedor, objSearch.strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMesModel> getReporteAcopioMes(string anio, string type)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));
                lstParametros.Add(new SqlParameter("strType", type));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_AcopioMes]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteAcopioMes(string anio=:{0}, string type=:{1})", anio, type);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMesModel> getReporteZonaMes(string anio)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_ZonaMes]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteZonaMes(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        private CollectionResponse<ReporteAcopioMesModel> getReporteZonaMes_Real(string anio, string zona)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));
                lstParametros.Add(new SqlParameter("strZona", zona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_ZonaMesReal]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteZonaMes_Real(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<ReporteAcopioMesModel> getReportePonderadoMes(string anio)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_PonderadoMes]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReportePonderadoMes(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMesModel> getReporteAcopiadorMes(string anio)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_AcopiadorMes]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteAcopiadorMes(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMineralModel> getReporteAcopiadorZona(ReporteAcopioMineralModel objSearch)
        {
            IEnumerable<ReporteAcopioMineralModel> lstListaGeneral = new List<ReporteAcopioMineralModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMineralModel>("dbo.[spReporte_ZonaAcopiador]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMineralModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteAcopiadorZona(ReporteAcopioMineralModel objSearch=: <strFechaInicio=:{0}, strFechafin=:{1}, strAcopiador=:{2}, strZona=:{3}>)",objSearch.strFechaInicio,objSearch.strFechaFin,objSearch.strAcopiador,objSearch.strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMineralModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMesModel> getReporteFinoMes(string anio)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_FinoMes]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteFinoMesRobin(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        private CollectionResponse<ReporteAcopioMesModel> getReporteFinoMes_Real(string anio, string zona)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", anio));
                lstParametros.Add(new SqlParameter("strZona", zona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteAcopioMesModel>("dbo.[spReporte_FinoMesReal]", lstParametros).ToList();
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteFinoMes_Real(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteAcopioMesModel> getReporteMineralRobin(string anio, string zona)
        {
            IEnumerable<ReporteAcopioMesModel> lstListaGeneral = new List<ReporteAcopioMesModel>();
            try
            {
                CollectionResponse<ReporteAcopioMesModel> DataMineral = getReporteZonaMes_Real(anio, zona);
                CollectionResponse<ReporteAcopioMesModel> DataMineralFino = getReporteFinoMes_Real(anio, zona);
                for ( int i= 0; i< DataMineralFino.Count; i++) {
                    DataMineral.Data.ElementAt(i).eneroActual = DataMineralFino.Data.ElementAt(i).enero;
                    DataMineral.Data.ElementAt(i).febreroActual = DataMineralFino.Data.ElementAt(i).febrero;
                    DataMineral.Data.ElementAt(i).marzoActual = DataMineralFino.Data.ElementAt(i).marzo;
                    DataMineral.Data.ElementAt(i).abrilActual = DataMineralFino.Data.ElementAt(i).abril;
                    DataMineral.Data.ElementAt(i).mayoActual = DataMineralFino.Data.ElementAt(i).mayo;
                    DataMineral.Data.ElementAt(i).junioActual = DataMineralFino.Data.ElementAt(i).junio;
                    DataMineral.Data.ElementAt(i).julioActual = DataMineralFino.Data.ElementAt(i).julio;
                    DataMineral.Data.ElementAt(i).agostoActual = DataMineralFino.Data.ElementAt(i).agosto;
                    DataMineral.Data.ElementAt(i).septiembreActual = DataMineralFino.Data.ElementAt(i).septiembre;
                    DataMineral.Data.ElementAt(i).octubreActual = DataMineralFino.Data.ElementAt(i).octubre;
                    DataMineral.Data.ElementAt(i).noviembreActual = DataMineralFino.Data.ElementAt(i).noviembre;
                    DataMineral.Data.ElementAt(i).diciembreActual = DataMineralFino.Data.ElementAt(i).diciembre;
                }
                return DataMineral;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMineralRobin(string anio=:{0})", anio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteAcopioMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteDetAcopioModel> getReporteDiarioRobin(RegistroSearchModel objQuery)
        {
            IEnumerable<ReporteDetAcopioModel> lstListaGeneral = new List<ReporteDetAcopioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
                lstParametros.Add(new SqlParameter("strProcedencia", objQuery.strProcedencia));
                lstParametros.Add(new SqlParameter("strAcopiador", objQuery.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objQuery.strProveedor));
                lstParametros.Add(new SqlParameter("strMaterial", objQuery.strMaterial));
                lstParametros.Add(new SqlParameter("strEstadoLote", objQuery.strEstadoLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objQuery.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objQuery.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteDetAcopioModel>("dbo.[spReporte_DiarioRobin]", lstParametros).ToList();
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteDiarioRobin(RegistroSearchModel objQuery)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteDetAcopioModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteMargenesModel> getReporteMargenes(ReporteMargenesModel objSearch)
        {
            IEnumerable<ReporteMargenesModel> lstListaGeneral = new List<ReporteMargenesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenesModel>("dbo.[spReporte_Margenes]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenes(ReporteMargenesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<ReporteMargenDetModel> getReporteMargenDetallado(ReporteMargenDetModel objSearch)
        {
            IEnumerable<ReporteMargenDetModel> lstListaGeneral = new List<ReporteMargenDetModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objSearch.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strRegion", objSearch.strRegion));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenDetModel>("dbo.[spReporte_MargenesDet]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenDetallado(ReporteMargenesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<ReporteMargenesModel> getReporteMargenProductivo(QueryModel objSearch)
        {
            IEnumerable<ReporteMargenesModel> lstListaGeneral = new List<ReporteMargenesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("sqlCommand", objSearch.strQuery));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenesModel>("dbo.[spReporte_MargenesProd]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenProductivo(QueryModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<ReporteMargenDetModel> getReporteMargenProdDetallado(QueryModel objSearch)
        {
            IEnumerable<ReporteMargenDetModel> lstListaGeneral = new List<ReporteMargenDetModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("sqlCommand", objSearch.strQuery));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenDetModel>("dbo.[spReporte_MargenesProdDet]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenProdDetallado(QueryModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteMargenesModel> getReporteMargenDiario(ReporteMargenesModel objSearch)
        {
            IEnumerable<ReporteMargenesModel> lstListaGeneral = new List<ReporteMargenesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenesModel>("dbo.[spReporte_MargenDiario]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenDiario(ReporteMargenesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteMargenDetModel> getReporteMargenDiarioDetallado(ReporteMargenDetModel objSearch)
        {
            IEnumerable<ReporteMargenDetModel> lstListaGeneral = new List<ReporteMargenDetModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objSearch.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strRegion", objSearch.strRegion));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenDetModel>("dbo.[spReporte_MargenDiarioDet]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenDiarioDetallado(ReporteMargenDetModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenDetModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteMargenesModel> getReporteMargenDiarioZona(ReporteMargenesModel objSearch)
        {
            IEnumerable<ReporteMargenesModel> lstListaGeneral = new List<ReporteMargenesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMargenesModel>("dbo.[spReporte_MargenDiarioZona]", lstParametros).ToList();
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Reporte:  Error en el metodo getReporteMargenDiario(ReporteMargenesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMargenesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<RegistroModel> getReporteMineralPlanta(RegistroSearchModel objQuery)
        {
            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
                lstParametros.Add(new SqlParameter("strProcedencia", objQuery.strProcedencia));
                lstParametros.Add(new SqlParameter("strAcopiador", objQuery.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objQuery.strProveedor));
                lstParametros.Add(new SqlParameter("strMaterial", objQuery.strMaterial));
                lstParametros.Add(new SqlParameter("strEstadoLote", objQuery.strEstadoLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objQuery.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objQuery.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<RegistroModel>("dbo.[spReporte_MineralPlanta]", lstParametros).ToList();
                return new CollectionResponse<RegistroModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo getReporteMineralPlanta(RegistroModel objQuery)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RegistroModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ReporteMineralProcesadoModel> getReporteMineralProcesado(ReporteMineralProcesadoModel objQuery)
        {
            IEnumerable<ReporteMineralProcesadoModel> lstListaGeneral = new List<ReporteMineralProcesadoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objQuery.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strClienteRazonS", objQuery.strClienteRazonS));
                lstParametros.Add(new SqlParameter("strNroRuma", objQuery.strNroRuma));
                lstParametros.Add(new SqlParameter("strNroCampania", objQuery.strNroCampania));
                lstParametros.Add(new SqlParameter("strMesContable", objQuery.strMesContable));

                lstListaGeneral = repositorio.ExecuteSP<ReporteMineralProcesadoModel>("dbo.[spReporte_MineralProcesado]", lstParametros).ToList();
                return new CollectionResponse<ReporteMineralProcesadoModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo getReporteMineralProcesado(ReporteMineralProcesadoModel objQuery)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteMineralProcesadoModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        //Reporte Provisiones
        public CollectionResponse<ReporteProvisionesModel> getReporteProvisiones(ReporteProvisionesModel objSearch)
        {
            IEnumerable<ReporteProvisionesModel> lstListaGeneral = new List<ReporteProvisionesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));

                lstListaGeneral = repositorio.ExecuteSP<ReporteProvisionesModel>("dbo.[spReporte_ProvisionesTruj]", lstParametros).ToList();
                return new CollectionResponse<ReporteProvisionesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Registro: Error en el metodo getReporteProvisiones(ReporteProvisionesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ReporteProvisionesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Leyes Referenciales]
        public CollectionResponse<LeyesReferencialesModel> SearchLeyesRef(LeyesReferencialesModel objSearch)
        {
            logger.InfoFormat("Servicio, Leyes Referenciales:   SearchLeyesRef(LeyesReferencialesModel objSearch)");

            IEnumerable<LeyesReferencialesModel> lstListaGeneral = new List<LeyesReferencialesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCorrelativo", objSearch.strCorrelativo));
                lstParametros.Add(new SqlParameter("strDescripcion", objSearch.strDescripcion));
                lstParametros.Add(new SqlParameter("strTipoMineral", objSearch.strTipoMineral));
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<LeyesReferencialesModel>("dbo.[spLeyesReferenciales_Search]", lstParametros).ToList();
                return new CollectionResponse<LeyesReferencialesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Referenciales:  Error en el metodo SearchLeyesRef(LeyesReferencialesModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LeyesReferencialesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutLeyRef(LeyesReferencialesModel objLeyRef)
        {
            logger.InfoFormat("Servicio, Leyes Referenciales:   PutLeyRef(LeyesReferencialesModel objLeyRef=:<strCorrelativo=:{0}, strUsuarioCrea=:{1}>)", objLeyRef.strCorrelativo, objLeyRef.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strCorrelativo", objLeyRef.strCorrelativo));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objLeyRef.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strDescripcion", objLeyRef.strDescripcion));
                lstParametros.Add(new SqlParameter("strTipoMineral", objLeyRef.strTipoMineral));
                lstParametros.Add(new SqlParameter("strZona", objLeyRef.strZona));
                lstParametros.Add(new SqlParameter("strAcopiador", objLeyRef.strAcopiador));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLeyRef.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesReferenciales_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Referenciales:  Error en el metodo PutLeyRef(LeyesReferencialesModel objLeyRef=:<strCorrelativo=:{0}, strUsuarioCrea=:{1}>)", objLeyRef.strCorrelativo, objLeyRef.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLeyRef_Bal(LeyesReferencialesModel objLeyRef)
        {
            logger.InfoFormat("Servicio, Leyes Referenciales:   UpdateLeyRef_Bal(LeyesReferencialesModel objLeyRef<intIdLeyRef=:{0}, strCorrelativo=:{1}, strUsuarioModif=:{2}>)", objLeyRef.intIdLeyRef, objLeyRef.strCorrelativo, objLeyRef.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdLeyRef", objLeyRef.intIdLeyRef));
                lstParametros.Add(new SqlParameter("strCorrelativo", objLeyRef.strCorrelativo));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objLeyRef.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strDescripcion", objLeyRef.strDescripcion));
                lstParametros.Add(new SqlParameter("strTipoMineral", objLeyRef.strTipoMineral));
                lstParametros.Add(new SqlParameter("strZona", objLeyRef.strZona));
                lstParametros.Add(new SqlParameter("strAcopiador", objLeyRef.strAcopiador));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLeyRef.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesReferenciales_UpdateBal]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Referenciales:  Error en el metodo UpdateLeyRef_Bal(LeyesReferencialesModel objLeyRef<intIdLeyRef=:{0}, strCorrelativo=:{1}, strUsuarioModif=:{2}>)", objLeyRef.intIdLeyRef, objLeyRef.strCorrelativo, objLeyRef.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLeyRef_Lab(LeyesReferencialesModel objLeyRef)
        {
            logger.InfoFormat("Servicio, Leyes Referenciales:   UpdateLeyRef_Lab(LeyesReferencialesModel objLeyRef<intIdLeyRef=:{0}, strUsuarioModif=:{1}>)", objLeyRef.intIdLeyRef, objLeyRef.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdLeyRef", objLeyRef.intIdLeyRef));
                lstParametros.Add(new SqlParameter("strPruebaMetalurgica", objLeyRef.strPruebaMetalurgica));
                lstParametros.Add(new SqlParameter("strPorcRcpLab", objLeyRef.strPorcRcpLab));
                lstParametros.Add(new SqlParameter("strLeyAuOzTc", objLeyRef.strLeyAuOzTc));
                lstParametros.Add(new SqlParameter("strLeyAuReeOzTc", objLeyRef.strLeyAuReeOzTc));
                lstParametros.Add(new SqlParameter("strConsumo", objLeyRef.strConsumo));
                lstParametros.Add(new SqlParameter("strLeyAgOzTc", objLeyRef.strLeyAgOzTc));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLeyRef.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesReferenciales_UpdateLab]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Referenciales:  Error en el metodo UpdateLeyRef_Lab(LeyesReferencialesModel objLeyRef<intIdLeyRef=:{0}, strUsuarioModif=:{1}>)", objLeyRef.intIdLeyRef, objLeyRef.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLeyRef_Com(LeyesReferencialesModel objLeyRef)
        {
            logger.InfoFormat("Servicio, Leyes Referenciales:   UpdateLeyRef_Com(LeyesReferencialesModel objLeyRef <intIdLeyRef=:{0}, strUsuarioModif=:{1}>)", objLeyRef.intIdLeyRef, objLeyRef.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdLeyRef", objLeyRef.intIdLeyRef));
                lstParametros.Add(new SqlParameter("strLeyAuCom", objLeyRef.strLeyAuCom));
                lstParametros.Add(new SqlParameter("strLeyAgCom", objLeyRef.strLeyAgCom));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLeyRef.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesReferenciales_UpdateLey]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Referenciales:  Error en el metodo UpdateLeyRef_Com(LeyesReferencialesModel objLeyRef <intIdLeyRef=:{0}, strUsuarioModif=:{1}>)", objLeyRef.intIdLeyRef, objLeyRef.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Liquidacion]
        public CollectionResponse<LiquidacionModel> GetLiquidacion(RegistroSearchModel objSearch)
        {
            //logger.InfoFormat("Servicio, Liquidacion:   GetLiquidacion(RegistroSearchModel objSearch)");
            IEnumerable<LiquidacionModel> lstListaGeneral = new List<LiquidacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strProcedencia", objSearch.strProcedencia));
                lstParametros.Add(new SqlParameter("strProveedor", objSearch.strProveedor));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strEstado", objSearch.strEstadoLote));
                lstParametros.Add(new SqlParameter("strFecLiqIni", objSearch.strFechaLiqInicio));
                lstParametros.Add(new SqlParameter("strFecLiqFin", objSearch.strFechaLiqFin));

                lstListaGeneral = repositorio.ExecuteSP<LiquidacionModel>("dbo.[spLiquidacion_Get]", lstParametros).ToList();
                return new CollectionResponse<LiquidacionModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Liquidacion:  Error en el metodo GetLiquidacion(RegistroSearchModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LiquidacionModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutLiquidacion(LiquidacionModel objLiquidacion)
        {
            logger.InfoFormat("Servicio, Liquidacion:   PutLiquidacion(LiquidacionModel objLiquidacion=: <intIdRegistro=:{0}, strUsuarioCrea=:{1}>)", objLiquidacion.intIdRegistro, objLiquidacion.strUsuarioCrea);
            IEnumerable<LiquidacionModel> lstListaGeneral = new List<LiquidacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objLiquidacion.intIdRegistro));
                lstParametros.Add(new SqlParameter("strTipoLiquid", objLiquidacion.strTipoLiquid));
                lstParametros.Add(new SqlParameter("strLeyAuLiq", objLiquidacion.strLeyAuLiq));
                lstParametros.Add(new SqlParameter("strLeyAgLiq", objLiquidacion.strLeyAgLiq));
                lstParametros.Add(new SqlParameter("strLeyAuMargen", objLiquidacion.strLeyAuMargen));
                lstParametros.Add(new SqlParameter("strPorcRecup", objLiquidacion.strPorcRecup));
                lstParametros.Add(new SqlParameter("strPrecioAu", objLiquidacion.strPrecioAu));
                lstParametros.Add(new SqlParameter("strMargenPI", objLiquidacion.strMargenPI));
                lstParametros.Add(new SqlParameter("strMaquila", objLiquidacion.strMaquila));
                lstParametros.Add(new SqlParameter("strConsumoQ", objLiquidacion.strConsumoQ));
                lstParametros.Add(new SqlParameter("strGastAdm", objLiquidacion.strGastAdm));
                lstParametros.Add(new SqlParameter("strRecAg", objLiquidacion.strRecAg));
                lstParametros.Add(new SqlParameter("strPrecioAg", objLiquidacion.strPrecioAg));
                lstParametros.Add(new SqlParameter("strMargenAg", objLiquidacion.strMargenAg));
                lstParametros.Add(new SqlParameter("fltCostoTotal", objLiquidacion.fltCostoTotal));
                lstParametros.Add(new SqlParameter("strCalculoEspec", objLiquidacion.strCalculoEspec));
                lstParametros.Add(new SqlParameter("strRecIntAu", objLiquidacion.strRecIntAu));
                lstParametros.Add(new SqlParameter("strMaquilaIntAu", objLiquidacion.strMaquilaIntAu));
                lstParametros.Add(new SqlParameter("strConsQIntAu", objLiquidacion.strConsQIntAu));
                lstParametros.Add(new SqlParameter("strGastAdmIntAu", objLiquidacion.strGastAdmIntAu));
                lstParametros.Add(new SqlParameter("strRecIntAg", objLiquidacion.strRecIntAg));
                lstParametros.Add(new SqlParameter("strEstadoLiq", objLiquidacion.strEstadoLiq));
                lstParametros.Add(new SqlParameter("strPorcImporte", objLiquidacion.strPorcImporte));
                lstParametros.Add(new SqlParameter("strNBaseAu", objLiquidacion.strNBaseAu));
                lstParametros.Add(new SqlParameter("strNBaseAg", objLiquidacion.strNBaseAg));
                lstParametros.Add(new SqlParameter("strCostoAnalisis", objLiquidacion.strCostoAnalisis));
                lstParametros.Add(new SqlParameter("strLoteVolado", objLiquidacion.strLoteVolado));
                lstParametros.Add(new SqlParameter("strMargenBru", objLiquidacion.strMargenBru));
                lstParametros.Add(new SqlParameter("strUtilidadBru", objLiquidacion.strUtilidadBru));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLiquidacion.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spLiquidacion_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Liquidacion:  Error en el metodo PutLiquidacion(LiquidacionModel objLiquidacion=: <intIdRegistro=:{0}, strUsuarioCrea=:{1}>)", objLiquidacion.intIdRegistro, objLiquidacion.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLiquidacion(LiquidacionModel objLiquidacion)
        {
            throw new NotImplementedException();
        }
        public CollectionResponse<LiquidacionModel> GetReintegro(string strLote)
        {
            logger.InfoFormat("Servicio, Liquidacion:   GetReintegro(string strLote=:{0})", strLote);
            IEnumerable<LiquidacionModel> lstListaGeneral = new List<LiquidacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", strLote));

                lstListaGeneral = repositorio.ExecuteSP<LiquidacionModel>("dbo.[spLiquidacion_GetReintegro]", lstParametros).ToList();
                return new CollectionResponse<LiquidacionModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Liquidacion:  Error en el metodo GetReintegro(string strLote=:{0})", strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LiquidacionModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public int PutReintegro(LiquidacionModel objLiquidacion)
        {
            logger.InfoFormat("Servicio, Liquidacion:   PutReintegro(LiquidacionModel objLiquidacion=: <intIdRegistro=:{0}, strUsuarioCrea=:{1}>)", objLiquidacion.intIdRegistro, objLiquidacion.strUsuarioCrea);
            IEnumerable<LiquidacionModel> lstListaGeneral = new List<LiquidacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objLiquidacion.intIdRegistro));
                lstParametros.Add(new SqlParameter("strTipoLiquid", objLiquidacion.strTipoLiquid));
                lstParametros.Add(new SqlParameter("strLeyAuLiq", objLiquidacion.strLeyAuLiq));
                lstParametros.Add(new SqlParameter("strLeyAgLiq", objLiquidacion.strLeyAgLiq));
                lstParametros.Add(new SqlParameter("strLeyAuMargen", objLiquidacion.strLeyAuMargen));
                lstParametros.Add(new SqlParameter("strPorcRecup", objLiquidacion.strPorcRecup));
                lstParametros.Add(new SqlParameter("strPrecioAu", objLiquidacion.strPrecioAu));
                lstParametros.Add(new SqlParameter("strMargenPI", objLiquidacion.strMargenPI));
                lstParametros.Add(new SqlParameter("strMaquila", objLiquidacion.strMaquila));
                lstParametros.Add(new SqlParameter("strConsumoQ", objLiquidacion.strConsumoQ));
                lstParametros.Add(new SqlParameter("strGastAdm", objLiquidacion.strGastAdm));
                lstParametros.Add(new SqlParameter("strRecAg", objLiquidacion.strRecAg));
                lstParametros.Add(new SqlParameter("strPrecioAg", objLiquidacion.strPrecioAg));
                lstParametros.Add(new SqlParameter("strMargenAg", objLiquidacion.strMargenAg));
                lstParametros.Add(new SqlParameter("fltCostoTotal", objLiquidacion.fltCostoTotal));
                lstParametros.Add(new SqlParameter("strCalculoEspec", objLiquidacion.strCalculoEspec));
                lstParametros.Add(new SqlParameter("strRecIntAu", objLiquidacion.strRecIntAu));
                lstParametros.Add(new SqlParameter("strMaquilaIntAu", objLiquidacion.strMaquilaIntAu));
                lstParametros.Add(new SqlParameter("strConsQIntAu", objLiquidacion.strConsQIntAu));
                lstParametros.Add(new SqlParameter("strGastAdmIntAu", objLiquidacion.strGastAdmIntAu));
                lstParametros.Add(new SqlParameter("strRecIntAg", objLiquidacion.strRecIntAg));
                lstParametros.Add(new SqlParameter("strEstadoLiq", objLiquidacion.strEstadoLiq));
                lstParametros.Add(new SqlParameter("strPorcImporte", objLiquidacion.strPorcImporte));
                lstParametros.Add(new SqlParameter("strNBaseAu", objLiquidacion.strNBaseAu));
                lstParametros.Add(new SqlParameter("strNBaseAg", objLiquidacion.strNBaseAg));
                lstParametros.Add(new SqlParameter("strMargenBru", objLiquidacion.strMargenBru));
                lstParametros.Add(new SqlParameter("strUtilidadBru", objLiquidacion.strUtilidadBru));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLiquidacion.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spLiquidacion_PutReintegro]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Liquidacion:  Error en el metodo PutReintegro(LiquidacionModel objLiquidacion=: <intIdRegistro=:{0}, strUsuarioCrea=:{1}>)", objLiquidacion.intIdRegistro, objLiquidacion.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Ventas]
        public CollectionResponse<VentasModel> GetVentas(VentasSearchModel objSearch)
        {
            logger.InfoFormat("Servicio, Ventas:    GetVentas(VentasSearchModel objSearch)");
            IEnumerable<VentasModel> lstListaGeneral = new List<VentasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<VentasModel>("dbo.[spVenta_Get]", lstParametros).ToList();
                foreach (VentasModel objVenta in lstListaGeneral) {
                    objVenta.lstDetVentas = ObtenerDetVenta(objVenta.intIdVenta);
                }
                return new CollectionResponse<VentasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Ventas:   Error en el metodo GetVentas(VentasSearchModel objSearch)");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<VentasModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutVenta(VentasModel objVenta)
        {
            logger.InfoFormat("Servicio, Ventas:    PutVenta(VentasModel objVenta)");
            try
            {
                SqlParameter parDetalle = GetDetalle("listaDetalleVenta", objVenta.lstDetVentas);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strPro", objVenta.strPro));
                lstParametros.Add(new SqlParameter("strPeriodo", objVenta.strPeriodo));
                lstParametros.Add(new SqlParameter("strOperacion", objVenta.strOperacion));
                lstParametros.Add(new SqlParameter("strMes", objVenta.strMes));
                lstParametros.Add(new SqlParameter("strAnio", objVenta.strAnio));
                lstParametros.Add(new SqlParameter("dtmFecha", objVenta.dtmFecha));
                lstParametros.Add(new SqlParameter("strNroDoc", objVenta.strNroDoc));
                lstParametros.Add(new SqlParameter("strImpFac", objVenta.strImpFac));
                lstParametros.Add(new SqlParameter("strNroDocND", objVenta.strNroDocND));
                lstParametros.Add(new SqlParameter("strImpND", objVenta.strImpND));
                lstParametros.Add(new SqlParameter("fltTotal", objVenta.fltTotal));
                lstParametros.Add(new SqlParameter("strKgVenta", objVenta.strKgVenta));
                lstParametros.Add(new SqlParameter("strPenVenta", objVenta.strPenVenta));
                lstParametros.Add(new SqlParameter("fltDolares", objVenta.fltDolares));
                lstParametros.Add(new SqlParameter("fltKgProducidos", objVenta.fltKgProducidos));
                lstParametros.Add(new SqlParameter("fltValorUS", objVenta.fltValorUS));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objVenta.strUsuarioCrea));
                lstParametros.Add(parDetalle);

                return repositorio.ExecuteSP<int>("dbo.[spVenta_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo PutVenta(VentasModel objVenta)");
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        private SqlParameter GetDetalle(string name, List<DetVentaModel> lstDetalleVenta)
        {
            logger.InfoFormat("Servicio, Ventas:   GetDetalle(string name=:{0}, List<DetVentaModel> lstDetalleVenta)", name);
            try
            {
                DataTable table = new DataTable("dbo.lstDetVenta");
                table.Columns.Add("strPro", typeof(string));
                table.Columns.Add("strPeriodo", typeof(string));
                table.Columns.Add("strOperacion", typeof(string));
                table.Columns.Add("fltPeso", typeof(float));
                table.Columns.Add("strLeyOro", typeof(string));
                table.Columns.Add("strGrOro", typeof(string));
                table.Columns.Add("fltKlgOro", typeof(float));

                foreach (DetVentaModel detalle in lstDetalleVenta)
                    table.Rows.Add(new object[] {
                        detalle.strPro,
                        detalle.strPeriodo,
                        detalle.strOperacion,
                        detalle.fltPeso,
                        detalle.strLeyOro,
                        detalle.strGrOro,
                        detalle.fltKlgOro
                    });

                SqlParameter parameter = new SqlParameter(name, table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.lstDetVenta";

                return parameter;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo GetAccesos(string name=:{0}, List<AccesoModel> lstListaAcc)", name);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        private List<DetVentaModel> ObtenerDetVenta(int? intIdVenta) {
            logger.InfoFormat("Servicio, Ventas:    ObtenerDetVenta(int intIdVenta=:{0})", intIdVenta);
            List<DetVentaModel> lstListaGeneral = new List<DetVentaModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdVenta", intIdVenta));
                lstListaGeneral = repositorio.ExecuteSP<DetVentaModel>("dbo.[spVenta_GetDetalle]", lstParametros).ToList();
                return lstListaGeneral;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Ventas:   Error en el metodo ObtenerDetVenta(int intIdVenta=:{0})", intIdVenta);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        #endregion

        #region Parametros
        public CollectionResponse<ParametrosModel> GetParametrosZona(string strZona, string strProveedor)
        {
            logger.InfoFormat("Servicio, Parametros:    GetParametrosZona(string strZona=:{0}, strProveedor=:{1})", strZona, strProveedor);
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", strZona));
                lstParametros.Add(new SqlParameter("strProveedor", strProveedor));

                lstListaGeneral = repositorio.ExecuteSP<ParametrosModel>("dbo.[spParametros_Get]", lstParametros).ToList();
                return new CollectionResponse<ParametrosModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo GetParametrosZona(string strZona=:{0}, strProveedor=:{1})", strZona, strProveedor);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ParametrosModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<MaquilasModel> GetMaquilas(MaquilasModel objSearch)
        {
            IEnumerable<MaquilasModel> lstListaGeneral = new List<MaquilasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdZona", objSearch.intIdZona));
                lstParametros.Add(new SqlParameter("strLeyOz", objSearch.strLeyOz));

                lstListaGeneral = repositorio.ExecuteSP<MaquilasModel>("dbo.[spMaquila_Get]", lstParametros).ToList();
                return new CollectionResponse<MaquilasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Maquilas: Error en el metodo GetMaquilas(MaquilasModel objSearch=: <intIdZona=:{0}, strLeyOz=:{1}>)", objSearch.intIdZona, objSearch.strLeyOz);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<MaquilasModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<ParametrosModel> GetAllParametros(string strType)
        {
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strType", strType));

                lstListaGeneral = repositorio.ExecuteSP<ParametrosModel>("dbo.[spParametros_GetAll]", lstParametros).ToList();
                return new CollectionResponse<ParametrosModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo GetAllParametros(string strType=:{0})", strType);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ParametrosModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public int PutParametroZona(ParametrosModel objParametro)
        {
            logger.InfoFormat("Servicio, Parametros:    PutParametroZona(ParametrosModel objParametro=: <intIdZona=:{0}, strUsuarioCrea=:{1}>)", objParametro.intIdZona, objParametro.strUsuarioCrea);
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdZona", objParametro.intIdZona));
                lstParametros.Add(new SqlParameter("fltRecComMax", objParametro.fltRecComMax));
                lstParametros.Add(new SqlParameter("fltMargenMax", objParametro.fltMargenMax));
                lstParametros.Add(new SqlParameter("fltMaquilaMin", objParametro.fltMaquilaMin));
                lstParametros.Add(new SqlParameter("fltMaquilaMax", objParametro.fltMaquilaMax));
                lstParametros.Add(new SqlParameter("fltConsumoMax", objParametro.fltConsumoMax));
                lstParametros.Add(new SqlParameter("fltGastAdmMax", objParametro.fltGastAdmMax));
                lstParametros.Add(new SqlParameter("fltGastLabMax", objParametro.fltGastLabMax));
                lstParametros.Add(new SqlParameter("fltPorcHumedMax", objParametro.fltPorcHumedMax));
                lstParametros.Add(new SqlParameter("fltPesoMax", objParametro.fltPesoMax));
                lstParametros.Add(new SqlParameter("fltRecAgMax", objParametro.fltRecAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgMax", objParametro.fltLeyAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgComMax", objParametro.fltLeyAgComMax));
                lstParametros.Add(new SqlParameter("fltMargenAgMax", objParametro.fltMargenAgMax));
                lstParametros.Add(new SqlParameter("fltRecAgComMax", objParametro.fltRecAgComMax));
                lstParametros.Add(new SqlParameter("fltRecIntMax", objParametro.fltRecIntMax));
                lstParametros.Add(new SqlParameter("fltMaquilaIntMax", objParametro.fltMaquilaIntMax));
                lstParametros.Add(new SqlParameter("fltConsumoIntMax", objParametro.fltConsumoIntMax));
                lstParametros.Add(new SqlParameter("fltTmsLiqMax", objParametro.fltTmsLiqMax));
                lstParametros.Add(new SqlParameter("fltTmsIntMax", objParametro.fltTmsIntMax));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objParametro.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spParametros_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo PutParametroZona(ParametrosModel objParametro=: <intIdZona=:{0}, strUsuarioCrea=:{1}>)", objParametro.intIdZona, objParametro.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateParametroZona(ParametrosModel objParametro)
        {
            logger.InfoFormat("Servicio, Parametros:    UpdateParametroZona(ParametrosModel objParametro=: <intParamCom=:{0}, intIdZona=:{1}, strUsuarioModif=:{2}>)", objParametro.intParamCom, objParametro.intIdZona, objParametro.strUsuarioModif);
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intParamCom", objParametro.intParamCom));
                lstParametros.Add(new SqlParameter("intIdZona", objParametro.intIdZona));
                lstParametros.Add(new SqlParameter("fltRecComMax", objParametro.fltRecComMax));
                lstParametros.Add(new SqlParameter("fltMargenMax", objParametro.fltMargenMax));
                lstParametros.Add(new SqlParameter("fltMaquilaMin", objParametro.fltMaquilaMin));
                lstParametros.Add(new SqlParameter("fltMaquilaMax", objParametro.fltMaquilaMax));
                lstParametros.Add(new SqlParameter("fltConsumoMax", objParametro.fltConsumoMax));
                lstParametros.Add(new SqlParameter("fltGastAdmMax", objParametro.fltGastAdmMax));
                lstParametros.Add(new SqlParameter("fltGastLabMax", objParametro.fltGastLabMax));
                lstParametros.Add(new SqlParameter("fltPorcHumedMax", objParametro.fltPorcHumedMax));
                lstParametros.Add(new SqlParameter("fltPesoMax", objParametro.fltPesoMax));
                lstParametros.Add(new SqlParameter("fltRecAgMax", objParametro.fltRecAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgMax", objParametro.fltLeyAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgComMax", objParametro.fltLeyAgComMax));
                lstParametros.Add(new SqlParameter("fltMargenAgMax", objParametro.fltMargenAgMax));
                lstParametros.Add(new SqlParameter("fltRecAgComMax", objParametro.fltRecAgComMax));
                lstParametros.Add(new SqlParameter("fltRecIntMax", objParametro.fltRecIntMax));
                lstParametros.Add(new SqlParameter("fltMaquilaIntMax", objParametro.fltMaquilaIntMax));
                lstParametros.Add(new SqlParameter("fltConsumoIntMax", objParametro.fltConsumoIntMax));
                lstParametros.Add(new SqlParameter("fltTmsLiqMax", objParametro.fltTmsLiqMax));
                lstParametros.Add(new SqlParameter("fltTmsIntMax", objParametro.fltTmsIntMax));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objParametro.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spParametros_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo UpdateParametroZona(ParametrosModel objParametro=: <intParamCom=:{0}, intIdZona=:{1}, strUsuarioModif=:{2}>)", objParametro.intParamCom, objParametro.intIdZona, objParametro.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public int PutParametroProv(ParametrosModel objParametro)
        {
            logger.InfoFormat("Servicio, Parametros:    PutParametroProv(ParametrosModel objParametro=: <intIdProveedor=:{0}, strUsuarioCrea=:{1}>)", objParametro.intIdProveedor, objParametro.strUsuarioCrea);
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdProveedor", objParametro.intIdProveedor));
                lstParametros.Add(new SqlParameter("fltRecComMax", objParametro.fltRecComMax));
                lstParametros.Add(new SqlParameter("fltMargenMax", objParametro.fltMargenMax));
                lstParametros.Add(new SqlParameter("fltMaquilaMin", objParametro.fltMaquilaMin));
                lstParametros.Add(new SqlParameter("fltMaquilaMax", objParametro.fltMaquilaMax));
                lstParametros.Add(new SqlParameter("fltConsumoMax", objParametro.fltConsumoMax));
                lstParametros.Add(new SqlParameter("fltGastAdmMax", objParametro.fltGastAdmMax));
                lstParametros.Add(new SqlParameter("fltGastLabMax", objParametro.fltGastLabMax));
                lstParametros.Add(new SqlParameter("fltPorcHumedMax", objParametro.fltPorcHumedMax));
                lstParametros.Add(new SqlParameter("fltPesoMax", objParametro.fltPesoMax));
                lstParametros.Add(new SqlParameter("fltRecAgMax", objParametro.fltRecAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgMax", objParametro.fltLeyAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgComMax", objParametro.fltLeyAgComMax));
                lstParametros.Add(new SqlParameter("fltMargenAgMax", objParametro.fltMargenAgMax));
                lstParametros.Add(new SqlParameter("fltRecAgComMax", objParametro.fltRecAgComMax));
                lstParametros.Add(new SqlParameter("fltRecIntMax", objParametro.fltRecIntMax));
                lstParametros.Add(new SqlParameter("fltMaquilaIntMax", objParametro.fltMaquilaIntMax));
                lstParametros.Add(new SqlParameter("fltConsumoIntMax", objParametro.fltConsumoIntMax));
                lstParametros.Add(new SqlParameter("fltTmsLiqMax", objParametro.fltTmsLiqMax));
                lstParametros.Add(new SqlParameter("fltTmsIntMax", objParametro.fltTmsIntMax));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objParametro.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spParametrosProv_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo PutParametroProv(ParametrosModel objParametro=: <intIdProveedor=:{0}, strUsuarioCrea=:{1}>)", objParametro.intIdProveedor, objParametro.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateParametroProv(ParametrosModel objParametro)
        {
            logger.InfoFormat("Servicio, Parametros:    UpdateParametroProv(ParametrosModel objParametro)");
            IEnumerable<ParametrosModel> lstListaGeneral = new List<ParametrosModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdParamComProv", objParametro.intIdParamComProv));
                lstParametros.Add(new SqlParameter("intIdProveedor", objParametro.intIdProveedor));
                lstParametros.Add(new SqlParameter("fltRecComMax", objParametro.fltRecComMax));
                lstParametros.Add(new SqlParameter("fltMargenMax", objParametro.fltMargenMax));
                lstParametros.Add(new SqlParameter("fltMaquilaMin", objParametro.fltMaquilaMin));
                lstParametros.Add(new SqlParameter("fltMaquilaMax", objParametro.fltMaquilaMax));
                lstParametros.Add(new SqlParameter("fltConsumoMax", objParametro.fltConsumoMax));
                lstParametros.Add(new SqlParameter("fltGastAdmMax", objParametro.fltGastAdmMax));
                lstParametros.Add(new SqlParameter("fltGastLabMax", objParametro.fltGastLabMax));
                lstParametros.Add(new SqlParameter("fltPorcHumedMax", objParametro.fltPorcHumedMax));
                lstParametros.Add(new SqlParameter("fltPesoMax", objParametro.fltPesoMax));
                lstParametros.Add(new SqlParameter("fltRecAgMax", objParametro.fltRecAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgMax", objParametro.fltLeyAgMax));
                lstParametros.Add(new SqlParameter("fltLeyAgComMax", objParametro.fltLeyAgComMax));
                lstParametros.Add(new SqlParameter("fltMargenAgMax", objParametro.fltMargenAgMax));
                lstParametros.Add(new SqlParameter("fltRecAgComMax", objParametro.fltRecAgComMax));
                lstParametros.Add(new SqlParameter("fltRecIntMax", objParametro.fltRecIntMax));
                lstParametros.Add(new SqlParameter("fltMaquilaIntMax", objParametro.fltMaquilaIntMax));
                lstParametros.Add(new SqlParameter("fltConsumoIntMax", objParametro.fltConsumoIntMax));
                lstParametros.Add(new SqlParameter("fltTmsLiqMax", objParametro.fltTmsLiqMax));
                lstParametros.Add(new SqlParameter("fltTmsIntMax", objParametro.fltTmsIntMax));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objParametro.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spParametrosProv_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo UpdateParametroProv(ParametrosModel objParametro)");
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public int DuplicateParametro(int? idParametro, string strUsuario)
        {
            logger.InfoFormat("Servicio, Parametros:    DuplicateParametro(int? idParametro=:{0}, string strUsuario=:{1})", idParametro, strUsuario);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intParamCom", idParametro));

                return repositorio.ExecuteSP<int>("dbo.[spParametros_Duplicate]", lstParametros).First();

            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo DuplicateParametro(int? idParametro=:{0}, string strUsuario=:{1})", idParametro, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DuplicateParametroProv(int? idParametro, string strUsuario)
        {
            logger.InfoFormat("Servicio, Parametros:    DuplicateParametroProv(int? idParametro=:{0}, string strUsuario=:{1})", idParametro, strUsuario);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdParamComProv", idParametro));

                return repositorio.ExecuteSP<int>("dbo.[spParametrosProv_Duplicate]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo DuplicateParametroProv(int? idParametro=:{0}, string strUsuario=:{1})", idParametro, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public CollectionResponse<PrecioInterModel> GetPrecioInter(string strName)
        {
            IEnumerable<PrecioInterModel> lstListaGeneral = new List<PrecioInterModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombre", strName));

                lstListaGeneral = repositorio.ExecuteSP<PrecioInterModel>("dbo.[spPrecioInter_Get]", lstParametros).ToList();
                return new CollectionResponse<PrecioInterModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo GetPrecioInter(string strName=:{0})", strName);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<PrecioInterModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public string UpdatePrecioInter(PrecioInterModel objPrecio)
        {
            logger.InfoFormat("Servicio, Parametros:    UpdatePrecioInter(PrecioInterModel objPrecio=: <strFecha=:{0}, strGoldAM=:{1}, strGoldPM=:{2}, strGoldSilver=:{3}, strUsuarioModif=:{4}>)",
                    objPrecio.dtmFecha, objPrecio.strGoldAM, objPrecio.strGoldPM, objPrecio.strSilver, objPrecio.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdPrecioInter", objPrecio.intIdPrecioInter));
                lstParametros.Add(new SqlParameter("dtmFecha", objPrecio.dtmFecha));
                lstParametros.Add(new SqlParameter("strGoldAM", objPrecio.strGoldAM));
                lstParametros.Add(new SqlParameter("strGoldPM", objPrecio.strGoldPM));
                lstParametros.Add(new SqlParameter("strSilver", objPrecio.strSilver));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objPrecio.strUsuarioModif));

                string success = repositorio.ExecuteSP<string>("dbo.[spPrecioInter_Update]", lstParametros).First();
                return success;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo UpdatePrecioInter(PrecioInterModel objPrecio=: <strFecha=:{0}, strGoldAM=:{1}, strGoldPM=:{2}, strGoldSilver=:{3}, strUsuarioModif=:{4}>)",
                    objPrecio.strFechaInicio, objPrecio.strGoldAM, objPrecio.strGoldPM, objPrecio.strSilver, objPrecio.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        public string PutPrecioInter(PrecioInterModel objPrecio)
        {
            logger.InfoFormat("Servicio, Parametros:    PutPrecioInter(PrecioInterModel objPrecio=: <strFecha=:{0}, strGoldAM=:{1}, strGoldPM=:{2}, strGoldSilver=:{3}, strUsuarioCrea=:{4}>)",
                    objPrecio.dtmFecha, objPrecio.strGoldAM, objPrecio.strGoldPM, objPrecio.strSilver, objPrecio.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("dtmFecha", objPrecio.dtmFecha));
                lstParametros.Add(new SqlParameter("strGoldAM", objPrecio.strGoldAM));
                lstParametros.Add(new SqlParameter("strGoldPM", objPrecio.strGoldPM));
                lstParametros.Add(new SqlParameter("strSilver", objPrecio.strSilver));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objPrecio.strUsuarioCrea));

                string success = repositorio.ExecuteSP<string>("dbo.[spPrecioInter_Put]", lstParametros).First();
                return success;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo PutPrecioInter(PrecioInterModel objPrecio=: <strFecha=:{0}, strGoldAM=:{1}, strGoldPM=:{2}, strGoldSilver=:{3}, strUsuarioCrea=:{4}>)",
                    objPrecio.strFechaInicio, objPrecio.strGoldAM, objPrecio.strGoldPM, objPrecio.strSilver, objPrecio.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        public CollectionResponse<PrecioInterModel> SearchPrecioInter(PrecioInterModel objSearch) {
            IEnumerable<PrecioInterModel> lstListaGeneral = new List<PrecioInterModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strType", objSearch.strType));

                lstListaGeneral = repositorio.ExecuteSP<PrecioInterModel>("dbo.[spPrecioInter_Search]", lstParametros).ToList();
                return new CollectionResponse<PrecioInterModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo SearchPrecioInter(PrecioInterModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}, strType=:{2}>)",
                    objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strType);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<PrecioInterModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public string UpdateFactorConversion(MaestroModel objMaestro)
        {
            logger.InfoFormat("Servicio, Parametros:    UpdateFactorConversion(MaestroModel objMaestro=: <strNombre:={0}, strValor=:{1}, strUsuarioModif=:{2}>)", objMaestro.strNombre,objMaestro.strValor, objMaestro.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombre", objMaestro.strNombre));
                lstParametros.Add(new SqlParameter("strValor", objMaestro.strValor));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objMaestro.strUsuarioModif));

                string success = repositorio.ExecuteSP<string>("dbo.[spMaestro_Update]", lstParametros).First();
                return success;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Parametros:   Error en el metodo UpdateFactorConversion(MaestroModel objMaestro=: <strNombre:={0}, strValor=:{1}, strUsuarioModif=:{2}>)", objMaestro.strNombre, objMaestro.strValor, objMaestro.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        #endregion

        #region[Leyes Laboratorio]
        public string PutLeyesLaboratorio(List<LeyesModel> lstLeyes)
        {
            if (lstLeyes.Count > 0)
            {
                logger.InfoFormat("Servicio, Leyes Laboratorio: PutLeyesLaboratorio(List<LeyesModel> lstLeyes=: <strLote=:{0}, strTipoProceso=:{1}, Count=:{2}, strUsuarioCrea=:{3}>)", lstLeyes[0].strLote, lstLeyes[0].strTipoProceso, lstLeyes.Count, lstLeyes[0].strUsuarioCrea);
                try
                {
                    //Eliminar las leyes anteriores
                    DeleteLeyes(lstLeyes[0].strLote);

                    foreach (LeyesModel item in lstLeyes) {
                        IList<SqlParameter> lstParametros = new List<SqlParameter>();
                        //lstParametros.Add(new SqlParameter("intIdLeyes", item.intIdLeyes));
                        lstParametros.Add(new SqlParameter("strTipoProceso", item.strTipoProceso));
                        lstParametros.Add(new SqlParameter("strItem", item.strItem));
                        lstParametros.Add(new SqlParameter("strBk", item.strBk));
                        lstParametros.Add(new SqlParameter("dtmFecha", item.dtmFecha));
                        lstParametros.Add(new SqlParameter("strTipoMineral", item.strTipoMineral));
                        lstParametros.Add(new SqlParameter("strLote", item.strLote));
                        lstParametros.Add(new SqlParameter("strPMFino", item.strPMFino));
                        lstParametros.Add(new SqlParameter("strPMGrueso", item.strPMGrueso));
                        lstParametros.Add(new SqlParameter("strPesoAuMasAg", item.strPesoAuMasAg));
                        lstParametros.Add(new SqlParameter("strPesoAuFino1", item.strPesoAuFino1));
                        lstParametros.Add(new SqlParameter("strPesoAuFino2", item.strPesoAuFino2));
                        lstParametros.Add(new SqlParameter("strPesoAuGrueso", item.strPesoAuGrueso));
                        lstParametros.Add(new SqlParameter("strLeyOzTcAuFino", item.strLeyOzTcAuFino));
                        lstParametros.Add(new SqlParameter("strLeyOzTcAuGrueso", item.strLeyOzTcAuGrueso));
                        lstParametros.Add(new SqlParameter("strLeyOzTcAuFinal", item.strLeyOzTcAuFinal));
                        lstParametros.Add(new SqlParameter("strLeyOzTcAgfinal1", item.strLeyOzTcAgfinal1));
                        lstParametros.Add(new SqlParameter("strLeyFinalAu", item.strLeyFinalAu));
                        lstParametros.Add(new SqlParameter("strLeyFinalAg", item.strLeyFinalAg));
                        lstParametros.Add(new SqlParameter("strPorcAuFino", item.strPorcAuFino));
                        lstParametros.Add(new SqlParameter("strPorcAuGrueso", item.strPorcAuGrueso));
                        lstParametros.Add(new SqlParameter("chrCheckStatus", item.chrCheckStatus));
                        lstParametros.Add(new SqlParameter("strUsuarioCrea", item.strUsuarioCrea));

                        repositorio.ExecuteSP<string>("dbo.[spLeyesLab_Put]", lstParametros).First();
                    }
                    
                    return "Las Leyes se guardaron correctamente";
                }
                catch (Exception e)
                {
                    logger.ErrorFormat("Servicio, Leyes Laboratorio:    Error en el metodo PutLeyesLaboratorio(List<LeyesModel> lstLeyes=: <strLote=:{0}, strTipoProceso=:{1}, Count=:{2}, strUsuarioCrea=:{3}>})", lstLeyes[0].strLote, lstLeyes[0].strTipoProceso, lstLeyes.Count, lstLeyes[0].strUsuarioCrea);
                    logger.ErrorFormat("Exception - {0}", e);
                    return null;
                    throw e;
                }
            }
            else
                return null;
        }
        private string DeleteLeyes(string strLote)
        {
            logger.InfoFormat("Servicio, Leyes: DeleteLeyes(string strLote=:{0})", strLote);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", strLote));

                return repositorio.ExecuteSP<string>("dbo.[spLeyesLab_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes:    Error en el metodo DeleteLeyes(string strLote=:{0})", strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        public string PutLeyesRegistro(RegistroModel objLeyRegistro)
        {
            logger.InfoFormat("Servicio, Leyes: PutLeyesRegistro(RegistroModel objLeyRegistro=: <strLote=:{0}>)", objLeyRegistro.strLote);

            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objLeyRegistro.strLote));
                lstParametros.Add(new SqlParameter("strLeyAuOzTc", objLeyRegistro.strLeyAuOzTc));
                lstParametros.Add(new SqlParameter("strLeyAgOzTc", objLeyRegistro.strLeyAgOzTc));
                lstParametros.Add(new SqlParameter("strLeyAuREE", objLeyRegistro.strLeyAuREE));
                lstParametros.Add(new SqlParameter("strLeyAgREE", objLeyRegistro.strLeyAgREE));
                lstParametros.Add(new SqlParameter("strLeyAuRemuestreo", objLeyRegistro.strLeyAuRemuestreo));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLeyRegistro.strUsuarioModif));

                repositorio.ExecuteSP<string>("dbo.[spLeyesLabRegistro_Put]", lstParametros).First();
                return "Las Leyes se guardaron correctamente";
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes:    Error en el metodo PutLeyesRegistro(RegistroModel objLeyRegistro=: <strLote=:{0}>)", objLeyRegistro.strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        private SqlParameter GetListaLeyes(string name, List<LeyesModel> lstLeyes)
        {
            logger.InfoFormat("Servicio, Leyes: GetListaLeyes(string name=:{0}, List<LeyesModel> lstLeyes)", name);

            try
            {
                DataTable table = new DataTable("dbo.lstLeyes");
                table.Columns.Add("intAcceso", typeof(int));
                table.Columns.Add("strNombre", typeof(string));
                table.Columns.Add("intIdLeyes", typeof(int));
                table.Columns.Add("strTipoProceso", typeof(string));
                table.Columns.Add("strItem", typeof(string));
                table.Columns.Add("strBk", typeof(string));
                table.Columns.Add("dtmFecha", typeof(DateTime));
                table.Columns.Add("strTipoMineral", typeof(string));
                table.Columns.Add("strLote", typeof(string));
                table.Columns.Add("strPMFino", typeof(string));
                table.Columns.Add("strPMGrueso", typeof(string));
                table.Columns.Add("strPesoAuMasAg", typeof(string));
                table.Columns.Add("strPesoAuFino1", typeof(string));
                table.Columns.Add("strPesoAuFino2", typeof(string));
                table.Columns.Add("strPesoAuGrueso", typeof(string));
                table.Columns.Add("strLeyOzTcAuFino", typeof(string));
                table.Columns.Add("strLeyOzTcAuGrueso", typeof(string));
                table.Columns.Add("strLeyOzTcAuFinal", typeof(string));
                table.Columns.Add("strLeyOzTcAgfinal1", typeof(string));
                table.Columns.Add("strLeyFinalAu", typeof(string));
                table.Columns.Add("strLeyFinalAg", typeof(string));
                table.Columns.Add("strPorcAuFino", typeof(string));
                table.Columns.Add("strPorcAuGrueso", typeof(string));
                table.Columns.Add("strUsuarioCrea", typeof(string));

                foreach (LeyesModel ley in lstLeyes)
                    table.Rows.Add(new object[] {
                        ley.intIdLeyes,
                        ley.strTipoProceso,
                        ley.strItem,
                        ley.strBk,
                        ley.dtmFecha,
                        ley.strTipoMineral,
                        ley.strLote,
                        ley.strPMFino,
                        ley.strPMGrueso,
                        ley.strPesoAuMasAg,
                        ley.strPesoAuFino1,
                        ley.strPesoAuFino2,
                        ley.strPesoAuGrueso,
                        ley.strLeyOzTcAuFino,
                        ley.strLeyOzTcAuGrueso,
                        ley.strLeyOzTcAuFinal,
                        ley.strLeyOzTcAgfinal1,
                        ley.strLeyFinalAu,
                        ley.strLeyFinalAg,
                        ley.strPorcAuFino,
                        ley.strPorcAuGrueso,
                        ley.strUsuarioCrea
                    });

                SqlParameter parameter = new SqlParameter(name, table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.lstLeyes";

                return parameter;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo GetAccesos(string name=:{0}, List<AccesoModel> lstListaAcc)", name);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }
        public CollectionResponse<LeyesModel> GetLeyesLaboratorio(string strLote)
        {
            IEnumerable<LeyesModel> lstListaGeneral = new List<LeyesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", strLote));

                lstListaGeneral = repositorio.ExecuteSP<LeyesModel>("dbo.[spLeyesLab_Get]", lstParametros).ToList();
                return new CollectionResponse<LeyesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Laboratorio:    Error en el metodo GetLeyesLaboratorio(string strLote=:{0})", strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LeyesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region[Consumos]
        public ConsumoModel GetConsumos(string strLote)
        {
            IEnumerable<ConsumoModel> lstListaGeneral = new List<ConsumoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", strLote));

                return repositorio.ExecuteSP<ConsumoModel>("dbo.[spConsumos_Get]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Consumos: Error en el metodo GetConsumos(string strLote=:{0})", strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public int PutConsumo(ConsumoModel objConsumo)
        {
            logger.InfoFormat("Servicio, Consumos:  PutConsumo(ConsumoModel objConsumo=: <strLote=:{0}>)", objConsumo.strLote);

            IEnumerable<ConsumoModel> lstListaGeneral = new List<ConsumoModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objConsumo.strLote));
                lstParametros.Add(new SqlParameter("strGeP1", objConsumo.strGeP1));
                lstParametros.Add(new SqlParameter("strGeP2", objConsumo.strGeP2));
                lstParametros.Add(new SqlParameter("strGeP3", objConsumo.strGeP3));
                lstParametros.Add(new SqlParameter("strGeP4", objConsumo.strGeP4));
                lstParametros.Add(new SqlParameter("strGeEspecif2", objConsumo.strGeEspecif2));
                lstParametros.Add(new SqlParameter("strPlDensidad", objConsumo.strPlDensidad));
                lstParametros.Add(new SqlParameter("strPlPorcMalla", objConsumo.strPlPorcMalla));
                lstParametros.Add(new SqlParameter("strPlPeso", objConsumo.strPlPeso));
                lstParametros.Add(new SqlParameter("str0HrsLixCN", objConsumo.str0HrsLixCN));
                lstParametros.Add(new SqlParameter("str2HrsLixCN", objConsumo.str2HrsLixCN));
                lstParametros.Add(new SqlParameter("str4HrsLixCN", objConsumo.str4HrsLixCN));
                lstParametros.Add(new SqlParameter("str8HrsLixCN", objConsumo.str8HrsLixCN));
                lstParametros.Add(new SqlParameter("str12HrsLixCN", objConsumo.str12HrsLixCN));
                lstParametros.Add(new SqlParameter("str24HrsLixCN", objConsumo.str24HrsLixCN));
                lstParametros.Add(new SqlParameter("str48HrsLixCN", objConsumo.str48HrsLixCN));
                lstParametros.Add(new SqlParameter("str72HrsLixCN", objConsumo.str72HrsLixCN));
                lstParametros.Add(new SqlParameter("str0HrsLixPH", objConsumo.str0HrsLixPH));
                lstParametros.Add(new SqlParameter("str2HrsLixPH", objConsumo.str2HrsLixPH));
                lstParametros.Add(new SqlParameter("str4HrsLixPH", objConsumo.str4HrsLixPH));
                lstParametros.Add(new SqlParameter("str8HrsLixPH", objConsumo.str8HrsLixPH));
                lstParametros.Add(new SqlParameter("str12HrsLixPH", objConsumo.str12HrsLixPH));
                lstParametros.Add(new SqlParameter("str24HrsLixPH", objConsumo.str24HrsLixPH));
                lstParametros.Add(new SqlParameter("str48HrsLixPH", objConsumo.str48HrsLixPH));
                lstParametros.Add(new SqlParameter("str72HrsLixPH", objConsumo.str72HrsLixPH));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH0", objConsumo.strHrsLixReactOH0));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH2", objConsumo.strHrsLixReactOH2));
	            lstParametros.Add(new SqlParameter("strHrsLixReactOH4", objConsumo.strHrsLixReactOH4));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH8", objConsumo.strHrsLixReactOH8));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH12", objConsumo.strHrsLixReactOH12));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH24", objConsumo.strHrsLixReactOH24));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH48", objConsumo.strHrsLixReactOH48));
                lstParametros.Add(new SqlParameter("strHrsLixReactOH72", objConsumo.strHrsLixReactOH72));
                lstParametros.Add(new SqlParameter("strVolDes2", objConsumo.strVolDes2));
                lstParametros.Add(new SqlParameter("strVolDes4", objConsumo.strVolDes4));
                lstParametros.Add(new SqlParameter("strVolDes8", objConsumo.strVolDes8));
                lstParametros.Add(new SqlParameter("strVolDes12", objConsumo.strVolDes12));
                lstParametros.Add(new SqlParameter("strVolDes24", objConsumo.strVolDes24));
                lstParametros.Add(new SqlParameter("strVolDes48", objConsumo.strVolDes48));
                lstParametros.Add(new SqlParameter("strVolDes72", objConsumo.strVolDes72));
                lstParametros.Add(new SqlParameter("strNaOH", objConsumo.strNaOH));
                lstParametros.Add(new SqlParameter("strNaCN", objConsumo.strNaCN));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objConsumo.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spConsumos_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Consumos: Error en el metodo PutConsumo(ConsumoModel objConsumo=: <strLote=:{0}>)", objConsumo.strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        #endregion

        #region [Recuperacion]
        public RecuperacionModel GetRecuperacion(string strLote, string strTipoProceso)
        {
            IEnumerable<RecuperacionModel> lstListaGeneral = new List<RecuperacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", strLote));
                lstParametros.Add(new SqlParameter("strTipoProceso", strTipoProceso));

                return repositorio.ExecuteSP<RecuperacionModel>("dbo.[spRecuperacion_Get]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Recuperacion: Error en el metodo GetRecuperacion(string strLote=:{0}, string strTipoProceso=:{1})", strLote, strTipoProceso);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public int PutRecuperacion(RecuperacionModel objRecuperacion)
        {
            logger.InfoFormat("Servicio, Recuperacion:  PutRecuperacion(RecuperacionModel objRecuperacion=: <strLote=:{0}, strUsuarioCrea=:{1}>)", objRecuperacion.strLote, objRecuperacion.strUsuarioCrea);
            IEnumerable<RecuperacionModel> lstListaGeneral = new List<RecuperacionModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objRecuperacion.strLote));
                lstParametros.Add(new SqlParameter("strTipoProceso", objRecuperacion.strTipoProceso));
                lstParametros.Add(new SqlParameter("strOroPpm2", objRecuperacion.strOroPpm2));
                lstParametros.Add(new SqlParameter("strOroPpm4", objRecuperacion.strOroPpm4));
                lstParametros.Add(new SqlParameter("strOroPpm8", objRecuperacion.strOroPpm8));
                lstParametros.Add(new SqlParameter("strOroPpm12", objRecuperacion.strOroPpm12));
                lstParametros.Add(new SqlParameter("strOroPpm24", objRecuperacion.strOroPpm24));
                lstParametros.Add(new SqlParameter("strOroPpm48", objRecuperacion.strOroPpm48));
                lstParametros.Add(new SqlParameter("strOroPpm72", objRecuperacion.strOroPpm72));
                lstParametros.Add(new SqlParameter("strOroPpm90", objRecuperacion.strOroPpm90));
                lstParametros.Add(new SqlParameter("strPlataPpm2", objRecuperacion.strPlataPpm2));
                lstParametros.Add(new SqlParameter("strPlataPpm4", objRecuperacion.strPlataPpm4));
                lstParametros.Add(new SqlParameter("strPlataPpm8", objRecuperacion.strPlataPpm8));
                lstParametros.Add(new SqlParameter("strPlataPpm12", objRecuperacion.strPlataPpm12));
                lstParametros.Add(new SqlParameter("strPlataPpm24", objRecuperacion.strPlataPpm24));
                lstParametros.Add(new SqlParameter("strPlataPpm48", objRecuperacion.strPlataPpm48));
                lstParametros.Add(new SqlParameter("strPlataPpm72", objRecuperacion.strPlataPpm72));
                lstParametros.Add(new SqlParameter("strPlataPpm90", objRecuperacion.strPlataPpm90));
                lstParametros.Add(new SqlParameter("strLeyAuRipio", objRecuperacion.strLeyAuRipio));
                lstParametros.Add(new SqlParameter("strLeyAgRipio", objRecuperacion.strLeyAgRipio));
                lstParametros.Add(new SqlParameter("strCobrePpm48", objRecuperacion.strCobrePpm48));
                lstParametros.Add(new SqlParameter("strCobrePpm72", objRecuperacion.strCobrePpm72));
                lstParametros.Add(new SqlParameter("strCobrePpm90", objRecuperacion.strCobrePpm90));
                lstParametros.Add(new SqlParameter("strRcpArtifFinal", objRecuperacion.strRcpArtifFinal));
                lstParametros.Add(new SqlParameter("strRcpOroFinal", objRecuperacion.strRcpOroFinal));
                lstParametros.Add(new SqlParameter("strRcpPlataFinal", objRecuperacion.strRcpPlataFinal));
                lstParametros.Add(new SqlParameter("strPorcCobreFinal", objRecuperacion.strPorcCobreFinal));
                lstParametros.Add(new SqlParameter("strREEDrive", objRecuperacion.strREEDrive));
                lstParametros.Add(new SqlParameter("strHorasAgitacion", objRecuperacion.strHorasAgitacion));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objRecuperacion.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spRecuperacion_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Recuperacion: Error en el metodo PutRecuperacion(RecuperacionModel objRecuperacion=: <strLote=:{0}>)", objRecuperacion.strLote, objRecuperacion.strTipoProceso);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Querys]
        public int ExecuteQuery(QueryModel objQuery)
        {
            logger.InfoFormat("Servicio, Querys:    ExecuteQuery(QueryModel objQuery=: <strQuery=:{0}, strUsuarioCrea=:{1}>)", objQuery.strQuery, objQuery.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("sqlCommand", objQuery.strQuery));

                return repositorio.ExecuteSP<int>("dbo.[spQuery_Execute]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Querys:   Error en el metodo ExecuteQuery(QueryModel objQuery=: <strQuery=:{0}, strUsuarioCrea=:{1}>)", objQuery.strQuery, objQuery.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Leyes Comerciales]
        public CollectionResponse<LeyesComercialesModel> GetLeyesComerciales()
        {
            IEnumerable<LeyesComercialesModel> lstListaGeneral = new List<LeyesComercialesModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<LeyesComercialesModel>("dbo.[spLeyesComerciales_Get]").ToList();
                return new CollectionResponse<LeyesComercialesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Comerciales:    Error en el metodo GetLeyesComerciales()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LeyesComercialesModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutLeyesComerciales(LeyesComercialesModel objLeyCom)
        {
            logger.InfoFormat("Servicio, Leyes Comerciales: PutLeyesComerciales(LeyesComercialesModel objLeyCom=: <intIdZona=:{0}, strUsuarioCrea=:{1}>)", objLeyCom.intIdZona, objLeyCom.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdZona", objLeyCom.intIdZona));
                lstParametros.Add(new SqlParameter("fltPuntoAu", objLeyCom.fltPuntoAu));
                lstParametros.Add(new SqlParameter("fltValorAuMenor", objLeyCom.fltValorAuMenor));
                lstParametros.Add(new SqlParameter("fltValorAuMayor", objLeyCom.fltValorAuMayor));
                lstParametros.Add(new SqlParameter("fltPuntoAuInter", objLeyCom.fltPuntoAuInter));
                lstParametros.Add(new SqlParameter("fltValueAuInter", objLeyCom.fltValueAuInter));
                lstParametros.Add(new SqlParameter("fltPuntoAuMax", objLeyCom.fltPuntoAuMax));
                lstParametros.Add(new SqlParameter("fltValorAuMax", objLeyCom.fltValorAuMax));
                lstParametros.Add(new SqlParameter("fltValorAg", objLeyCom.fltValorAg));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLeyCom.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesComerciales_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Comerciales:    Error en el metodo PutLeyesComerciales(LeyesComercialesModel objLeyCom=: <intIdZona=:{0}, strUsuarioCrea=:{1}>)", objLeyCom.intIdZona, objLeyCom.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLeyesComerciales(LeyesComercialesModel objLeyCom)
        {
            logger.InfoFormat("Servicio, Leyes Comerciales: UpdateLeyesComerciales(LeyesComercialesModel objLeyCom=: <intIdValue=:{0}, intIdZona=:{1}, strUsuarioModif=:{2}>)", objLeyCom.intIdValue, objLeyCom.intIdZona, objLeyCom.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdValue", objLeyCom.intIdValue));
                lstParametros.Add(new SqlParameter("intIdZona", objLeyCom.intIdZona));
                lstParametros.Add(new SqlParameter("fltPuntoAu", objLeyCom.fltPuntoAu));
                lstParametros.Add(new SqlParameter("fltValorAuMenor", objLeyCom.fltValorAuMenor));
                lstParametros.Add(new SqlParameter("fltValorAuMayor", objLeyCom.fltValorAuMayor));
                lstParametros.Add(new SqlParameter("fltPuntoAuInter", objLeyCom.fltPuntoAuInter));
                lstParametros.Add(new SqlParameter("fltValueAuInter", objLeyCom.fltValueAuInter));
                lstParametros.Add(new SqlParameter("fltPuntoAuMax", objLeyCom.fltPuntoAuMax));
                lstParametros.Add(new SqlParameter("fltValorAuMax", objLeyCom.fltValorAuMax));
                lstParametros.Add(new SqlParameter("fltValorAg", objLeyCom.fltValorAg));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLeyCom.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesComerciales_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Comerciales:    Error en el metodo UpdateLeyesComerciales(LeyesComercialesModel objLeyCom=: <intIdValue=:{0}, intIdZona=:{1}, strUsuarioModif=:{2}>)", objLeyCom.intIdValue, objLeyCom.intIdZona, objLeyCom.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteLeyesComerciales(int? intIdLeyCom, string strUsuario)
        {
            logger.InfoFormat("Servicio, Leyes Comerciales: DeleteLeyesComerciales(int? intIdLeyCom=:{0}, string strUsuario=:{1})", intIdLeyCom, strUsuario);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdValue", intIdLeyCom));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));

                return repositorio.ExecuteSP<int>("dbo.[spLeyesComerciales_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Leyes Comerciales:    Error en el metodo DeleteLeyesComerciales(int? intIdLeyCom=:{0}, string strUsuario=:{1})", intIdLeyCom, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Region]
        public CollectionResponse<ZonaModel> getRegiones()
        {
            IEnumerable<ZonaModel> lstListaGeneral = new List<ZonaModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<ZonaModel>("dbo.[spRegion_Get]").ToList();
                return new CollectionResponse<ZonaModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Region:   Error en el metodo getRegiones()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ZonaModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Rumas]
        public CollectionResponse<RumasModel> GetRumas(RumasModel objSearch)
        {
            IEnumerable<RumasModel> lstListaGeneral = new List<RumasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstListaGeneral = repositorio.ExecuteSP<RumasModel>("dbo.[spRumas_Get]", lstParametros).ToList();
                return new CollectionResponse<RumasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rumas:    Error en el metodo GetRumas(RumasModel objSearch=: <strLote=:{0}, strFechaInicio=:{1}, strFechaFin=:{2}>)", objSearch.strLote, objSearch.strFechaInicio, objSearch.strFechaFin);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RumasModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutRuma(RumasModel objRumas)
        {
            logger.InfoFormat("Servicio, Rumas: PutRuma(RumasModel objRumas=: <intIdRegistro=:{0}, strLote=:{1}, strNroRuma=:{2}, strNroCampania=:{3}, dtmFecha=:{4}, strMesContable=:{5}, strUsuarioCrea=:{6}>)",
                objRumas.intIdRegistro, objRumas.strLote, objRumas.strNroRuma, objRumas.strNroCampania, objRumas.dtmFecha, objRumas.strMesContable, objRumas.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objRumas.intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", objRumas.strLote));
                lstParametros.Add(new SqlParameter("strNroRuma", objRumas.strNroRuma));
                lstParametros.Add(new SqlParameter("dtmFecha", objRumas.dtmFecha));
                lstParametros.Add(new SqlParameter("strMesContable", objRumas.strMesContable));
                lstParametros.Add(new SqlParameter("dtmFechaPerContable", objRumas.dtmFechaPerContable));
                lstParametros.Add(new SqlParameter("strNroCampania", objRumas.strNroCampania));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objRumas.strUsuarioCrea));
                return repositorio.ExecuteSP<int>("dbo.[spRumas_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rumas:    Error en el metodo PutRuma(RumasModel objRumas=: <intIdRegistro=:{0}, strLote=:{1}, strNroRuma=:{2}, strNroCampania=:{3}, dtmFecha=:{4}, strMesContable=:{5}, strUsuarioCrea=:{6}>)",
                objRumas.intIdRegistro, objRumas.strLote, objRumas.strNroRuma, objRumas.strNroCampania, objRumas.dtmFecha, objRumas.strMesContable, objRumas.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public CollectionResponse<RumasModel> SearchRumas(RumasModel objSearch) {
            IEnumerable<RumasModel> lstListaGeneral = new List<RumasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strEstadoRuma", objSearch.strEstadoRuma));
                lstParametros.Add(new SqlParameter("strNroRuma", objSearch.strNroRuma));
                lstListaGeneral = repositorio.ExecuteSP<RumasModel>("dbo.[spRumas_Search]", lstParametros).ToList();
                return new CollectionResponse<RumasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rumas:    Error en el metodo SearchRumas(RumasModel objSearch=: <strLote=:{0}, strFechaInicio=:{1}, strFechaFin=:{2}, strEstadoRuma=:{3}, strNroRuma=:{4}>)", objSearch.strLote, objSearch.strFechaInicio, objSearch.strFechaFin, objSearch.strEstadoRuma, objSearch.strNroRuma);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RumasModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<RumasModel> GetAllRumas(RumasModel objSearch)
        {
            IEnumerable<RumasModel> lstListaGeneral = new List<RumasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstListaGeneral = repositorio.ExecuteSP<RumasModel>("dbo.[spRumas_GetAll]", lstParametros).ToList();
                return new CollectionResponse<RumasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rumas:    Error en el metodo GetAllRumas(RumasModel objSearch=: <strFechaInicio=:{0}, strFechaFin=:{1}>)", objSearch.strFechaInicio, objSearch.strFechaFin);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RumasModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<RumasModel> GetStockMineral(RumasModel objSearch)
        {
            IEnumerable<RumasModel> lstListaGeneral = new List<RumasModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstParametros.Add(new SqlParameter("strNroRuma", objSearch.strNroRuma));
                lstParametros.Add(new SqlParameter("strNroCampania", objSearch.strNroCampania));
                lstParametros.Add(new SqlParameter("strMesContable", objSearch.strMesContable));
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                
                lstListaGeneral = repositorio.ExecuteSP<RumasModel>("dbo.[spRumas_StockMineral]", lstParametros).ToList();
                return new CollectionResponse<RumasModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rumas:    Error en el metodo GetStockMineral(RumasModel objSearch=: <strLote=:{0}, strNroRuma=:{1}, strNroCampania=:{2}, strMesContable=:{3}, strFechaInicio=:{4}, strFechaFin=:{5}>)",
                    objSearch.strLote, objSearch.strNroRuma, objSearch.strNroCampania, objSearch.strMesContable, objSearch.strFechaInicio, objSearch.strFechaFin);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RumasModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Gasto Administrativo]
        public CollectionResponse<GastoAdmModel> GetGastoAdm(GastoAdmModel objSearch)
        {
            IEnumerable<GastoAdmModel> lstListaGeneral = new List<GastoAdmModel>();
            try
            {
                DateTime _fecha = DateTime.Parse(objSearch.dtmFecha.ToString());

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));
                lstParametros.Add(new SqlParameter("strAcopiador", objSearch.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objSearch.strProveedor));
                lstParametros.Add(new SqlParameter("strFecha", _fecha.ToString("yyyy-MM-dd")));
                lstListaGeneral = repositorio.ExecuteSP<GastoAdmModel>("dbo.[spGastoAdm_Get]", lstParametros).ToList();
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo GetGastoAdm(GastoAdmModel objSearch=: <strZona=:{0}, strProveedor=:{1}, strFecha=:{2}>)", objSearch.strZona, objSearch.strProveedor, objSearch.dtmFecha.ToString());
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutGastAdm(GastoAdmModel objGastoAdm)
        {
            logger.InfoFormat("Servicio, Gasto Administrativo:  PutGastAdm(GastoAdmModel objGastoAdm=: <strZona=:{0}, strAcopiador=:{1}, strProveedor=:{2}, fltGasto=:{3}, strMes=:{4}, strUsuarioCrea=:{5}>)", objGastoAdm.strZona, objGastoAdm.strAcopiador, objGastoAdm.strProveedor, objGastoAdm.fltGasto, objGastoAdm.strMes, objGastoAdm.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", objGastoAdm.strZona));
                lstParametros.Add(new SqlParameter("strAcopiador", objGastoAdm.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objGastoAdm.strProveedor));
                lstParametros.Add(new SqlParameter("fltGasto", objGastoAdm.fltGasto));
                lstParametros.Add(new SqlParameter("dtmFecha", objGastoAdm.dtmFecha));
                lstParametros.Add(new SqlParameter("strMes", objGastoAdm.strMes));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objGastoAdm.strUsuarioCrea));
                return repositorio.ExecuteSP<int>("dbo.[spGastoAdm_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo PutGastAdm(GastoAdmModel objGastoAdm=: <strZona=:{0}, strAcopiador=:{1}, strProveedor=:{2}, fltGasto=:{3}, strMes=:{4}, strUsuarioCrea=:{5}>)", objGastoAdm.strZona, objGastoAdm.strAcopiador, objGastoAdm.strProveedor, objGastoAdm.fltGasto, objGastoAdm.strMes, objGastoAdm.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public int UpdateGastAdm(GastoAdmModel objGastoAdm)
        {
            logger.InfoFormat("Servicio, Gasto Administrativo:  UpdateGastAdm(GastoAdmModel objGastoAdm=: <intIdGastoAdm=:{0}, strZona=:{1}, strAcopiador=:{2}, strProveedor=:{3}, fltGasto=:{4}, strMes=:{5}, strUsuarioModif=:{6}>)", objGastoAdm.intIdGastosAdm, objGastoAdm.strZona, objGastoAdm.strAcopiador, objGastoAdm.strProveedor, objGastoAdm.fltGasto, objGastoAdm.strMes, objGastoAdm.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdGastoAdm", objGastoAdm.intIdGastosAdm));
                lstParametros.Add(new SqlParameter("strZona", objGastoAdm.strZona));
                lstParametros.Add(new SqlParameter("strAcopiador", objGastoAdm.strAcopiador));
                lstParametros.Add(new SqlParameter("strProveedor", objGastoAdm.strProveedor));
                lstParametros.Add(new SqlParameter("fltGasto", objGastoAdm.fltGasto));
                lstParametros.Add(new SqlParameter("dtmFecha", objGastoAdm.dtmFecha));
                lstParametros.Add(new SqlParameter("strMes", objGastoAdm.strMes));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objGastoAdm.strUsuarioModif));
                return repositorio.ExecuteSP<int>("dbo.[spGastoAdm_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo UpdateGastAdm(GastoAdmModel objGastoAdm=: <intIdGastoAdm=:{0}, strZona=:{1}, strAcopiador=:{2}, strProveedor=:{3}, fltGasto=:{4}, strMes=:{5}, strUsuarioModif=:{6}>)", objGastoAdm.intIdGastosAdm, objGastoAdm.strZona, objGastoAdm.strAcopiador, objGastoAdm.strProveedor, objGastoAdm.fltGasto, objGastoAdm.strMes, objGastoAdm.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public CollectionResponse<GastoAdmModel> SearchGastoAdm(GastoAdmModel objSearch)
        {
            IEnumerable<GastoAdmModel> lstListaGeneral = new List<GastoAdmModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strMes", objSearch.strMes));
                lstListaGeneral = repositorio.ExecuteSP<GastoAdmModel>("dbo.[spGastoAdm_Search]", lstParametros).ToList();
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo SearchGastoAdm(GastoAdmModel objSearch=: <strMes=:{0}>)", objSearch.strMes);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<GastoAdmModel> GetProveedores(GastoAdmModel objSearch)
        {
            IEnumerable<GastoAdmModel> lstListaGeneral = new List<GastoAdmModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strZona", objSearch.strZona));
                lstListaGeneral = repositorio.ExecuteSP<GastoAdmModel>("dbo.[spGastoAdm_GetProv]", lstParametros).ToList();
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo GetProveedores(GastoAdmModel objSearch=: <strZona=:{0}>)", objSearch.strZona);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public CollectionResponse<GastoAdmModel> GetAcopiadores()
        {
            IEnumerable<GastoAdmModel> lstListaGeneral = new List<GastoAdmModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<GastoAdmModel>("dbo.[spGastoAdm_GetAcop]").ToList();
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo GetAcopiadores()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<GastoAdmModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        public int DeleteGastAdm(int? IdGastoAdm, string strUsuario)
        {
            logger.InfoFormat("Servicio, Gasto Administrativo:  DeleteGastAdm(int? IdGastoAdm=:{0}, string strUsuario=:{1})", IdGastoAdm, strUsuario);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdGastoadm", IdGastoAdm));
                lstParametros.Add(new SqlParameter("strUsuario", strUsuario));
                return repositorio.ExecuteSP<int>("dbo.[spGastoAdm_Delete]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo DeleteGastAdm(int? IdGastoAdm=:{0}, string strUsuario=:{1})", IdGastoAdm, strUsuario);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        public CollectionResponse<GastoAdmMesModel> GetGastoAdmMes(GastoAdmModel objSearch)
        {
            IEnumerable<GastoAdmMesModel> lstListaGeneral = new List<GastoAdmMesModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strAnio", objSearch.strAnio));
                lstListaGeneral = repositorio.ExecuteSP<GastoAdmMesModel>("dbo.[spGastoAdm_GetMes]", lstParametros).ToList();
                return new CollectionResponse<GastoAdmMesModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Gasto Administrativo: Error en el metodo GetGastoAdmMes(GastoAdmModel objSearch=: <strAnio=:{0}>)", objSearch.strAnio);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<GastoAdmMesModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region[Tanques]
        public CollectionResponse<TanqueModel> GetTanques()
        {
            IEnumerable<TanqueModel> lstListaGeneral = new List<TanqueModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<TanqueModel>("dbo.[spTanque_Get]").ToList();
                return new CollectionResponse<TanqueModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Tanques:  Error en el metodo GetTanques()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<TanqueModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutTanque(TanqueModel objTanque)
        {
            logger.InfoFormat("Servicio, Tanques:   PutTanque(TanqueModel objTanque=: <intIdTanque=:{0}, strNroTanque=:{1}, strUsuarioCrea=:{2}>)", objTanque.intIdTanque, objTanque.strNroTanque, objTanque.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdTanque", objTanque.intIdTanque));
                lstParametros.Add(new SqlParameter("dtmFecha", objTanque.dtmFecha));
                lstParametros.Add(new SqlParameter("strNroTanque", objTanque.strNroTanque));
                lstParametros.Add(new SqlParameter("strCarbonSeco", objTanque.strCarbonSeco));
                lstParametros.Add(new SqlParameter("strGrAuKgC", objTanque.strGrAuKgC));
                lstParametros.Add(new SqlParameter("strGrAgKgC", objTanque.strGrAgKgC));
                lstParametros.Add(new SqlParameter("strDensPulpa", objTanque.strDensPulpa));
                lstParametros.Add(new SqlParameter("strVolPulpa", objTanque.strVolPulpa));
                lstParametros.Add(new SqlParameter("strGAum3", objTanque.strGAum3));
                lstParametros.Add(new SqlParameter("strGAut", objTanque.strGAut));
                lstParametros.Add(new SqlParameter("strGAgm3", objTanque.strGAgm3));
                lstParametros.Add(new SqlParameter("strGAgt", objTanque.strGAgt));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objTanque.strUsuarioCrea));
                return repositorio.ExecuteSP<int>("dbo.[spTanque_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Tanques:  Error en el metodo PutTanque(TanqueModel objTanque=: <intIdTanque=:{0}, strNroTanque=:{1}, strUsuarioCrea=:{2}>)", objTanque.intIdTanque, objTanque.strNroTanque, objTanque.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Cierre Inventario]
        public CollectionResponse<CierreInventarioModel> GetCierreInventarios()
        {
            IEnumerable<CierreInventarioModel> lstListaGeneral = new List<CierreInventarioModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<CierreInventarioModel>("dbo.[spCierreInv_Get]").ToList();
                return new CollectionResponse<CierreInventarioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cierre Inventario:    Error en el metodo GetCierreInventarios()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<CierreInventarioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<CierreInventarioModel> GetPesoInventario(CierreInventarioModel objCierre)
        {
            IEnumerable<CierreInventarioModel> lstListaGeneral = new List<CierreInventarioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaFin", objCierre.strFechaFin));

                lstListaGeneral = repositorio.ExecuteSP<CierreInventarioModel>("dbo.[spCierreInv_GetPesos]", lstParametros).ToList();
                return new CollectionResponse<CierreInventarioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cierre Inventario:    Error en el metodo GetPesoInventario()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<CierreInventarioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public CollectionResponse<RumasInventarioModel> GetRumasInventario()
        {
            IEnumerable<RumasInventarioModel> lstListaGeneral = new List<RumasInventarioModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<RumasInventarioModel>("dbo.[spCierreInv_GetRumas]").ToList();
                return new CollectionResponse<RumasInventarioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Cierre Inventario:    Error en el metodo GetRumasInventario()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RumasInventarioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutCierreInventario(CierreInventarioModel objCierre)
        {
            logger.InfoFormat("Servicio, Cierre Inventario: PutCierreInventario(CierreInventarioModel objCierre=: <fltPesoTmh=:{0}, fltPesoSecoTM=:{1}, strRumasDesc=:{2}, strLotesDesc=:{3}, strMes=:{4}, strAnio=:{5}, strUsuarioCrea=:{6}>)",
                objCierre.fltPesoTmh, objCierre.fltPesoSecoTM, objCierre.strRumasDesc, objCierre.strLotesDesc, objCierre.strMes, objCierre.strAnio, objCierre.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("fltPesoTmh", objCierre.fltPesoTmh));
                lstParametros.Add(new SqlParameter("fltPesoSecoTM", objCierre.fltPesoSecoTM));
                lstParametros.Add(new SqlParameter("strRumasDesc", objCierre.strRumasDesc));
                lstParametros.Add(new SqlParameter("strLotesDesc", objCierre.strLotesDesc));
                lstParametros.Add(new SqlParameter("dtmFecha", objCierre.dtmFecha));
                lstParametros.Add(new SqlParameter("strMes", objCierre.strMes));
                lstParametros.Add(new SqlParameter("strAnio", objCierre.strAnio));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objCierre.strUsuarioCrea));
                return repositorio.ExecuteSP<int>("dbo.[spCierreInv_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Tanques:  Error en el metodo PutCierreInventario(CierreInventarioModel objCierre=: <fltPesoTmh=:{0}, fltPesoSecoTM=:{1}, strRumasDesc=:{2}, strLotesDesc=:{3}, strMes=:{4}, strAnio=:{5}, strUsuarioCrea=:{6}>)",
                    objCierre.fltPesoTmh, objCierre.fltPesoSecoTM, objCierre.strRumasDesc, objCierre.strLotesDesc, objCierre.strMes, objCierre.strAnio, objCierre.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Lotes AS]
        public CollectionResponse<LotesAsModel> SearchLotesAS(DetLotesASModel objSearch)
        {
            IEnumerable<LotesAsModel> lstListaGeneral = new List<LotesAsModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strFechaInicio", objSearch.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objSearch.strFechaFin));
                lstParametros.Add(new SqlParameter("strNroAS", objSearch.strNroAS));
                lstParametros.Add(new SqlParameter("strLote", objSearch.strLote));
                lstListaGeneral = repositorio.ExecuteSP<LotesAsModel>("dbo.[spLotesAs_Search]", lstParametros).ToList();
                return new CollectionResponse<LotesAsModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Lotes AS: Error en el metodo GetLotesAS(DetLotesASModel objSearch=: <strNroAS:={0}, strLote=:{1}>)", objSearch.strNroAS, objSearch.strLote);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LotesAsModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutLotesAS(LotesAsModel objLotesAs)
        {
            logger.InfoFormat("Servicio, Lotes AS:  PutLotesAS(LotesAsModel objLotesAs=: <strLote=:{0}, strUsuarioCrea=:{1}>)", objLotesAs.strLote, objLotesAs.strUsuarioCrea);

            try
            {
                SqlParameter parLotes = GetLotes("listaLotes", objLotesAs.lstLotes);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objLotesAs.strLote));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objLotesAs.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objLotesAs.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strProcedenciaLg", objLotesAs.strProcedenciaLg));
                lstParametros.Add(new SqlParameter("strClienteRuc", objLotesAs.strClienteRuc));
                lstParametros.Add(new SqlParameter("strClienteRazonS", objLotesAs.strClienteRazonS));
                lstParametros.Add(new SqlParameter("strMaterial", objLotesAs.strMaterial));
                lstParametros.Add(new SqlParameter("intNroSacos", objLotesAs.intNroSacos));
                lstParametros.Add(new SqlParameter("fltPesoTmh", objLotesAs.fltPesoTmh));
                lstParametros.Add(new SqlParameter("fltPesoTmhHistorico", objLotesAs.fltPesoTmhHistorico));
                lstParametros.Add(new SqlParameter("strDuenioMineral", objLotesAs.strDuenioMineral));
                lstParametros.Add(new SqlParameter("strObservacion", objLotesAs.strObservacion));
                lstParametros.Add(new SqlParameter("strNombreTransportista", objLotesAs.strNombreTransportista));
                lstParametros.Add(new SqlParameter("strNroPlaca", objLotesAs.strNroPlaca));
                lstParametros.Add(new SqlParameter("strGuiaTransport", objLotesAs.strGuiaTransport));
                lstParametros.Add(new SqlParameter("strDniAcopiador", objLotesAs.strDniAcopiador));
                lstParametros.Add(new SqlParameter("strNombreAcopiador", objLotesAs.strNombreAcopiador));
                lstParametros.Add(new SqlParameter("strTurnoRegistro", objLotesAs.strTurnoRegistro));
                lstParametros.Add(new SqlParameter("strCodConcesion", objLotesAs.strCodConcesion));
                lstParametros.Add(new SqlParameter("strNombreConcesion", objLotesAs.strNombreConcesion));
                lstParametros.Add(new SqlParameter("strGuiaRemitente", objLotesAs.strGuiaRemitente));
                lstParametros.Add(new SqlParameter("strCodCompromiso", objLotesAs.strCodCompromiso));
                lstParametros.Add(new SqlParameter("strPorcH2O", objLotesAs.strPorcH2O));
                lstParametros.Add(new SqlParameter("strPSecoTM", objLotesAs.strPSecoTM));
                lstParametros.Add(new SqlParameter("strLeyAuOzTc", objLotesAs.strLeyAuOzTc));
                lstParametros.Add(new SqlParameter("strLeyAuREE", objLotesAs.strLeyAuREE));
                lstParametros.Add(new SqlParameter("strLeyAgOzTc", objLotesAs.strLeyAgOzTc));
                lstParametros.Add(new SqlParameter("strLeyAgREE", objLotesAs.strLeyAgREE));
                lstParametros.Add(new SqlParameter("strPorcRCPRef12hrs", objLotesAs.strPorcRCPRef12hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu30hrs", objLotesAs.strPorcRCPAu30hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu70hrs", objLotesAs.strPorcRCPAu70hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPReeAu72hrs", objLotesAs.strPorcRCPReeAu72hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAg30hrs", objLotesAs.strPorcRCPAg30hrs));
                lstParametros.Add(new SqlParameter("strNaCNKgTM", objLotesAs.strNaCNKgTM));
                lstParametros.Add(new SqlParameter("strNaOHKgTM", objLotesAs.strNaOHKgTM));
                lstParametros.Add(new SqlParameter("strPorcCobre", objLotesAs.strPorcCobre));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLotesAs.strUsuarioCrea));
                lstParametros.Add(parLotes);

                return repositorio.ExecuteSP<int>("dbo.[spLotesAs_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo PutLotesAS(LotesAsModel objLotesAs=: <strLote=:{0}, strUsuarioCrea=:{1}>)", objLotesAs.strLote, objLotesAs.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        private SqlParameter GetLotes(string name, List<DetLotesASModel> lstListaLotes)
        {
            try
            {
                DataTable table = new DataTable("dbo.lstDetLotesAs");
                table.Columns.Add("intIdRegistro", typeof(int));
                table.Columns.Add("strLote", typeof(string));

                foreach (DetLotesASModel lote in lstListaLotes)
                    table.Rows.Add(new object[] { lote.intIdRegistro, lote.strLote });

                SqlParameter parameter = new SqlParameter(name, table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.lstDetLotesAs";

                return parameter;
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Lotes AS: Error en el metodo GetLotes(string name=:{0}, List<DetLotesASModel> lstListaLotes)", name);
                logger.ErrorFormat("Exception - {0}", e);
                return null;
                throw e;
            }
        }

        public CollectionResponse<RegistroModel> SearchLotesLJ(RegistroSearchModel objQuery)
        {
            IEnumerable<RegistroModel> lstListaGeneral = new List<RegistroModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strLote", objQuery.strLote));
                lstParametros.Add(new SqlParameter("strSubLote", objQuery.strSubLote));
                lstParametros.Add(new SqlParameter("strProcedencia", objQuery.strProcedencia));
                lstParametros.Add(new SqlParameter("strFechaInicio", objQuery.strFechaInicio));
                lstParametros.Add(new SqlParameter("strFechaFin", objQuery.strFechaFin));
                lstListaGeneral = repositorio.ExecuteSP<RegistroModel>("dbo.[spLotesAs_GetLJ]", lstParametros).ToList();
                return new CollectionResponse<RegistroModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Lotes AS: Error en el metodo SearchLotesLJ(RegistroSearchModel objQuery=: <strLote=:{0}, strProcedencia=:{1}, strFechaInicio=:{2}, strFechaFin=:{3}>)", objQuery.strLote, objQuery.strProcedencia, objQuery.strFechaInicio, objQuery.strFechaFin);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<RegistroModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int UpdateLotesAS(LotesAsModel objLotesAs)
        {
            logger.InfoFormat("Servicio, Lotes AS:  UpdateLotesAS(LotesAsModel objLotesAs=: <intIdRegistro=:{0}, strLote=:{1}, strUsuarioModif=:{2}>)", objLotesAs.intIdRegistro, objLotesAs.strLote, objLotesAs.strUsuarioModif);

            try
            {
                SqlParameter parLotes = GetLotes("listaLotes", objLotesAs.lstLotes);

                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdRegistro", objLotesAs.intIdRegistro));
                lstParametros.Add(new SqlParameter("strLote", objLotesAs.strLote));
                lstParametros.Add(new SqlParameter("dtmFechaRecepcion", objLotesAs.dtmFechaRecepcion));
                lstParametros.Add(new SqlParameter("strProcedenciaSt", objLotesAs.strProcedenciaSt));
                lstParametros.Add(new SqlParameter("strProcedenciaLg", objLotesAs.strProcedenciaLg));
                lstParametros.Add(new SqlParameter("strClienteRuc", objLotesAs.strClienteRuc));
                lstParametros.Add(new SqlParameter("strClienteRazonS", objLotesAs.strClienteRazonS));
                lstParametros.Add(new SqlParameter("strMaterial", objLotesAs.strMaterial));
                lstParametros.Add(new SqlParameter("intNroSacos", objLotesAs.intNroSacos));
                lstParametros.Add(new SqlParameter("fltPesoTmh", objLotesAs.fltPesoTmh));
                lstParametros.Add(new SqlParameter("fltPesoTmhHistorico", objLotesAs.fltPesoTmhHistorico));
                lstParametros.Add(new SqlParameter("strDuenioMineral", objLotesAs.strDuenioMineral));
                lstParametros.Add(new SqlParameter("strObservacion", objLotesAs.strObservacion));
                lstParametros.Add(new SqlParameter("strNombreTransportista", objLotesAs.strNombreTransportista));
                lstParametros.Add(new SqlParameter("strNroPlaca", objLotesAs.strNroPlaca));
                lstParametros.Add(new SqlParameter("strGuiaTransport", objLotesAs.strGuiaTransport));
                lstParametros.Add(new SqlParameter("strDniAcopiador", objLotesAs.strDniAcopiador));
                lstParametros.Add(new SqlParameter("strNombreAcopiador", objLotesAs.strNombreAcopiador));
                lstParametros.Add(new SqlParameter("strTurnoRegistro", objLotesAs.strTurnoRegistro));
                lstParametros.Add(new SqlParameter("strCodConcesion", objLotesAs.strCodConcesion));
                lstParametros.Add(new SqlParameter("strNombreConcesion", objLotesAs.strNombreConcesion));
                lstParametros.Add(new SqlParameter("strGuiaRemitente", objLotesAs.strGuiaRemitente));
                lstParametros.Add(new SqlParameter("strCodCompromiso", objLotesAs.strCodCompromiso));
                lstParametros.Add(new SqlParameter("strPorcH2O", objLotesAs.strPorcH2O));
                lstParametros.Add(new SqlParameter("strPSecoTM", objLotesAs.strPSecoTM));
                lstParametros.Add(new SqlParameter("strLeyAuOzTc", objLotesAs.strLeyAuOzTc));
                lstParametros.Add(new SqlParameter("strLeyAuREE", objLotesAs.strLeyAuREE));
                lstParametros.Add(new SqlParameter("strLeyAgOzTc", objLotesAs.strLeyAgOzTc));
                lstParametros.Add(new SqlParameter("strLeyAgREE", objLotesAs.strLeyAgREE));
                lstParametros.Add(new SqlParameter("strPorcRCPRef12hrs", objLotesAs.strPorcRCPRef12hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu30hrs", objLotesAs.strPorcRCPAu30hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAu70hrs", objLotesAs.strPorcRCPAu70hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPReeAu72hrs", objLotesAs.strPorcRCPReeAu72hrs));
                lstParametros.Add(new SqlParameter("strPorcRCPAg30hrs", objLotesAs.strPorcRCPAg30hrs));
                lstParametros.Add(new SqlParameter("strNaCNKgTM", objLotesAs.strNaCNKgTM));
                lstParametros.Add(new SqlParameter("strNaOHKgTM", objLotesAs.strNaOHKgTM));
                lstParametros.Add(new SqlParameter("strPorcCobre", objLotesAs.strPorcCobre));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLotesAs.strUsuarioModif));
                lstParametros.Add(parLotes);

                return repositorio.ExecuteSP<int>("dbo.[spLotesAs_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Rol:  Error en el metodo UpdateLotesAS(LotesAsModel objLotesAs=: <intIdRegistro=:{0}, strLote=:{1}, strUsuarioModif=:{2}>)", objLotesAs.intIdRegistro, objLotesAs.strLote, objLotesAs.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion

        #region [Proveedor]
        public CollectionResponse<ProveedorModel> GetAllProveedores()
        {
            throw new NotImplementedException();
        }

        public int PutProveedor(ProveedorModel objProveedor)
        {
            logger.InfoFormat("Servicio, Proveedor: PutProveedor(ProveedorModel objProveedor=: <strRuc=:{0}, strRazonS=:{1}, strCalculoEspecial=:{2}, strUsuarioCrea=:{3}>)", objProveedor.strRuc, objProveedor.strRazonS, objProveedor.strCalculoEspecial, objProveedor.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strRuc", objProveedor.strRuc));
                lstParametros.Add(new SqlParameter("strRazonS", objProveedor.strRazonS));
                lstParametros.Add(new SqlParameter("strCalculoEspecial", objProveedor.strCalculoEspecial));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objProveedor.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spProveedor_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Proveedor:    Error en el metodo PutProveedor(ProveedorModel objProveedor=: <strRuc=:{0}, strRazonS=:{1}, strCalculoEspecial=:{2}, strUsuarioCrea=:{3}>)", objProveedor.strRuc, objProveedor.strRazonS, objProveedor.strCalculoEspecial, objProveedor.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateProveedor(ProveedorModel objProveedor)
        {
            logger.InfoFormat("Servicio, Proveedor: UpdateProveedor(ProveedorModel objProveedor=: <intIdProveedor=:{0}, strRuc=:{1}, strRazonS=:{2}, strCalculoEspecial=:{3}, strUsuarioModif=:{4}>)", objProveedor.intIdProveedor, objProveedor.strRuc, objProveedor.strRazonS, objProveedor.strCalculoEspecial, objProveedor.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdProveedor", objProveedor.intIdProveedor));
                lstParametros.Add(new SqlParameter("strRuc", objProveedor.strRuc));
                lstParametros.Add(new SqlParameter("strRazonS", objProveedor.strRazonS));
                lstParametros.Add(new SqlParameter("strCalculoEspecial", objProveedor.strCalculoEspecial));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objProveedor.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spProveedor_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Proveedor:    Error en el metodo UpdateProveedor(ProveedorModel objProveedor=: <intIdProveedor=:{0}, strRuc=:{1}, strRazonS=:{2}, strCalculoEspecial=:{3}, strUsuarioModif=:{4}>)", objProveedor.intIdProveedor, objProveedor.strRuc, objProveedor.strRazonS, objProveedor.strCalculoEspecial, objProveedor.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int DeleteProveedor(int? intIdProveedor, string strUsuario)
        {
            throw new NotImplementedException();
        }

        public CollectionResponse<ProveedorModel> SearchProveedor(ProveedorModel objSearch)
        {
            IEnumerable<ProveedorModel> lstListaGeneral = new List<ProveedorModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strRazonS", objSearch.strRazonS));
                lstParametros.Add(new SqlParameter("strRuc", objSearch.strRuc));

                lstListaGeneral = repositorio.ExecuteSP<ProveedorModel>("dbo.[spProveedor_Search]", lstParametros).ToList();
                return new CollectionResponse<ProveedorModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Proveedor:    Error en el metodo SearchProveedor(ProveedorModel objSearch=: <strRazonS=:{0}, strRuc=:{1}>)", objSearch.strRazonS, objSearch.strRuc);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<ProveedorModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Laboratorio]
        public CollectionResponse<LaboratorioModel> GetAllLaboratorios()
        {
            IEnumerable<LaboratorioModel> lstListaGeneral = new List<LaboratorioModel>();
            try
            {
                lstListaGeneral = repositorio.ExecuteSPGET<LaboratorioModel>("dbo.[spLaboratorio_GetAll]").ToList();
                return new CollectionResponse<LaboratorioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo GetAllLaboratorios()");
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LaboratorioModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutLaboratorio(LaboratorioModel objLaboratorio)
        {
            logger.InfoFormat("Servicio, Laboratorio:   PutLaboratorio(LaboratorioModel objLaboratorio=: <strNombre=:{0}, strDescripcion=:{1}, strUsuarioCrea=:{2}>)", objLaboratorio.strNombre, objLaboratorio.strDescripcion, objLaboratorio.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombre", objLaboratorio.strNombre));
                lstParametros.Add(new SqlParameter("strDescripcion", objLaboratorio.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objLaboratorio.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spLaboratorio_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo PutLaboratorio(LaboratorioModel objLaboratorio=: <strNombre=:{0}, strDescripcion=:{1}, strUsuarioCrea=:{2}>)", objLaboratorio.strNombre, objLaboratorio.strDescripcion, objLaboratorio.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateLaboratorio(LaboratorioModel objLaboratorio)
        {
            logger.InfoFormat("Servicio, Laboratorio:   UpdateLaboratorio(LaboratorioModel objLaboratorio=: <intIdLaboratorio=:{0}, strNombre=:{1}, strDescripcion=:{2}, strUsuarioModif=:{3}>)", objLaboratorio.intIdLaboratorio, objLaboratorio.strNombre, objLaboratorio.strDescripcion, objLaboratorio.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdLaboratorio", objLaboratorio.intIdLaboratorio));
                lstParametros.Add(new SqlParameter("strNombre", objLaboratorio.strNombre));
                lstParametros.Add(new SqlParameter("strDescripcion", objLaboratorio.strDescripcion));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objLaboratorio.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spLaboratorio_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo UpdateLaboratorio(LaboratorioModel objLaboratorio=: <intIdLaboratorio=:{0}, strNombre=:{1}, strDescripcion=:{2}, strUsuarioModif=:{3}>)", objLaboratorio.intIdLaboratorio, objLaboratorio.strNombre, objLaboratorio.strDescripcion, objLaboratorio.strUsuarioModif);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public CollectionResponse<LaboratorioModel> SearchLaboratorios(LaboratorioModel objSearch)
        {
            IEnumerable<LaboratorioModel> lstListaGeneral = new List<LaboratorioModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNombre", objSearch.strNombre));
                lstParametros.Add(new SqlParameter("strDescripcion", objSearch.strDescripcion));
                lstListaGeneral = repositorio.ExecuteSP<LaboratorioModel>("dbo.[spLaboratorio_Search]", lstParametros).ToList();
                return new CollectionResponse<LaboratorioModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo SearchLaboratorios(LaboratorioModel objSearch=: <strNombre=:{0}, strDescripcion=:{1}>)", objSearch.strNombre, objSearch.strDescripcion);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<LaboratorioModel>(lstListaGeneral, -1);
                throw e;
            }
        }
        #endregion

        #region [Valorizacion bullon]
        public CollectionResponse<BarrasBullonModel> GetBarrasBullon(BarrasBullonModel objSearch)
        {
            IEnumerable<BarrasBullonModel> lstListaGeneral = new List<BarrasBullonModel>();
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNroCampania", objSearch.strNroCampania));
                lstListaGeneral = repositorio.ExecuteSP<BarrasBullonModel>("dbo.[spBarraBullon_Get]", lstParametros).ToList();
                return new CollectionResponse<BarrasBullonModel>(lstListaGeneral);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Valorizacion bullon:  Error en el metodo GetBarrasBullon(BarrasBullonModel objSearch=: <strNroCampania=:{0}>)", objSearch.strNroCampania);
                logger.ErrorFormat("Exception - {0}", e);
                return new CollectionResponse<BarrasBullonModel>(lstListaGeneral, -1);
                throw e;
            }
        }

        public int PutBarrasBullon(BarrasBullonModel objBarra)
        {
            logger.InfoFormat("Servicio, Valorizacion bullon:   PutBarrasBullon(BarrasBullonModel objBarra=: <strNroCampania=:{0}, strFecha=:{1}, fltPeso=:{2}, fltLeyAu=:{3}, fltLeyAg=:{4}, strUsuarioCrea=:{5}>)", objBarra.strNroCampania, objBarra.strFecha, objBarra.fltPeso, objBarra.fltLeyAu, objBarra.fltLeyAg, objBarra.strUsuarioCrea);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("strNroCampania", objBarra.strNroCampania.ToUpper()));
                lstParametros.Add(new SqlParameter("strFecha", objBarra.strFecha));
                lstParametros.Add(new SqlParameter("fltPeso", objBarra.fltPeso));
                lstParametros.Add(new SqlParameter("fltLeyAu", objBarra.fltLeyAu));
                lstParametros.Add(new SqlParameter("fltLeyAg", objBarra.fltLeyAg));
                lstParametros.Add(new SqlParameter("fltFinoAu", objBarra.fltFinoAu));
                lstParametros.Add(new SqlParameter("fltFinoAg", objBarra.fltFinoAg));
                lstParametros.Add(new SqlParameter("strUsuarioCrea", objBarra.strUsuarioCrea));

                return repositorio.ExecuteSP<int>("dbo.[spBarraBullon_Put]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo PutBarrasBullon(BarrasBullonModel objBarra=: <strNroCampania=:{0}, strFecha=:{1}, fltPeso=:{2}, fltLeyAu=:{3}, fltLeyAg=:{4}, strUsuarioCrea=:{5}>)", objBarra.strNroCampania, objBarra.strFecha, objBarra.fltPeso, objBarra.fltLeyAu, objBarra.fltLeyAg, objBarra.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }

        public int UpdateBarrasBullon(BarrasBullonModel objBarra)
        {
            logger.InfoFormat("Servicio, Valorizacion bullon:   UpdateBarrasBullon(BarrasBullonModel objBarra=: <intIdBarra=:{0}, strNroCampania=:{1}, strFecha=:{2}, fltPeso=:{3}, fltLeyAu=:{4}, fltLeyAg=:{5}, strUsuarioCrea=:{6}>)", objBarra.intIdBarra, objBarra.strNroCampania, objBarra.strFecha, objBarra.fltPeso, objBarra.fltLeyAu, objBarra.fltLeyAg, objBarra.strUsuarioModif);
            try
            {
                IList<SqlParameter> lstParametros = new List<SqlParameter>();
                lstParametros.Add(new SqlParameter("intIdBarra", objBarra.intIdBarra));
                lstParametros.Add(new SqlParameter("strNroCampania", objBarra.strNroCampania.ToUpper()));
                lstParametros.Add(new SqlParameter("fltPeso", objBarra.fltPeso));
                lstParametros.Add(new SqlParameter("fltLeyAu", objBarra.fltLeyAu));
                lstParametros.Add(new SqlParameter("fltLeyAg", objBarra.fltLeyAg));
                lstParametros.Add(new SqlParameter("fltFinoAu", objBarra.fltFinoAu));
                lstParametros.Add(new SqlParameter("fltFinoAg", objBarra.fltFinoAg));
                lstParametros.Add(new SqlParameter("strUsuarioModif", objBarra.strUsuarioModif));

                return repositorio.ExecuteSP<int>("dbo.[spBarraBullon_Update]", lstParametros).First();
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Servicio, Laboratorio:  Error en el metodo UpdateBarrasBullon(BarrasBullonModel objBarra=: <strNroCampania=:{0}, strFecha=:{1}, fltPeso=:{2}, fltLeyAu=:{3}, fltLeyAg=:{4}, strUsuarioCrea=:{5}>)", objBarra.strNroCampania, objBarra.strFecha, objBarra.fltPeso, objBarra.fltLeyAu, objBarra.fltLeyAg, objBarra.strUsuarioCrea);
                logger.ErrorFormat("Exception - {0}", e);
                return -1;
                throw e;
            }
        }
        #endregion
    }
}
