﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.Servicio
{
    public interface IServicioBalanza
    {
        #region [Acceso]
        /// <summary>
        /// Obtener todos los registros de la tabla tblAcceso
        /// </summary>
        /// <returns></returns>
        CollectionResponse<AccesoModel> GetAllAccesos();
        #endregion

        #region [Rol]
        /// <summary>
        /// Obtener todos los registros de la tabla tblRol
        /// </summary>
        /// <returns>Retorna todos los roles</returns>
        CollectionResponse<RolModel> GetAllRoles();
        /// <summary>
        /// Insertar Rol con sus Accesos
        /// </summary>
        /// <param name="objRol">Objeto de la clase RolModel</param>
        /// <returns>Retorna el codigo del Rol insertado</returns>
        int PutRol(RolModel objRol);
        /// <summary>
        /// Modificar Rol y sus Accesos
        /// </summary>
        /// <param name="objRol">Objeto de la clase RolModel con datos modificados</param>
        /// <returns>Retorna codigo del registro modificado</returns>
        int UpdateRol(RolModel objRol);
        /// <summary>
        /// Eliminar Rol y sus Accesos
        /// </summary>
        /// <param name="intIdRol">Codigo del Rol a eliminar</param>
        /// <param name="strUsuario">Usuario que eliminara el registro</param>
        /// <returns>Retorna codigo del rol eliminado</returns>
        int DeleteRol(int? intIdRol, string strUsuario);
        #endregion

        #region [RolAcceso]
        CollectionResponse<RolAccesoModel> GetRolAcceso(int? intIdRol);
        #endregion

        #region [Usuarios]
        /// <summary>
        /// Obtener todos los registros de la tabla tblUsuarios
        /// </summary>
        /// <returns>Retorna todos los usuarios registrados en el sistema</returns>
        CollectionResponse<UsuarioModel> GetAllUsuarios();
        /// <summary>
        /// Inserta un usuario
        /// </summary>
        /// <param name="objUsuario">Objeto de la clase UsuarioModel</param>
        /// <returns>Retorna codigo del usuario insertado</returns>
        string PutUsuario(UsuarioModel objUsuario);
        /// <summary>
        /// Actualizar datos del usuario
        /// </summary>
        /// <param name="objUsuario">Objeto de la clase UsuarioModel que se actualizará</param>
        /// <returns>Retorna codigo del Usuario actualizado</returns>
        string UpdateUsuario(UsuarioModel objUsuario);
        /// <summary>
        /// Eliminar usuario
        /// </summary>
        /// <param name="intIdUsuario">Codigo del usuario que se eliminará</param>
        /// <param name="strUsuario">Usuario que eliminara el registro</param>
        /// <returns>Retorna codigo del usuario eliminado</returns>
        string DeleteUsuario(string strIdUsuario, string strUsuario);
        /// <summary>
        /// Obtener datos del usuario que se logeará
        /// </summary>
        /// <param name="strUsuario">Nombre de usuario</param>
        /// <param name="strPassword">Contraseña del usuario</param>
        /// <returns>Retorna un objeto del clase UsuarioModel</returns>
        UsuarioModel LoginUsuario(string strUsuario, string strPassword);
        /// <summary>
        /// Busqueda de Usuarios de acuerdo a los filtros de busqueda
        /// </summary>
        /// <param name="objQuery">Objeto que contiene los filtros de busqueda</param>
        /// <returns>Retorna todos los registros que coincidan con la busqueda</returns>
        CollectionResponse<UsuarioModel> SearchUsuarios(UsuarioModel objQuery);
        /// <summary>
        /// Obtener los accesos del usuario
        /// </summary>
        /// <param name="strCargo">Cargo del usuario</param>
        /// <returns>Retorna todos los accesos del usuario de acuerdo a su cargo</returns>
        CollectionResponse<RolAccesoModel> GetUsuarioAccesos(string strCargo);

        UsuarioModel GetPersona(string strIdPersona);
        string UpdatePersona(UsuarioModel objPersona);
        string ChangeUsuario(UsuarioModel objUsuario);
        #endregion

        #region [Material]
        CollectionResponse<MaterialModel> GetAllMaterial();
        int PutMaterial(MaterialModel objMaterial);
        int UpdateMaterial(MaterialModel objMaterial);
        int DeleteMaterial(int? intIdMaterial, string strUsuario);
        #endregion

        #region [Registro]
        CollectionResponse<RegistroModel> GetRegistros();
        int PutRegistro(RegistroModel objRegistro);
        int UpdateRegistroBalanza(RegistroModel objRegistro);
        int UpdateRegistroLaboratorio(RegistroModel objRegistro);
        int UpdateRegistroComercial(RegistroModel objRegistro);
        int DeleteRegistro(int? intIdRegistro, string strUsuario);
        CollectionResponse<RegistroModel> SearchRegistros(RegistroSearchModel objQuery);
        CollectionResponse<RegistroModel> SearchRegistrosComercial(RegistroSearchModel objQuery);
        int GetLastRegistro();
        int DuplicateRegistro(int? intIdRegistro, string strLote);
        #endregion

        #region [Maestro]
        MaestroModel SearchMaestro(string strNombre);
        #endregion

        #region [Acopiador]
        CollectionResponse<AcopiadorModel> GetAllAcopiador();
        int PutAcopiador(AcopiadorModel objAcopiador);
        int UpdateAcopiador(AcopiadorModel objAcopiador);
        int DeleteAcopiador(int? intIdAcopiador, string strUsuario);
        #endregion

        #region [Zona]
        CollectionResponse<ZonaModel> GetAllZonas();
        int PutZona(ZonaModel objZona);
        int UpdateZona(ZonaModel objZona);
        int DeleteZona(int? intIdZona, string strUsuario);
        ZonaModel SearchZona(string strZona);
        #endregion

        #region [Cuota]
        CollectionResponse<CuotaModel> GetAllCuotas();
        int PutCuota(CuotaModel objCuota);
        int UpdateCuota(CuotaModel objCuota);
        int DeleteCuota(int? intIdCuota, string strUsuario);
        CollectionResponse<CuotaModel> SearchCuotas(CuotaModel objSearch);
        #endregion

        #region [Reportes]
        CollectionResponse<ReporteAcopioMineralModel> getReporteAcopioMineral(ReporteAcopioMineralModel objSearch);
        CollectionResponse<ReporteControlCuotasModel> getReporteControlCuotas(ReporteControlCuotasModel objSearch);
        CollectionResponse<ReporteDetAcopioModel> getDetalleReporteAcopio(ReporteDetAcopioModel objSearch);
        CollectionResponse<ReporteDetAcopioModel> getReporteAcopioMRL_Detallado(ReporteDetAcopioModel objSearch);
        CollectionResponse<ReporteAcopioMesModel> getReporteAcopioMes(string anio, string type);
        CollectionResponse<ReporteAcopioMesModel> getReporteZonaMes(string anio);
        CollectionResponse<ReporteAcopioMesModel> getReportePonderadoMes(string anio);
        CollectionResponse<ReporteAcopioMesModel> getReporteAcopiadorMes(string anio);
        CollectionResponse<ReporteAcopioMineralModel> getReporteAcopiadorZona(ReporteAcopioMineralModel objSearch);
        CollectionResponse<ReporteAcopioMesModel> getReporteFinoMes(string anio);
        //REPORTES ROBIN
        CollectionResponse<ReporteAcopioMesModel> getReporteMineralRobin(string anio, string zona);
        CollectionResponse<ReporteDetAcopioModel> getReporteDiarioRobin(RegistroSearchModel objQuery);
        //REPORTE MARGENES
        CollectionResponse<ReporteMargenesModel> getReporteMargenes(ReporteMargenesModel objSearch);
        CollectionResponse<ReporteMargenDetModel> getReporteMargenDetallado(ReporteMargenDetModel objSearch);
        CollectionResponse<ReporteMargenesModel> getReporteMargenProductivo(QueryModel objSearch);
        CollectionResponse<ReporteMargenDetModel> getReporteMargenProdDetallado(QueryModel objSearch);
        CollectionResponse<ReporteMargenesModel> getReporteMargenDiario(ReporteMargenesModel objSearch);
        CollectionResponse<ReporteMargenDetModel> getReporteMargenDiarioDetallado(ReporteMargenDetModel objSearch);
        CollectionResponse<ReporteMargenesModel> getReporteMargenDiarioZona(ReporteMargenesModel objSearch);
        //REPORTES PLANTA
        CollectionResponse<RegistroModel> getReporteMineralPlanta(RegistroSearchModel objQuery);
        CollectionResponse<ReporteMineralProcesadoModel> getReporteMineralProcesado(ReporteMineralProcesadoModel objQuery);
        //REPORTE PROVISIONES
        CollectionResponse<ReporteProvisionesModel> getReporteProvisiones(ReporteProvisionesModel objSearch);
        #endregion

        #region [Leyes Referenciales]
        CollectionResponse<LeyesReferencialesModel> SearchLeyesRef(LeyesReferencialesModel objSearch);
        int PutLeyRef(LeyesReferencialesModel objLeyRef);
        int UpdateLeyRef_Bal(LeyesReferencialesModel objLeyRef);
        int UpdateLeyRef_Lab(LeyesReferencialesModel objLeyRef);
        int UpdateLeyRef_Com(LeyesReferencialesModel objLeyRef);
        #endregion

        #region [Liquidacion]
        CollectionResponse<LiquidacionModel> GetLiquidacion(RegistroSearchModel objSearch);
        int PutLiquidacion(LiquidacionModel objLiquidacion);
        int UpdateLiquidacion(LiquidacionModel objLiquidacion);
        CollectionResponse<LiquidacionModel> GetReintegro(string strLote);
        int PutReintegro(LiquidacionModel objLiquidacion);
        #endregion

        #region [Parametros]
        CollectionResponse<ParametrosModel> GetParametrosZona(string strZona, string strProveedor);
        CollectionResponse<MaquilasModel> GetMaquilas(MaquilasModel objSearch);
        CollectionResponse<ParametrosModel> GetAllParametros(string strType);
        int PutParametroZona(ParametrosModel objParametro);
        int UpdateParametroZona(ParametrosModel objParametro);

        int PutParametroProv(ParametrosModel objParametro);
        int UpdateParametroProv(ParametrosModel objParametro);
        int DuplicateParametro(int? idParametro, string strUsuario);
        int DuplicateParametroProv(int? idParametro, string strUsuario);

        //Precio internacional
        CollectionResponse<PrecioInterModel> GetPrecioInter(string strName);
        string UpdatePrecioInter(PrecioInterModel objPrecio);
        string PutPrecioInter(PrecioInterModel objPrecio);
        CollectionResponse<PrecioInterModel> SearchPrecioInter(PrecioInterModel objSearch);
        //Factor de Conversion
        string UpdateFactorConversion(MaestroModel objMaestro);
        #endregion

        #region[Ventas]
        CollectionResponse<VentasModel> GetVentas(VentasSearchModel objSearch);
        int PutVenta(VentasModel objVenta);
        #endregion

        #region[Leyes Laboratorio]
        string PutLeyesLaboratorio(List<LeyesModel> lstLeyes);
        CollectionResponse<LeyesModel> GetLeyesLaboratorio(string strLote);
        string PutLeyesRegistro(RegistroModel objLeyRegistro);
        #endregion

        #region[Consumos]
        ConsumoModel GetConsumos(string strLote);
        int PutConsumo(ConsumoModel objConsumo);
        #endregion

        #region [Recuperacion]
        RecuperacionModel GetRecuperacion(string strLote, string strTipoProceso);
        int PutRecuperacion(RecuperacionModel objRecuperacion);
        #endregion

        #region [Querys]
        int ExecuteQuery(QueryModel objQuery);
        #endregion

        #region [Leyes Comerciales]
        CollectionResponse<LeyesComercialesModel> GetLeyesComerciales();
        int PutLeyesComerciales(LeyesComercialesModel objLeyCom);
        int UpdateLeyesComerciales(LeyesComercialesModel objLeyCom);
        int DeleteLeyesComerciales(int? intIdLeyCom, string strUsuario);
        #endregion

        #region [Region]
        CollectionResponse<ZonaModel> getRegiones();
        #endregion

        #region [Rumas]
        CollectionResponse<RumasModel> GetRumas(RumasModel objSearch);
        int PutRuma(RumasModel objRumas);
        CollectionResponse<RumasModel> SearchRumas(RumasModel objSearch);
        CollectionResponse<RumasModel> GetAllRumas(RumasModel objSearch);
        CollectionResponse<RumasModel> GetStockMineral(RumasModel objSearch);
        #endregion

        #region [Gasto Administrativo]
        CollectionResponse<GastoAdmModel> GetGastoAdm(GastoAdmModel objSearch);
        CollectionResponse<GastoAdmModel> SearchGastoAdm(GastoAdmModel objSearch);
        int PutGastAdm(GastoAdmModel objGastoAdm);
        int UpdateGastAdm(GastoAdmModel objGastoAdm);
        CollectionResponse<GastoAdmModel> GetProveedores(GastoAdmModel objSearch);
        CollectionResponse<GastoAdmModel> GetAcopiadores();
        CollectionResponse<GastoAdmMesModel> GetGastoAdmMes(GastoAdmModel objSearch);
        int DeleteGastAdm(int? IdGastoAdm, string strUsuario);
        #endregion

        #region [Tanques]
        CollectionResponse<TanqueModel> GetTanques();
        int PutTanque(TanqueModel objTanque);
        #endregion

        #region [CierreInventario]
        CollectionResponse<CierreInventarioModel> GetCierreInventarios();
        CollectionResponse<CierreInventarioModel> GetPesoInventario(CierreInventarioModel objCierre);
        CollectionResponse<RumasInventarioModel> GetRumasInventario();
        int PutCierreInventario(CierreInventarioModel objCierre);
        #endregion

        #region [Lotes AS]
        CollectionResponse<LotesAsModel> SearchLotesAS(DetLotesASModel objSearch);
        int PutLotesAS(LotesAsModel objLotesAs);
        int UpdateLotesAS(LotesAsModel objLotesAs);
        CollectionResponse<RegistroModel> SearchLotesLJ(RegistroSearchModel objQuery);
        #endregion

        #region[Proveedor]
        CollectionResponse<ProveedorModel> GetAllProveedores();
        int PutProveedor(ProveedorModel objProveedor);
        int UpdateProveedor(ProveedorModel objProveedor);
        int DeleteProveedor(int? intIdProveedor, string strUsuario);
        CollectionResponse<ProveedorModel> SearchProveedor(ProveedorModel objSearch);
        #endregion

        #region[Laboratorio]
        CollectionResponse<LaboratorioModel> GetAllLaboratorios();
        int PutLaboratorio(LaboratorioModel objLaboratorio);
        int UpdateLaboratorio(LaboratorioModel objLaboratorio);
        CollectionResponse<LaboratorioModel> SearchLaboratorios(LaboratorioModel objSearch);
        #endregion

        #region [Valorizacion Bullon]
        CollectionResponse<BarrasBullonModel> GetBarrasBullon(BarrasBullonModel objSearch);
        int PutBarrasBullon(BarrasBullonModel objBarra);
        int UpdateBarrasBullon(BarrasBullonModel objBarra);
        #endregion

    }
}
