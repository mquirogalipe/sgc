﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Balanza.Common
{
    public class ComponentLoader
    {
        public static void LoadContainer(IUnityContainer iunContainer, string strPath, string strPattern)
        {
            var varDirCat = new DirectoryCatalog(strPath, strPattern);
            var varImportDef = BuildImportDefinition();
            try
            {
                using (var varAggregateCatalog = new AggregateCatalog())
                {
                    varAggregateCatalog.Catalogs.Add(varDirCat);

                    using (var varComponsitionContainer = new CompositionContainer(varAggregateCatalog))
                    {
                        IEnumerable<Export> lstExports = varComponsitionContainer.GetExports(varImportDef);

                        IEnumerable<IComponent> lstModules =
                            lstExports.Select(export => export.Value as IComponent).Where(m => m != null);

                        var registerComponent = new RegisterComponent(iunContainer);
                        foreach (IComponent module in lstModules)
                        {
                            module.SetUp(registerComponent);
                        }
                    }
                }
            }
            catch (ReflectionTypeLoadException typeLoadException)
            {
                var varBuilder = new StringBuilder();
                foreach (Exception loaderException in typeLoadException.LoaderExceptions)
                {
                    varBuilder.AppendFormat("{0}\n", loaderException.Message);
                }

                throw new TypeLoadException(varBuilder.ToString(), typeLoadException);
            }
        }

        private static ImportDefinition BuildImportDefinition()
        {
            return new ImportDefinition(
                def => true, typeof(IComponent).FullName, ImportCardinality.ZeroOrMore, false, false);
        }
    }
   
    internal class RegisterComponent : IRegisterComponent
    {
        private readonly IUnityContainer _container;

        public RegisterComponent(IUnityContainer iunContainer)
        {
            this._container = iunContainer;
        }

        public void RegisterType<TFrom, TTo>(bool blnWithInterception = false) where TTo : TFrom
        {
            if (blnWithInterception)
            {
                //register with interception
            }
            else
            {
                this._container.RegisterType<TFrom, TTo>();
            }
        }

        public void RegisterTypeWithControlledLifeTime<T>(bool blnWithInterception = false)
        {
            this._container.RegisterType<T>(new HierarchicalLifetimeManager());
        }

        public void RegisterTypeWithControlledLifeTime<T>()
        {
            this._container.RegisterType<T>(new HierarchicalLifetimeManager());
        }

        public void RegisterInstance<T>(T tInstance)
        {
            this._container.RegisterInstance<T>(tInstance);
        }

        public void RegisterType<TFrom, TTo>(params InjectionMember[] arrMembers) where TTo : TFrom
        {
            this._container.RegisterType<TFrom, TTo>(arrMembers);
        }
    }
}
