﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.Common
{
    public interface IRegisterComponent
    {
        void RegisterType<TFrom, TTo>(bool blnWithInterception = false) where TTo : TFrom;

        void RegisterTypeWithControlledLifeTime<T>(bool blnWithInterception = false);

        void RegisterType<TFrom, TTo>(params InjectionMember[] arrMembers) where TTo : TFrom;

        void RegisterTypeWithControlledLifeTime<T>();

        void RegisterInstance<T>(T tInstance);
    }
}
