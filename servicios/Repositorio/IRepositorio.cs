﻿using Balanza.Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.Repositorio
{
    public interface IRepositorio
    {
        IEnumerable<TEntity> GetAll<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> lstOrderBy = null,
            string strIncludeProperties = null, int? intSkip = null, int? intTake = null)
            where TEntity : tblEntidadBase;

        IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> lstOrderBy = null, string strIncludeProperties = null, int? intSkip = null, int? intTake = null)
            where TEntity : tblEntidadBase;

        TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, string strIncludeProperties = null)
            where TEntity : tblEntidadBase;

        TEntity GetFirst<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> lstOrderBy = null, string strIncludeProperties = null)
            where TEntity : tblEntidadBase;

        TEntity GetById<TEntity>(object objId)
            where TEntity : tblEntidadBase;

        int GetCount<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null)
            where TEntity : tblEntidadBase;

        bool GetExists<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null)
            where TEntity : tblEntidadBase;

        void Create<TEntity>(TEntity tEntity, string strCreatedBy = null)
            where TEntity : tblEntidadBase;
        /*

        void CreateRange<TEntity>(IList<TEntity> entities, string createdBy = null)
            where TEntity : tblEntidadBase;*/

        void Update<TEntity>(TEntity tEntity, string strModifiedBy = null)
            where TEntity : tblEntidadBase;

        void Delete<TEntity>(object objId)
            where TEntity : tblEntidadBase;

        void Delete<TEntity>(TEntity tEntity)
            where TEntity : tblEntidadBase;

        void Save();

        IList<TObject> ExecuteSP<TObject>(String strName, IList<SqlParameter> lstParameters);
        IList<TObject> ExecuteSPGET<TObject>(string strSpName);    

    }
}
