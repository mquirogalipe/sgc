﻿using Balanza.Common;
using Balanza.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.Repositorio
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IRepositorio, RepositorioEF<LiquidacionModel>>();
            registerComponent.RegisterTypeWithControlledLifeTime<LiquidacionModel>();
        }
    }
}
