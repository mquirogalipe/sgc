﻿
using Balanza.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Balanza.Repositorio
{
    public class RepositorioEF<TContext> : IRepositorio
            where TContext : DbContext
    {
        protected readonly TContext _tContext;

        public RepositorioEF(TContext tContext)
        {
            _tContext = tContext;
        }

        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> lstOrderBy = null, string strIncludeProperties = null,
            int? intSkip = null, int? intTake = null)
            where TEntity : tblEntidadBase
        {
            strIncludeProperties = strIncludeProperties ?? string.Empty;
            IQueryable<TEntity> entQuery = _tContext.Set<TEntity>();

            if (lstFilter != null)
            {
                entQuery = entQuery.Where(lstFilter);
            }

            foreach (var includeProperty in strIncludeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                entQuery = entQuery.Include(includeProperty);
            }

            if (lstOrderBy != null)
            {
                entQuery = lstOrderBy(entQuery);
            }

            if (intSkip.HasValue)
            {
                entQuery = entQuery.Skip(intSkip.Value);
            }

            if (intTake.HasValue)
            {
                entQuery = entQuery.Take(intTake.Value);
            }

            return entQuery;
        }

        public IEnumerable<TEntity> GetAll<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> lstOrderBy = null,
            string strIncludeProperties = null, int? intSkip = null, int? intTake = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(null, lstOrderBy, strIncludeProperties, intSkip, intTake).ToList();
        }

        public IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> lstOrderBy = null,
            string strIncludeProperties = null, int? intSkip = null, int? intTake = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(lstFilter, lstOrderBy, strIncludeProperties, intSkip, intTake).ToList();
        }

        public TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, string strIncludeProperties = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(lstFilter, null, strIncludeProperties).SingleOrDefault();
        }

        public TEntity GetFirst<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null, Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> lstOrderBy = null, string strIncludeProperties = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(lstFilter, lstOrderBy, strIncludeProperties).FirstOrDefault();
        }

        public TEntity GetById<TEntity>(object objId)
            where TEntity : tblEntidadBase
        {
            return _tContext.Set<TEntity>().Find(objId);
        }

        public int GetCount<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(lstFilter).Count();
        }

        public bool GetExists<TEntity>(Expression<Func<TEntity, bool>> lstFilter = null)
            where TEntity : tblEntidadBase
        {
            return GetQueryable<TEntity>(lstFilter).Any();
        }

        public void Create<TEntity>(TEntity tEntity, string strCreatedBy = null)
            where TEntity : tblEntidadBase
        {
            /*  entity.FechaCrea = DateTime.UtcNow;
              entity.UsuarioCrea = createdBy == null ? "admin" : createdBy;
              entity.FechaModifica = DateTime.UtcNow;
              entity.UsuarioModifica = createdBy == null ? "admin" : createdBy;*/


            tEntity.chrEstado = "A";
            _tContext.Set<TEntity>().Add(tEntity);
        }
        
      public void CreateRange<TEntity>(IList<TEntity> lstEntities, string strCreatedBy = null) where TEntity : tblEntidadBase
        {
            foreach (var varEntity in lstEntities)
            {
                /* entity.FechaCrea = DateTime.UtcNow;
                 entity.UsuarioCrea = createdBy == null ? "admin" : createdBy;
                 entity.FechaModifica = DateTime.UtcNow;
                 entity.UsuarioModifica = createdBy == null ? "admin" : createdBy;
                entity.Estado = "A";*/
            }
            _tContext.Set<TEntity>().AddRange(lstEntities);
        }

        public void Update<TEntity>(TEntity tEntity, string strModifiedBy = null)
            where TEntity : tblEntidadBase
        {
            /*entity.FechaModifica = DateTime.UtcNow;
            entity.UsuarioModifica = modifiedBy == null ? "admin" : modifiedBy;*/

            _tContext.Set<TEntity>().Attach(tEntity);
            _tContext.Entry(tEntity).State = EntityState.Modified;
        }

        public void Delete<TEntity>(object objId)
            where TEntity : tblEntidadBase
        {
            TEntity entity = _tContext.Set<TEntity>().Find(objId);
            Delete(entity);
        }



        public void Delete<TEntity>(TEntity tEntity)
            where TEntity : tblEntidadBase
        {

            /*var dbSet = context.Set<TEntity>();
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
            */
            tEntity.chrEstado = "E";
            //entity.UsuarioModifica = deletedBy == null ? "admin" : deletedBy;
            _tContext.Set<TEntity>().Attach(tEntity);
            _tContext.Entry(tEntity).State = EntityState.Modified;
        }

        public void Save()
        {
            try
            {
                _tContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }
        }

        protected virtual void ThrowEnhancedValidationException(DbEntityValidationException e)
        {
            var varErrorMessages = e.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

            var varFullErrorMessage = string.Join("; ", varErrorMessages);
            var varExceptionMessage = string.Concat(e.Message, " The validation errors are: ", varFullErrorMessage);
            throw new DbEntityValidationException(varExceptionMessage, e.EntityValidationErrors);
        }


        public IList<TObject> ExecuteSP<TObject>(string strSpName, IList<System.Data.SqlClient.SqlParameter> lstParameters)
        {
            try
            {
                string parString = "@" + lstParameters[0].ParameterName;

                for (int i = 1; i < lstParameters.Count; i++)
                {
                    parString += ", @" + lstParameters[i].ParameterName;
                }

                object[] arrParametros = new object[lstParameters.Count];

                for (int i = 0; i < lstParameters.Count; i++)
                {
                    arrParametros[i] = lstParameters[i];
                }
                IList<TObject> lstData = _tContext.Database.SqlQuery<TObject>("exec " + strSpName + " " + parString, arrParametros).ToList();

                return lstData;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        
        public IList<TObject> ExecuteSPGET<TObject>(string strSpName)
        {
            try
            {          
                IList<TObject> data = _tContext.Database.SqlQuery<TObject>("exec " + strSpName).ToList();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}