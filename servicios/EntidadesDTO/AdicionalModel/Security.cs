﻿using System;

namespace Balanza.EntidadesDTO.AdicionalModel
{
    public static class Security
    {
        public static string Encriptar(this string cadEncriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(cadEncriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        public static string DesEncriptar(this string cadDesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(cadDesencriptar);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }
}
