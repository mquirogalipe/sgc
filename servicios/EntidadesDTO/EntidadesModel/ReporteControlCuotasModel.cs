﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteControlCuotasModel
    {
        public String strMesCuota { get; set; }
        public String strAnioCuota { get; set; }
        public String strFecha { get; set; }
        public String strAcopiador { get; set; }
        public String strNombres { get; set; }
        public String strApellidos { get; set; }
        public String strZona { get; set; }
        public double fltCuota { get; set; }
        public double fltCuotaActual { get; set; }
        public double fltDiffCuota { get; set; }
    }
}
