﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RegistroSearchModel
    {
        public String strLote { get; set; }
        public String strSubLote { get; set; }
        public String strProcedencia { get; set; }
        public String strAcopiador { get; set; }
        public String strProveedor { get; set; }
        public String strMaterial { get; set; }
        public String strEstadoLote { get; set; }
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strFechaLiqInicio { get; set; }
        public String strFechaLiqFin { get; set; }
    }
}
