﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class LotesAsModel
    {
        public int? intIdRegistro { get; set; }
        public String strLote { get; set; }
        public String strSubLote { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strProcedenciaSt { get; set; }
        public String strProcedenciaLg { get; set; }
        public String strClienteRuc { get; set; }
        public String strClienteRazonS { get; set; }
        public String strMaterial { get; set; }
        public int? intNroSacos { get; set; }
        public double? fltPesoTmh { get; set; }
        public double? fltPesoTmhHistorico { get; set; }
        public String strDuenioMineral { get; set; }
        public String strObservacion { get; set; }
        public String strNombreTransportista { get; set; }
        public String strNroPlaca { get; set; }
        public String strGuiaTransport { get; set; }
        public String strDniAcopiador { get; set; }
        public String strNombreAcopiador { get; set; }
        public String strTurnoRegistro { get; set; }
        public String strCodConcesion { get; set; }
        public String strNombreConcesion { get; set; }
        public String strGuiaRemitente { get; set; }
        public String strCodCompromiso { get; set; }
        public String strPorcH2O { get; set; }
        public String strPSecoTM { get; set; }
        public String strLeyAuOzTc { get; set; }
        public String strLeyAuREE { get; set; }
        public String strLeyAgOzTc { get; set; }
        public String strLeyAgREE { get; set; }
        public double? fltFinoAu { get; set; }
        public int? intCantLotes { get; set; }
        public double? fltLJFinoAu { get; set; }
        public String strPorcRCPRef12hrs { get; set; }
        public String strPorcRCPAu30hrs { get; set; }
        public String strPorcRCPAu70hrs { get; set; }
        public String strPorcRCPReeAu72hrs { get; set; }
        public String strPorcRCPAg30hrs { get; set; }
        public String strNaCNKgTM { get; set; }
        public String strNaOHKgTM { get; set; }
        public String strPorcCobre { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public List<DetLotesASModel> lstLotes { get; set; }
    }
}
