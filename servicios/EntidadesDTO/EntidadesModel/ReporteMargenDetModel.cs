﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteMargenDetModel
    {
        public String strFechaInicio { get; set; }
	    public String strFechaFin { get; set; }
        public String strLote { get; set; }
        public String strFechaRecepcion { get; set; }
        public String strProcedenciaSt { get; set; }
        public String strRegion { get; set; }
        public String strNroRuma { get; set; }
        public String strMesContable { get; set; }
        public String strNroCampania { get; set; }
        public String strEstadoLiq { get; set; }
        public double? fltPesoTmh { get; set; }
        public String strPSecoTM { get; set; }
        public double? fltMargenBru { get; set; }
        public double? fltUtilidadBru { get; set; }
        public double? fltRecuperacion { get; set; }
        public double? fltMaquila { get; set; }
        public double? fltMargenPI { get; set; }
        public double? fltConsumoQ { get; set; }
        public double? fltGastoAdm { get; set; }
        public double? fltPrecioInterAu { get; set; }
        public double? fltRecIntAu { get; set; }
        public double? fltMaquilaIntAu { get; set; }
        public double? fltConsQIntAu { get; set; }
        public double? fltGastoAdmIntAu { get; set; }
        public double? fltCostoTotal { get; set; }
    }
}
