﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class CuotaModel
    {
        public int? intIdCuota { get; set; }
        public int? intIdAcopiador { get; set; }
        public int? intIdZona { get; set; }
        public String strDni { get; set; }
        public String strRuc { get; set; }
        public String strNombres { get; set; }
        public String strApellidos { get; set; }
        public String strZona { get; set; }
        public String strProcedencia { get; set; }
        public double? fltCuota { get; set; }
        public String strLeyAu { get; set; }
        public String strFinoAu { get; set; }
        public String strUnidadMedida { get; set; }
        public String strAnio { get; set; }
        public String strMes { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
