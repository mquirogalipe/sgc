﻿using Balanza.Entidades;
using System;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class AcopiadorModel
    {
        public int? intIdAcopiador { get; set; }
        public String strDni { get; set; }
        public String strRuc { get; set; }
        public String strNombres { get; set; }
        public String strApellidos { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public AcopiadorModel() {
            this.intIdAcopiador = intIdAcopiador;
            this.strDni = strDni;
            this.strRuc = strRuc;
            this.strNombres = strNombres;
            this.strApellidos = strApellidos;
            this.strUsuarioCrea = strUsuarioCrea;
            this.dtmFechaCrea = dtmFechaCrea;
            this.strUsuarioModif = strUsuarioModif;
            this.dtmFechaModif = dtmFechaModif;
            this.chrEstado = chrEstado;
        }
        public AcopiadorModel(tblAcopiador objAcopiador) {
            this.intIdAcopiador = objAcopiador.intIdAcopiador;
            this.strDni = objAcopiador.strDni;
            this.strRuc = objAcopiador.strRuc;
            this.strNombres = objAcopiador.strNombres;
            this.strApellidos = objAcopiador.strApellidos;
            this.strUsuarioCrea = objAcopiador.strUsuarioCrea;
            this.dtmFechaCrea = objAcopiador.dtmFechaCrea;
            this.strUsuarioModif = objAcopiador.strUsuarioModif;
            this.dtmFechaModif = objAcopiador.dtmFechaModif;
            this.chrEstado = objAcopiador.chrEstado;
        }
    }
}
