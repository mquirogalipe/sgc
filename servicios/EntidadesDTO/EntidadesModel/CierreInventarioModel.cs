﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class CierreInventarioModel
    {
        public int? intIdCierreInv { get; set; }
        public double? fltPesoTmh { get; set; }
        public double? fltPesoSecoTM { get; set; }
        public String strRumasDesc { get; set; }
        public String strLotesDesc { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strMes { get; set; }
        public String strAnio { get; set; }
        public String strFechaIni { get; set; }
        public String strFechaFin { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
