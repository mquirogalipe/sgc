﻿using System;
using System.Collections.Generic;
using Balanza.Entidades;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RolModel
    {
        public int? intCodRol { get; set; }
        public String strDescripcion { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public List<AccesoModel> lstAccesos {get; set;}
        public RolModel()
        {
            this.intCodRol = intCodRol;
            this.strDescripcion = strDescripcion;
            this.strUsuarioCrea = strUsuarioCrea;
            this.dtmFechaCrea = dtmFechaCrea;
            this.strUsuarioModif = strUsuarioModif;
            this.dtmFechaModif = dtmFechaModif;
            this.chrEstado = chrEstado;
            this.lstAccesos = lstAccesos;
        }
        public RolModel(tblRol objRol)
        {
            this.intCodRol = objRol.intCodRol;
            this.strDescripcion = objRol.strDescripcion;
            this.strUsuarioCrea = objRol.strUsuarioCrea;
            this.dtmFechaCrea = objRol.dtmFechaCrea;
            this.strUsuarioModif = objRol.strUsuarioModif;
            this.dtmFechaModif = objRol.dtmFechaModif;
            this.chrEstado = objRol.chrEstado;
        }
    }
}
