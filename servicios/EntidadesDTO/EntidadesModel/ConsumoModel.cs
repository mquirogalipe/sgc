﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ConsumoModel
    {
        public int? intIdConsumo { get; set; }
        public String strLote { get; set; }
        public String strGeP1 { get; set; }
        public String strGeP2 { get; set; }
        public String strGeP3 { get; set; }
        public String strGeP4 { get; set; }
        public String strGeEspecif2 { get; set; }
        public String strPlDensidad { get; set; }
        public String strPlPorcMalla { get; set; }
        public String strPlPeso { get; set; }
        public String str0HrsLixCN { get; set; }
        public String str2HrsLixCN { get; set; }
        public String str4HrsLixCN { get; set; }
        public String str8HrsLixCN { get; set; }
        public String str12HrsLixCN { get; set; }
        public String str24HrsLixCN { get; set; }
        public String str48HrsLixCN { get; set; }
        public String str72HrsLixCN { get; set; }
        public String str0HrsLixPH { get; set; }
        public String str2HrsLixPH { get; set; }
        public String str4HrsLixPH { get; set; }
        public String str8HrsLixPH { get; set; }
        public String str12HrsLixPH { get; set; }
        public String str24HrsLixPH { get; set; }
        public String str48HrsLixPH { get; set; }
        public String str72HrsLixPH { get; set; }
        public String strHrsLixReactOH0 { get; set; }
        public String strHrsLixReactOH2 { get; set; }
        public String strHrsLixReactOH4 { get; set; }
        public String strHrsLixReactOH8 { get; set; }
        public String strHrsLixReactOH12 { get; set; }
        public String strHrsLixReactOH24 { get; set; }
        public String strHrsLixReactOH48 { get; set; }
        public String strHrsLixReactOH72 { get; set; }
        public String strVolDes2 { get; set; }
        public String strVolDes4 { get; set; }
        public String strVolDes8 { get; set; }
        public String strVolDes12 { get; set; }
        public String strVolDes24 { get; set; }
        public String strVolDes48 { get; set; }
        public String strVolDes72 { get; set; }
        public String strNaOH { get; set; }
        public String strNaCN { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
