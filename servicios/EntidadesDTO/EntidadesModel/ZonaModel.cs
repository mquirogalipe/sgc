﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ZonaModel
    {
        public int? intIdZona { get; set; }
        public String strCodZona { get; set; }
        public String strZona { get; set; }
        public String strProcedencia { get; set; }
        public String strRegion { get; set; }
        public String strCalculoEspecial { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
