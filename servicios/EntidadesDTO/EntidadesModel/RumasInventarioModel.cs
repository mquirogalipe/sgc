﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RumasInventarioModel
    {
        public String strNroRuma { get; set; }
        public String strMesContable { get; set; }
        public int? intCantLotes { get; set; }
        public double? fltSumPesoTmh { get; set; }
        public double? fltSumPSecoTM { get; set; }
    }
}
