﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteAcopioMesModel
    {
        public int? intIdCuota { get; set; }
        public String strNombres { get; set; }
        public String strApellidos { get; set; }
        public String strAcopiador { get; set; }
        public String strZona { get; set; }
        public String strRegion { get; set; }
        public String strAnio { get; set; }
        public String strType { get; set; }
        public String strDni { get; set; }
        public double? enero { get; set; }
        public double? febrero { get; set; }
        public double? marzo { get; set; }
        public double? abril { get; set; }
        public double? mayo { get; set; }
        public double? junio { get; set; }
        public double? julio { get; set; }
        public double? agosto { get; set; }
        public double? septiembre { get; set; }
        public double? octubre { get; set; }
        public double? noviembre { get; set; }
        public double? diciembre { get; set; }
        public double? eneroActual { get; set; }
        public double? febreroActual { get; set; }
        public double? marzoActual { get; set; }
        public double? abrilActual { get; set; }
        public double? mayoActual { get; set; }
        public double? junioActual { get; set; }
        public double? julioActual { get; set; }
        public double? agostoActual { get; set; }
        public double? septiembreActual { get; set; }
        public double? octubreActual { get; set; }
        public double? noviembreActual { get; set; }
        public double? diciembreActual { get; set; }
    }
}
