﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RecuperacionModel
    {
        public int? intIdRecuperacion { get; set; }
        public String strLote { get; set; }
        public String strTipoProceso { get; set; }
        public String strOroPpm2 { get; set; }
        public String strOroPpm4 { get; set; }
        public String strOroPpm8 { get; set; }
        public String strOroPpm12 { get; set; }
        public String strOroPpm24 { get; set; }
        public String strOroPpm48 { get; set; }
        public String strOroPpm72 { get; set; }
        public String strOroPpm90 { get; set; }
        public String strPlataPpm2 { get; set; }
        public String strPlataPpm4 { get; set; }
        public String strPlataPpm8 { get; set; }
        public String strPlataPpm12 { get; set; }
        public String strPlataPpm24 { get; set; }
        public String strPlataPpm48 { get; set; }
        public String strPlataPpm72 { get; set; }
        public String strPlataPpm90 { get; set; }
        public String strLeyAuRipio { get; set; }
        public String strLeyAgRipio { get; set; }
        public String strCobrePpm48 { get; set; }
        public String strCobrePpm72 { get; set; }
        public String strCobrePpm90 { get; set; }
        public String strRcpArtifFinal { get; set; }
        public String strRcpOroFinal { get; set; }
        public String strRcpPlataFinal { get; set; }
        public String strPorcCobreFinal { get; set; }
        public String strREEDrive { get; set; }
        public String strHorasAgitacion { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
