﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class DetVentaModel
    {
        public int? intIdDetVenta { get; set; }
        public int? intIdVenta { get; set; }
        public String strPro { get; set; }
        public String strPeriodo { get; set; }
        public String strOperacion { get; set; }
        public double? fltPeso { get; set; }
        public String strLeyOro { get; set; }
        public String strGrOro { get; set; }
        public double? fltKlgOro { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
