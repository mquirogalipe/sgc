﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteProvisionesModel
    {
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strLote { get; set; }
        public String strFechaRecepcion { get; set; }
        public String strZona { get; set; }
        public String strClienteRuc { get; set; }
        public String strClienteRazonS { get; set; }
        public int? intNroSacos { get; set; }
        public double? fltPesoTmh { get; set; }
        public String strPSecoCom { get; set; }
        public String strSubLote { get; set; }
        public String fltPSecoAS { get; set; }
        public double? fltPrecioTotalAS { get; set; }
        public double? fltPrecioUnitLJ { get; set; }
        public double? fltCalculo { get; set; }
    }
}
