﻿using System;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RolAccesoModel
    {
        public int? intCodRol { get; set; }
        public int? intCodAcceso { get; set; }
        public int? chkEstado { get; set; }
        public String strTipoAcceso { get; set; }
        public String strNombre { get; set; }
        public String strDescripcion { get; set; }
        public String strEnlace { get; set; }
        public int? intPadre { get; set; }
        public int? intNivel { get; set; }
        public int? intSubNivel { get; set; }
        public String strIndex { get; set; }
        public String strClickName { get; set; }
        public String strIconName { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
