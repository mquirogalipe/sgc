﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteMineralProcesadoModel
    {
        public int? intIdRegistro { get; set; }
        public String strLote { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strProcedenciaLg { get; set; }
        public String strProcedenciaSt { get; set; }
        public String strClienteRuc { get; set; }
        public String strClienteRazonS { get; set; }
        public String strMaterial { get; set; }
        public int? intNroSacos { get; set; }
        public double? fltPesoTmh { get; set; }
        public double? fltPesoTmhHistorico { get; set; }
        public String strPorcH2O { get; set; }
        public String strCheckH2O { get; set; }
        public String strPSecoTM { get; set; }
        public String strPSecoHistorico { get; set; }
        public String strPorcRCPRef12hrs { get; set; }
        public String strPorcRCPAu30hrs { get; set; }
        public String strPorcRCPAu70hrs { get; set; }
        public String strPorcRCPReeAu72hrs { get; set; }
        public String strLeyAuOzTc { get; set; }
        public String strLeyAuREE { get; set; }
        public String strLeyAuGrTm { get; set; }
        public String strFinoAuGr { get; set; }
        public String strPorcRCPAg30hrs { get; set; }
        public String strPorcRCPAg70hrs { get; set; }
        public String strLeyAgOzTc { get; set; }
        public String strLeyAgREE { get; set; }
        public String strPorcCobre { get; set; }
        public String strNaOHKgTM { get; set; }
        public String strNaCNKgTM { get; set; }
        public String strHorasAgitacion { get; set; }
        public String strLeyAuRemuestreo { get; set; }

        public String fltFinoAu { get; set; }
        public String fltFinoAg { get; set; }

        public int? intIdRuma { get; set; }
        public String strNroRuma { get; set; }
        public String strEstadoRuma { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strMesContable { get; set; }
        public String strNroCampania { get; set; }
        public DateTime? dtmFechaPerContable { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
