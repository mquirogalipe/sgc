﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class LeyesModel
    {
        public int? intIdLeyes { get; set; }
        public String strTipoProceso { get; set; }
        public String strItem { get; set; }
        public String strBk { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strTipoMineral { get; set; }
        public String strLote { get; set; }
        public String strPMFino { get; set; }
        public String strPMGrueso { get; set; }
        public String strPesoAuMasAg { get; set; }
        public String strPesoAuFino1 { get; set; }
        public String strPesoAuFino2 { get; set; }
        public String strPesoAuGrueso { get; set; }
        public String strLeyOzTcAuFino { get; set; }
        public String strLeyOzTcAuGrueso { get; set; }
        public String strLeyOzTcAuFinal { get; set; }
        public String strLeyOzTcAgfinal1 { get; set; }
        public String strLeyFinalAu { get; set; }
        public String strLeyFinalAg { get; set; }
        public String strPorcAuFino { get; set; }
        public String strPorcAuGrueso { get; set; }
        public String chrCheckStatus { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
