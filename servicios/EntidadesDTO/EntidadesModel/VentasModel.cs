﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class VentasModel
    {
        public int? intIdVenta { get; set; }
        public String strPro { get; set; }
        public String strPeriodo { get; set; }
        public String strOperacion { get; set; }
        public String strMes { get; set; }
        public String strAnio { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strNroDoc { get; set; }
        public String strImpFac { get; set; }
        public String strNroDocND { get; set; }
        public String strImpND { get; set; }
        public double? fltTotal { get; set; }
        public String strKgVenta { get; set; }
        public String strPenVenta { get; set; }
        public double? fltDolares { get; set; }
        public double? fltKgProducidos { get; set; }
        public double? fltValorUS { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public List<DetVentaModel> lstDetVentas { get; set; }
    }
}
