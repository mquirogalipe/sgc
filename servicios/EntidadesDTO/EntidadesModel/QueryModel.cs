﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class QueryModel
    {
        public String strQuery { get; set; }
        public String strUsuarioCrea { get; set; }
    }
}
