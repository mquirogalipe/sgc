﻿using System;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class BarrasBullonModel
    {
        public int? intIdBarra { get; set; }
        public String strFecha { get; set; }
        public String strNroCampania { get; set; }
        public int? intNroBarra { get; set; }
        public String strNombreBarra { get; set; }
        public double? fltPeso { get; set; }
        public double? fltLeyAu { get; set; }
        public double? fltLeyAg { get; set; }
        public double? fltFinoAu { get; set; }
        public double? fltFinoAg { get; set; }
        public double? fltDFundido { get; set; }
        public double? fltFinoAuMerma { get; set; }
        public double? fltFinoAuOz { get; set; }
        public double? fltFinoAgMerma { get; set; }
        public double? fltFinoAgOz { get; set; }
        public String strInterAu { get; set; }
        public String strInterAg { get; set; }
        public double? fltValorAu { get; set; }
        public double? fltValorAg { get; set; }
        public double? fltSubTotal { get; set; }
        public double? fltComExp { get; set; }
        public double? fltIGV { get; set; }
        public double? fltTotalCom { get; set; }
        public double? fltPagar { get; set; }
        public double? fltFactorF { get; set; }
        public double? fltMerma { get; set; }
        public double? fltFactorFinos { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
