﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class VentasSearchModel
    {
        public String strFechaInicio { get; set; }
		public String strFechaFin { get; set; }
        public String strAnio { get; set; }
        public String strMes { get; set; }
    }
}
