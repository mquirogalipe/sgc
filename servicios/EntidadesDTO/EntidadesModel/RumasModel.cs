﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class RumasModel
    {
        public int? intIdRegistro { get; set; }
        public String strLote { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strProcedenciaSt { get; set; }
		public String strClienteRazonS { get; set; }
        public String strClienteRuc { get; set; }
        public int? intNroSacos { get; set; }
        public String strMaterial { get; set; }
	    public double? fltPesoTmh { get; set; }
        public String strPorcH2O { get; set; }
        public String strPSecoTM { get; set; }
        public double? fltFino { get; set; }
        public int? intCantLotes { get; set; }

        public int? intIdRuma { get; set; }
        public String strNroRuma { get; set; }
        public String strEstadoRuma { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strMesContable { get; set; }
        public DateTime? dtmFechaPerContable { get; set; }
        public String strNroCampania { get; set; }
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
