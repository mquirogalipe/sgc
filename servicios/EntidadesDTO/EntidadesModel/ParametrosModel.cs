﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ParametrosModel
    {
        public int? intParamCom { get; set; }
        public int? intIdParamComProv { get; set; }
        public int intIdZona { get; set; }
        public int intIdProveedor { get; set; }
        public String strZona { get; set; }
        public String strProveedor { get; set; }
        public double? fltRecComMax { get; set; }
        public double? fltMargenMax { get; set; }
        public double? fltMaquilaMin { get; set; }
        public double? fltMaquilaMax { get; set; }
        public double? fltConsumoMax { get; set; }
        public double? fltGastAdmMax { get; set; }
        public double? fltGastLabMax { get; set; }
        public double? fltPorcHumedMax { get; set; }
        public double? fltPesoMax { get; set; }
		public double? fltRecAgMax { get; set; }
		public double? fltLeyAgMax { get; set; }
		public double? fltLeyAgComMax { get; set; }
		public double? fltMargenAgMax { get; set; }
		public double? fltRecAgComMax { get; set; }
        public double? fltRecIntMax { get; set; }
        public double? fltMaquilaIntMax { get; set; }
        public double? fltConsumoIntMax { get; set; }
        public double? fltTmsLiqMax { get; set; }
        public double? fltTmsIntMax { get; set; }
        public String strFactorConv { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
