﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class GastoAdmMesModel
    {
        public String strZona { get; set; }
        public String strAcopiador { get; set; }
        public String strProveedor { get; set; }
        public String strAnio { get; set; }
        public double? enero { get; set; }
        public double? febrero { get; set; }
        public double? marzo { get; set; }
        public double? abril { get; set; }
        public double? mayo { get; set; }
        public double? junio { get; set; }
        public double? julio { get; set; }
        public double? agosto { get; set; }
        public double? septiembre { get; set; }
        public double? octubre { get; set; }
        public double? noviembre { get; set; }
        public double? diciembre { get; set; }
    }
}
