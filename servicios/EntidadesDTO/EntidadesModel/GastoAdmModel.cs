﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class GastoAdmModel
    {
        public int? intIdGastosAdm { get; set; }
        public String strZona { get; set; }
        public String strAcopiador { get; set; }
        public String strProveedor { get; set; }
        public String strRucProveedor { get; set; }
        public double? fltGasto { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strMes { get; set; }
        public String strAnio { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
