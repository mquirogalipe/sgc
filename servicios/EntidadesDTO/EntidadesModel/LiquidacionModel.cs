﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class LiquidacionModel
    {
        public int? intIdRegistro { get; set; }
        public String strLote { get; set; }
        public String strTipoLiquid { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strProcedenciaSt { get; set; }
        public String strClienteRazonS { get; set; }
        public String strClienteRuc { get; set; }
        public int? intNroSacos { get; set; }
        public String strMaterial { get; set; }
        public String strNombreAcopiador { get; set; }
        public double? fltPesoTmh { get; set; }
        public String strPorcH2O { get; set; }
        public String strCheckH2O { get; set; }
        public String strPSecoTM { get; set; }
        public String strLeyAuOzTc { get; set; }
        public String strLeyAuREE { get; set; }
        public String strLeyAuRemuestreo { get; set; }
        public String strLeyAuComercial { get; set; }
        public String strLeyAgOzTc { get; set; }
        public String strLeyAgREE { get; set; }
        public String strLeyAgComercial { get; set; }
        public String strPorcRCPRef12hrs { get; set; }
        public String strNaCNKgTM { get; set; }
        public String strNaOHKgTM { get; set; }
        public String strPorcCobre { get; set; }
        public String strLeyAuLiq { get; set; }
        public String strLeyAgLiq { get; set; }
        public int? intIdLaboratorio { get; set; }
        public String strLeyCertificado { get; set; }
        public String strLeyCertificadoAG { get; set; }
        public String strFechaCertificado { get; set; }
        public String strLeyDirimencia { get; set; }
        public String strLeyDirimenciaAG { get; set; }
        public String strTipoCertificado { get; set; }
        public double? fltFinoAu { get; set; }

        public int? intIdLiquidacion { get; set; }
        public String strLeyAuMargen { get; set; }
        public String strPorcRecup { get; set; }
        public String strPrecioAu { get; set; }
        public String strMargenPI { get; set; }
        public String strMaquila { get; set; }
        public String strConsumoQ { get; set; }
        public String strGastAdm { get; set; }
        public String strRecAg { get; set; }
        public String strPrecioAg { get; set; }
        public String strMargenAg { get; set; }
        public double? fltCostoTotal { get; set; }
        public String strCalculoEspec { get; set; }
        public String strRecIntAu { get; set; }
        public String strMaquilaIntAu { get; set; }
        public String strConsQIntAu { get; set; }
        public String strGastAdmIntAu { get; set; }
        public String strRecIntAg { get; set; }
        public String strPorcImporte { get; set; }
        public String strEstadoLiq { get; set; }
        public String strNroFactura { get; set; }
        public DateTime? dtmFechaFacturacion { get; set; }
        public String strPersonaPropuesta { get; set; }
        public String strPersonaLiquidado { get; set; }
        public DateTime? dtmFechaPropuesta { get; set; }
        public DateTime? dtmFechaLiquidacion { get; set; }
        public String strNBaseAu { get; set; }
        public String strNBaseAg { get; set; }
        public String strCostoAnalisis { get; set; }
        public String strMargenBru { get; set; }
        public String strUtilidadBru { get; set; }

        public String strLoteVolado { get; set; }
        public String strNroExportacion { get; set; }
        public DateTime? dtmFechaExportacion { get; set; }
        public String chrEnableEdit { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
