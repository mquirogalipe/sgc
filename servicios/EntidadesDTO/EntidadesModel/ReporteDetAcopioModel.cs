﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteDetAcopioModel
    {
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strLote { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strZona { get; set; }
        public String strAcopiador { get; set; }
        public String strProveedor { get; set; }
        public String strEstadoLote { get; set; }
        public double? fltPesoTmh { get; set; }
        public double? fltLeyAuOz { get; set; }
        public double? fltLeyAgOz { get; set; }
        public double? fltPSecoTM { get; set; }
        public double? fltFinoAuGr { get; set; }
    }
}
