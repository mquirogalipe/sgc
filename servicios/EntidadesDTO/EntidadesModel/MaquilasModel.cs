﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class MaquilasModel
    {
        public int? intIdMaquila { get; set; }
        public int? intIdZona { get; set; }
        public String strLeyOz { get; set; }
        public double? fltLeyMin { get; set; }
        public double? fltLeyMax { get; set; }
        public double? fltMaquila { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
