﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class LeyesReferencialesModel
    {
        public int? intIdLeyRef { get; set; }
        public String strCorrelativo { get; set; }
        public DateTime? dtmFechaRecepcion { get; set; }
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strDescripcion { get; set; }
        public String strTipoMineral { get; set; }
        public String strZona { get; set; }
        public String strAcopiador { get; set; }
        public String strPruebaMetalurgica { get; set; }
        public String strPorcRcpLab { get; set; }
        public String strLeyAuOzTc { get; set; }
        public String strLeyAuReeOzTc { get; set; }
        public String strConsumo { get; set; }
        public String strLeyAgOzTc { get; set; }
        public String strLeyAuCom { get; set; }
        public String strLeyAgCom { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
