﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteMargenesModel
    {
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strRegion { get; set; }
        public String strZona { get; set; }
        public double? fltPesoTMHSum { get; set; }
        public double? fltMargenBruProm { get; set; }
        public double? fltUtilidadBruSum { get; set; }
        public double? fltPrecioInterAu { get; set; }
        public double? fltRecuperacionProm { get; set; }
        public double? fltMaquilaProm { get; set; }
        public double? fltMargenPIProm { get; set; }
        public double? fltConsumoQProm { get; set; }
        public double? fltGastoAdmProm { get; set; }
        public double? fltRecIntAuProm { get; set; }
        public double? fltMaquilaIntAuProm { get; set; }
        public double? fltConsQIntAuProm { get; set; }
        public double? fltGastoAdmIntAuProm { get; set; }
        public double? fltCostoTotalSum { get; set; }
    }
}
