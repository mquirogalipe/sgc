﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class ReporteAcopioMineralModel
    {
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strAcopiador { get; set; }
        public String strProveedor { get; set; }
        public String strZona { get; set; }
        public String strRegion { get; set; }
        public String strEstadoLote { get; set; }
        public double? fltSumPesoTmh { get; set; }
        public double? fltPromPesoTmh { get; set; }
        public double? fltPromLeyAuOzTc { get; set; }
        public double? fltPromLeyAgOzTc { get; set; }
        public double? fltSumPSecoTM { get; set; }
        public double? fltFinalLeyAuOzTc { get; set; }
        public double? fltFinalLeyAgOzTc { get; set; }
        public double? fltPromFinoAuGr { get; set; }
        public double? fltPromLeyAuComercial { get; set; }
        public int? intNroLotes { get; set; }
    }
}
