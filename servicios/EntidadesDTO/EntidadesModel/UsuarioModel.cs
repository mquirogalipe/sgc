﻿using System;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class UsuarioModel
    {
        public String strCodUsuario { get; set; }
        public String strCodPersona { get; set; }
        public String strUsuario { get; set; }
        public String strPassword { get; set; }
        public String strSuperAdmin { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public String strNombres { get; set; }
        public String strApellidoPat { get; set; }
        public String strApellidoMat { get; set; }
        public int? intCodRol { get; set; }
        public String strCargo { get; set; }
        public String strEmail { get; set; }
        public String strTelefono { get; set; }

        public UsuarioModel()
        {
            this.strCodUsuario = strCodUsuario;
            this.strCodPersona = strCodPersona;
            this.strUsuario = strUsuario;
            this.strPassword = strPassword;
            this.strUsuarioCrea = strUsuarioCrea;
            this.dtmFechaCrea = dtmFechaCrea;
            this.strUsuarioModif = strUsuarioModif;
            this.dtmFechaModif = dtmFechaModif;
            this.chrEstado = chrEstado;
            this.strNombres = strNombres;
            this.strApellidoPat = strApellidoPat;
            this.strApellidoMat = strApellidoMat;
            this.intCodRol = intCodRol;
            this.strCargo = strCargo;
            this.strEmail = strEmail;
            this.strTelefono = strTelefono;
    }
    }
}
