﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class PrecioInterModel
    {
        public int intIdPrecioInter { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strType { get; set; }
        public String strGoldAM { get; set; }
        public String strGoldPM { get; set; }
        public String strSilver { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
