﻿using System;
using Balanza.Entidades;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class UsuarioRolModel
    {
        public String strCodUsuario { get; set; }
        public int? intCodRol { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }

        public UsuarioRolModel()
        {
            this.strCodUsuario = strCodUsuario;
            this.intCodRol = intCodRol;
            this.strUsuarioCrea = strUsuarioCrea;
            this.dtmFechaCrea = dtmFechaCrea;
            this.strUsuarioModif = strUsuarioModif;
            this.dtmFechaModif = dtmFechaModif;
            this.chrEstado = chrEstado;
        }
        public UsuarioRolModel(tblUsuarioRol objUsuarioRol)
        {
            this.strCodUsuario = objUsuarioRol.strCodUsuario;
            this.intCodRol = objUsuarioRol.intCodRol;
            this.strUsuarioCrea = objUsuarioRol.strUsuarioCrea;
            this.dtmFechaCrea = objUsuarioRol.dtmFechaCrea;
            this.strUsuarioModif = objUsuarioRol.strUsuarioModif;
            this.dtmFechaModif = objUsuarioRol.dtmFechaModif;
            this.chrEstado = objUsuarioRol.chrEstado;
        }
    }
}
