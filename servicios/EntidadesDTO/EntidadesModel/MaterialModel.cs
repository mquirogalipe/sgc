﻿using System;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class MaterialModel
    {
        public int? intIdMaterial { get; set; }
        public String strDescripcion { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
