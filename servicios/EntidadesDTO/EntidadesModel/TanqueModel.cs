﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class TanqueModel
    {
        public int? intIdTanque { get; set; }
        public DateTime? dtmFecha { get; set; }
        public String strNroTanque { get; set; }
        public String strCarbonSeco { get; set; }
        public String strGrAuKgC { get; set; }
        public String strGrAgKgC { get; set; }
        public String strDensPulpa { get; set; }
        public String strVolPulpa { get; set; }
        public String strGAum3 { get; set; }
        public String strGAut { get; set; }
        public String strGAgm3 { get; set; }
        public String strGAgt { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
