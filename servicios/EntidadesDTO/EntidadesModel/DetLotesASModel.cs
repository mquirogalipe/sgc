﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class DetLotesASModel
    {
        public int? intIdCorrelativoDet { get; set; }
        public int? intIdLoteAs { get; set; }
        public int? intIdRegistro { get; set; }
        public String strFechaInicio { get; set; }
        public String strFechaFin { get; set; }
        public String strNroAS { get; set; }
        public String strLote { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
