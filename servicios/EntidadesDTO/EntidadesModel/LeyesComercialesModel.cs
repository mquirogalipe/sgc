﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class LeyesComercialesModel
    {
        public int? intIdValue { get; set; }
        public int? intIdZona { get; set; }
        public String strDescripcion { get; set; }
        public double? fltValorAuMenor { get; set; }
        public double? fltPuntoAu { get; set; }
        public double? fltValorAuMayor { get; set; }
        public double? fltPuntoAuInter { get; set; }
        public double? fltValueAuInter { get; set; }
        public double? fltPuntoAuMax { get;set; }
        public double? fltValorAuMax { get; set; }
        public double? fltValorAg { get; set; }
        public String strZona { get; set; }
        public String strProcedencia { get; set; }
        public String strRegion { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
    }
}
