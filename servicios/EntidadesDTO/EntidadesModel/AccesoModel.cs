﻿using System;
using Balanza.Entidades;

namespace Balanza.EntidadesDTO.EntidadesModel
{
    public class AccesoModel
    {
        public int?intCodAcceso { get; set; }
        public String strNombre { get; set; }
        public String strDescripcion { get; set; }
        public String strEnlace { get; set; }
        public int? intNivel { get; set; }
        public int? intSubNivel { get; set; }
        public String strIndex { get; set; }
        public int? intPadre { get; set; }
        public String strClickName { get; set; }
        public String strIconName { get; set; }
        public String strTipoAcceso { get; set; }
        public String strUsuarioCrea { get; set; }
        public DateTime? dtmFechaCrea { get; set; }
        public String strUsuarioModif { get; set; }
        public DateTime? dtmFechaModif { get; set; }
        public String chrEstado { get; set; }
        public AccesoModel()
        {
            this.intCodAcceso = intCodAcceso;
            this.strNombre = strNombre;
            this.strDescripcion = strDescripcion;
            this.strEnlace = strEnlace;
            this.intNivel = intNivel;
            this.intSubNivel = intSubNivel;
            this.strIndex = strIndex;
            this.intPadre = intPadre;
            this.strClickName = strClickName;
            this.strIconName = strIconName;
            this.strTipoAcceso = "lectura";
            this.strUsuarioCrea = strUsuarioCrea;
            this.dtmFechaCrea = dtmFechaCrea;
            this.strUsuarioModif = strUsuarioModif;
            this.dtmFechaModif = dtmFechaModif;
            this.chrEstado = chrEstado;
        }
        public AccesoModel(tblAcceso objAcceso)
        {
            this.intCodAcceso = objAcceso.intCodAcceso;
            this.strNombre = objAcceso.strNombre;
            this.strDescripcion = objAcceso.strDescripcion;
            this.strEnlace = objAcceso.strEnlace;
            this.intNivel = objAcceso.intNivel;
            this.intSubNivel = objAcceso.intSubNivel;
            this.strIndex = objAcceso.strIndex;
            this.intPadre = objAcceso.intPadre;
            this.strClickName = objAcceso.strClickName;
            this.strIconName = objAcceso.strIconName;
            this.strUsuarioCrea = objAcceso.strUsuarioCrea;
            this.dtmFechaCrea = objAcceso.dtmFechaCrea;
            this.strUsuarioModif = objAcceso.strUsuarioModif;
            this.dtmFechaModif = objAcceso.dtmFechaModif;
            this.chrEstado = objAcceso.chrEstado;
        }
    }
}
