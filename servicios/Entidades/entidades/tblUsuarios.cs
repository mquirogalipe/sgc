namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblUsuarios : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblUsuarios()
        {
            tblUsuarioRol = new HashSet<tblUsuarioRol>();
        }

        [Key]
        [StringLength(20)]
        public string strCodUsuario { get; set; }

        [Required]
        [StringLength(20)]
        public string strCodPersona { get; set; }

        [StringLength(35)]
        public string strUsuario { get; set; }

        [StringLength(50)]
        public string strPassword { get; set; }

        [StringLength(10)]
        public string strSuperAdmin { get; set; }

        public virtual tblPersona tblPersona { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblUsuarioRol> tblUsuarioRol { get; set; }
    }
}
