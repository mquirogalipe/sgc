namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblRumas : tblEntidadBase
    {
        [Key]
        public int intIdRuma { get; set; }

        public int? intIdRegistro { get; set; }

        [StringLength(20)]
        public string strLote { get; set; }

        [StringLength(30)]
        public string strNroRuma { get; set; }

        public DateTime? dtmFecha { get; set; }

        [StringLength(30)]
        public string strMesContable { get; set; }

        [StringLength(20)]
        public string strEstadoRuma { get; set; }

        [StringLength(30)]
        public string strNroCampania { get; set; }
    }
}
