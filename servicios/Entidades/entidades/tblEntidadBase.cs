﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Balanza.Entidades
{
    public class tblEntidadBase
    {
        [StringLength(20)]
        public string strUsuarioCrea { get; set; }

        public DateTime? dtmFechaCrea { get; set; }

        [StringLength(20)]
        public string strUsuarioModif { get; set; }

        public DateTime? dtmFechaModif { get; set; }

        [StringLength(1)]
        public string chrEstado { get; set; }
    }
}
