namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblZona")]
    public partial class tblZona : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblZona()
        {
            tblCuota = new HashSet<tblCuota>();
            tblLeyesComerciales = new HashSet<tblLeyesComerciales>();
            tblMaquilas = new HashSet<tblMaquilas>();
            tblParametrosCom = new HashSet<tblParametrosCom>();
        }

        [Key]
        public int intIdZona { get; set; }

        [StringLength(10)]
        public string strCodZona { get; set; }

        [StringLength(200)]
        public string strZona { get; set; }

        [StringLength(200)]
        public string strProcedencia { get; set; }

        [StringLength(50)]
        public string strRegion { get; set; }

        [StringLength(10)]
        public string strCalculoEspecial { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCuota> tblCuota { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblLeyesComerciales> tblLeyesComerciales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblMaquilas> tblMaquilas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblParametrosCom> tblParametrosCom { get; set; }
    }
}
