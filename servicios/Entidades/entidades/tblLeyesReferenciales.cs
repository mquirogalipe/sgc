namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblLeyesReferenciales : tblEntidadBase
    {
        [Key]
        public int intIdLeyRef { get; set; }

        [StringLength(20)]
        public string strCorrelativo { get; set; }

        public DateTime? dtmFechaRecepcion { get; set; }

        [StringLength(60)]
        public string strDescripcion { get; set; }

        [StringLength(30)]
        public string strTipoMineral { get; set; }

        [StringLength(50)]
        public string strZona { get; set; }

        [StringLength(50)]
        public string strAcopiador { get; set; }

        [StringLength(30)]
        public string strPruebaMetalurgica { get; set; }

        [StringLength(30)]
        public string strPorcRcpLab { get; set; }

        [StringLength(30)]
        public string strLeyAuOzTc { get; set; }

        [StringLength(30)]
        public string strLeyAuReeOzTc { get; set; }

        [StringLength(30)]
        public string strConsumo { get; set; }

        [StringLength(30)]
        public string strLeyAgOzTc { get; set; }

        [StringLength(30)]
        public string strLeyAuCom { get; set; }

        [StringLength(30)]
        public string strLeyAgCom { get; set; }
    }
}
