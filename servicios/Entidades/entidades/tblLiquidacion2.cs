namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblLiquidacion2 : tblEntidadBase
    {
        [Key]
        public int intCodLiquidacion { get; set; }

        [Required]
        [StringLength(20)]
        public string strLote { get; set; }

        [StringLength(20)]
        public string strCodLiquidacion { get; set; }

        public DateTime? dtmProAuFecRecepcion { get; set; }

        public DateTime? dtmProAuFecLiquidacion { get; set; }

        [StringLength(50)]
        public string strProAuZona { get; set; }

        [StringLength(50)]
        public string strProAuAcopiador { get; set; }

        public double? fltProAuTmh { get; set; }

        public double? fltProAuPorcH2O { get; set; }

        public double? fltProAuTms { get; set; }

        public double? fltProAuRec { get; set; }

        public double? fltProAuLeyOzTc { get; set; }

        public double? fltProAuInter { get; set; }

        public double? fltProAuMargen { get; set; }

        public double? fltProAuMaquila { get; set; }

        public double? fltProAuConsQ { get; set; }

        public double? fltProAuGastAdm { get; set; }

        public double? fltProAuPUTms { get; set; }

        public double? fltProAuImporte { get; set; }

        public double? fltProAuIGV { get; set; }

        public double? fltProAuTotal { get; set; }

        public double? fltLiqAuTmh { get; set; }

        public double? fltLiqAuPorcH2O { get; set; }

        public double? fltfltLiqAuTms { get; set; }

        public double? fltLiqAuRec { get; set; }

        public double? fltLiqAuLeyOzTc { get; set; }

        public double? fltLiqAuInter { get; set; }

        public double? fltLiqAuMaquila { get; set; }

        public double? fltLiqAuConsQ { get; set; }

        public double? fltLiqAuGastAdm { get; set; }

        public double? fltLiqAuPUTms { get; set; }

        public double? fltLiqAuImporte { get; set; }

        public double? fltVenAuTmh { get; set; }

        public double? fltVenAuPorcH2O { get; set; }

        public double? fltVenAuTms { get; set; }

        public double? fltVenAuRec { get; set; }

        public double? fltVenAuLeyOzTc { get; set; }

        public double? fltVenAuInter { get; set; }

        public double? fltVenAuPUTms { get; set; }

        public double? fltVenAuImporte { get; set; }

        public double? fltProAgTmh { get; set; }

        public double? fltProAgPorcH2O { get; set; }

        public double? fltProAgTms { get; set; }

        public double? fltProAgRec { get; set; }

        public double? fltProAgLeyOzTc { get; set; }

        public double? fltProAgInter { get; set; }

        public double? fltProAgMargen { get; set; }

        public double? fltProAgPUTms { get; set; }

        public double? fltProAgImporte { get; set; }

        public double? fltProAgIGV { get; set; }

        public double? fltProAgTotal { get; set; }

        public double? fltLiqAgTmh { get; set; }

        public double? fltLiqAgTPorcH2O { get; set; }

        public double? fltLiqAgTms { get; set; }

        public double? fltLiqAgRec { get; set; }

        public double? fltLiqAgLeyOzTc { get; set; }

        public double? fltLiqAgLeyOzTc2 { get; set; }

        public double? fltLiqAgInter { get; set; }

        public double? fltLiqAgPUTms { get; set; }

        public double? fltLiqAgImporte { get; set; }

        public double? fltAuMargenBruto { get; set; }

        public double? fltAuUtilidadBruta { get; set; }

        public double? fltAgMargenBruto { get; set; }

        public double? fltAgUtilidadBruta { get; set; }

        public double? fltUtLoteMargenCompra { get; set; }

        public double? fltUtLoteMargenBruto { get; set; }

        public double? fltUtLoteUtilidadBruta { get; set; }

        public double? fltOtro { get; set; }
    }
}
