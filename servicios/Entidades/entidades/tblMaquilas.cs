namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblMaquilas: tblEntidadBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intIdMaquila { get; set; }

        public int? intIdZona { get; set; }

        public double? fltLeyMin { get; set; }

        public double? fltLeyMax { get; set; }

        public double? fltMaquila { get; set; }

        public virtual tblZona tblZona { get; set; }
    }
}
