namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblUsuarioRol")]
    public partial class tblUsuarioRol : tblEntidadBase
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string strCodUsuario { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intCodRol { get; set; }

        public virtual tblRol tblRol { get; set; }

        public virtual tblUsuarios tblUsuarios { get; set; }
    }
}
