namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblLiquidacion")]
    public partial class tblLiquidacion : tblEntidadBase
    {
        [Key]
        public int intIdLiquidacion { get; set; }

        public int intIdRegistro { get; set; }

        [StringLength(20)]
        public string strLote { get; set; }

        public DateTime? dtmFechaLiquidacion { get; set; }

        [StringLength(30)]
        public string strLeyAuLiq { get; set; }

        [StringLength(30)]
        public string strLeyAgLiq { get; set; }

        [StringLength(20)]
        public string strLeyAuMargen { get; set; }

        [StringLength(20)]
        public string strPorcRecup { get; set; }

        [StringLength(20)]
        public string strPrecioAu { get; set; }

        [StringLength(20)]
        public string strMargenPI { get; set; }

        [StringLength(20)]
        public string strMaquila { get; set; }

        [StringLength(20)]
        public string strConsumoQ { get; set; }

        [StringLength(20)]
        public string strGastAdm { get; set; }

        [StringLength(20)]
        public string strRecAg { get; set; }

        [StringLength(20)]
        public string strPrecioAg { get; set; }

        [StringLength(20)]
        public string strMargenAg { get; set; }

        public double? fltCostoTotal { get; set; }

        [StringLength(30)]
        public string strRecIntAu { get; set; }

        [StringLength(30)]
        public string strMaquilaIntAu { get; set; }

        [StringLength(30)]
        public string strConsQIntAu { get; set; }

        [StringLength(30)]
        public string strGastAdmIntAu { get; set; }

        [StringLength(30)]
        public string strRecIntAg { get; set; }

        [StringLength(20)]
        public string strPorcImporte { get; set; }

        [StringLength(30)]
        public string strEstadoLiq { get; set; }

        [StringLength(20)]
        public string strNroFactura { get; set; }

        public DateTime? dtmFechaFacturacion { get; set; }

        [StringLength(100)]
        public string strPersonaPropuesta { get; set; }

        [StringLength(100)]
        public string strPersonaLiquidado { get; set; }

        public DateTime? dtmFechaPropuesta { get; set; }

        public double? fltNBaseAu { get; set; }

        public double? fltNBaseAg { get; set; }

        [StringLength(20)]
        public string strEstadoReintegro { get; set; }

        [StringLength(30)]
        public string strLeyAuReintegro { get; set; }

        [StringLength(30)]
        public string strLeyAgReintegro { get; set; }

        [StringLength(20)]
        public string strMargenPIRei { get; set; }

        [StringLength(20)]
        public string strMaquilaRei { get; set; }

        [StringLength(20)]
        public string strConsumoQRei { get; set; }

        [StringLength(20)]
        public string strGastAdmRei { get; set; }

        [StringLength(20)]
        public string strMargenAgRei { get; set; }

        public DateTime? dtmFechaReintegro { get; set; }

        [StringLength(100)]
        public string strPersonaReintegro { get; set; }

        [StringLength(30)]
        public string strNotaDebito { get; set; }

        [StringLength(30)]
        public string strNroExportacion { get; set; }

        public DateTime? dtmFechaExportacion { get; set; }

        public virtual tblRegistro tblRegistro { get; set; }
    }
}
