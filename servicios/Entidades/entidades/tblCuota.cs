namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblCuota")]
    public partial class tblCuota : tblEntidadBase
    {
        [Key]
        public int intIdCuota { get; set; }

        public int intIdAcopiador { get; set; }

        public int intIdZona { get; set; }

        public double? fltCuota { get; set; }

        [StringLength(30)]
        public string strLeyAu { get; set; }

        [StringLength(30)]
        public string strFinoAu { get; set; }

        [StringLength(50)]
        public string strUnidadMedida { get; set; }

        [StringLength(10)]
        public string strAnio { get; set; }

        [StringLength(20)]
        public string strMes { get; set; }

        public virtual tblAcopiador tblAcopiador { get; set; }

        public virtual tblZona tblZona { get; set; }
    }
}
