namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblAcceso")]
    public partial class tblAcceso : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblAcceso()
        {
            tblRolAcceso = new HashSet<tblRolAcceso>();
        }

        [Key]
        public int intCodAcceso { get; set; }

        [StringLength(50)]
        public string strNombre { get; set; }

        [StringLength(200)]
        public string strDescripcion { get; set; }

        [StringLength(150)]
        public string strEnlace { get; set; }

        public int? intNivel { get; set; }

        public int? intSubNivel { get; set; }

        [StringLength(10)]
        public string strIndex { get; set; }

        public int? intPadre { get; set; }

        [StringLength(40)]
        public string strClickName { get; set; }

        [StringLength(50)]
        public string strIconName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblRolAcceso> tblRolAcceso { get; set; }
    }
}
