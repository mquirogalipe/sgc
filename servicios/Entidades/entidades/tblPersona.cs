namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblPersona")]
    public partial class tblPersona : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblPersona()
        {
            tblUsuarios = new HashSet<tblUsuarios>();
        }

        [Key]
        [StringLength(20)]
        public string strCodPersona { get; set; }

        [StringLength(60)]
        public string strNombres { get; set; }

        [StringLength(50)]
        public string strApellidoPat { get; set; }

        [StringLength(50)]
        public string strApellidoMat { get; set; }

        [StringLength(30)]
        public string strCargo { get; set; }

        [StringLength(100)]
        public string strDireccion { get; set; }

        [StringLength(100)]
        public string strEmail { get; set; }

        [StringLength(20)]
        public string strSexo { get; set; }

        [StringLength(10)]
        public string strDni { get; set; }

        public DateTime? dtmFecNacimiento { get; set; }

        [StringLength(30)]
        public string strTelefono { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblUsuarios> tblUsuarios { get; set; }
    }
}
