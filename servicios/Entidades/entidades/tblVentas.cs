namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblVentas : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblVentas()
        {
            tblDetVenta = new HashSet<tblDetVenta>();
        }

        [Key]
        public int intIdVenta { get; set; }

        [StringLength(15)]
        public string strPro { get; set; }

        [StringLength(30)]
        public string strPeriodo { get; set; }

        [StringLength(30)]
        public string strOperacion { get; set; }

        [StringLength(20)]
        public string strMes { get; set; }

        [StringLength(10)]
        public string strAnio { get; set; }

        public DateTime? dtmFecha { get; set; }

        [StringLength(30)]
        public string strNroDoc { get; set; }

        [StringLength(30)]
        public string strImpFac { get; set; }

        [StringLength(30)]
        public string strNroDocND { get; set; }

        [StringLength(30)]
        public string strImpND { get; set; }

        public double? fltTotal { get; set; }

        [StringLength(30)]
        public string strKgVenta { get; set; }

        [StringLength(20)]
        public string strPenVenta { get; set; }

        public double? fltDolares { get; set; }

        public double? fltKgProducidos { get; set; }

        public double? fltValorUS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDetVenta> tblDetVenta { get; set; }
    }
}
