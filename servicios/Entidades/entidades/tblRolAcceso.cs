namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblRolAcceso")]
    public partial class tblRolAcceso : tblEntidadBase
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intCodRol { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intCodAcceso { get; set; }

        [StringLength(30)]
        public string strTipoAcceso { get; set; }

        public virtual tblAcceso tblAcceso { get; set; }

        public virtual tblRol tblRol { get; set; }
    }
}
