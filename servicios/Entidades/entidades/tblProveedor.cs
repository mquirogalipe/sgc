namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblProveedor")]
    public partial class tblProveedor : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblProveedor()
        {
            tblParametrosComProv = new HashSet<tblParametrosComProv>();
        }

        [Key]
        public int intIdProveedor { get; set; }

        [StringLength(20)]
        public string strRuc { get; set; }

        [StringLength(100)]
        public string strRazonS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblParametrosComProv> tblParametrosComProv { get; set; }
    }
}
