namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblDetVenta")]
    public partial class tblDetVenta : tblEntidadBase
    {
        [Key]
        [Column(Order = 0)]
        public int intIdDetVenta { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intIdVenta { get; set; }

        [StringLength(10)]
        public string strPro { get; set; }

        [StringLength(30)]
        public string strPeriodo { get; set; }

        [StringLength(30)]
        public string strOperacion { get; set; }

        public double? fltPeso { get; set; }

        [StringLength(30)]
        public string strLeyOro { get; set; }

        [StringLength(30)]
        public string strGrOro { get; set; }

        public double? fltKlgOro { get; set; }

        public virtual tblVentas tblVentas { get; set; }
    }
}
