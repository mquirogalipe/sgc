namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblTanques : tblEntidadBase
    {
        [Key]
        public int intIdTanque { get; set; }

        public DateTime? dtmFecha { get; set; }

        [StringLength(20)]
        public string strNroTanque { get; set; }

        [StringLength(30)]
        public string strCarbonSeco { get; set; }

        [StringLength(30)]
        public string strGrAuKgC { get; set; }

        [StringLength(30)]
        public string strGrAgKgC { get; set; }

        [StringLength(30)]
        public string strDensPulpa { get; set; }

        [StringLength(30)]
        public string strVolPulpa { get; set; }

        [StringLength(30)]
        public string strGAum3 { get; set; }

        [StringLength(30)]
        public string strGAut { get; set; }

        [StringLength(30)]
        public string strGAgm3 { get; set; }

        [StringLength(30)]
        public string strGAgt { get; set; }
    }
}
