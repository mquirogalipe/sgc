namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblAcopiador")]
    public partial class tblAcopiador : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblAcopiador()
        {
            tblCuota = new HashSet<tblCuota>();
        }

        [Key]
        public int intIdAcopiador { get; set; }

        [Required]
        [StringLength(10)]
        public string strDni { get; set; }

        [StringLength(20)]
        public string strRuc { get; set; }

        [StringLength(60)]
        public string strNombres { get; set; }

        [StringLength(60)]
        public string strApellidos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCuota> tblCuota { get; set; }
    }
}
