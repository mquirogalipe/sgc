namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblRecuperacion")]
    public partial class tblRecuperacion : tblEntidadBase
    {
        [Key]
        public int intIdRecuperacion { get; set; }

        [StringLength(20)]
        public string strLote { get; set; }

        [StringLength(20)]
        public string strTipoProceso { get; set; }

        [StringLength(20)]
        public string strOroPpm2 { get; set; }

        [StringLength(20)]
        public string strOroPpm4 { get; set; }

        [StringLength(20)]
        public string strOroPpm8 { get; set; }

        [StringLength(20)]
        public string strOroPpm12 { get; set; }

        [StringLength(20)]
        public string strOroPpm24 { get; set; }

        [StringLength(20)]
        public string strOroPpm48 { get; set; }

        [StringLength(20)]
        public string strOroPpm72 { get; set; }

        [StringLength(20)]
        public string strOroPpm90 { get; set; }

        [StringLength(20)]
        public string strPlataPpm2 { get; set; }

        [StringLength(20)]
        public string strPlataPpm4 { get; set; }

        [StringLength(20)]
        public string strPlataPpm8 { get; set; }

        [StringLength(20)]
        public string strPlataPpm12 { get; set; }

        [StringLength(20)]
        public string strPlataPpm24 { get; set; }

        [StringLength(20)]
        public string strPlataPpm48 { get; set; }

        [StringLength(20)]
        public string strPlataPpm72 { get; set; }

        [StringLength(20)]
        public string strPlataPpm90 { get; set; }

        [StringLength(20)]
        public string strLeyAuRipio { get; set; }

        [StringLength(20)]
        public string strLeyAgRipio { get; set; }

        [StringLength(20)]
        public string strCobrePpm48 { get; set; }

        [StringLength(20)]
        public string strCobrePpm72 { get; set; }

        [StringLength(20)]
        public string strCobrePpm90 { get; set; }
    }
}
