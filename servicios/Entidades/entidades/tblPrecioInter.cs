namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblPrecioInter")]
    public partial class tblPrecioInter : tblEntidadBase
    {
        [Key]
        public int intIdPrecioInter { get; set; }

        public DateTime? dtmFecha { get; set; }

        [StringLength(50)]
        public string strGoldAM { get; set; }

        [StringLength(50)]
        public string strGoldPM { get; set; }

        [StringLength(50)]
        public string strSilver { get; set; }
    }
}
