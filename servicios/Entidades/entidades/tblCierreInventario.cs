namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblCierreInventario")]
    public partial class tblCierreInventario : tblEntidadBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intIdCierreInv { get; set; }

        public double? fltPesoTmh { get; set; }

        public double? fltPesoSecoTM { get; set; }

        [StringLength(100)]
        public string strRumasDesc { get; set; }

        [StringLength(2000)]
        public string strLotesDesc { get; set; }
    }
}
