namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblMaterial")]
    public partial class tblMaterial : tblEntidadBase
    {
        [Key]
        public int intIdMaterial { get; set; }

        [StringLength(40)]
        public string strDescripcion { get; set; }
    }
}
