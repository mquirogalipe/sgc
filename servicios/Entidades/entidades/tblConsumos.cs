namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblConsumos : tblEntidadBase
    {
        [Key]
        public int intIdConsumo { get; set; }

        [StringLength(20)]
        public string strLote { get; set; }

        [StringLength(20)]
        public string strGeP1 { get; set; }

        [StringLength(20)]
        public string strGeP2 { get; set; }

        [StringLength(20)]
        public string strGeP3 { get; set; }

        [StringLength(20)]
        public string strGeP4 { get; set; }

        [StringLength(20)]
        public string strGeEspecif2 { get; set; }

        [StringLength(20)]
        public string strPlDensidad { get; set; }

        [StringLength(20)]
        public string strPlPorcMalla { get; set; }

        [StringLength(20)]
        public string strPlPeso { get; set; }

        [StringLength(20)]
        public string str0HrsLixCN { get; set; }

        [StringLength(20)]
        public string str2HrsLixCN { get; set; }

        [StringLength(20)]
        public string str4HrsLixCN { get; set; }

        [StringLength(20)]
        public string str8HrsLixCN { get; set; }

        [StringLength(20)]
        public string str12HrsLixCN { get; set; }

        [StringLength(20)]
        public string str24HrsLixCN { get; set; }

        [StringLength(20)]
        public string str48HrsLixCN { get; set; }

        [StringLength(20)]
        public string str72HrsLixCN { get; set; }

        [StringLength(20)]
        public string str0HrsLixPH { get; set; }

        [StringLength(20)]
        public string str2HrsLixPH { get; set; }

        [StringLength(20)]
        public string str4HrsLixPH { get; set; }

        [StringLength(20)]
        public string str8HrsLixPH { get; set; }

        [StringLength(20)]
        public string str12HrsLixPH { get; set; }

        [StringLength(20)]
        public string str24HrsLixPH { get; set; }

        [StringLength(20)]
        public string str48HrsLixPH { get; set; }

        [StringLength(20)]
        public string str72HrsLixPH { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH0 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH2 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH4 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH8 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH12 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH24 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH48 { get; set; }

        [StringLength(20)]
        public string strHrsLixReactOH72 { get; set; }

        [StringLength(20)]
        public string strVolDes2 { get; set; }

        [StringLength(20)]
        public string strVolDes4 { get; set; }

        [StringLength(20)]
        public string strVolDes8 { get; set; }

        [StringLength(20)]
        public string strVolDes12 { get; set; }

        [StringLength(20)]
        public string strVolDes24 { get; set; }

        [StringLength(20)]
        public string strVolDes48 { get; set; }

        [StringLength(20)]
        public string strVolDes72 { get; set; }
    }
}
