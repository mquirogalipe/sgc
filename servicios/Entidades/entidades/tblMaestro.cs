namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblMaestro")]
    public partial class tblMaestro : tblEntidadBase
    {
        [Key]
        public int intIdMaestro { get; set; }

        [StringLength(50)]
        public string strNombre { get; set; }

        [StringLength(100)]
        public string strDescripcion { get; set; }

        [StringLength(100)]
        public string strValor { get; set; }
    }
}
