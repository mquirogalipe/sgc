namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblParametrosComProv")]
    public partial class tblParametrosComProv : tblEntidadBase
    {
        [Key]
        public int intIdParamComProv { get; set; }

        public int intIdProveedor { get; set; }

        public double? fltRecComMin { get; set; }

        public double? fltRecComMax { get; set; }

        public double? fltMargenMin { get; set; }

        public double? fltMargenMax { get; set; }

        public double? fltMaquilaMin { get; set; }

        public double? fltMaquilaMax { get; set; }

        public double? fltConsumoMin { get; set; }

        public double? fltConsumoMax { get; set; }

        public double? fltGastAdmMin { get; set; }

        public double? fltGastAdmMax { get; set; }

        public double? fltGastLabMin { get; set; }

        public double? fltGastLabMax { get; set; }

        public double? fltPorcHumedMin { get; set; }

        public double? fltPorcHumedMax { get; set; }

        public double? fltPesoMin { get; set; }

        public double? fltPesoMax { get; set; }

        public double? fltRecAgMin { get; set; }

        public double? fltRecAgMax { get; set; }

        public double? fltPrecioAgMin { get; set; }

        public double? fltPrecioAgMax { get; set; }

        public double? fltLeyAgMin { get; set; }

        public double? fltLeyAgMax { get; set; }

        public double? fltLeyAgComMin { get; set; }

        public double? fltLeyAgComMax { get; set; }

        public double? fltMargenAgMin { get; set; }

        public double? fltMargenAgMax { get; set; }

        public double? fltRecAgComMin { get; set; }

        public double? fltRecAgComMax { get; set; }

        public double? fltGastAdmIntMin { get; set; }

        public double? fltGastAdmIntMax { get; set; }

        public double? fltRecIntMin { get; set; }

        public double? fltRecIntMax { get; set; }

        public double? fltMaquilaIntMin { get; set; }

        public double? fltMaquilaIntMax { get; set; }

        public double? fltConsumoIntMin { get; set; }

        public double? fltConsumoIntMax { get; set; }

        public double? fltPrecioInterMin { get; set; }

        public double? fltPrecioInterMax { get; set; }

        public double? fltTmsLiqMin { get; set; }

        public double? fltTmsLiqMax { get; set; }

        public double? fltTmsIntMin { get; set; }

        public double? fltTmsIntMax { get; set; }

        public virtual tblProveedor tblProveedor { get; set; }
    }
}
