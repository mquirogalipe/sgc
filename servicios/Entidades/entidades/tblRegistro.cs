namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblRegistro")]
    public partial class tblRegistro : tblEntidadBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblRegistro()
        {
            tblLiquidacion = new HashSet<tblLiquidacion>();
        }

        [Key]
        public int intIdRegistro { get; set; }

        [Required]
        [StringLength(20)]
        public string strLote { get; set; }

        [StringLength(20)]
        public string strSubLote { get; set; }

        public DateTime dtmFechaRecepcion { get; set; }

        [StringLength(30)]
        public string strMes { get; set; }

        [StringLength(200)]
        public string strProcedenciaLg { get; set; }

        [StringLength(60)]
        public string strProcedenciaSt { get; set; }

        [StringLength(40)]
        public string strRegion { get; set; }

        [StringLength(20)]
        public string strClienteRuc { get; set; }

        [StringLength(100)]
        public string strClienteRazonS { get; set; }

        [StringLength(40)]
        public string strCodConcesion { get; set; }

        [StringLength(50)]
        public string strNombreConcesion { get; set; }

        [StringLength(50)]
        public string strGuiaRemitente { get; set; }

        [StringLength(40)]
        public string strCodCompromiso { get; set; }

        [StringLength(40)]
        public string strMaterial { get; set; }

        public int? intNroSacos { get; set; }

        public double? fltPesoTmh { get; set; }

        public double? fltPesoTmhHistorico { get; set; }

        [StringLength(20)]
        public string strHoraIngreso { get; set; }

        [StringLength(20)]
        public string strHoraSalida { get; set; }

        [StringLength(20)]
        public string strTiempoDescarga { get; set; }

        [StringLength(100)]
        public string strDuenioMineral { get; set; }

        [StringLength(30)]
        public string strFechaMuestreo { get; set; }

        [StringLength(20)]
        public string strTurnoMuestreo { get; set; }

        [StringLength(30)]
        public string strFechaRetiro { get; set; }

        [StringLength(100)]
        public string strNombreTransportista { get; set; }

        [StringLength(20)]
        public string strNroPlaca { get; set; }

        [StringLength(20)]
        public string strGuiaTransport { get; set; }

        [StringLength(20)]
        public string strDniAcopiador { get; set; }

        [StringLength(200)]
        public string strNombreAcopiador { get; set; }

        [StringLength(20)]
        public string strTurnoRegistro { get; set; }

        [StringLength(200)]
        public string strObservacion { get; set; }

        [StringLength(20)]
        public string strPorcH2O { get; set; }

        [StringLength(20)]
        public string strCheckH2O { get; set; }

        [StringLength(30)]
        public string strPSecoTM { get; set; }

        public DateTime? dtmFechaHoraHumedad { get; set; }

        public DateTime? dtmFechaHoraMuestreo { get; set; }

        [StringLength(30)]
        public string strPorcRCPRef12hrs { get; set; }

        [StringLength(30)]
        public string strPorcRCPAu30hrs { get; set; }

        [StringLength(30)]
        public string strPorcRCPAu70hrs { get; set; }

        [StringLength(30)]
        public string strPorcRCPReeAu72hrs { get; set; }

        [StringLength(30)]
        public string strLeyAuOzTc { get; set; }

        [StringLength(30)]
        public string strLeyAuREE { get; set; }

        [StringLength(30)]
        public string strLeyAuGrTm { get; set; }

        [StringLength(30)]
        public string strFinoAuGr { get; set; }

        [StringLength(30)]
        public string strPorcRCPAg30hrs { get; set; }

        [StringLength(30)]
        public string strPorcRCPAg70hrs { get; set; }

        [StringLength(30)]
        public string strLeyAgOzTc { get; set; }

        [StringLength(30)]
        public string strLeyAgREE { get; set; }

        [StringLength(30)]
        public string strPorcCobre { get; set; }

        [StringLength(30)]
        public string strNaOHKgTM { get; set; }

        [StringLength(30)]
        public string strNaCNKgTM { get; set; }

        [StringLength(30)]
        public string strHorasAgitacion { get; set; }

        [StringLength(30)]
        public string strPenalidad { get; set; }

        [StringLength(30)]
        public string strLeyAuComercial { get; set; }

        [StringLength(30)]
        public string strLeyAgComercial { get; set; }

        [StringLength(30)]
        public string strLeyCertificado { get; set; }

        [StringLength(30)]
        public string strLeyDirimencia { get; set; }

        [StringLength(30)]
        public string strLeyAuHistorico { get; set; }

        [StringLength(30)]
        public string strLeyAgHistorico { get; set; }

        [StringLength(20)]
        public string strEstadoLote { get; set; }

        [StringLength(50)]
        public string strRolModif { get; set; }

        [StringLength(200)]
        public string strMotivoModif { get; set; }

        [StringLength(1)]
        public string chrEstadoModifBal { get; set; }

        [StringLength(1)]
        public string chrEstadoModifMet { get; set; }

        [StringLength(1)]
        public string chrEstadoModifLab { get; set; }

        [StringLength(1)]
        public string chrEstadoModifCom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblLiquidacion> tblLiquidacion { get; set; }
    }
}
