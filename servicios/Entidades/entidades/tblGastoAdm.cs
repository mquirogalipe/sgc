namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblGastoAdm")]
    public partial class tblGastoAdm : tblEntidadBase
    {
        [Key]
        public int intIdGastosAdm { get; set; }

        [StringLength(50)]
        public string strZona { get; set; }

        [StringLength(100)]
        public string strProveedor { get; set; }

        public double? fltGasto { get; set; }

        public DateTime? dtmFecha { get; set; }

        [StringLength(30)]
        public string strMes { get; set; }

        [StringLength(4)]
        public string strAnio { get; set; }
    }
}
