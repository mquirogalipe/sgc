namespace Balanza.Entidades
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblLeyesComerciales : tblEntidadBase
    {
        [Key]
        public int intIdValue { get; set; }

        public int? intIdZona { get; set; }

        [StringLength(50)]
        public string strDescripcion { get; set; }

        public double? fltPuntoAu { get; set; }

        public double? fltValorAuMenor { get; set; }

        public double? fltValorAuMayor { get; set; }

        public double? fltValorAg { get; set; }

        public virtual tblZona tblZona { get; set; }
    }
}
