namespace Balanza.Entidades
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LiquidacionModel : DbContext
    {
        public LiquidacionModel()
            : base("name=LiquidacionModel")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<tblAcceso> tblAcceso { get; set; }
        public virtual DbSet<tblAcopiador> tblAcopiador { get; set; }
        public virtual DbSet<tblCierreInventario> tblCierreInventario { get; set; }
        public virtual DbSet<tblConsumos> tblConsumos { get; set; }
        public virtual DbSet<tblCuota> tblCuota { get; set; }
        public virtual DbSet<tblDetVenta> tblDetVenta { get; set; }
        public virtual DbSet<tblGastoAdm> tblGastoAdm { get; set; }
        public virtual DbSet<tblLeyesComerciales> tblLeyesComerciales { get; set; }
        public virtual DbSet<tblLeyesReferenciales> tblLeyesReferenciales { get; set; }
        public virtual DbSet<tblLiquidacion> tblLiquidacion { get; set; }
        public virtual DbSet<tblLiquidacion2> tblLiquidacion2 { get; set; }
        public virtual DbSet<tblMaestro> tblMaestro { get; set; }
        public virtual DbSet<tblMaquilas> tblMaquilas { get; set; }
        public virtual DbSet<tblMaterial> tblMaterial { get; set; }
        public virtual DbSet<tblParametrosCom> tblParametrosCom { get; set; }
        public virtual DbSet<tblParametrosComProv> tblParametrosComProv { get; set; }
        public virtual DbSet<tblPersona> tblPersona { get; set; }
        public virtual DbSet<tblPrecioInter> tblPrecioInter { get; set; }
        public virtual DbSet<tblProveedor> tblProveedor { get; set; }
        public virtual DbSet<tblRecuperacion> tblRecuperacion { get; set; }
        public virtual DbSet<tblRegistro> tblRegistro { get; set; }
        public virtual DbSet<tblRol> tblRol { get; set; }
        public virtual DbSet<tblRolAcceso> tblRolAcceso { get; set; }
        public virtual DbSet<tblRumas> tblRumas { get; set; }
        public virtual DbSet<tblTanques> tblTanques { get; set; }
        public virtual DbSet<tblUsuarioRol> tblUsuarioRol { get; set; }
        public virtual DbSet<tblUsuarios> tblUsuarios { get; set; }
        public virtual DbSet<tblVentas> tblVentas { get; set; }
        public virtual DbSet<tblZona> tblZona { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strNombre)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strEnlace)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strIndex)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strClickName)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strIconName)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblAcceso>()
                .HasMany(e => e.tblRolAcceso)
                .WithRequired(e => e.tblAcceso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strDni)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strRuc)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strNombres)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strApellidos)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblAcopiador>()
                .HasMany(e => e.tblCuota)
                .WithRequired(e => e.tblAcopiador)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblCierreInventario>()
                .Property(e => e.strRumasDesc)
                .IsUnicode(false);

            modelBuilder.Entity<tblCierreInventario>()
                .Property(e => e.strLotesDesc)
                .IsUnicode(false);

            modelBuilder.Entity<tblCierreInventario>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblCierreInventario>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblCierreInventario>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strGeP1)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strGeP2)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strGeP3)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strGeP4)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strGeEspecif2)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strPlDensidad)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strPlPorcMalla)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strPlPeso)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str0HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str2HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str4HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str8HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str12HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str24HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str48HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str72HrsLixCN)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str0HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str2HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str4HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str8HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str12HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str24HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str48HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.str72HrsLixPH)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH0)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH2)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH4)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH8)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH12)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH24)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH48)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strHrsLixReactOH72)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes2)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes4)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes8)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes12)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes24)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes48)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strVolDes72)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblConsumos>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strLeyAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strFinoAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strUnidadMedida)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strAnio)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strMes)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblCuota>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strPro)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strPeriodo)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strLeyOro)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strGrOro)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblDetVenta>()
                .Property(e => e.chrEstado)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strZona)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strProveedor)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strMes)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strAnio)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblGastoAdm>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesComerciales>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesComerciales>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesComerciales>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesComerciales>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strCorrelativo)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strTipoMineral)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strZona)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strAcopiador)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strPruebaMetalurgica)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strPorcRcpLab)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strLeyAuOzTc)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strLeyAuReeOzTc)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strConsumo)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strLeyAgOzTc)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strLeyAuCom)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strLeyAgCom)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblLeyesReferenciales>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLeyAuLiq)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLeyAgLiq)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLeyAuMargen)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPorcRecup)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPrecioAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMargenPI)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMaquila)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strConsumoQ)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strGastAdm)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strRecAg)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPrecioAg)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMargenAg)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strRecIntAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMaquilaIntAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strConsQIntAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strGastAdmIntAu)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strRecIntAg)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPorcImporte)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strEstadoLiq)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strNroFactura)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPersonaPropuesta)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPersonaLiquidado)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strEstadoReintegro)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLeyAuReintegro)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strLeyAgReintegro)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMargenPIRei)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMaquilaRei)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strConsumoQRei)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strGastAdmRei)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strMargenAgRei)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strPersonaReintegro)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strNotaDebito)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strNroExportacion)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strCodLiquidacion)
                .IsFixedLength();

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strProAuZona)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strProAuAcopiador)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblLiquidacion2>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.strNombre)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.strValor)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaestro>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblMaquilas>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaquilas>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaquilas>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblMaterial>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaterial>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaterial>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblMaterial>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosCom>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosCom>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosCom>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosComProv>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosComProv>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblParametrosComProv>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strCodPersona)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strNombres)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strApellidoPat)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strApellidoMat)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strCargo)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strDireccion)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strSexo)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strDni)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strTelefono)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblPersona>()
                .HasMany(e => e.tblUsuarios)
                .WithRequired(e => e.tblPersona)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.strGoldAM)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.strGoldPM)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.strSilver)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrecioInter>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .Property(e => e.strRuc)
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .Property(e => e.strRazonS)
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblProveedor>()
                .HasMany(e => e.tblParametrosComProv)
                .WithRequired(e => e.tblProveedor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strTipoProceso)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm2)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm4)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm8)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm12)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm24)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm48)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm72)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strOroPpm90)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm2)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm4)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm8)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm12)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm24)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm48)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm72)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strPlataPpm90)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strLeyAuRipio)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strLeyAgRipio)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strCobrePpm48)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strCobrePpm72)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strCobrePpm90)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRecuperacion>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strSubLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strMes)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strProcedenciaLg)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strProcedenciaSt)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strRegion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strClienteRuc)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strClienteRazonS)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strCodConcesion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNombreConcesion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strGuiaRemitente)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strCodCompromiso)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strMaterial)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strHoraIngreso)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strHoraSalida)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strTiempoDescarga)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strDuenioMineral)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strFechaMuestreo)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strTurnoMuestreo)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strFechaRetiro)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNombreTransportista)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNroPlaca)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strGuiaTransport)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strDniAcopiador)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNombreAcopiador)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strTurnoRegistro)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strObservacion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcH2O)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strCheckH2O)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPSecoTM)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPRef12hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPAu30hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPAu70hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPReeAu72hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAuOzTc)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAuREE)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAuGrTm)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strFinoAuGr)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPAg30hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcRCPAg70hrs)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAgOzTc)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAgREE)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPorcCobre)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNaOHKgTM)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strNaCNKgTM)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strHorasAgitacion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strPenalidad)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAuComercial)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAgComercial)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyCertificado)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyDirimencia)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAuHistorico)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strLeyAgHistorico)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strEstadoLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strRolModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.strMotivoModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.chrEstadoModifBal)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.chrEstadoModifMet)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.chrEstadoModifLab)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .Property(e => e.chrEstadoModifCom)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRegistro>()
                .HasMany(e => e.tblLiquidacion)
                .WithRequired(e => e.tblRegistro)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblRol>()
                .Property(e => e.strDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tblRol>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblRol>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRol>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRol>()
                .HasMany(e => e.tblRolAcceso)
                .WithRequired(e => e.tblRol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblRol>()
                .HasMany(e => e.tblUsuarioRol)
                .WithRequired(e => e.tblRol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblRolAcceso>()
                .Property(e => e.strTipoAcceso)
                .IsUnicode(false);

            modelBuilder.Entity<tblRolAcceso>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblRolAcceso>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRolAcceso>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strLote)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strNroRuma)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strMesContable)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strEstadoRuma)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strNroCampania)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblRumas>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strNroTanque)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strCarbonSeco)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGrAuKgC)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGrAgKgC)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strDensPulpa)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strVolPulpa)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGAum3)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGAut)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGAgm3)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strGAgt)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblTanques>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarioRol>()
                .Property(e => e.strCodUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarioRol>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarioRol>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarioRol>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strCodUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strCodPersona)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strPassword)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strSuperAdmin)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblUsuarios>()
                .HasMany(e => e.tblUsuarioRol)
                .WithRequired(e => e.tblUsuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strPro)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strPeriodo)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strMes)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strAnio)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strNroDoc)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strImpFac)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strNroDocND)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strImpND)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strKgVenta)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strPenVenta)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblVentas>()
                .HasMany(e => e.tblDetVenta)
                .WithRequired(e => e.tblVentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strCodZona)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strZona)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strProcedencia)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strRegion)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strCalculoEspecial)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strUsuarioCrea)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.strUsuarioModif)
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .Property(e => e.chrEstado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblZona>()
                .HasMany(e => e.tblCuota)
                .WithRequired(e => e.tblZona)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblZona>()
                .HasMany(e => e.tblParametrosCom)
                .WithRequired(e => e.tblZona)
                .WillCascadeOnDelete(false);
        }
    }
}
