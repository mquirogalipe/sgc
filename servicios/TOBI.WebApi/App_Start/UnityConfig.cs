using Microsoft.Practices.Unity;
using System.Web.Http;
using Balanza.Common;
using Balanza.Servicio;
using Unity.WebApi;
using Balanza.WebApi.Authorization.Interfaces;
using Balanza.WebApi.Authorization.Implementaciones;

namespace Balanza.WebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            ComponentLoader.LoadContainer(container, ".\\bin", "Balanza.*.dll");
            container.RegisterType<IServicioBalanza, ServicioBalanza>();
            container.RegisterType<IMembershipProvider, MembershipProvider>();
            container.RegisterType<IRSAKeyProvider, RSAKeyProvider>();
            container.RegisterType<IAuthService, AuthService>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}