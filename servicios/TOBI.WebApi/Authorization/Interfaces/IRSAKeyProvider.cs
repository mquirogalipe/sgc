﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balanza.WebApi.Authorization.Interfaces
{
    public interface IRSAKeyProvider
    {
        Task<String> GetPrivateAndPublicKeyAsync();
    }
}
