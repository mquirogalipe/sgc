﻿using Balanza.EntidadesDTO.EntidadesModel;
using System.Collections.Generic;
using System.Security.Claims;

namespace Balanza.WebApi.Authorization.Interfaces
{
    public interface IMembershipProvider
    {
        List<Claim> GetUserClaims(UsuarioModel usuario);
        bool VerifiyUserPassword(string username, string password);
    }
}
