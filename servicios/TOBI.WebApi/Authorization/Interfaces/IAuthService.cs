﻿using Balanza.EntidadesDTO.EntidadesModel;
using System;
using System.Threading.Tasks;

namespace Balanza.WebApi.Authorization.Interfaces
{
    public interface IAuthService
    {
        Task<String> GenerateJwtTokenAsync(UsuarioModel usuario);
        Task<Boolean> ValidateTokenAsync(string TokenString);
    }
}
