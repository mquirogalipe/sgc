﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.WebApi.Authorization.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Balanza.WebApi.Authorization.Implementaciones
{
    public class MembershipProvider : IMembershipProvider
    {
        public List<Claim> GetUserClaims(UsuarioModel usuario)
        {
            //HashSet tiene que devolver los claims(informacion de seguridad) del usuario
            //Deberíamos llamar a un método que nos devuelva dicha información desde base de datos
            //Esta implementacion es solo un ejemplo, cambiarla en la versión final

            List<Claim> claims = new List<Claim>();
            //claims.Add(new Claim(ClaimTypes.Role, "Admin"));
            claims.Add(new Claim(ClaimTypes.Name, usuario.strCodUsuario));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, usuario.strCodPersona));
            return claims;
        }

        public bool VerifiyUserPassword(string username, string password)
        {

            //Versión de ejemplo, acceder a bd para verificar, cambiarla a la versión final
            if (username == "admin" && password == "12345")
                return true;



            return false;
        }
    }
}