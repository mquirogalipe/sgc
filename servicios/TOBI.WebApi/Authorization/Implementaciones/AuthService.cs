﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.WebApi.Authorization.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Balanza.WebApi.Authorization.Implementaciones
{
    public class AuthService : IAuthService
    {
        private readonly IMembershipProvider _membershipProvider;
        private readonly IRSAKeyProvider _rsaProvider;

        public AuthService(IMembershipProvider membershipProvider, IRSAKeyProvider rsaProvider)
        {
            _membershipProvider = membershipProvider;
            _rsaProvider = rsaProvider;
        }

        public async Task<String> GenerateJwtTokenAsync(UsuarioModel usuario)
        {
            /*
            if (!_membershipProvider.VerifiyUserPassword(username, password))
                throw new InvalidCredentialException("Credenciales de usuario erróneas.");
                */
            int TimeToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TiempoToken"]);
            List<Claim> claims = _membershipProvider.GetUserClaims(usuario);

            string publicAndPrivateKey = await _rsaProvider.GetPrivateAndPublicKeyAsync();
            if (publicAndPrivateKey == null)
                throw new InvalidRSAKeyException("Error al obtener la llave RSA.");

            RSACryptoServiceProvider publicAndPrivate = new RSACryptoServiceProvider();
            publicAndPrivate.FromXmlString(publicAndPrivateKey);

            JwtSecurityToken jwtToken = new JwtSecurityToken
            (
                issuer: "http://issuer.com",
                audience: "http://mysite.com",
                claims: claims,
                signingCredentials: new SigningCredentials(new RsaSecurityKey(publicAndPrivate), SecurityAlgorithms.RsaSha256Signature),
                expires: DateTime.Now.AddMinutes(TimeToken)
            );

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            string tokenString = tokenHandler.WriteToken(jwtToken);

            return tokenString;
        }

        public async Task<Boolean> ValidateTokenAsync(string TokenString)
        {
            Boolean result = false;

            try
            {
                SecurityToken securityToken = new JwtSecurityToken(TokenString);
                JwtSecurityTokenHandler securityTokenHandler = new JwtSecurityTokenHandler();
                RSACryptoServiceProvider publicAndPrivate = new RSACryptoServiceProvider();

                string publicAndPrivateKey = await _rsaProvider.GetPrivateAndPublicKeyAsync();
                if (publicAndPrivateKey == null)
                    return result;

                publicAndPrivate.FromXmlString(publicAndPrivateKey);

                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = "http://issuer.com",
                    ValidAudience = "http://mysite.com",
                    IssuerSigningKey = new RsaSecurityKey(publicAndPrivate),
                    ValidateLifetime = true
                };

                ClaimsPrincipal claimsPrincipal = securityTokenHandler.ValidateToken(TokenString, validationParameters, out securityToken);

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Console.WriteLine(ex);
            }
            return result;
        }
    }
    public class InvalidCredentialException : Exception
    {
        public InvalidCredentialException()
        {
        }
        public InvalidCredentialException(string message)
            : base(message)
        {
        }
    }

    public class InvalidRSAKeyException : Exception
    {
        public InvalidRSAKeyException()
        {
        }
        public InvalidRSAKeyException(string message)
            : base(message)
        {
        }
    }
}