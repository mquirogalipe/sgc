﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/lotes")]
    public class registroController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public registroController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }

        [Route("get/all")]
        //[TokenAuthenticate]
        [HttpGet]
        public CollectionResponse<RegistroModel> GetLotes()
        {
            return _objServicio.GetRegistros();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutLote(RegistroModel objRegistro)
        {
            return _objServicio.PutRegistro(objRegistro);
        }
        [Route("update/balanza")] //UPDATE
        //[TokenAuthenticate]
        [HttpPost]
        public int UpdateLoteBalanza(RegistroModel objRegistro)
        {
            return _objServicio.UpdateRegistroBalanza(objRegistro);
        }
        [Route("update/laboratorio")] //UPDATE
        //[TokenAuthenticate]
        [HttpPost]
        public int UpdateLoteLaboratorio(RegistroModel objRegistro)
        {
            return _objServicio.UpdateRegistroLaboratorio(objRegistro);
        }
        [Route("update/comercial")] //UPDATE
        //[TokenAuthenticate]
        [HttpPost]
        public int UpdateLoteComercial(RegistroModel objRegistro)
        {
            return _objServicio.UpdateRegistroComercial(objRegistro);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        //[TokenAuthenticate]
        [HttpDelete]
        public int DeleteLote(int? id, string usuario)
        {
            return _objServicio.DeleteRegistro(id, usuario);
        }
        [Route("search")]
        //[TokenAuthenticate]
        [HttpPost]
        public CollectionResponse<RegistroModel> SearchLotes(RegistroSearchModel objQuery)
        {
            return _objServicio.SearchRegistros(objQuery);
        }
        [Route("search/comercial")]
        //[TokenAuthenticate]
        [HttpPost]
        public CollectionResponse<RegistroModel> SearchLotesComercial(RegistroSearchModel objQuery)
        {
            return _objServicio.SearchRegistrosComercial(objQuery);
        }
        [Route("get/last")]
        [HttpGet]
        public int GetLastRegistro() {
            return _objServicio.GetLastRegistro();
        }
        [Route("duplicate/{id}/{lote}")] //DUPLICAR
        [HttpGet]
        public int DuplicarLote(int? id, string lote)
        {
            return _objServicio.DuplicateRegistro(id, lote);
        }
    }
}
