﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/query")]
    //[TokenAuthenticate]
    public class queryController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public queryController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("execute")] //EXECUTE
        [HttpPost]
        public int ExecuteQuery(QueryModel objQuery)
        {
            return _objServicio.ExecuteQuery(objQuery);
        }
    }
}
