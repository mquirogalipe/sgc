﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/ventas")]
    //[TokenAuthenticate]
    public class ventasController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public ventasController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //OBTENER
        [HttpPost]
        public CollectionResponse<VentasModel> GetVentas(VentasSearchModel objSearch)
        {
            return _objServicio.GetVentas(objSearch);
        }
        [Route("put")]
        [HttpPost]
        public int PutVenta(VentasModel objVenta) {
            return _objServicio.PutVenta(objVenta);
        }
    }
}
