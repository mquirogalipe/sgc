﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/laboratorio")]
    public class laboratoriosController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public laboratoriosController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //OBTENER
        [HttpPost]
        public CollectionResponse<LaboratorioModel> SearchLaboratorios(LaboratorioModel objSearch)
        {
            return _objServicio.SearchLaboratorios(objSearch);
        }
        [Route("put")] //OBTENER
        [HttpPost]
        public int PutLaboratorio(LaboratorioModel objLaboratorio)
        {
            return _objServicio.PutLaboratorio(objLaboratorio);
        }
        [Route("update")] //OBTENER
        [HttpPost]
        public int UpdateLaboratorio(LaboratorioModel objLaboratorio)
        {
            return _objServicio.UpdateLaboratorio(objLaboratorio);
        }
    }
}
