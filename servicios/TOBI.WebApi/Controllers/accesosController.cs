﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/accesos")]
    //[TokenAuthenticate]
    public class accesosController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public accesosController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")] //OBTENER
        [HttpGet]
        public CollectionResponse<AccesoModel> GetAllAccesos()
        {
            return _objServicio.GetAllAccesos();
        }
    }
}
