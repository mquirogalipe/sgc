﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/leyreferencial")]
    //[TokenAuthenticate]
    public class leyReferencialController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public leyReferencialController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //BUSCAR
        [HttpPost]
        public CollectionResponse<LeyesReferencialesModel> GetAllAccesos(LeyesReferencialesModel objSearch)
        {
            return _objServicio.SearchLeyesRef(objSearch);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutLeyReferencial(LeyesReferencialesModel objLeyRef)
        {
            return _objServicio.PutLeyRef(objLeyRef);
        }
        [Route("update/balanza")] //ACTUALIZAR BALANZA
        [HttpPost]
        public int UpdateLeyRef_Bal(LeyesReferencialesModel objLeyRef)
        {
            return _objServicio.UpdateLeyRef_Bal(objLeyRef);
        }
        [Route("update/laboratorio")] //ACTUALIZAR BALANZA
        [HttpPost]
        public int UpdateLeyRef_Lab(LeyesReferencialesModel objLeyRef)
        {
            return _objServicio.UpdateLeyRef_Lab(objLeyRef);
        }
        [Route("update/comercial")] //ACTUALIZAR COMERCIAL
        [HttpPost]
        public int UpdateLeyRef_Com(LeyesReferencialesModel objLeyRef)
        {
            return _objServicio.UpdateLeyRef_Com(objLeyRef);
        }
    }
}
