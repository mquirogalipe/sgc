﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System.Collections.Generic;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/lotestrujillo")]
    public class lotesASController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public lotesASController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //BUSCAR
        [HttpPost]
        public CollectionResponse<LotesAsModel> SearchLotesAS(DetLotesASModel objLotesAs)
        {
            return _objServicio.SearchLotesAS(objLotesAs);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutLotesAS(LotesAsModel objLotesAs)
        {
            return _objServicio.PutLotesAS(objLotesAs);
        }
        [Route("put/any")] //INSERTAR LISTA
        [HttpPost]
        public int PutLotesAS(List<LotesAsModel> lstLotesAs)
        {
            foreach(LotesAsModel item in lstLotesAs) {
                _objServicio.PutLotesAS(item);
            }
            return 1;
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateLotesAS(LotesAsModel objLotesAs)
        {
            return _objServicio.UpdateLotesAS(objLotesAs);
        }
        [Route("search/loteslj")] //SEARCH LOTES LJ
        [HttpPost]
        public CollectionResponse<RegistroModel> SearchLotesLJ(RegistroSearchModel objLotesAs)
        {
            return _objServicio.SearchLotesLJ(objLotesAs);
        }
    }
}
