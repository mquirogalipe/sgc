﻿using System;
using System.Web.Http;
using Balanza.Servicio;
using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.WebApi.Authorization;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/rol")]
    //[TokenAuthenticate]
    public class rolController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public rolController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<RolModel> GetAllRoles()
        {
            return _objServicio.GetAllRoles();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutRol(RolModel rol)
        {
            return _objServicio.PutRol(rol);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateRol(RolModel rol)
        {
            return _objServicio.UpdateRol(rol);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteRol(int? id, string usuario)
        {
            return _objServicio.DeleteRol(id, usuario);
        }
    }
}
