﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/proveedor")]
    public class proveedorController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public proveedorController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //SEARCH
        [HttpPost]
        public CollectionResponse<ProveedorModel> SearchProveedor(ProveedorModel objSearch)
        {
            return _objServicio.SearchProveedor(objSearch);
        }
        [Route("put")] //SEARCH
        [HttpPost]
        public int PutProveedor(ProveedorModel objProveedor)
        {
            return _objServicio.PutProveedor(objProveedor);
        }
        [Route("update")] //SEARCH
        [HttpPost]
        public int UpdateProveedor(ProveedorModel objProveedor)
        {
            return _objServicio.UpdateProveedor(objProveedor);
        }
    }
}
