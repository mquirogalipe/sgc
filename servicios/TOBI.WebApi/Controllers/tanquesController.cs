﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/tanques")]
    public class tanquesController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public tanquesController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpGet]
        public CollectionResponse<TanqueModel> GetTanques()
        {
            return _objServicio.GetTanques();
        }
        [Route("put")] //OBTENER
        [HttpPost]
        public int PutTanque(TanqueModel objTanque)
        {
            return _objServicio.PutTanque(objTanque);
        }
    }
}
