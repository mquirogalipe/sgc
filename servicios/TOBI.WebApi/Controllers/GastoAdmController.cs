﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/gastoadm")]
    public class GastoAdmController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public GastoAdmController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //OBTENER
        [HttpPost]
        public CollectionResponse<GastoAdmModel> SearchGastoAdm(GastoAdmModel objSearch)
        {
            return _objServicio.SearchGastoAdm(objSearch);
        }
        [Route("get")] //OBTENER
        [HttpPost]
        public CollectionResponse<GastoAdmModel> GetGastoAdm(GastoAdmModel objSearch)
        {
            return _objServicio.GetGastoAdm(objSearch);
        }
        [Route("get/proveedor")] //OBTENER PROVEEDORES
        [HttpPost]
        public CollectionResponse<GastoAdmModel> GetProveedores(GastoAdmModel objSearch)
        {
            return _objServicio.GetProveedores(objSearch);
        }
        [Route("get/acopiador")] //OBTENER ACOPIADORES
        [HttpGet]
        public CollectionResponse<GastoAdmModel> GetAcopiadores()
        {
            return _objServicio.GetAcopiadores();
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutGastoAdm(GastoAdmModel objSearch)
        {
            return _objServicio.PutGastAdm(objSearch);
        }
        [Route("update")] //ACTUALZIAR
        [HttpPost]
        public int UpdateGastoAdm(GastoAdmModel objSearch)
        {
            return _objServicio.UpdateGastAdm(objSearch);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteRol(int? id, string usuario)
        {
            return _objServicio.DeleteGastAdm(id, usuario);
        }
        [Route("get/mes")] //BUSCAR
        [HttpPost]
        public CollectionResponse<GastoAdmMesModel> GetGastoAdmMes(GastoAdmModel objSearch)
        {
            return _objServicio.GetGastoAdmMes(objSearch);
        }
    }
}
