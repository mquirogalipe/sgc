﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/cierreinventario")]
    public class cierreInventarioController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public cierreInventarioController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpGet]
        public CollectionResponse<CierreInventarioModel> GetCierreInventario()
        {
            return _objServicio.GetCierreInventarios();
        }
        [Route("get/rumas")] //OBTENER RUMAS
        [HttpGet]
        public CollectionResponse<RumasInventarioModel> GetRumasInventario()
        {
            return _objServicio.GetRumasInventario();
        }
        [Route("get/pesos")] //OBTENER PESOS
        [HttpPost]
        public CollectionResponse<CierreInventarioModel> GetPesosInventario(CierreInventarioModel objCierre)
        {
            return _objServicio.GetPesoInventario(objCierre);
        }
        [Route("put")] //INSERTAR CIERRE INVENTARIO
        [HttpPost]
        public int PutCierreInventario(CierreInventarioModel objCierre)
        {
            return _objServicio.PutCierreInventario(objCierre);
        }
    }
}
