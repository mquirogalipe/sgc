﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/recuperacion")]
    //[TokenAuthenticate]
    public class recuperacionController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public recuperacionController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpPost]
        public RecuperacionModel GetConsumos(RecuperacionModel objRecuperacion)
        {
            return _objServicio.GetRecuperacion(objRecuperacion.strLote, objRecuperacion.strTipoProceso);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutRecuperacion(RecuperacionModel objRecuperacion)
        {
            return _objServicio.PutRecuperacion(objRecuperacion);
        }
    }
}
