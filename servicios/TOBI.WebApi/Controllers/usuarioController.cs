﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/usuario")]
    //[TokenAuthenticate]
    public class usuarioController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public usuarioController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<UsuarioModel> GetAllUsuarios()
        {
            return _objServicio.GetAllUsuarios();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public string PutUsuario(UsuarioModel objUsuario)
        {
            return _objServicio.PutUsuario(objUsuario);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public string UpdateUsuario(UsuarioModel objUsuario)
        {
            return _objServicio.UpdateUsuario(objUsuario);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public string DeleteUsuario(string id, string usuario)
        {
            return _objServicio.DeleteUsuario(id, usuario);
        }
        [Route("search")]
        [HttpPost]
        public CollectionResponse<UsuarioModel> searchUsuarios(UsuarioModel objQuery)
        {
            return _objServicio.SearchUsuarios(objQuery);
        }
        [Route("get/accesos/{strCargo}")]
        [HttpGet]
        public CollectionResponse<RolAccesoModel> GetUsuarioAccesos(string strCargo)
        {
            return _objServicio.GetUsuarioAccesos(strCargo);
        }
        [Route("get/persona/{strCodPersona}")]
        [HttpGet]
        public UsuarioModel GetPersona(string strCodPersona)
        {
            return _objServicio.GetPersona(strCodPersona);
        }
        [Route("update/persona")]
        [HttpPost]
        public string UpdatePersona(UsuarioModel objPersona)
        {
            return _objServicio.UpdatePersona(objPersona);
        }
        [Route("change")]
        [HttpPost]
        public string ChangeUsuario(UsuarioModel objUsuario)
        {
            return _objServicio.ChangeUsuario(objUsuario);
        }
    }
}
