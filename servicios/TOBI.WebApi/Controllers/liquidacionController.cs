﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/liquidacion")]
    //[TokenAuthenticate]
    public class liquidacionController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public liquidacionController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search")] //SEARCH
        [HttpPost]
        public CollectionResponse<LiquidacionModel> GetLiquidacion(RegistroSearchModel objSearch)
        {
            return _objServicio.GetLiquidacion(objSearch);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutLiquidacion(LiquidacionModel objLiquidacion)
        {
            return _objServicio.PutLiquidacion(objLiquidacion);
        }
        [Route("get/reintegro")] //GET REINTEGRO
        [HttpPost]
        public CollectionResponse<LiquidacionModel> GetReintegro(RegistroSearchModel objLiquidacion)
        {
            return _objServicio.GetReintegro(objLiquidacion.strLote);
        }
        [Route("put/reintegro")] //GET REINTEGRO
        [HttpPost]
        public int PutReintegro(LiquidacionModel objLiquidacion)
        {
            return _objServicio.PutReintegro(objLiquidacion);
        }
        [Route("parametros")] //SEARCH PARAMETROS
        [HttpPost]
        public CollectionResponse<ParametrosModel> GetParametrosZona(ParametrosModel objSearch)
        {
            return _objServicio.GetParametrosZona(objSearch.strZona, objSearch.strProveedor);
        }
        [Route("maquila")]
        [HttpPost]
        public CollectionResponse<MaquilasModel> GetMaquilas(MaquilasModel objSearch)
        {
            return _objServicio.GetMaquilas(objSearch);
        }
        [Route("laboratorio/get")]
        [HttpGet]
        public CollectionResponse<LaboratorioModel> GetLAboratorios()
        {
            return _objServicio.GetAllLaboratorios();
        }
    }
}
