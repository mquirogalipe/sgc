﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/reportes")]
    //[TokenAuthenticate]
    public class reportesController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public reportesController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/acopiomineral")] //OBTENER REPORTE ACOPIO
        [HttpPost]
        public CollectionResponse<ReporteAcopioMineralModel> GetReporteAcopioMineral(ReporteAcopioMineralModel objSearch)
        {
            return _objServicio.getReporteAcopioMineral(objSearch);
        }
        [Route("get/controlcuotas")] //OBTENER REPORTE CONTROL CUOTAS
        [HttpPost]
        public CollectionResponse<ReporteControlCuotasModel> getReporteControlCuotas(ReporteControlCuotasModel objSearch)
        {
            return _objServicio.getReporteControlCuotas(objSearch);
        }
        [Route("get/detalle/acopio")]
        [HttpPost]
        public CollectionResponse<ReporteDetAcopioModel> getDetalleReporteAcopio(ReporteDetAcopioModel objSearch)
        {
            return _objServicio.getDetalleReporteAcopio(objSearch);
        }
        [Route("get/acopiomineral/detallado")]
        [HttpPost]
        public CollectionResponse<ReporteDetAcopioModel> getReporteAcopioMRL_Detallado(ReporteDetAcopioModel objSearch)
        {
            return _objServicio.getReporteAcopioMRL_Detallado(objSearch);
        }
        [Route("get/acopiomes")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getAcopioMes(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReporteAcopioMes(objSearch.strAnio, objSearch.strType);
        }

        //news
        [Route("get/zonames")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getZonaMes(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReporteZonaMes(objSearch.strAnio);
        }
        [Route("get/ponderadomes")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getPonderadoMes(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReportePonderadoMes(objSearch.strAnio);
        }
        [Route("get/acopiadormes")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getAcopiadorMes(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReporteAcopiadorMes(objSearch.strAnio);
        }
        [Route("get/acopiadorzona")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMineralModel> getAcopiadorZona(ReporteAcopioMineralModel objSearch)
        {
            return _objServicio.getReporteAcopiadorZona(objSearch);
        }
        [Route("get/finomes")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getFinoMes(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReporteFinoMes(objSearch.strAnio);
        }
        [Route("get/mineralrobin")]
        [HttpPost]
        public CollectionResponse<ReporteAcopioMesModel> getMineralRobin(ReporteAcopioMesModel objSearch)
        {
            return _objServicio.getReporteMineralRobin(objSearch.strAnio, objSearch.strZona);
        }
        [Route("get/reportediariorobin")]
        [HttpPost]
        public CollectionResponse<ReporteDetAcopioModel> getReporteDiarioRobin(RegistroSearchModel objQuery)
        {
            return _objServicio.getReporteDiarioRobin(objQuery);
        }
        [Route("get/reportemargenes")]
        [HttpPost]
        public CollectionResponse<ReporteMargenesModel> getReporteMargenes(ReporteMargenesModel objQuery)
        {
            return _objServicio.getReporteMargenes(objQuery);
        }
        [Route("get/reportemargenes/detallado")]
        [HttpPost]
        public CollectionResponse<ReporteMargenDetModel> getReporteMargenDetallado(ReporteMargenDetModel objQuery)
        {
            return _objServicio.getReporteMargenDetallado(objQuery);
        }
        [Route("get/reportemargenes/productivo")]
        [HttpPost]
        public CollectionResponse<ReporteMargenesModel> getReporteMargenProductivo(QueryModel objQuery)
        {
            return _objServicio.getReporteMargenProductivo(objQuery);
        }
        [Route("get/reportemargenes/productivo/detallado")]
        [HttpPost]
        public CollectionResponse<ReporteMargenDetModel> getReporteMargenProdDetallado(QueryModel objQuery)
        {
            return _objServicio.getReporteMargenProdDetallado(objQuery);
        }
        [Route("get/reportemargenes/margendiario")]
        [HttpPost]
        public CollectionResponse<ReporteMargenesModel> getReporteMargenDiario(ReporteMargenesModel objQuery)
        {
            return _objServicio.getReporteMargenDiario(objQuery);
        }
        [Route("get/reportemargenes/margendiario/detallado")]
        [HttpPost]
        public CollectionResponse<ReporteMargenDetModel> getReporteMargenDiarioDetallado(ReporteMargenDetModel objQuery)
        {
            return _objServicio.getReporteMargenDiarioDetallado(objQuery);
        }
        [Route("get/reportemargenes/margendiario/zona")]
        [HttpPost]
        public CollectionResponse<ReporteMargenesModel> getReporteMargenDiarioZona(ReporteMargenesModel objQuery)
        {
            return _objServicio.getReporteMargenDiarioZona(objQuery);
        }
        [Route("get/reporteplanta/mineralplanta")]
        [HttpPost]
        public CollectionResponse<RegistroModel> getReporteMineralPlanta(RegistroSearchModel objQuery)
        {
            return _objServicio.getReporteMineralPlanta(objQuery);
        }
        [Route("get/reporteplanta/mineralprocesado")]
        [HttpPost]
        public CollectionResponse<ReporteMineralProcesadoModel> getReporteMineralProcesado(ReporteMineralProcesadoModel objQuery)
        {
            return _objServicio.getReporteMineralProcesado(objQuery);
        }
        //Reporte Provisiones
        [Route("get/reportemargenes/provisiones")]
        [HttpPost]
        public CollectionResponse<ReporteProvisionesModel> getReporteProvisiones(ReporteProvisionesModel objQuery)
        {
            return _objServicio.getReporteProvisiones(objQuery);
        }
    }
}
