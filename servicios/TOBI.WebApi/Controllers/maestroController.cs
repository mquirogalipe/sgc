﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/maestro")]
    //[TokenAuthenticate]
    public class maestroController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public maestroController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("search/{strNombre}")] //OBTENER
        [HttpGet]
        public MaestroModel SearchMaestro(string strNombre)
        {
            return _objServicio.SearchMaestro(strNombre);
        }
    }
}
