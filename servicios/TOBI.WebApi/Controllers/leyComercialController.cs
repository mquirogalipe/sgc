﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/leycomercial")]
    //[TokenAuthenticate]
    public class leyComercialController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public leyComercialController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpGet]
        public CollectionResponse<LeyesComercialesModel> GetLeyesComerciales()
        {
            return _objServicio.GetLeyesComerciales();
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutLeyesComerciales(LeyesComercialesModel objLeyCom)
        {
            return _objServicio.PutLeyesComerciales(objLeyCom);
        }
        [Route("update")] //ACTUALIZAR
        [HttpPost]
        public int UpdateLeyesComerciales(LeyesComercialesModel objLeyCom)
        {
            return _objServicio.UpdateLeyesComerciales(objLeyCom);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteLeyComercial(int? id, string usuario)
        {
            return _objServicio.DeleteLeyesComerciales(id, usuario);
        }
    }
}
