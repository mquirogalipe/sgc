﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/cuota")]
    //[TokenAuthenticate]
    public class cuotaController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public cuotaController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<CuotaModel> GetAllCuotas()
        {
            return _objServicio.GetAllCuotas();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutCuota(CuotaModel objCuota)
        {
            return _objServicio.PutCuota(objCuota);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateCuota(CuotaModel objCuota)
        {
            return _objServicio.UpdateCuota(objCuota);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteCuota(int? id, string usuario)
        {
            return _objServicio.DeleteCuota(id, usuario);
        }
        [Route("search")]
        [HttpPost]
        public CollectionResponse<CuotaModel> SearchCuotas(CuotaModel objSearch)
        {
            return _objServicio.SearchCuotas(objSearch);
        }
    }
}
