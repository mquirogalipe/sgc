﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/zona")]
    //[TokenAuthenticate]
    public class zonaController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public zonaController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<ZonaModel> GetAllZonas()
        {
            return _objServicio.GetAllZonas();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutZona(ZonaModel objZona)
        {
            return _objServicio.PutZona(objZona);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateZona(ZonaModel objZona)
        {
            return _objServicio.UpdateZona(objZona);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteZona(int? id, string usuario)
        {
            return _objServicio.DeleteZona(id, usuario);
        }

        [Route("region/get")]
        [HttpGet]
        public CollectionResponse<ZonaModel> GetRegiones()
        {
            return _objServicio.getRegiones();
        }

        [Route("search/{zona}")]
        [HttpGet]
        public ZonaModel SearchZona(string zona) {
            return _objServicio.SearchZona(zona);
        }
    }
}
