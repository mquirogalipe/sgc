﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization.Implementaciones;
using Balanza.WebApi.Authorization.Interfaces;
using log4net;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/membership")]
    public class membershipController : ApiController
    {
        private readonly IAuthService _authService;
        private readonly IServicioBalanza _objServicio;
        protected ILog logger;

        public membershipController(IAuthService authService, IServicioBalanza objServicio)
        {
            _authService = authService;
            this._objServicio = objServicio;
            log4net.Config.XmlConfigurator.Configure();
            ILog Log = LogManager.GetLogger("Liquidacion");
            logger = Log;
        }
        [Route("login")]
        [HttpPost]
        public UsuarioModel LoginUsuario(UsuarioModel usuario)
        {
            logger.InfoFormat("WebApi, Membership: LoginUsuario(string strUsuario=:{0}, string strPassword=:******)", usuario.strUsuario);
            return _objServicio.LoginUsuario(usuario.strUsuario, usuario.strPassword);
        }
        [Route("authentication")]
        [HttpPost]
        [ResponseType(typeof(String))]
        public async Task<IHttpActionResult> Authenticate(UsuarioModel objUser)
        {
            logger.InfoFormat("WebApi, Membership: Authenticate(string strUsuario=:{0})", objUser.strUsuario);
            try
            {
                //UsuarioModel usuario = servicio.LoginUsuario(objUser.strUsuario, objUser.strPassword);

                if (objUser.chrEstado.Equals("A"))
                {
                    string Token = await _authService.GenerateJwtTokenAsync(objUser);
                    return Ok(Token);
                }

                return Content(HttpStatusCode.BadRequest, objUser.chrEstado);
            }
            catch (InvalidCredentialException e)
            {
                logger.ErrorFormat("WebApi, Membership: Error en el metodo Authenticate(string strUsuario=:{0})", objUser.strUsuario);
                logger.ErrorFormat("InvalidCredentialException - {0}", e);
                return Unauthorized();
            }
            catch (InvalidRSAKeyException ex)
            {
                logger.ErrorFormat("WebApi, Membership: Error en el metodo Authenticate(string strUsuario=:{0})", objUser.strUsuario);
                logger.ErrorFormat("InvalidRSAKeyException - {0}", ex);
                return InternalServerError();
            }
        }
    }
}
