﻿using System.Web.Http;
using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/rolacceso")]
    //[TokenAuthenticate]
    public class rolaccesoController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public rolaccesoController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/{id}")]
        [HttpGet]
        public CollectionResponse<RolAccesoModel> GetRolAcceso(int id)
        {
            return _objServicio.GetRolAcceso(id);
        }
    }
}
