﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Collections.Generic;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/parametro")]
    //[TokenAuthenticate]
    public class parametroController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public parametroController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }

        [Route("get/all/{type}")]
        [HttpGet]
        public CollectionResponse<ParametrosModel> GetAllParametros(string type)
        {
            return _objServicio.GetAllParametros(type);
        }
        [Route("precio/get/{name}")]
        [HttpGet]
        public CollectionResponse<PrecioInterModel> GetPrecioInter(string name)
        {
            return _objServicio.GetPrecioInter(name);
        }
        [Route("precio/update")]
        [HttpPost]
        public string UpdatePrecioInter(PrecioInterModel objPrecio)
        {
            return _objServicio.UpdatePrecioInter(objPrecio);
        }
        [Route("precio/put")]
        [HttpPost]
        public string PutPrecioInter(PrecioInterModel objPrecio)
        {
            return _objServicio.PutPrecioInter(objPrecio);
        }
        [Route("precio/search")]
        [HttpPost]
        public CollectionResponse<PrecioInterModel> SearchPrecioInter(PrecioInterModel objPrecio)
        {
            return _objServicio.SearchPrecioInter(objPrecio);
        }
        [Route("factorconv/update")]
        [HttpPost]
        public string UpdateFactorConversion(MaestroModel objMaestro)
        {
            return _objServicio.UpdateFactorConversion(objMaestro);
        }
        [Route("zona/put")]
        [HttpPost]
        public int PutParametroZona(ParametrosModel objParametro)
        {
            return _objServicio.PutParametroZona(objParametro);
        }
        [Route("zona/update")]
        [HttpPost]
        public int UpdateParametroZona(ParametrosModel objParametro)
        {
            return _objServicio.UpdateParametroZona(objParametro);
        }
        [Route("zona/duplicate/{id}/{usuario}")]
        [HttpGet]
        public int DuplicateParametro(int? id, string usuario)
        {
            return _objServicio.DuplicateParametro(id, usuario);
        }
        [Route("proveedor/put")]
        [HttpPost]
        public int PutParametroProv(ParametrosModel objParametro)
        {
            return _objServicio.PutParametroProv(objParametro);
        }
        [Route("proveedor/update")]
        [HttpPost]
        public int UpdateParametroProv(ParametrosModel objParametro)
        {
            return _objServicio.UpdateParametroProv(objParametro);
        }
        [Route("proveedor/duplicate/{id}/{usuario}")]
        [HttpGet]
        public int DuplicateParametroProv(int? id, string usuario)
        {
            return _objServicio.DuplicateParametroProv(id, usuario);
        }
    }
}
