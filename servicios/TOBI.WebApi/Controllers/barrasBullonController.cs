﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/barrabullon")]
    public class barrasBullonController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public barrasBullonController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpPost]
        public CollectionResponse<BarrasBullonModel> GetBarrasBullon(BarrasBullonModel objSearch)
        {
            return _objServicio.GetBarrasBullon(objSearch);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutBarraBullon(BarrasBullonModel objBarra)
        {
            return _objServicio.PutBarrasBullon(objBarra);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateBarraBullon(BarrasBullonModel objBarra)
        {
            return _objServicio.UpdateBarrasBullon(objBarra);
        }
    }
}
