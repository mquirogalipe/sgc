﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Collections.Generic;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/leyes/laboratorio")]
    //[TokenAuthenticate]
    public class leyesController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public leyesController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/{lote}")] //OBTENER LEYES LABORATORIO
        [HttpGet]
        public CollectionResponse<LeyesModel> GetLeyesLaboratorio(string lote)
        {
            return _objServicio.GetLeyesLaboratorio(lote);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public string PutLeyesLaboratorio(List<LeyesModel> lstLeyes)
        {
            return _objServicio.PutLeyesLaboratorio(lstLeyes);
        }
        [Route("lote/put")] //INSERTAR LEYES LOTE
        [HttpPost]
        public string PutLeyesLote(RegistroModel objLeyRegistro)
        {
            return _objServicio.PutLeyesRegistro(objLeyRegistro);
        }
    }
}