﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/material")]
    //[TokenAuthenticate]
    public class materialController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public materialController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<MaterialModel> GetAllMaterial()
        {
            Console.Write("Get All Material");
            return _objServicio.GetAllMaterial();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutMaterial(MaterialModel objMaterial)
        {
            return _objServicio.PutMaterial(objMaterial);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateMaterial(MaterialModel objMaterial)
        {
            return _objServicio.UpdateMaterial(objMaterial);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteMaterial(int? id, string usuario)
        {
            return _objServicio.DeleteMaterial(id, usuario);
        }
    }
}
