﻿using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/consumo")]
    //[TokenAuthenticate]
    public class consumoController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public consumoController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/{lote}")] //OBTENER
        [HttpGet]
        public ConsumoModel GetConsumos(string lote)
        {
            return _objServicio.GetConsumos(lote);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutConsumo(ConsumoModel objConsumo)
        {
            return _objServicio.PutConsumo(objConsumo);
        }
    }
}
