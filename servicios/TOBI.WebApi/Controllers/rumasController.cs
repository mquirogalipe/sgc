﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/rumas")]
    public class rumasController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public rumasController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get")] //OBTENER
        [HttpPost]
        public CollectionResponse<RumasModel> GetRumas(RumasModel objSearch)
        {
            return _objServicio.GetRumas(objSearch);
        }
        [Route("put")] //INSERTAR
        [HttpPost]
        public int PutRumas(RumasModel objRuma)
        {
            return _objServicio.PutRuma(objRuma);
        }
        [Route("search")] //BUSCAR
        [HttpPost]
        public CollectionResponse<RumasModel> BuscarRumas(RumasModel objSearch)
        {
            return _objServicio.SearchRumas(objSearch);
        }
        [Route("get/all")] //BUSCAR
        [HttpPost]
        public CollectionResponse<RumasModel> GetAllRumas(RumasModel objSearch)
        {
            return _objServicio.GetAllRumas(objSearch);
        }
        [Route("get/stock/mineral")] //OBTENER STOCK MINERAL
        [HttpPost]
        public CollectionResponse<RumasModel> GetStockMineral(RumasModel objSearch)
        {
            return _objServicio.GetStockMineral(objSearch);
        }
    }
}
