﻿using Balanza.EntidadesDTO.AdicionalModel;
using Balanza.EntidadesDTO.EntidadesModel;
using Balanza.Servicio;
using Balanza.WebApi.Authorization;
using System.Web.Http;

namespace Balanza.WebApi.Controllers
{
    [RoutePrefix("api/acopiador")]
    //[TokenAuthenticate]
    public class acopiadorController : ApiController
    {
        private readonly IServicioBalanza _objServicio;
        public acopiadorController(IServicioBalanza objServicio)
        {
            _objServicio = objServicio;
        }
        [Route("get/all")]
        [HttpGet]
        public CollectionResponse<AcopiadorModel> GetAllAcopiadores()
        {
            return _objServicio.GetAllAcopiador();
        }
        [Route("put")] //INSERCION
        [HttpPost]
        public int PutAcopiador(AcopiadorModel objAcopiador)
        {
            return _objServicio.PutAcopiador(objAcopiador);
        }
        [Route("update")] //UPDATE
        [HttpPost]
        public int UpdateAcopiador(AcopiadorModel objAcopiador)
        {
            return _objServicio.UpdateAcopiador(objAcopiador);
        }
        [Route("delete/{id}/{usuario}")] //DELETE
        [HttpDelete]
        public int DeleteAcopiador(int? id, string usuario)
        {
            return _objServicio.DeleteAcopiador(id, usuario);
        }
    }
}
