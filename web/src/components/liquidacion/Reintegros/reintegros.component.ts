import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LiquidacionService from '@/services/liquidacion.service';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { Loading } from 'element-ui';
import QueryService from '@/services/query.service';
@Component({
   name: 'reintegros',
   props:{
    'intIdLote':{
      type: Number,
      required: true
    },
    'strLote':{
      type: String,
      required: true
    },
    'data':{
      type: Object,
      required: true
    },
    'accesoEscritura':{
      type: Boolean,
      required: true
    }
  },
})
export default class ReintegrosComponent extends Vue {
  LoteLiq: string = '';
  //Reintegro
  activeName: any = 'first';
  fechaNegociacionAu: any = '';
  fechaNegociacionAg: any = '';
  fechaLiqMargen: any = '';
  TipoConsumo: any = 'no';
  ShowConsumo: boolean = true;
  ShowCostoAMineral: boolean = true;
  rowReintegro: any = {};
  dataPropuestaAU: any = [];
  dataPropuestaAG: any = [];
  dataMargen: any = [];
  disableExpPdfMargen: boolean = false;
  disableExpPdfPropuesta: boolean = false;
  guardarViewButton: boolean = true;
  userDec: any = {};
  PSecoTMS: any = '';
  fechaCertificado: any = '';
  rowCertificado = {tipoLey: 'certificado', intIdLaboratorio: '', fechaCertif: '',
                    strLeyAuCertif:'', strLeyComAnt:'', strDiffCertif:'', strPromCertif:'', strLeyAuReintegro:'',
                    strLeyAgCertif:'', strLeyAgComAnt:'', strDiffAgCertif:'', strPromAgCertif:'', strLeyAgReintegro:'',
                    strLeyAuDirimencia:'', strLeyAgDirimencia:''};
  LeyesLiq: any = {LeyAuLiqPrev: '', LeyAgLiqPrev: ''};
  checkedCalculoEsp: boolean = false;
  disabledCalculoEsp: boolean = true;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  showErrorCetif: boolean = false;
  showErrorCetifAG: boolean = false;
  showErrorDirimAU: boolean = false;
  showErrorDirimAG: boolean = false;
  ShowLeyMaquilaTM: boolean = false;
  ShowLeyTMAG: boolean = false;

  ParametrosZona: any = {};
  ParametrosDefault: any = {
    fltConsumoMax:'',
    fltGastAdmMax:'',
    fltGastLabMax:'',
    fltMaquilaMax:'',
    fltMaquilaMin:'',
    fltMargenMax:'',
    fltPesoMax:'',
    fltPorcHumedMax:'',
    fltRecComMax:'',

		fltRecAgMax: '',
		fltLeyAgMax: '',
		fltLeyAgComMax: '',
		fltMargenAgMax: '',
		fltRecAgComMax: '',

		fltRecIntMax: '',
		fltMaquilaIntMax: '',
		fltConsumoIntMax: '',
		fltTmsLiqMin: '',
		fltTmsLiqMax: '',
		fltTmsIntMax: ''
  }
  dataLaboratorios: any = [];
  statusLiquid: string = '';
  checkTipoLey: boolean = true;
  fechaNotaDebito: any = '';
  checkedFacElect: boolean = false;
  NroFacGenerado: string = '';
  viewAccessEscritura: boolean = false;
  constructor (){
    super()
    var session = window.sessionStorage.getItem("session_user");
    this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
  }

  getInfo(idLote, Lote, Data, accesoEscritura){
    if((this.LoteLiq === '') || (this.LoteLiq != Lote)){
      this.viewAccessEscritura = accesoEscritura;
      this.clearDataCertificado();
      this.activeName = 'first';
      this.TipoConsumo = 'no';
      this.ShowLeyMaquilaTM = false;
      this.ShowLeyTMAG = false;
      this.checkedFacElect = false;
      this.LoteLiq = Lote;
      this.rowReintegro = {};
      this.rowReintegro = Data;
      this.statusLiquid = (Data.strEstadoLiq == null)? 'Pendiente' : Data.strEstadoLiq;
      //Cargar data General
      this.PSecoTMS = this.getNumberFix(this.rowReintegro.fltPesoTmh * (100 - Number(this.rowReintegro.strLeyAuMargen))/100, 3);
      this.LeyesLiq.LeyAuLiqPrev = Data.strLeyAuLiq;
      this.LeyesLiq.LeyAgLiqPrev = Data.strLeyAgLiq;
      this.rowCertificado.strLeyComAnt = Data.strLeyAuLiq;
      this.rowCertificado.strLeyAgComAnt = Data.strLeyAgLiq;
      this.checkedCalculoEsp = (this.rowReintegro.strCalculoEspec=='true')? true : false;
      this.dataPropuestaAU[4].CostoIntegro = Data.strNBaseAu;
      this.dataPropuestaAG[4].CostoIntegro = Data.strNBaseAg;
      this.dataMargen[3].REC = Data.strRecIntAu;
      this.dataMargen[3].Maquila = Data.strMaquilaIntAu;
      this.dataMargen[3].ConsQ = Data.strConsQIntAu;
      this.dataMargen[3].GastAdm = Data.strGastAdmIntAu;
      this.dataMargen[6].REC = Data.strRecIntAg;

      this.fechaNegociacionAu = new Date();
      this.fechaNegociacionAg = new Date();
      this.fechaLiqMargen = new Date();
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
      this.handleParametros(-1, Data);
      this.getDataLaboratorio();
      LiquidacionService.GetReintegro(this.LoteLiq)
      .then(response =>{
        if(response.Count > 0){
          this.CargarDataReintegro(response.Data[0]);
        }
        else{
          this.fechaNotaDebito = new Date();
          this.NroFacGenerado = '';
          this.rowReintegro.strNroFactura = '';
        }
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reintegro.');
      })
    }
  }
  clearDataCertificado(){
    this.rowCertificado.tipoLey = 'certificado';
    this.rowCertificado.intIdLaboratorio = '';
    this.rowCertificado.fechaCertif = ''
    this.rowCertificado.strLeyAuCertif = '';
    this.rowCertificado.strLeyComAnt = '';
    this.rowCertificado.strDiffCertif = '';
    this.rowCertificado.strPromCertif = '';
    this.rowCertificado.strLeyAuReintegro = '';
    this.rowCertificado.strLeyAgCertif = '';
    this.rowCertificado.strLeyAgComAnt = '';
    this.rowCertificado.strDiffAgCertif = '';
    this.rowCertificado.strPromAgCertif = '';
    this.rowCertificado.strLeyAgReintegro = '';
  }
  CargarDataReintegro(Data){
    this.fechaNotaDebito = (Data.dtmFechaFacturacion==null)? new Date() : new Date(Data.dtmFechaFacturacion)
    this.fechaNegociacionAu = (Data.dtmFechaPropuesta === null)? new Date() : new Date(Data.dtmFechaPropuesta);
    this.fechaNegociacionAg = (Data.dtmFechaPropuesta === null)? new Date() : new Date(Data.dtmFechaPropuesta);
    this.fechaLiqMargen = (Data.dtmFechaLiquidacion === null)? new Date() : new Date(Data.dtmFechaLiquidacion);
    this.NroFacGenerado = (Data.strNroFactura == null || Data.strNroFactura == '')? '' : Data.strNroFactura;
    this.rowCertificado.intIdLaboratorio = (this.rowReintegro.intIdLaboratorio==-1)?'':this.rowReintegro.intIdLaboratorio;
    this.fechaCertificado = (this.rowReintegro.strFechaCertificado==''||this.rowReintegro.strFechaCertificado==null)?'':this.rowReintegro.strFechaCertificado+'T00:00:00';
    this.rowCertificado.tipoLey = this.rowReintegro.strTipoCertificado;
    if(this.rowCertificado.tipoLey === 'certificado'){
      this.rowCertificado.strLeyAuCertif = this.rowReintegro.strLeyCertificado;
      this.rowCertificado.strLeyAgCertif = this.rowReintegro.strLeyCertificadoAG;
      this.changeLeyCertificado();
      this.changeLeyAgCertificado();
    }
    else{
      this.rowCertificado.strLeyAuDirimencia = this.rowReintegro.strLeyDirimencia;
      this.rowCertificado.strLeyAgDirimencia = this.rowReintegro.strLeyDirimenciaAG;
      this.changeLeyAuDirimencia();
      this.changeLeyAgDirimencia();
    }
    if(Data.strEstadoLiq === 'Reintegro'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = false;
    }
    else {
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
    }
    this.rowReintegro.strLeyAuLiq = Data.strLeyAuLiq;
    this.rowReintegro.strLeyAgLiq = Data.strLeyAgLiq;
    this.rowReintegro.strPorcRecup = Data.strPorcRecup;
    this.rowReintegro.strPrecioAu = Data.strPrecioAu;
    this.rowReintegro.strMargenPI = Data.strMargenPI;
    this.rowReintegro.strMaquila = Data.strMaquila;
    this.rowReintegro.strConsumoQ = Data.strConsumoQ;
    this.rowReintegro.strGastAdm = Data.strGastAdm;
    this.rowReintegro.strRecAg = Data.strRecAg;
    this.rowReintegro.strPrecioAg = Data.strPrecioAg;
    this.rowReintegro.strMargenAg = Data.strMargenAg;
    this.rowReintegro.strRecIntAu = Data.strRecIntAu;
    this.rowReintegro.strMaquilaIntAu = Data.strMaquilaIntAu;
    this.rowReintegro.strConsQIntAu = Data.strConsQIntAu;
    this.rowReintegro.strGastAdmIntAu = Data.strGastAdmIntAu;
    this.rowReintegro.strRecIntAg = Data.strRecIntAg;
    this.rowReintegro.strNroFactura = Data.strNroFactura;
    this.rowReintegro.dtmFechaFacturacion = Data.dtmFechaFacturacion;
    this.rowReintegro.strPersonaPropuesta = Data.strPersonaPropuesta;
    this.rowReintegro.strPersonaLiquidado = Data.strPersonaLiquidado;
    this.guardarViewButton = true;//(Data.chrEnableEdit === 'T' || this.userDec.strSuperAdmin == 'true')? true : false;
    this.changeCostoTotal();
  }

  handleParametros(index, row){
    this.ParametrosZona = this.ParametrosDefault;
    var Data = {
      strZona : row.strProcedenciaSt,
      strProveedor : row.strClienteRazonS
    }
    LiquidacionService.GetParametros(Data)
    .then(response =>{
      if(response.Count === 0) {
        this.ParametrosZona = this.ParametrosDefault;
      }
      else {
        this.ParametrosZona = response.Data[0];
      }
      this.dataMargen[0].TMH = this.ParametrosZona.strFactorConv;
      // this.handlePropuestaAU(index, row);
      // this.handlePropuestaAG(index, row);
      // this.handleMargen(index, row);
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener parametros.');
    })
  }
  getDataLaboratorio(){
    LiquidacionService.GetLaboratorios()
    .then(response =>{
      this.dataLaboratorios = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener laboratorios.');
    })
  }
  ActualizarReintegro(rowEdit){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando reintegro...'
    });
    this.GuardarLeyesCertificado(rowEdit);
    var loadPropAU = this.handlePropuestaAU(-1, this.rowReintegro);
    var loadPropAU = this.handlePropuestaAG(-1, this.rowReintegro);
    var splitM = this.dataMargen[9].GastAdm.split('%');
    var _margenbru = Number(splitM[0].trim())/100;

    var FormUpdate = {
      intIdRegistro : (rowEdit.intIdRegistro === null)? '' : rowEdit.intIdRegistro,
      strTipoLiquid: 'REI',
      strLeyAuLiq : (rowEdit.strLeyAuLiq === null)? '': rowEdit.strLeyAuLiq,
      strLeyAgLiq : (rowEdit.strLeyAgLiq === null)? '': rowEdit.strLeyAgLiq,
      strLeyAuMargen : (rowEdit.strLeyAuMargen === null)? '' : rowEdit.strLeyAuMargen,
      strPorcRecup : (rowEdit.strPorcRecup === null)? '' : rowEdit.strPorcRecup,
      strPrecioAu : (rowEdit.strPrecioAu === null)? '' : rowEdit.strPrecioAu,
      strMargenPI : (rowEdit.strMargenPI === null)? '' : rowEdit.strMargenPI,
      strMaquila : (rowEdit.strMaquila === null)? '' : rowEdit.strMaquila,
      strConsumoQ : (rowEdit.strConsumoQ === null)? '' : rowEdit.strConsumoQ,
      strGastAdm : (rowEdit.strGastAdm === null)? '' : rowEdit.strGastAdm,
      strRecAg : (rowEdit.strRecAg === null)? '' : rowEdit.strRecAg,
      strPrecioAg : (rowEdit.strPrecioAg === null)? '' : rowEdit.strPrecioAg,
      strMargenAg : (rowEdit.strMargenAg === null)? '' : rowEdit.strMargenAg,
      fltCostoTotal : (rowEdit.fltCostoTotal === null)? 0 : rowEdit.fltCostoTotal,
      strCalculoEspec : this.checkedCalculoEsp.toString(),
      strRecIntAu : (this.dataMargen[3].REC === null)? '' : this.dataMargen[3].REC,
      strMaquilaIntAu : (this.dataMargen[3].Maquila === null)? '' : this.dataMargen[3].Maquila,
      strConsQIntAu : (this.dataMargen[3].ConsQ === null)? '' : this.dataMargen[3].ConsQ,
      strGastAdmIntAu : (this.dataMargen[3].GastAdm === null)? '' : this.dataMargen[3].GastAdm,
      strRecIntAg : (this.dataMargen[6].REC === null)? '' : this.dataMargen[6].REC,
      strEstadoLiq: 'Reintegro',
      strPorcImporte: (isNaN(this.dataMargen[10].Zona)||this.dataMargen[10].Zona==undefined||this.dataMargen[10].Zona==null)?'':this.dataMargen[10].Zona,
      strNBaseAu: (isNaN(this.dataPropuestaAU[6].CostoIntegro)||this.dataPropuestaAU[6].CostoIntegro==undefined||this.dataPropuestaAU[6].CostoIntegro==null)?'':this.dataPropuestaAU[6].CostoIntegro.toString(),
      strNBaseAg: (isNaN(this.dataPropuestaAG[6].CostoIntegro)||this.dataPropuestaAG[6].CostoIntegro==undefined||this.dataPropuestaAG[6].CostoIntegro==null)?'':this.dataPropuestaAG[6].CostoIntegro.toString(),
      strMargenBru: isNaN(_margenbru) ? '' : _margenbru.toString(),//(this.dataMargen[9].GastAdm==undefined||this.dataMargen[9].GastAdm==null)?'':this.dataMargen[9].GastAdm.toString(),
      strUtilidadBru: (isNaN(this.dataMargen[10].GastAdm)||this.dataMargen[10].GastAdm==undefined||this.dataMargen[10].GastAdm==null)?'':this.dataMargen[10].GastAdm.toString(),
      strUsuarioCrea : this.userDec.strUsuario
    }
    if(FormUpdate.strEstadoLiq == 'Reintegro'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = false;
      this.statusLiquid = 'Reintegro'
    }
    else{
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
    }
    this.rowReintegro.strPersonaPropuesta = this.userDec.strNombre;
    this.rowReintegro.strPersonaLiquidado = this.userDec.strNombre;
    //this.guardarViewButton = (this.userDec.strSuperAdmin != 'true') ? false : true;
    LiquidacionService.PutReintegro(FormUpdate)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar liquidación:"+response);
      else if(response === -2) this.openMessage("Se actualizó correctamente.");
      else this.openMessage('Liquidación guardada correctamente.');
      this.$emit('guardarReintegro','Reintegro Guardado');
      loadingInstance.close();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar liquidación.');
    })
  }
  GuardarLeyesCertificado(rowEdit){
    var fCertif = (this.fechaCertificado===null||this.fechaCertificado===undefined||this.fechaCertificado.toString()=='')?'':GLOBAL.getParseDate(this.fechaCertificado);
    let idLab = ((this.rowCertificado.intIdLaboratorio==''||this.rowCertificado.intIdLaboratorio==null)?-1 : this.rowCertificado.intIdLaboratorio);
    var query: string = '';
    if(this.rowCertificado.tipoLey === 'certificado'){
      query = "UPDATE tblRegistro SET intIdLaboratorio="+ idLab+
              ", strLeyCertificado = '"+this.rowCertificado.strLeyAuCertif+
              "', strLeyCertificadoAG ='"+this.rowCertificado.strLeyAgCertif+
              "', strFechaCertificado ='"+fCertif+
              "', strTipoCertificado ='"+this.rowCertificado.tipoLey+
              "' WHERE intIdRegistro = "+rowEdit.intIdRegistro;
    }
    else{
      query = "UPDATE tblRegistro SET intIdLaboratorio="+ ((this.rowCertificado.intIdLaboratorio==''||this.rowCertificado.intIdLaboratorio==null)?-1:this.rowCertificado.intIdLaboratorio)+
              ", strLeyDirimencia = '"+this.rowCertificado.strLeyAuDirimencia+
              "', strLeyDirimenciaAG ='"+this.rowCertificado.strLeyAgDirimencia+
              "', strFechaCertificado ='"+fCertif+
              "', strTipoCertificado ='"+this.rowCertificado.tipoLey+
              "' WHERE intIdRegistro = "+rowEdit.intIdRegistro;
    }
    var DataQ = {
      strQuery: query,
      strUsuarioCrea: this.userDec.strUsuario
    }
    QueryService.ExecuteQuery(DataQ)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al guardar.')
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar leyes.');
    })
  }

  //||=============================================||
  //||                PROPUESTA AU                 ||
  //||=============================================||
  handlePropuestaAU(index, row){
    this.dataPropuestaAU[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAu);
    this.cargarDataPropuestaAu();
  }
  cargarDataPropuestaAu(){
    this.ShowCostoAMineral = true;
    this.dataPropuestaAU[2].Lote = this.rowReintegro.strLote;
    this.dataPropuestaAU[2].FechaRec = GLOBAL.getDateFormat(this.rowReintegro.dtmFechaRecepcion);
    this.dataPropuestaAU[2].TMH = this.rowReintegro.fltPesoTmh;
    this.dataPropuestaAU[2].PorcH2O = this.rowReintegro.strLeyAuMargen;
    this.dataPropuestaAU[2].TMS = this.PSecoTMS;
    this.dataPropuestaAU[2].LeyOz = this.rowReintegro.strLeyAuLiq;
    this.dataPropuestaAU[2].PorcRecup = this.rowReintegro.strPorcRecup;
    this.dataPropuestaAU[2].PrecioInt = this.rowReintegro.strPrecioAu;
    this.dataPropuestaAU[2].Maquila = this.rowReintegro.strMaquila;
    this.dataPropuestaAU[2].ConsumoKgTm = (this.TipoConsumo=='si')
      ? this.getNumberFix((Number(this.rowReintegro.strConsumoQ) + Number(this.rowReintegro.strGastAdm))/7, 2)
      : this.getNumberFix(Number(this.rowReintegro.strConsumoQ) + Number(this.rowReintegro.strGastAdm), 2);

    this.changeVerCostoAnalisis();
  }
  changeVerCostoAnalisis(){
    var _leyoz = Number(this.rowReintegro.strLeyAuLiq), _porcrec=Number(this.rowReintegro.strPorcRecup),
        _precioint = Number(this.rowReintegro.strPrecioAu), _margenpi=Number(this.rowReintegro.strMargenPI),
        _consq = Number(this.rowReintegro.strConsumoQ), _gastadm = Number(this.rowReintegro.strGastAdm),
        _maqui = Number(this.rowReintegro.strMaquila);

    if(this.checkedCalculoEsp === true)
      this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)*1.1023 -_maqui-_consq-_gastadm), 2);
    else
      this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)-_maqui-_consq-_gastadm)*1.1023, 2);

    this.dataPropuestaAU[2].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[2].CostoTM * Number(this.PSecoTMS), 2);

    this.dataPropuestaAU[6].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[2].CostoIntegro - Number(this.dataPropuestaAU[4].CostoIntegro), 2); //NBASE
    this.dataPropuestaAU[7].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[6].CostoIntegro * 0.18, 2); //IGV
    this.dataPropuestaAU[8].CostoIntegro = this.getNumberFix(Number(this.dataPropuestaAU[6].CostoIntegro) + Number(this.dataPropuestaAU[7].CostoIntegro), 2);//TOTAL
  }

  //||=============================================||
  //||                PROPUESTA AG                 ||
  //||=============================================||
  handlePropuestaAG(index, row){
    this.dataPropuestaAG[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAg);
    this.cargarDataPropuestaAg();
  }
  cargarDataPropuestaAg(){
    this.dataPropuestaAG[2].Lote = this.rowReintegro.strLote;
    this.dataPropuestaAG[2].FechaRec = GLOBAL.getDateFormat(this.rowReintegro.dtmFechaRecepcion);
    this.dataPropuestaAG[2].TMH = this.rowReintegro.fltPesoTmh;
    this.dataPropuestaAG[2].PorcH2O = this.rowReintegro.strLeyAuMargen;
    this.dataPropuestaAG[2].TMS = this.PSecoTMS;//this.rowReintegro.strPSecoTM;
    this.dataPropuestaAG[2].LeyOz = this.rowReintegro.strLeyAgLiq;
    this.dataPropuestaAG[2].PorcRecup = this.rowReintegro.strRecAg;
    this.dataPropuestaAG[2].PrecioInt = this.rowReintegro.strPrecioAg;

    var _leyagcom = Number(this.rowReintegro.strLeyAgLiq), _recag=Number(this.rowReintegro.strRecAg),
        _precioag = Number(this.rowReintegro.strPrecioAg), _margenag=Number(this.rowReintegro.strMargenAg);

    if(this.rowReintegro.strProcedenciaSt === 'TRUJILLO')
      this.dataPropuestaAG[2].CostoTM = this.getNumberFix(_leyagcom * _recag/100 *(_precioag - _margenag)*1.1023, 2);
    else
      this.dataPropuestaAG[2].CostoTM = this.getNumberFix(_leyagcom * _recag/100 *(_precioag - _margenag), 2);
    this.dataPropuestaAG[2].CostoIntegro = this.getNumberFix(this.dataPropuestaAG[2].CostoTM * Number(this.PSecoTMS), 2);
    this.dataPropuestaAG[6].CostoIntegro = this.getNumberFix(Number(this.dataPropuestaAG[2].CostoIntegro) - Number(this.dataPropuestaAG[4].CostoIntegro), 2);
    this.dataPropuestaAG[7].CostoIntegro = this.getNumberFix(this.dataPropuestaAG[6].CostoIntegro * 0.18, 2);
    this.dataPropuestaAG[8].CostoIntegro = this.getNumberFix(Number(this.dataPropuestaAG[6].CostoIntegro) + Number(this.dataPropuestaAG[7].CostoIntegro), 2);
  }


  //||=============================================||
  //||                   MARGEN                    ||
  //||=============================================||
  handleMargen(index, row){
    this.cargarDataMargen();
    this.dataMargen[0].FechaRec = GLOBAL.getDateFormat(this.fechaLiqMargen); //X
  }

  cargarDataMargen(){
    this.dataMargen[1].Lote = this.rowReintegro.strLote;
    this.dataMargen[2].FechaRec = GLOBAL.getDateFormat(this.rowReintegro.dtmFechaRecepcion);
    this.dataMargen[2].Zona = this.rowReintegro.strProcedenciaSt;
    this.dataMargen[2].Proveedor = this.rowReintegro.strClienteRazonS;
    this.dataMargen[2].TMH = this.rowReintegro.fltPesoTmh; //this.getNumberFix(this.rowReintegro.fltPesoTmh, 2);
    this.dataMargen[2].PorcH2O = this.rowReintegro.strLeyAuMargen;
    this.dataMargen[2].TMS = this.PSecoTMS;//this.rowReintegro.strPSecoTM; //this.getNumberFix(this.rowReintegro.strPSecoTM, 2);
    this.dataMargen[2].LeyOz = this.rowReintegro.strLeyAuLiq;
    this.dataMargen[2].REC = this.rowReintegro.strPorcRecup;
    this.dataMargen[2].Inter = this.rowReintegro.strPrecioAu;
    this.dataMargen[2].Margen = this.rowReintegro.strMargenPI;
    this.dataMargen[2].Maquila = this.rowReintegro.strMaquila;
    this.dataMargen[2].ConsQ = this.rowReintegro.strConsumoQ;
    this.dataMargen[2].GastAdm = this.rowReintegro.strGastAdm;
    //CALCULO DE FINOS
    if(this.rowReintegro.strProcedenciaSt === 'TRUJILLO'){
      this.dataMargen[2].Fino = this.getNumberFix(Number(this.dataMargen[2].TMS)*Number(this.dataMargen[2].LeyOz)*34.285, 2);
    }

    var _leyozau=Number(this.dataMargen[2].LeyOz), _rec=Number(this.dataMargen[2].REC), _inter=Number(this.dataMargen[2].Inter),
        _margen=Number(this.dataMargen[2].Margen), _maqui=Number(this.dataMargen[2].Maquila), _consQ=Number(this.dataMargen[2].ConsQ),
        _gastadm=Number(this.dataMargen[2].GastAdm);
    if(this.checkedCalculoEsp === true)
      this.dataMargen[2].PrecioxTMS = this.getNumberFix((_leyozau*_rec/100*(_inter - _margen)*1.1023 - _maqui - _consQ - _gastadm), 2);
    else
      this.dataMargen[2].PrecioxTMS = this.getNumberFix((_leyozau*_rec/100*(_inter - _margen)- _maqui - _consQ - _gastadm)*1.1023, 2);
    this.dataMargen[2].Importe = this.getNumberFix(Number(this.dataMargen[2].TMS) * Number(this.dataMargen[2].PrecioxTMS) ,2);

    //Liq Int(Au)
    this.changeLiqIntAu();
    //Propuesta AG
    this.changeMargenAg();
  }
  changeLiqIntAu(){
    this.rowReintegro.strRecIntAu = this.dataMargen[3].REC;
    this.rowReintegro.strMaquilaIntAu = this.dataMargen[3].Maquila;
    this.rowReintegro.strConsQIntAu = this.dataMargen[3].ConsQ;
    this.rowReintegro.strGastAdmIntAu = this.dataMargen[3].GastAdm;
    this.rowReintegro.strRecIntAg = this.dataMargen[6].REC;

    this.dataMargen[3].TMH = (this.checkedCalculoEsp===true)
      ? this.dataMargen[2].TMH
      : this.getNumberFix(this.dataMargen[2].TMH/(this.ParametrosZona.fltTmsLiqMax), 3); //: this.getNumberFix(this.dataMargen[2].TMH/0.99, 3);
    this.dataMargen[3].PorcH2O = Number(this.rowReintegro.strPorcH2O);
    this.dataMargen[3].TMS = this.getNumberFix((this.dataMargen[3].TMH - this.dataMargen[3].PorcH2O*this.dataMargen[3].TMH/100), 3);
    this.dataMargen[3].LeyOz = Math.max(Number(this.rowReintegro.strLeyAuOzTc),Number(this.rowReintegro.strLeyAuREE)); //Add REE
    this.dataMargen[3].Inter = this.dataMargen[2].Inter;
    var _rec3=Number(this.dataMargen[3].REC), _leyozau3=Number(this.dataMargen[3].LeyOz), _inter3=Number(this.dataMargen[3].Inter),
        _factconv=Number(this.dataMargen[0].TMH), _maquila3 = Number(this.dataMargen[3].Maquila), _consq3=Number(this.dataMargen[3].ConsQ),
        _gastadm3=Number(this.dataMargen[3].GastAdm);
    this.dataMargen[3].PrecioxTMS = this.getNumberFix((_rec3 * _leyozau3 * _inter3/100)*_factconv - _maquila3 - _consq3 - _gastadm3, 2);
    this.dataMargen[3].Importe = this.getNumberFix(Number(this.dataMargen[3].PrecioxTMS) * Number(this.dataMargen[3].TMS), 2);

    //CALCULAR FINOS INT
    if(this.rowReintegro.strProcedenciaSt === 'TRUJILLO'){
      this.dataMargen[3].Fino = this.getNumberFix(Number(this.dataMargen[3].TMS)*Number(this.dataMargen[3].LeyOz)*34.285, 2);
    }
    this.changeMargenUtilidad()
  }
  changeMargenAg(){
    //Propuesta AG
    this.dataMargen[5].TMH = Number(this.dataMargen[2].TMH);
    this.dataMargen[5].PorcH2O = this.getNumberFix(Number(this.dataMargen[2].PorcH2O), 2);
    this.dataMargen[5].TMS = this.getNumberFix(Number(this.dataMargen[5].TMH) - Number(this.dataMargen[5].PorcH2O) * Number(this.dataMargen[5].TMH)/100, 3);
    this.dataMargen[5].REC = Number(this.rowReintegro.strRecAg);
    this.dataMargen[5].LeyOz = Number(this.rowReintegro.strLeyAgLiq);
    this.dataMargen[5].Inter = Number(this.rowReintegro.strPrecioAg);
    this.dataMargen[5].Margen = Number(this.rowReintegro.strMargenAg);
    this.dataMargen[5].PrecioxTMS = this.getNumberFix(Number(this.dataMargen[5].LeyOz)*Number(this.dataMargen[5].REC)*(Number(this.dataMargen[5].Inter)-Number(this.dataMargen[5].Margen))/100, 2);
    this.dataMargen[5].Importe = this.getNumberFix(Number(this.dataMargen[5].PrecioxTMS) * Number(this.dataMargen[5].TMS), 2);
    //Liq Int AG
    this.dataMargen[6].TMH = Number(this.dataMargen[3].TMH); //this.getNumberFix(Number(this.dataMargen[3].TMH), 2);
    this.dataMargen[6].PorcH2O = this.getNumberFix(Number(this.dataMargen[3].PorcH2O), 2);
    this.dataMargen[6].TMS = this.getNumberFix(Number(this.dataMargen[6].TMH) - Number(this.dataMargen[6].PorcH2O) * Number(this.dataMargen[6].TMH/100), 3);
    this.dataMargen[6].LeyOz = Number(this.rowReintegro.strLeyAgOzTc);
    this.dataMargen[6].Inter = this.dataMargen[5].Inter;
    this.dataMargen[6].PrecioxTMS = this.getNumberFix(Number(this.dataMargen[6].LeyOz) *Number(this.dataMargen[6].REC) *Number(this.dataMargen[6].Inter)*Number(this.dataMargen[0].TMH) /100, 2);
    this.dataMargen[6].Importe = this.getNumberFix(Number(this.dataMargen[6].PrecioxTMS) * Number(this.dataMargen[6].TMS) * 0.935, 2);
    this.changeMargenUtilidad();
  }
  changeMargenUtilidad(){
    var _inter=Number(this.dataMargen[3].Inter), _leyozau=Number(this.dataMargen[3].LeyOz), _rec=Number(this.dataMargen[3].REC),
        _tms=Number(this.dataMargen[3].TMS), _importe=Number(this.dataMargen[2].Importe), _maquila=Number(this.dataMargen[3].Maquila),
        _consQ=Number(this.dataMargen[3].ConsQ), _gastadm=Number(this.dataMargen[3].GastAdm), _tms=Number(this.dataMargen[3].TMS),
        _factconv=Number(this.dataMargen[0].TMH)
    //Margen Au
    this.dataMargen[10].PorcH2O = this.getNumberFix((_inter* _leyozau* _rec* _factconv/100 *0.985 *_tms) - _importe - (_maquila+_consQ+_gastadm)* _tms, 2);
    var margen_bruAu = this.dataMargen[10].PorcH2O/(_inter * _leyozau * _rec /100 * _factconv * 0.985 * _tms);
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
    this.dataMargen[9].PorcH2O = formatter.format(margen_bruAu);
    //Margen Ag
    this.dataMargen[10].Inter = this.getNumberFix(Number(this.dataMargen[6].Importe) - Number(this.dataMargen[5].Importe), 2);
    var margen_bruAg = Number(this.dataMargen[10].Inter) / Number(this.dataMargen[6].Importe);
    this.dataMargen[9].Inter = formatter.format(margen_bruAg);
    //Margen x Lote
    this.dataMargen[10].GastAdm = this.getNumberFix(Number(this.dataMargen[10].PorcH2O) + Number(this.dataMargen[10].Inter), 2);
    var margen_bruLote = Number(this.dataMargen[10].GastAdm) / (Number(this.dataMargen[6].Importe) + (_inter * _leyozau * _rec * _factconv /100 * 0.985 * _tms));
    this.dataMargen[9].GastAdm = formatter.format(margen_bruLote);
    //Porcentage
    var Aux_PrecioxTMS = this.getNumberFix((Number(this.dataMargen[3].REC) * Number(this.dataMargen[3].LeyOz) * Number(this.dataMargen[3].Inter)/100)*_factconv, 2);
    var Aux_Importe = this.getNumberFix(Number(Aux_PrecioxTMS) * Number(this.dataMargen[3].TMS), 2);
    this.dataMargen[9].Zona = Aux_Importe;
    this.dataMargen[10].Zona = this.getNumberFix(Number(this.dataMargen[2].Importe)/Number(Aux_Importe), 2);
  }

  changeLeyCertificado(){
    if(Number(this.rowCertificado.strLeyAuCertif) >= Number(this.rowCertificado.strLeyComAnt)){
      this.showErrorCetif = false;
      this.rowCertificado.strDiffCertif = this.getNumberFix(Math.abs(Number(this.rowCertificado.strLeyAuCertif) - Number(this.rowCertificado.strLeyComAnt)), 3);
    }
    else{
      this.showErrorCetif = (this.rowCertificado.strLeyAuCertif === '')? false : true;
      this.rowCertificado.strDiffCertif = '0'
    }
    this.rowCertificado.strPromCertif = (Number(this.rowCertificado.strDiffCertif)/2).toString();
    let _leyLiq = this.getNumberFix(Number(this.rowCertificado.strLeyComAnt) + Number(this.rowCertificado.strPromCertif), 4);
    this.rowCertificado.strLeyAuReintegro = (GLOBAL.truncateNumber(Number(_leyLiq), 3)).toString();

    this.rowReintegro.strLeyAuLiq = this.rowCertificado.strLeyAuReintegro;
    this.changeCostoTotal();
  }
  changeLeyAgCertificado(){
    if(Number(this.rowCertificado.strLeyAgCertif) >= Number(this.rowCertificado.strLeyAgComAnt)){
      this.showErrorCetifAG = false;
      this.rowCertificado.strDiffAgCertif = this.getNumberFix(Math.abs(Number(this.rowCertificado.strLeyAgCertif) - Number(this.rowCertificado.strLeyAgComAnt)), 3);
    }
    else{
      this.showErrorCetifAG = (this.rowCertificado.strLeyAgCertif === '')? false : true;
      this.rowCertificado.strDiffAgCertif = '0';
    }
    this.rowCertificado.strPromAgCertif = (Number(this.rowCertificado.strDiffAgCertif)/2).toString();
    let leyAgLiq = this.getNumberFix(Number(this.rowCertificado.strLeyAgComAnt) + Number(this.rowCertificado.strPromAgCertif), 4);
    this.rowCertificado.strLeyAgReintegro = (GLOBAL.truncateNumber(Number(leyAgLiq), 3)).toString();
    this.rowReintegro.strLeyAgLiq = this.rowCertificado.strLeyAgReintegro;
  }
  changeCostoTotal(){
    var row = this.rowReintegro;
    var LeyAuCom: number=(row.strLeyAuLiq===null || row.strLeyAuLiq==='') ? 0 : Number(row.strLeyAuLiq.trim()),
        PorcRec: number=(row.strPorcRecup===null || row.strPorcRecup==='') ? 0 : Number(row.strPorcRecup.trim()),
        PrecioAu: number=(row.strPrecioAu===null || row.strPrecioAu==='') ? 0 : Number(row.strPrecioAu.trim()),
        MargenPI: number=(row.strMargenPI===null || row.strMargenPI==='') ? 0 : Number(row.strMargenPI.trim()),
        Maquila: number=(row.strMaquila===null || row.strMaquila==='') ? 0 : Number(row.strMaquila.trim()),
        ConsumoQ: number=(row.strConsumoQ===null || row.strConsumoQ==='') ? 0 : Number(row.strConsumoQ.trim()),
        GastAdm: number=(row.strGastAdm===null || row.strGastAdm==='') ? 0 : Number(row.strGastAdm.trim()),
        PesoTM: number=(this.PSecoTMS===null || this.PSecoTMS==='') ? 0 : Number(this.PSecoTMS.trim())
    var CostoTM, Costo_Total;
    if(this.checkedCalculoEsp === true)
      CostoTM = this.getNumberFix((LeyAuCom*PorcRec/100 *(PrecioAu-MargenPI)*1.1023 -Maquila-ConsumoQ-GastAdm), 2);
    else
      CostoTM = this.getNumberFix((LeyAuCom*PorcRec/100 *(PrecioAu-MargenPI)-Maquila-ConsumoQ-GastAdm)*1.1023, 2);
    Costo_Total = CostoTM * PesoTM;
    this.rowReintegro.fltCostoTotal = Costo_Total.toFixed(2);
  }
  changeTipoConsumo(){
    if(this.TipoConsumo === 'si'){
      this.dataPropuestaAU[1].ConsumoKgTm = "Consumo(Kg/Tm)";
      this.dataPropuestaAU[2].ConsumoKgTm = this.getNumberFix((Number(this.rowReintegro.strConsumoQ) + Number(this.rowReintegro.strGastAdm))/7, 2);
    }
    else{
      this.dataPropuestaAU[1].ConsumoKgTm = "Consumo($/Tm)";
      this.dataPropuestaAU[2].ConsumoKgTm = this.getNumberFix(Number(this.rowReintegro.strConsumoQ) + Number(this.rowReintegro.strGastAdm), 2);
    }
  }
  changeLeyMaquilaTM(){
    if(this.ShowLeyMaquilaTM === true){
      this.dataPropuestaAU[1].LeyOz = "Ley(Oz/Tm)";
      this.dataPropuestaAU[2].LeyOz = this.getNumberFix((Number(this.rowReintegro.strLeyAuLiq) * 1.1023), 3);
      this.dataPropuestaAU[1].Maquila = "Maquila";
      this.dataPropuestaAU[2].Maquila = this.getNumberFix((Number(this.rowReintegro.strMaquila) * 1.1023), 2);
    }
    else{
      this.dataPropuestaAU[1].LeyOz = "Ley(Oz/Tc)";
      this.dataPropuestaAU[2].LeyOz = this.rowReintegro.strLeyAuLiq;
      this.dataPropuestaAU[1].Maquila = "Maquila ($)";
      this.dataPropuestaAU[2].Maquila = this.rowReintegro.strMaquila;
    }
  }
  changeLeyTMAG(){
    if(this.ShowLeyTMAG === true){
      this.dataPropuestaAG[1].LeyOz = "Ley Ag(Oz/Tm)";
      this.dataPropuestaAG[2].LeyOz = this.getNumberFix((Number(this.rowReintegro.strLeyAgLiq) * 1.1023), 3);
    }
    else{
      this.dataPropuestaAG[1].LeyOz = "Ley Ag(Oz/Tc)";
      this.dataPropuestaAG[2].LeyOz = this.rowReintegro.strLeyAgLiq;
    }
  }
  changeLeyAuDirimencia(){
    if(Number(this.rowCertificado.strLeyAuDirimencia) >= Number(this.rowCertificado.strLeyComAnt)){
      this.showErrorDirimAU = false;
      this.rowCertificado.strLeyAuReintegro = this.rowCertificado.strLeyAuDirimencia;
    }
    else{
      this.showErrorDirimAU = (this.rowCertificado.strLeyAuDirimencia === '')? false : true;
      this.rowCertificado.strLeyAuReintegro = this.rowCertificado.strLeyComAnt;
    }
    this.rowReintegro.strLeyAuLiq = this.rowCertificado.strLeyAuReintegro;
    this.changeCostoTotal();
  }
  changeLeyAgDirimencia(){
    if(Number(this.rowCertificado.strLeyAgDirimencia) >= Number(this.rowCertificado.strLeyAgComAnt)){
      this.showErrorDirimAG = false;
      this.rowCertificado.strLeyAgReintegro = this.rowCertificado.strLeyAgDirimencia;
    }
    else {
      this.showErrorDirimAG = (this.rowCertificado.strLeyAgDirimencia === '')? false :  true;
      this.rowCertificado.strLeyAgReintegro = this.rowCertificado.strLeyAgComAnt;
    }
    this.rowReintegro.strLeyAgLiq = this.rowCertificado.strLeyAgReintegro;
    this.changeCostoTotal();
  }
  changePropAu(){
    this.rowReintegro.strPorcRecup = this.dataMargen[2].REC;
    this.rowReintegro.strMargenPI = this.dataMargen[2].Margen;
    this.rowReintegro.strMaquila = this.dataMargen[2].Maquila;
    this.rowReintegro.strGastAdm = this.dataMargen[2].GastAdm;
    this.changeCostoTotal()
    this.cargarDataMargen();
  }

  //||=============================================||
  //||                NOTA DEBITO                  ||
  //||=============================================||
  GuardarNotaDebito(nroNotaDebito){
    if(nroNotaDebito==='' || nroNotaDebito===null)
      this.openMessageError('Ingrese N° Nota de Debito');
    else {
      var query = "UPDATE tblLiquidacion SET strNroFactura = '"+nroNotaDebito+
                  "', dtmFechaFacturacion = '"+GLOBAL.getParseDate(this.fechaNotaDebito)+
                  "' WHERE intIdRegistro = "+this.rowReintegro.intIdRegistro+
                  " AND strTipoLiquid='REI'"
      var DataQ = {
        strQuery: query,
        strUsuarioCrea: this.userDec.strUsuario
      }
      QueryService.ExecuteQuery(DataQ)
      .then(response =>{
        if(response == -1) this.openMessageError('SERVICE: Error al guardar.')
        else this.openMessage('Se guardo correctamente.');
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al guardar.');
      })
    }
  }
  changeNroFactura(){
    var nFact = this.rowReintegro.strNroFactura;
    if(this.checkedFacElect == false) {
      var split = nFact.split('-');
      var f1 = this.generateNumber(split[0], 4);
      var f2 = (split[1]==undefined)? '' : this.generateNumber(split[1], 7);
      this.NroFacGenerado = f1+'-'+f2;

      if(nFact.length == 4 && nFact.indexOf('-') == -1)
        this.rowReintegro.strNroFactura = this.rowReintegro.strNroFactura + '-';
    }
    else {
      var Letter = nFact.substring(0, 1);
      var factura =  nFact.substring(1, nFact.length);
      var split = factura.split('-');
      var f1 = this.generateNumber(split[0], 3);
      this.NroFacGenerado = Letter.toUpperCase() + f1 + '-' + ((split[1]==undefined)? '':split[1]);

      if(nFact.length == 4 && nFact.indexOf('-') == -1)
        this.rowReintegro.strNroFactura = this.rowReintegro.strNroFactura + '-';
    }
  }
  generateNumber(number, len) {
    var aux = '0000000000';
    var num1 = (aux+number).slice(-len);
    return num1;
  }
  handleBlur(){
    this.rowReintegro.strNroFactura = this.NroFacGenerado;
  }

  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }
  handleTabClick(tab, event) {
    if(tab.name=="first"){
      this.changeCostoTotal();
    }
    if(tab.name=='second'){
      this.handlePropuestaAU(0, this.rowReintegro);
    }
    if(tab.name=="third"){
      this.handlePropuestaAG(0, this.rowReintegro);
    }
    if(tab.name=="fourth"){
      this.handleMargen(0, this.rowReintegro);
    }
  }

  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  parseNumberFix(number, fix:number): number{
    var num: number =  Number(parseFloat(number).toFixed(fix));
    return num;
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  CerrarReintegro(){
    this.$emit('cerrarReintegro','Close Dialog');
  }
  ExportarMargenPDF(){
    var Zona = this.rowReintegro.strProcedenciaSt;
    var table:any = this.dataMargen;
    var doc = new jsPDF('l', 'pt', 'letter', true);
    doc.setFontType('bold')
    doc.setFontSize(14);
    doc.text(30, 30, 'Liquidación Margen');
    doc.line(30, 35, 765, 35)
    doc.setFontType('normal')
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(i===1 || i ===8){
        doc.setFontSize(7);
        doc.setFontType('bold')
      }
      else{
        doc.setFontType('normal')
        doc.setFontSize(6);
      }
      doc.cell(15,50,54,15,(item.Lote === null)?'':item.Lote.toString(), i);
      doc.cell(15,50,42,15,(item.FechaRec === null)?'':item.FechaRec.toString(), i);
      doc.cell(15,50,52,15,(item.Zona === null)?'':item.Zona.toString(), i);
      doc.cell(15,50,130,15,(item.Proveedor === null)?'':item.Proveedor.toString(), i);
      doc.cell(15,50,37,15,(item.TMH === null)?'':item.TMH.toString(), i);
      doc.cell(15,50,29,15,(item.PorcH2O === null)?'':item.PorcH2O.toString(), i);
      doc.cell(15,50,26,15,(item.TMS === null)?'':item.TMS.toString(), i);
      doc.cell(15,50,22,15,(item.REC === null)?'':item.REC.toString(), i);
      doc.cell(15,50,41,15,(item.LeyOz === null)?'':item.LeyOz.toString(), i);
      doc.cell(15,50,30,15,(item.Inter === null)?'':item.Inter.toString(), i);
      if(i > 0){
        doc.cell(15,50,39,15,(item.Margen === null)?'':item.Margen.toString(), i);
        doc.cell(15,50,40,15,(item.Maquila === null)?'':item.Maquila.toString(), i);
        doc.cell(15,50,57,15,(item.ConsQ === null)?'':item.ConsQ.toString(), i);
      }
      doc.cell(15,50,45,15,(item.GastAdm === null)?'':item.GastAdm.toString(), i);
      doc.cell(15,50,55,15,(item.PrecioxTMS === null)?'':item.PrecioxTMS.toString(), i);
      doc.cell(15,50,40,15,(item.Importe === null)?'':item.Importe.toString(), i);
      if(Zona === 'TRUJILLO'){
        doc.cell(15,50,30,15,(item.Fino === null)?'':item.Fino.toString(), i);
      }
      i++;
    });
    doc.save('Liq_Margen_'+this.rowReintegro.strLote+'.pdf');
  }
  ExportarPropuestaAuPDF(){
    var VerConsumo = this.ShowConsumo;
    var verCostoAnalisis = this.ShowCostoAMineral;
    var PersonaProp = (this.rowReintegro.strPersonaPropuesta==null||this.rowReintegro.strPersonaPropuesta=='') ? this.userDec.strNombre : this.rowReintegro.strPersonaPropuesta.trim();
    var table:any = this.dataPropuestaAU;
    var doc = new jsPDF('p', 'pt', 'letter', true);
    doc.setFontType('bold');
    doc.setFontSize(12);
    doc.text(30, 30, 'Propuesta Au');
    doc.line(30, 35, 580, 35);

    doc.setFontSize(9);
    doc.text(30, 50, 'Proveedor:');
    doc.text(350, 50, 'RUC:');
    doc.text(30, 70, 'Tipo Mineral:');
    doc.text(200, 70, 'N° Sacos:');
    doc.setFontType('normal');
    doc.text(90, 50, this.rowReintegro.strClienteRazonS);
    doc.text(385, 50, this.rowReintegro.strClienteRuc);
    doc.text(100, 70, this.rowReintegro.strMaterial);
    doc.text(250, 70, (this.rowReintegro.intNroSacos).toString());
    doc.text(155,310, 'La Joya Mining SAC', 'center');
    doc.text(455,310, 'Proveedor', 'center');
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(verCostoAnalisis == false && i===4){
        i++;
      }
      else {
        if(i===1 || i ===8){
          doc.setFontSize(7);
          doc.setFontType('bold')
        }
        else{
          doc.setFontType('normal')
          doc.setFontSize(7);
        }
        doc.cell(25,100,70,15,(item.Lote === null)?'': item.Lote.toString(), i);
        doc.cell(25,100,50,15,(item.FechaRec === null)?'': item.FechaRec.toString(), i);
        doc.cell(25,100,27,15,(item.TMH === null)?'': item.TMH.toString(), i);
        doc.cell(25,100,30,15,(item.PorcH2O === null)?'': item.PorcH2O.toString(), i);
        doc.cell(25,100,27,15,(item.TMS === null)?'': item.TMS.toString(), i);
        doc.cell(25,100,43,15,(item.LeyOz === null)?'': item.LeyOz.toString(), i);
        doc.cell(25,100,38,15,(item.PorcRecup === null)?'': item.PorcRecup.toString(), i);
        doc.cell(25,100,48,15,(item.PrecioInt === null)?'': item.PrecioInt.toString(), i);
        doc.cell(25,100,42,15,(item.Maquila === null)?'': item.Maquila.toString(), i);
        if(VerConsumo === true) doc.cell(25,100,60,15,(item.ConsumoKgTm === null)?'': item.ConsumoKgTm.toString(), i);
        doc.cell(25,100,75,15,(item.CostoTM === null)?'': item.CostoTM.toString(), i);
        if(i > 1) doc.setFontSize(8);
        doc.cell(25,100,56,15,(item.CostoIntegro === null)?'': item.CostoIntegro.toString(), i);
        i++;
      }
    });
    doc.line(80, 300, 230, 300);
    doc.text(155, 325, (PersonaProp).toUpperCase(),'center');
    doc.line(380, 300, 530, 300);
    doc.text(455, 325, this.rowReintegro.strClienteRazonS, 'center');
    doc.save('Liq_PropuestaAu_'+this.rowReintegro.strLote+'.pdf');
  }
  ExportarPropuestaAgPDF(){
    var PersonaProp = (this.rowReintegro.strPersonaPropuesta==null||this.rowReintegro.strPersonaPropuesta=='') ? this.userDec.strNombre : this.rowReintegro.strPersonaPropuesta.trim();
    var table:any = this.dataPropuestaAG;
    var doc = new jsPDF('p', 'pt', 'letter', true);
    doc.setFontType('bold');
    doc.setFontSize(12);
    doc.text(30, 30, 'Propuesta AG');
    doc.line(30, 35, 580, 35)

    doc.setFontSize(9);
    doc.text(30, 50, 'Proveedor:');
    doc.text(350, 50, 'RUC:');
    doc.text(30, 70, 'Tipo Mineral:');
    doc.text(200, 70, 'N° Sacos:');
    doc.setFontType('normal');
    doc.text(90, 50, this.rowReintegro.strClienteRazonS);
    doc.text(385, 50, this.rowReintegro.strClienteRuc);
    doc.text(100, 70, this.rowReintegro.strMaterial);
    doc.text(250, 70, (this.rowReintegro.intNroSacos).toString());
    doc.text(155,310, 'La Joya Mining SAC', 'center');
    doc.text(455,310, 'Proveedor', 'center');
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(i===1 || i ===8){
        doc.setFontSize(8);
        doc.setFontType('bold')
      }
      else{
        doc.setFontType('normal')
        doc.setFontSize(8);
      }
      doc.cell(30,100,75,15,(item.Lote === null)?'': item.Lote.toString(), i);
      doc.cell(30,100,55,15,(item.FechaRec === null)?'': item.FechaRec.toString(), i);
      doc.cell(30,100,40,15,(item.TMH === null)?'': item.TMH.toString(), i);
      doc.cell(30,100,37,15,(item.PorcH2O === null)?'': item.PorcH2O.toString(), i);
      doc.cell(30,100,35,15,(item.TMS === null)?'': item.TMS.toString(), i);
      doc.cell(30,100,60,15,(item.LeyOz === null)?'': item.LeyOz.toString(), i);
      doc.cell(30,100,50,15,(item.PorcRecup === null)?'': item.PorcRecup.toString(), i);
      doc.cell(30,100,55,15,(item.PrecioInt === null)?'': item.PrecioInt.toString(), i);
      doc.cell(30,100,80,15,(item.CostoTM === null)?'': item.CostoTM.toString(), i);
      doc.cell(30,100,70,15,(item.CostoIntegro === null)?'': item.CostoIntegro.toString(), i);
      i++;
    });
    doc.line(80, 300, 230, 300);
    doc.text(155, 325, (PersonaProp).toUpperCase(),'center');
    doc.line(380, 300, 530, 300);
    doc.text(455, 325, this.rowReintegro.strClienteRazonS, 'center');
    doc.save('Liq_PropuestaAg_'+this.rowReintegro.strLote+'.pdf');
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      dataMargen:[
        {Lote:'Fecha Liquidación',FechaRec:'',Zona:'',Proveedor:'Factor de Conversión',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[0]
        {Lote:'',FechaRec:'Fecha Rec.',Zona:'Zona',Proveedor:'Proveedor',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',REC:'REC',LeyOz:'Ley(Oz/Tc)',Inter:'Inter($)',Margen:'Margen($)',Maquila:'Maquila($)',ConsQ:'Cons Q(Kg/Tm)',GastAdm:'Gast Adm($)',PrecioxTMS:'PrecioxTMS($)',Importe:'Importe($)'}, //[1] Lote - Titulos
        {Lote:'Propuesta(Au)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[2] Propuesta (Au)
        {Lote:'Liq Int(Au)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[3] Liq Int(Au)
        {Lote:'',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[4] Vacio
        {Lote:'Propuesta(Ag)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[5] Propuesta (Ag)
        {Lote:'Liq Int(Ag)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[6] Liq Int(Ag)
        {Lote:'',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:''}, //[7] Vacio
        {Lote:'',FechaRec:'',Zona:'Importe',Proveedor:'',TMH:'',PorcH2O:'Au',TMS:'',REC:'',LeyOz:'',Inter:'Ag',Margen:'',Maquila:'',ConsQ:'',GastAdm:'Por Lote',PrecioxTMS:'',Importe:''}, //[8] Au - Ag - Por lote
        {Lote:'',FechaRec:'Liq Int(Au)',Zona:'',Proveedor:'',TMH:'Margen Bru',PorcH2O:'',TMS:'',REC:'',LeyOz:'Margen Bru',Inter:'',Margen:'',Maquila:'',ConsQ:'Margen Bru',GastAdm:'',PrecioxTMS:'',Importe:''}, //[9] Margen Bru
        {Lote:'',FechaRec:'%',Zona:'',Proveedor:'',TMH:'Utilidad Bru',PorcH2O:'',TMS:'',REC:'',LeyOz:'Utilidad Bru',Inter:'',Margen:'',Maquila:'',ConsQ:'Utilidad Bru',GastAdm:'',PrecioxTMS:'',Importe:''}, //[10] Utilidad Bru
      ],
      dataPropuestaAU:[
        {Lote:'Fecha Negociación',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''}, //Fecha Negociacion
        {Lote:'Lote',FechaRec:'Fecha Rec.',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',LeyOz:'Ley(Oz/Tc)',PorcRecup:'% Recup.',PrecioInt:'Precio Inter',Maquila:'Maquila ($)',ConsumoKgTm:'Consumo($/Tm)',CostoTM:'Costo/TM',CostoIntegro:'Costo Integro'}, //Cabeceras Tabla
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''}, //Datos
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'Negociación Anterior',CostoIntegro:''}, //Costo Anterior
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'NBASE',CostoIntegro:''}, //NBASE
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'IGV. 18%',CostoIntegro:''}, //IGV 18%
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'TOTAL',CostoIntegro:''} //Total
      ],
      dataPropuestaAG:[
        {Lote:'Fecha Negociación',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''}, //Fecha Negociacion
        {Lote:'Lote',FechaRec:'Fecha Rec.',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',LeyOz:'Ley Ag(Oz/Tc)',PorcRecup:'% Recup.',PrecioInt:'Precio Inter',CostoTM:'Costo/TM',CostoIntegro:'Costo Integro'}, //Cabeceras Tabla
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''}, //Datos
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'Negociación Anterior',CostoIntegro:''}, //Costo Anterior
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'NBASE',CostoIntegro:''}, //IGV 18%
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'IGV. 18%',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'TOTAL',CostoIntegro:''} //Total
      ],
      DataTipoConsumo: [
        {id:'no', name:'Consumo($/Tm)'},
        {id:'si', name:'Consumo(Kg/Tm)'},
      ],
      DataTipoLey: [
        {id:'certificado', name:'Certificado'},
        {id:'dirimencia', name:'Dirimencia'},
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {

    }
  }

}
