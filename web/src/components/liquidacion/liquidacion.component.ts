import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LiquidacionService from '@/services/liquidacion.service';
import { Loading } from 'element-ui';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import Reintegros from '@/components/liquidacion/Reintegros/reintegros.vue';
import ParametroService from '@/services/parametro.service';
import ZonaService from '@/services/zona.service';
import QueryService from '@/services/query.service';
import GastoAdmService from '@/services/GastoAdm.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';

@Component({
   name: 'liquidacion',
   components:{
     'download-excel' : JsonExcel,
     'reintegros': Reintegros,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class LiquidacionComponent extends Vue {
  userDec: any = {};
  CompleteData: any = [];
  gridData : any = [];
  countGridData = 0;
  FormSearch: any = {};
  fechaSearchLiq: any = '';
  fechaSearch: Date = new Date();
  blnfiltroavanzado:boolean=false;
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  rowSelectedEdit: any = {};
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  titleExcel:string ='Liquidacion_'+CONFIG.DATE_STRING+'.xls';

  dialogParametrosVisible: boolean = false;
  dialogParametrosPropuestaAu:boolean=false;
  dialogParametrosPropuestaAg:boolean=false;
  dialogParametrosMargen:boolean=false;
  dialogParametrosFactura:boolean=false;

  dialogReintegrosVisible: boolean = false;
  dataPropuestaAU: any = [];
  dataPropuestaAG: any = [];
  dataMargen: any = [];
  fechaNegociacionAu: any = '';
  fechaNegociacionAg: any = '';
  fechaLiqMargen: any = '';
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  activeName: any='first';

  ParametrosZona: any = {};
  ParametrosDefault: any = {
    fltConsumoMax:'',
    fltGastAdmMax:'',
    fltGastLabMax:'',
    fltMaquilaMax:'',
    fltMaquilaMin:'',
    fltMargenMax:'',
    fltPesoMax:'',
    fltPorcHumedMax:'',
    fltRecComMax:'',

		fltRecAgMax: '',
		fltLeyAgMax: '',
		fltLeyAgComMax: '',
		fltMargenAgMax: '',
		fltRecAgComMax: '',

		fltRecIntMax: '',
		fltMaquilaIntMax: '',
		fltConsumoIntMax: '',
		fltTmsLiqMin: '',
		fltTmsLiqMax: '',
		fltTmsIntMax: ''
  }
  VerErrorParametros: boolean = false;

  viewMsgRecCheck: boolean = false;
  viewMsgRecError: boolean = false;
  viewMsgMaqError: boolean = false;
  viewMsgMaqCheck: boolean = false;
  viewMsgMargenError: boolean = false;
  viewMsgMargenCheck: boolean = false;
  viewMsgConsQError: boolean = false;
  viewMsgConsQCheck: boolean = false;
  viewMsgGastAdmError: boolean = false;
  viewMsgGastAdmCheck: boolean = false;

  viewMsgRecupAgError: boolean = false;
  viewMsgRecupAgCheck: boolean = false;
  viewMsgPrecioAgError: boolean = false;
  viewMsgPrecioAgCheck: boolean = false;
  viewMsgMargenAgError: boolean = false;
  viewMsgMargenAgCheck: boolean = false;

  viewMsgLeyAuLiqError: boolean = false;
  viewMsgLeyAuLiqCheck: boolean = false;
  viewMsgLeyAuLiqEqual: boolean = false;
  viewMsgLeyAgLiqError: boolean = false;
  viewMsgLeyAgLiqCheck: boolean = false;
  viewMsgLeyAgLiqEqual: boolean = false;

  viewEstadoLoteLiqCheck: boolean = false;
  viewEstadoLoteLiqError: boolean = false;
  // LeyAuRecomendada: any = '';
  // LeyAgRecomendada: any = '';
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  loadingGetData:boolean = false;
  precioInterDate: string = '';
  dataPrecioOro: any = [
    {id: 'AM', value: ''},
    {id: 'PM', value: ''}
  ]
  dataPrecioPlata: any = [
    {id: '3', value: ''}
  ]
  PSecoTMS: any = '';
  ShowConsumo: boolean = true;
  TipoConsumo: any = 'no';
  ShowLeyMaquilaTM: boolean = false;
  ShowLeyTMAG: boolean = false;
  ShowCostoAMineral: boolean = true;

  checkedCalculoEsp: boolean = false;
  disabledCalculoEsp: boolean = true;
  guardarViewButton: boolean = false;
  CostoAnalsis: string = '15.00'
  disableExpPdfMargen: boolean = false;
  disableExpPdfPropuesta: boolean = false;
  statusLiquid: string = '';
  fechaEmisionFactura: any = ''
  checkedFacElect: boolean = false;
  NroFacGenerado: string = '';
  GastoAdmZona: any = 0;
  checkedSearch: boolean = false;
  nroExportacionAny: string = '';
  fechaExportacion: Date = new Date();
  checkLoteVolado: boolean = false;
  checkActualizarLeyesLiq: boolean = false;
  //Reintegro
  rowReintegro: any = {IdLote: '', Lote:''};
  dataReintegro: any = {};
  //verErrorGastoAdm: boolean = false;
  //Exportaciones
  multipleSelection:any = [];
  marcarLoteVolado: boolean = false;
  checkedCostoAnalisis: boolean = true;

  //MAQUILAS
  colorMsgMaquila: string = 'green';
  colorMsgMargen: string = 'green';
  colorMsgConsQ: string = 'green';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1010){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else {
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.BuscarLotes();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else{
      var fSearch = this.fechaSearch;
      var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
      var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
      this.FormSearch.strFechaInicio = bFechaIni;
      this.FormSearch.strFechaFin = bFechaFin;
    }

    this.FormSearch.strFechaLiqInicio = (this.fechaSearchLiq === '')? '*' : GLOBAL.getParseDate(this.fechaSearchLiq);
    this.FormSearch.strFechaLiqFin = this.FormSearch.strFechaLiqInicio;
    LiquidacionService.SearchLiquidaciones(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar liquidaciones.');
    })
  }

  ActualizarLiquidacion(rowEdit){
    // if(this.verErrorGastoAdm === true && this.activeName === 'fourth')
    //   this.openMessageError('Solucione los errores para guardar');
    // else{
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando liquidacion...'
    });
    var loadPropAU = this.handlePropuestaAU(-1, this.rowSelectedEdit);
    var loadPropAG = this.handlePropuestaAG(-1, this.rowSelectedEdit);

    var _margenbru: any;
    var _utilidadbru: any;
    if(this.activeName === 'fourth'){
      var splitM = this.dataMargen[9].GastAdm.split('%');
      _margenbru = Number(splitM[0].trim())/100;
      _utilidadbru = (isNaN(this.dataMargen[10].GastAdm)||this.dataMargen[10].GastAdm==undefined||this.dataMargen[10].GastAdm==null)?'':this.dataMargen[10].GastAdm.toString();
    }
    else{
      _margenbru = '';
      _utilidadbru = '';
    }

    var FormUpdate = {
      intIdRegistro : (rowEdit.intIdRegistro === null)? '' : rowEdit.intIdRegistro,
      strTipoLiquid: 'LIQ',
      strLeyAuLiq : (rowEdit.strLeyAuLiq === null)? '': rowEdit.strLeyAuLiq,
      strLeyAgLiq : (rowEdit.strLeyAgLiq === null)? '': rowEdit.strLeyAgLiq,
      strLeyAuMargen : (rowEdit.strLeyAuMargen === null)? '' : rowEdit.strLeyAuMargen,
      strPorcRecup : (rowEdit.strPorcRecup === null)? '' : rowEdit.strPorcRecup,
      strPrecioAu : (rowEdit.strPrecioAu === null)? '' : rowEdit.strPrecioAu,
      strMargenPI : (rowEdit.strMargenPI === null)? '' : rowEdit.strMargenPI,
      strMaquila : (rowEdit.strMaquila === null)? '' : rowEdit.strMaquila,
      strConsumoQ : (rowEdit.strConsumoQ === null)? '' : rowEdit.strConsumoQ,
      strGastAdm : (rowEdit.strGastAdm === null)? '' : rowEdit.strGastAdm,
      strRecAg : (rowEdit.strRecAg === null)? '' : rowEdit.strRecAg,
      strPrecioAg : (rowEdit.strPrecioAg === null)? '' : rowEdit.strPrecioAg,
      strMargenAg : (rowEdit.strMargenAg === null)? '' : rowEdit.strMargenAg,
      fltCostoTotal : (rowEdit.fltCostoTotal === null)? 0 : rowEdit.fltCostoTotal,
      strCalculoEspec : this.checkedCalculoEsp.toString(),
      strRecIntAu : (this.dataMargen[3].REC === null)? '' : this.dataMargen[3].REC,
      strMaquilaIntAu : (this.dataMargen[3].Maquila === null)? '' : this.dataMargen[3].Maquila,
      strConsQIntAu : (this.dataMargen[3].ConsQ === null)? '' : this.dataMargen[3].ConsQ,
      strGastAdmIntAu : (this.dataMargen[3].GastAdm === null)? '' : this.dataMargen[3].GastAdm,
      strRecIntAg : (this.dataMargen[6].REC === null)? '' : this.dataMargen[6].REC,
      strEstadoLiq: (this.activeName === 'fourth')? 'Liquidado' : 'Propuesta',
      strPorcImporte: (isNaN(this.dataMargen[10].Zona)||this.dataMargen[10].Zona==undefined||this.dataMargen[10].Zona==null)?'':this.dataMargen[10].Zona,
      strNBaseAu: (isNaN(this.dataPropuestaAU[6].CostoIntegro)||this.dataPropuestaAU[6].CostoIntegro==undefined||this.dataPropuestaAU[6].CostoIntegro==null)?'':this.dataPropuestaAU[6].CostoIntegro.toString(),
      strNBaseAg: (isNaN(this.dataPropuestaAG[4].CostoIntegro)||this.dataPropuestaAG[4].CostoIntegro==undefined||this.dataPropuestaAG[4].CostoIntegro==null)?'':this.dataPropuestaAG[4].CostoIntegro.toString(),
      strCostoAnalisis: (isNaN(this.dataPropuestaAU[4].CostoIntegro)||this.dataPropuestaAU[4].CostoIntegro==undefined||this.dataPropuestaAU[4].CostoIntegro==null)?'':this.dataPropuestaAU[4].CostoIntegro.toString(),
      strLoteVolado: this.marcarLoteVolado.toString(),
      strMargenBru: isNaN(_margenbru) ? '' : _margenbru.toString(),//(this.dataMargen[9].GastAdm==undefined||this.dataMargen[9].GastAdm==null)?'':this.dataMargen[9].GastAdm.toString(),
      strUtilidadBru: _utilidadbru,
      strUsuarioCrea : this.userDec.strUsuario
    }
    if(FormUpdate.strEstadoLiq === 'Liquidado' || FormUpdate.strEstadoLiq === 'Facturado'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = false;
      this.guardarViewButton = (this.userDec.strSuperAdmin != 'true') ? false : true;

      this.viewEstadoLoteLiqCheck = true;
      this.viewEstadoLoteLiqError = false;
      this.statusLiquid = 'Liquidado'
    }
    else if(FormUpdate.strEstadoLiq === 'Propuesta'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = true;

      this.viewEstadoLoteLiqCheck = true;
      this.viewEstadoLoteLiqError = false;
      this.statusLiquid = 'Propuesta'
    }
    else{
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
    }
    LiquidacionService.ActualizarLiquidacion(FormUpdate)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar liquidación:"+response);
      else if(response === -2) this.openMessage("Se actualizó correctamente.");
      else this.openMessage('Liquidación guardada correctamente.');
      loadingInstance.close();
      this.BuscarLotes();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar liquidación.');
    })

  }
  CancelarLiquidacion(){
    if(this.rowSelectedEdit.intIdLiquidacion === null){
      this.rowSelectedEdit.strLeyAuMargen = '';
      this.rowSelectedEdit.strPorcRecup = '';
      this.rowSelectedEdit.strPrecioAu = '';
      this.rowSelectedEdit.strMargenPI = '';
      this.rowSelectedEdit.strMaquila = '';
      this.rowSelectedEdit.strConsumoQ = '';
      this.rowSelectedEdit.strGastAdm = '';
      this.rowSelectedEdit.strRecAg = '';
      this.rowSelectedEdit.strPrecioAg = '';
      this.rowSelectedEdit.strMargenAg = '';
    }
    this.dialogParametrosVisible = false;
  }
  getPrecioInter(row){
    var Data: any = {
      strFechaInicio: GLOBAL.getParseDate(new Date(row.dtmFechaRecepcion)),
      strFechaFin: '',
      strType: 'get'
    }
    ParametroService.SearchPrecioInter(Data)
    .then(response =>{
      if(response.Count === 0) this.precioInterDate = 'No se encontró precio Inter'
      else {
        this.precioInterDate = GLOBAL.getDateFormat(response.Data[0].dtmFecha);
        this.dataPrecioOro[0].value = response.Data[0].strGoldAM;
        this.dataPrecioOro[1].value = response.Data[0].strGoldPM;
        this.dataPrecioPlata[0].value = response.Data[0].strSilver;
        if(this.rowSelectedEdit.intIdLiquidacion === null){
          this.rowSelectedEdit.strPrecioAu = (Number(this.dataPrecioOro[0].value) < Number(this.dataPrecioOro[1].value))? response.Data[0].strGoldAM : response.Data[0].strGoldPM;
        }
      }
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      this.openMessageError('Error al obtener precio Internacional');
    })
  }
  getGastoAdm(row){
    var data = {
      strZona: row.strProcedenciaSt,
      strAcopiador: row.strNombreAcopiador,
      strProveedor: row.strClienteRazonS,
      dtmFecha: GLOBAL.getParseDate(row.dtmFechaRecepcion)
    };
    this.GastoAdmZona = 0;
    GastoAdmService.GetGastoAdm(data)
    .then(response =>{
      if(response.Count > 0){
        this.dataMargen[3].GastAdm = (row.strGastAdmIntAu==''||this.rowSelectedEdit.strGastAdmIntAu==null)
          ? response.Data[0].fltGasto
          : row.strGastAdmIntAu;
        this.GastoAdmZona = response.Data[0].fltGasto;
      }
      else{
        this.dataMargen[3].GastAdm = (row.strGastAdmIntAu==''||this.rowSelectedEdit.strGastAdmIntAu==null)? this.GastoAdmZona: row.strGastAdmIntAu;
        console.log('No se encontro gasto administrativo');
      }
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else this.openMessageError('Error al obtener gasto administrativo.');
    })
  }
  BuscarZona(zona:string){
    this.checkedCalculoEsp = false;
    this.disabledCalculoEsp = true;

    ZonaService.SearchZona(zona)
    .then(response =>{
      if(response.strCalculoEspecial === 'true') {
        this.disabledCalculoEsp = false;
        if(this.rowSelectedEdit.strCalculoEspec === null){
          this.checkedCalculoEsp = true;
        }
        else{
          this.checkedCalculoEsp = (this.rowSelectedEdit.strCalculoEspec=='true')? true : false;
        }
      }
      else {
        this.checkedCalculoEsp = false;
        this.disabledCalculoEsp = true;
      }
      this.changeCostoTotal();
    }).catch(e =>{
      console.log(e);
    })
  }

  handleTabClick(tab, event) {
    if(tab.name=="first"){
      // this.handleParametros(0,this.rowSelectedEdit);
      this.changeCostoTotal();
    }
    if(tab.name=='second'){
      this.handlePropuestaAU(0, this.rowSelectedEdit);
    }
    if(tab.name=="third"){
      this.handlePropuestaAG(0, this.rowSelectedEdit);
    }
    if(tab.name=="fourth"){
      this.handleMargen(0, this.rowSelectedEdit);
    }
    if(tab.name=="propuestaau"){
      this.dialogParametrosPropuestaAu=true;
    }
    if(tab.name=="propuestaag"){
      this.dialogParametrosPropuestaAg=true;
    }
    if(tab.name=="margen"){
      this.dialogParametrosMargen=true;
    }
    if(tab.name=="factura"){
      this.dialogParametrosFactura=true;
    }
    // if(tab.name=="reintegro"){
    //   this.dialogReintegrosVisible=true;
    // }
  }

  //||=============================================||
  //||                PARAMETROS                   ||
  //||=============================================||
  handleLiquidacion(index, row){
    this.checkedCostoAnalisis = true;
    this.rowSelectedEdit = {};
    this.rowSelectedEdit = row;
    this.dataMargen[3].GastAdm = ''; //Limpiar gasto
    this.statusLiquid = (row.strEstadoLiq == null)? 'Pendiente' : row.strEstadoLiq;
    this.marcarLoteVolado = (row.strLoteVolado == 'true') ? true : false;
    this.guardarViewButton = ((this.statusLiquid === 'Liquidado'||this.statusLiquid === 'Facturado'||this.viewAccessEscritura==false) && this.userDec.strSuperAdmin != 'true') ? false : true;
    this.fechaLiqMargen = (row.dtmFechaLiquidacion === null)? new Date() : new Date(row.dtmFechaLiquidacion);
    this.fechaNegociacionAu = (row.dtmFechaPropuesta === null)? new Date() : new Date(row.dtmFechaPropuesta);
    this.fechaNegociacionAg = (row.dtmFechaPropuesta === null)? new Date() : new Date(row.dtmFechaPropuesta);
    this.fechaEmisionFactura = (row.dtmFechaFacturacion === null)? new Date() : new Date(row.dtmFechaFacturacion);
    this.rowSelectedEdit.strLeyAuLiq = (this.rowSelectedEdit.strLeyAuLiq==''||this.rowSelectedEdit.strLeyAuLiq==null)?row.strLeyAuComercial : row.strLeyAuLiq;
    this.rowSelectedEdit.strLeyAgLiq = (this.rowSelectedEdit.strLeyAgLiq==''||this.rowSelectedEdit.strLeyAgLiq==null)?row.strLeyAgComercial : row.strLeyAgLiq;
    this.rowSelectedEdit.strLeyAuMargen = (this.rowSelectedEdit.strLeyAuMargen==''||this.rowSelectedEdit.strLeyAuMargen==null)
      ? ((row.strCheckH2O=='true') ? row.strPorcH2O : this.getNumberFix(Number(row.strPorcH2O)+1,2))
      : row.strLeyAuMargen;
    this.PSecoTMS = this.getNumberFix(this.rowSelectedEdit.fltPesoTmh * (100 - Number(this.rowSelectedEdit.strLeyAuMargen))/100, 3);

    this.checkedCostoAnalisis = (this.rowSelectedEdit.strCostoAnalisis == null || Number(this.rowSelectedEdit.strCostoAnalisis) > 0)?true: false;

    this.dataMargen[3].GastAdm = (this.rowSelectedEdit.strGastAdmIntAu==''||this.rowSelectedEdit.strGastAdmIntAu==null)
      ? this.dataMargen[3].GastAdm
      : this.rowSelectedEdit.strGastAdmIntAu;
    // this.dataMargen[6].REC = (this.rowSelectedEdit.strRecIntAg==''||this.rowSelectedEdit.strRecIntAg==null)?'40':this.rowSelectedEdit.strRecIntAg;
    if(row.intIdLiquidacion === null) {
      this.viewEstadoLoteLiqCheck = false;
      this.viewEstadoLoteLiqError = true;
    }
    else { this.viewEstadoLoteLiqCheck=true; this.viewEstadoLoteLiqError=false }

    //Habilitar descarga PDF
    if(this.statusLiquid === 'Liquidado' || this.statusLiquid === 'Facturado'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = false;
    }
    else if(this.statusLiquid === 'Propuesta'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = true;
    }
    else{
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
    }

    this.activeName = 'first';
    this.TipoConsumo = 'no';
    this.ShowLeyMaquilaTM = false;
    this.ShowLeyTMAG = false;
    this.NroFacGenerado = (row.strNroFactura == null || row.strNroFactura == '')? '' : row.strNroFactura;
    this.checkedFacElect = false;

    this.handleParametros(index, row);
  }
  LimpiarParametros(){
    this.viewMsgRecCheck = false;
    this.viewMsgRecError = false;
    this.viewMsgMaqError = false;
    this.viewMsgMaqCheck = false;
    this.viewMsgMargenError = false;
    this.viewMsgMargenCheck = false;
    this.viewMsgConsQError = false;
    this.viewMsgConsQCheck = false;
    this.viewMsgGastAdmError = false;
    this.viewMsgGastAdmCheck = false;
    this.viewMsgRecupAgError = false;
    this.viewMsgRecupAgCheck = false;
    this.viewMsgPrecioAgError = false;
    this.viewMsgPrecioAgCheck = false;
    this.viewMsgMargenAgError = false;
    this.viewMsgMargenAgCheck = false;

    this.viewMsgLeyAuLiqError = false;
    this.viewMsgLeyAuLiqCheck = false;
    this.viewMsgLeyAuLiqEqual = false;
    this.viewMsgLeyAgLiqError = false;
    this.viewMsgLeyAgLiqCheck = false;
    this.viewMsgLeyAgLiqEqual = false;
  }
  handleParametros(index, row){
    this.LimpiarParametros();
    this.VerErrorParametros = false;
    this.ParametrosZona = this.ParametrosDefault;

    if(row.strProcedenciaSt.trim() === '' || row.strProcedenciaSt.trim() === null){
      this.VerErrorParametros = true;
    }
    else{
      var Data = {
        strZona : row.strProcedenciaSt,
        strProveedor : row.strClienteRazonS
      }
      LiquidacionService.GetParametros(Data)
      .then(response =>{
        if(response.Count === 0) {
          // this.ParametrosZona = this.ParametrosDefault;
          this.openMessageError('No se encontró parametros comerciales de la Zona')
          this.VerErrorParametros = true;
        }
        else {
          this.ParametrosZona = response.Data[0];
          this.VerErrorParametros = false;

          this.dataMargen[3].REC = (this.rowSelectedEdit.strRecIntAu==='' || this.rowSelectedEdit.strRecIntAu==null)? this.ParametrosZona.fltRecIntMax:this.rowSelectedEdit.strRecIntAu;
          this.dataMargen[3].Maquila = (this.rowSelectedEdit.strMaquilaIntAu==''||this.rowSelectedEdit.strMaquilaIntAu==null)? this.ParametrosZona.fltMaquilaIntMax:this.rowSelectedEdit.strMaquilaIntAu;
          this.dataMargen[3].ConsQ = (this.rowSelectedEdit.strConsQIntAu==''||this.rowSelectedEdit.strConsQIntAu==null)? this.ParametrosZona.fltConsumoIntMax:this.rowSelectedEdit.strConsQIntAu;
          this.dataMargen[6].REC = (this.rowSelectedEdit.strRecIntAg==''||this.rowSelectedEdit.strRecIntAg==null)?'40':this.rowSelectedEdit.strRecIntAg;
          this.dataMargen[0].TMH = this.ParametrosZona.strFactorConv;
          this.getPrecioInter(row);
          this.BuscarZona(row.strProcedenciaSt);
          this.getGastoAdm(row);
          this.handlePropuestaAU(index, row);
          this.handlePropuestaAG(index, row);
          this.handleMargen(index, row);
          //this.dialogParametrosVisible = true;
        }
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al obtener parametros.');
      })
    }
  }
  changeCostoTotal(){
    var row = this.rowSelectedEdit;
    var LeyAuCom: number=(row.strLeyAuLiq===null || row.strLeyAuLiq==='') ? 0 : Number(row.strLeyAuLiq.trim()),
        PorcRec: number=(row.strPorcRecup===null || row.strPorcRecup==='') ? 0 : Number(row.strPorcRecup.trim()),
        PrecioAu: number=(row.strPrecioAu===null || row.strPrecioAu==='') ? 0 : Number(row.strPrecioAu.trim()),
        MargenPI: number=(row.strMargenPI===null || row.strMargenPI==='') ? 0 : Number(row.strMargenPI.trim()),
        Maquila: number=(row.strMaquila===null || row.strMaquila==='') ? 0 : Number(row.strMaquila.trim()),
        ConsumoQ: number=(row.strConsumoQ===null || row.strConsumoQ==='') ? 0 : Number(row.strConsumoQ.trim()),
        GastAdm: number=(row.strGastAdm===null || row.strGastAdm==='') ? 0 : Number(row.strGastAdm.trim()),
        PesoTM: number=(this.PSecoTMS===null || this.PSecoTMS==='') ? 0 : Number(this.PSecoTMS.trim())
    var CostoTM, Costo_Total;
    if(this.checkedCalculoEsp === true){
      // Costo_Total = (LeyAuCom*PorcRec/100*(PrecioAu-MargenPI)*1.1023-Maquila-ConsumoQ-GastAdm)*PesoTM;
      CostoTM = this.getNumberFix((LeyAuCom*PorcRec/100 *(PrecioAu-MargenPI)*1.1023 -Maquila-ConsumoQ-GastAdm), 2);
    }
    else{
      // Costo_Total = (LeyAuCom*PorcRec/100*(PrecioAu-MargenPI)-Maquila-ConsumoQ-GastAdm)*1.1023*PesoTM;
      CostoTM = this.getNumberFix((LeyAuCom*PorcRec/100 *(PrecioAu-MargenPI)-Maquila-ConsumoQ-GastAdm)*1.1023, 2);
    }
    Costo_Total = CostoTM * PesoTM;
    this.rowSelectedEdit.fltCostoTotal = Costo_Total.toFixed(2);

    // if(this.checkedCalculoEsp === true)
    //   this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)*1.1023 -_maqui-_consq-_gastadm), 2);
    // else
    //   this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)-_maqui-_consq-_gastadm)*1.1023, 2);
    //
    // this.dataPropuestaAU[2].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[2].CostoTM * Number(this.PSecoTMS), 2);
  }
  changeParamRecup(Value, ParamMax){
    if(ParamMax === '' || Number(ParamMax)===0){ this.viewMsgRecCheck = false; this.viewMsgRecError = false; }
    else{
      var recup = Number(Value);
      if(recup <= ParamMax) { this.viewMsgRecCheck = true; this.viewMsgRecError = false; }
      else { this.viewMsgRecError = true; this.viewMsgRecCheck = false; }
    }
  }
  changeParamMaquila(Value, ParamMin, ParamMax){
    this.viewMsgMaqCheck = false; this.viewMsgMaqError = false;
    // if(ParamMax === ''){ this.viewMsgMaqCheck = false; this.viewMsgMaqError = false; }
    // else {
    //   var maq = Number(Value);
    //   if(maq <= ParamMax) { this.viewMsgMaqCheck = true; this.viewMsgMaqError = false; }
    //   else { this.viewMsgMaqError = true; this.viewMsgMaqCheck = false; }
    // }
  }
  changeParamMargenPI(Value, ParamMax){
    if(ParamMax === '' && Number(ParamMax)===0){ this.viewMsgMargenCheck = false; this.viewMsgMargenError = false; }
    else {
      var margenpi = Number(Value);
      if(margenpi <= ParamMax) { this.viewMsgMargenCheck = true; this.viewMsgMargenError = false; }
      else { this.viewMsgMargenError = true; this.viewMsgMargenCheck = false; }
    }
  }
  changeParamConsumoQ(Value, ParamMax){
    if(ParamMax === '' && Number(ParamMax)===0){ this.viewMsgConsQCheck = false; this.viewMsgConsQError = false; }
    else {
      var consq = Number(Value);
      if(consq <= ParamMax) { this.viewMsgConsQCheck = true; this.viewMsgConsQError = false; }
      else { this.viewMsgConsQError = true; this.viewMsgConsQCheck = false; }
    }
  }
  changeParamGastosAdm(Value, ParamMax){
    if(ParamMax === '' && Number(ParamMax)===0){ this.viewMsgGastAdmCheck = false; this.viewMsgGastAdmError = false; }
    else {
      var gastadm = Number(Value);
      if(gastadm <= ParamMax) { this.viewMsgGastAdmCheck = true; this.viewMsgGastAdmError = false; }
      else { this.viewMsgGastAdmError = true; this.viewMsgGastAdmCheck = false; }
    }
  }
  changeParamRecAg(Value, ParamMax){
    if(ParamMax === '' && Number(ParamMax)===0){ this.viewMsgRecupAgCheck = false; this.viewMsgRecupAgError = false; }
    else {
      var recag = Number(Value);
      if(recag <= ParamMax) { this.viewMsgRecupAgCheck = true; this.viewMsgRecupAgError = false; }
      else { this.viewMsgRecupAgError = true; this.viewMsgRecupAgCheck = false; }
    }
  }
  changeParamMargenAg(Value, ParamMax){
    if(ParamMax === '' && Number(ParamMax)===0){ this.viewMsgMargenAgCheck = false; this.viewMsgMargenAgError = false; }
    else {
      var recup = Number(Value);
      if(recup === ParamMax) { this.viewMsgMargenAgCheck = true; this.viewMsgMargenAgError = false; }
      else { this.viewMsgMargenAgError = true; this.viewMsgMargenAgCheck = false; }
    }
  }
  changeH2OLiq(){
    this.PSecoTMS = this.getNumberFix(this.rowSelectedEdit.fltPesoTmh * (100 - Number(this.rowSelectedEdit.strLeyAuMargen))/100, 3);
    this.changeCostoTotal();
  }

  changeNroFactura(){
    var nFact = this.rowSelectedEdit.strNroFactura;
    if(this.checkedFacElect == false) {
      var split = nFact.split('-');
      var f1 = this.generateNumber(split[0], 4);
      var f2 = (split[1]==undefined)? '' : this.generateNumber(split[1], 7);
      this.NroFacGenerado = f1+'-'+f2;

      if(nFact.length == 4 && nFact.indexOf('-') == -1)
        this.rowSelectedEdit.strNroFactura = this.rowSelectedEdit.strNroFactura + '-';
    }
    else {
      var Letter = nFact.substring(0, 1);
      var factura =  nFact.substring(1, nFact.length);
      var split = factura.split('-');
      var f1 = this.generateNumber(split[0], 3);
      this.NroFacGenerado = Letter.toUpperCase() + f1 + '-' + ((split[1]==undefined)? '':split[1]);

      if(nFact.length == 4 && nFact.indexOf('-') == -1)
        this.rowSelectedEdit.strNroFactura = this.rowSelectedEdit.strNroFactura + '-';
    }
  }
  generateNumber(number, len) {
    var aux = '0000000000';
    var num1 = (aux+number).slice(-len);
    return num1;
  }
  handleBlur(){
    this.rowSelectedEdit.strNroFactura = this.NroFacGenerado;
  }

  //||=============================================||
  //||                  FACTURAS                   ||
  //||=============================================||
  GuardarFactura(nroFactura){
    var nFact = this.rowSelectedEdit.strNroFactura;
    if(nFact==='' || nFact===null)
      this.openMessageError('Ingrese N° factura');
    else {
      var query = "UPDATE tblLiquidacion SET strNroFactura = '"+nFact+
                  "', dtmFechaFacturacion = '"+GLOBAL.getParseDate(this.fechaEmisionFactura)+
                  "', strEstadoLiq='Facturado' WHERE intIdRegistro = "+this.rowSelectedEdit.intIdRegistro+
                  " AND strTipoLiquid='LIQ'";
      var DataQ = {
        strQuery: query,
        strUsuarioCrea: this.userDec.strUsuario
      }
      QueryService.ExecuteQuery(DataQ)
      .then(response =>{
        if(response == -1) this.openMessageError('SERVICE: Error al guardar.')
        else {
          this.openMessage('Se guardo correctamente.');
          this.statusLiquid = 'Facturado';
          this.BuscarLotes();
        }
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al guardar.');
      })
      // this.ExecuteConsulta(query);
      // this.statusLiquid = 'Facturado';
      // this.BuscarLotes();
    }
  }
  ExecuteConsulta(query){
    var DataQ = {
      strQuery: query,
      strUsuarioCrea: this.userDec.strUsuario
    }
    QueryService.ExecuteQuery(DataQ)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al guardar.')
      else this.openMessage('Se guardo correctamente.');
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar.');
    })
  }

  //||=============================================||
  //||                PROPUESTA AU                 ||
  //||=============================================||
  handlePropuestaAU(index, row){
    this.dataPropuestaAU[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAu);
    this.dataPropuestaAU[4].CostoIntegro = (this.checkedCalculoEsp === true || this.marcarLoteVolado == true || this.checkedCostoAnalisis==false)
      ? '0.00' : this.getNumberFix(this.ParametrosZona.fltGastLabMax, 2);
    this.cargarDataPropuestaAu();
  }
  cargarDataPropuestaAu(){
    this.ShowCostoAMineral = true;
    this.dataPropuestaAU[2].Lote = this.rowSelectedEdit.strLote;
    this.dataPropuestaAU[2].FechaRec = GLOBAL.getDateFormat(this.rowSelectedEdit.dtmFechaRecepcion);
    this.dataPropuestaAU[2].TMH = this.rowSelectedEdit.fltPesoTmh;
    this.dataPropuestaAU[2].PorcH2O = this.rowSelectedEdit.strLeyAuMargen;
    this.dataPropuestaAU[2].TMS = this.PSecoTMS;//this.rowSelectedEdit.strPSecoTM;
    this.dataPropuestaAU[2].LeyOz = this.rowSelectedEdit.strLeyAuLiq;
    this.dataPropuestaAU[2].PorcRecup = this.rowSelectedEdit.strPorcRecup;
    this.dataPropuestaAU[2].PrecioInt = this.rowSelectedEdit.strPrecioAu;
    this.dataPropuestaAU[2].Maquila = this.rowSelectedEdit.strMaquila;
    this.dataPropuestaAU[2].ConsumoKgTm = (this.TipoConsumo=='si')
      ? this.getNumberFix((Number(this.rowSelectedEdit.strConsumoQ) + Number(this.rowSelectedEdit.strGastAdm))/7, 2)
      : this.getNumberFix(Number(this.rowSelectedEdit.strConsumoQ) + Number(this.rowSelectedEdit.strGastAdm), 2);

    this.changeVerCostoAnalisis();
  }
  cargarDataLoteVolado(){
    this.ShowCostoAMineral = true;
    this.dataPropuestaAU[2].Lote = this.rowSelectedEdit.strLote;
    this.dataPropuestaAU[2].FechaRec = GLOBAL.getDateFormat(this.rowSelectedEdit.dtmFechaRecepcion);
    this.dataPropuestaAU[2].TMH = this.rowSelectedEdit.fltPesoTmh;
    this.dataPropuestaAU[2].PorcH2O = this.rowSelectedEdit.strLeyAuMargen;
    this.dataPropuestaAU[2].TMS = this.PSecoTMS;//this.rowSelectedEdit.strPSecoTM;
    this.dataPropuestaAU[2].LeyOz = this.rowSelectedEdit.strLeyAuLiq;
    this.dataPropuestaAU[2].PorcRecup = '0.00';
    this.dataPropuestaAU[2].PrecioInt = this.rowSelectedEdit.strPrecioAu;
    this.dataPropuestaAU[2].Maquila = '0.00';
    this.dataPropuestaAU[2].ConsumoKgTm = '0.00'

    this.changeVerCostoAnalisis();
  }

  //||=============================================||
  //||                PROPUESTA AG                 ||
  //||=============================================||
  handlePropuestaAG(index, row){
    this.dataPropuestaAG[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAg);
    this.cargarDataPropuestaAg();
  }
  cargarDataPropuestaAg(){
    this.dataPropuestaAG[2].Lote = this.rowSelectedEdit.strLote;
    this.dataPropuestaAG[2].FechaRec = GLOBAL.getDateFormat(this.rowSelectedEdit.dtmFechaRecepcion);
    this.dataPropuestaAG[2].TMH = this.rowSelectedEdit.fltPesoTmh;
    this.dataPropuestaAG[2].PorcH2O = this.rowSelectedEdit.strLeyAuMargen;
    this.dataPropuestaAG[2].TMS = this.PSecoTMS;//this.rowSelectedEdit.strPSecoTM;
    this.dataPropuestaAG[2].LeyOz = this.rowSelectedEdit.strLeyAgLiq;
    this.dataPropuestaAG[2].PorcRecup = this.rowSelectedEdit.strRecAg;
    this.dataPropuestaAG[2].PrecioInt = this.rowSelectedEdit.strPrecioAg;

    // LeyAg*PorcRec/100*(PrecioInter-MargenAg)
    var _leyagcom = Number(this.rowSelectedEdit.strLeyAgLiq), _recag=Number(this.rowSelectedEdit.strRecAg),
        _precioag = Number(this.rowSelectedEdit.strPrecioAg), _margenag=Number(this.rowSelectedEdit.strMargenAg);
    // var _leyoz = Number(this.rowSelectedEdit.strLeyAuComercial), _porcrec=Number(this.rowSelectedEdit.strPorcRecup),
    //     _precioint = Number(this.rowSelectedEdit.strPrecioAu), _margenpi=Number(this.rowSelectedEdit.strMargenPI),
    //     _consq = Number(this.rowSelectedEdit.strConsumoQ), _gastadm = Number(this.rowSelectedEdit.strGastAdm),
    //     _maqui = Number(this.rowSelectedEdit.strMaquila);

    if(this.rowSelectedEdit.strProcedenciaSt === 'TRUJILLO' || this.rowSelectedEdit.strProcedenciaSt === 'CAJAMARCA')
      this.dataPropuestaAG[2].CostoTM = this.getNumberFix(_leyagcom * _recag/100 *(_precioag - _margenag)*1.1023, 2);
    else
      this.dataPropuestaAG[2].CostoTM = this.getNumberFix(_leyagcom * _recag/100 *(_precioag - _margenag), 2);
    this.dataPropuestaAG[2].CostoIntegro = this.getNumberFix(this.dataPropuestaAG[2].CostoTM * Number(this.PSecoTMS), 2);
    this.dataPropuestaAG[4].CostoIntegro = this.dataPropuestaAG[2].CostoIntegro;
    this.dataPropuestaAG[5].CostoIntegro = this.getNumberFix(this.dataPropuestaAG[2].CostoIntegro * 0.18, 2);
    this.dataPropuestaAG[6].CostoIntegro = this.getNumberFix(Number(this.dataPropuestaAG[4].CostoIntegro) + Number(this.dataPropuestaAG[5].CostoIntegro), 2);
  }

  //||=============================================||
  //||                   MARGEN                    ||
  //||=============================================||
  changeGastoAdm(){
    this.checkLoteVolado = (Number(this.dataMargen[2].Importe)<=0 && (this.dataMargen[2].REC==''&&this.dataMargen[2].Margen==''&&this.dataMargen[2].Maquila==''&&this.dataMargen[2].GastAdm=='')) ? true : false;
  }
  handleMargen(index, row){
    this.cargarDataMargen();
    this.dataMargen[0].FechaRec = GLOBAL.getDateFormat(this.fechaLiqMargen); //X
    this.changeGastoAdm();
  }

  cargarDataMargen(){
    this.dataMargen[1].Lote = this.rowSelectedEdit.strLote;
    this.dataMargen[2].FechaRec = GLOBAL.getDateFormat(this.rowSelectedEdit.dtmFechaRecepcion);
    this.dataMargen[2].Zona = this.rowSelectedEdit.strProcedenciaSt;
    this.dataMargen[2].Proveedor = this.rowSelectedEdit.strClienteRazonS;
    this.dataMargen[2].TMH = this.rowSelectedEdit.fltPesoTmh; //this.getNumberFix(this.rowSelectedEdit.fltPesoTmh, 2);
    this.dataMargen[2].PorcH2O = this.rowSelectedEdit.strLeyAuMargen;
    this.dataMargen[2].TMS = this.PSecoTMS;//this.rowSelectedEdit.strPSecoTM; //this.getNumberFix(this.rowSelectedEdit.strPSecoTM, 2);
    this.dataMargen[2].LeyOz = this.rowSelectedEdit.strLeyAuLiq;
    this.dataMargen[2].REC = this.rowSelectedEdit.strPorcRecup;
    this.dataMargen[2].Inter = this.rowSelectedEdit.strPrecioAu;
    this.dataMargen[2].Margen = this.rowSelectedEdit.strMargenPI;
    this.dataMargen[2].Maquila = this.rowSelectedEdit.strMaquila;
    this.dataMargen[2].ConsQ = this.rowSelectedEdit.strConsumoQ;
    this.dataMargen[2].GastAdm = this.rowSelectedEdit.strGastAdm;
    //CALCULO DE FINOS
    if(this.rowSelectedEdit.strProcedenciaSt === 'TRUJILLO'){
      this.dataMargen[2].Fino = this.getNumberFix(Number(this.dataMargen[2].TMS)*Number(this.dataMargen[2].LeyOz)*34.285, 2);
    }

    var _leyozau=Number(this.dataMargen[2].LeyOz), _rec=Number(this.dataMargen[2].REC), _inter=Number(this.dataMargen[2].Inter),
        _margen=Number(this.dataMargen[2].Margen), _maqui=Number(this.dataMargen[2].Maquila), _consQ=Number(this.dataMargen[2].ConsQ),
        _gastadm=Number(this.dataMargen[2].GastAdm);
    if(this.checkedCalculoEsp === true)
      this.dataMargen[2].PrecioxTMS = this.getNumberFix((_leyozau*_rec/100*(_inter - _margen)*1.1023 - _maqui - _consQ - _gastadm), 2);
    else
      this.dataMargen[2].PrecioxTMS = this.getNumberFix((_leyozau*_rec/100*(_inter - _margen)- _maqui - _consQ - _gastadm)*1.1023, 2);
    this.dataMargen[2].Importe = this.getNumberFix(Number(this.dataMargen[2].TMS) * Number(this.dataMargen[2].PrecioxTMS) ,2);

    //CALCULO DE MAQUILAS
    var maq_calc= Number(this.rowSelectedEdit.strLeyAuOzTc) * 100 + this.ParametrosZona.fltMaquilaMax;
    var resMaq = this.getNumberFix(Number(this.rowSelectedEdit.strMaquila) - maq_calc, 0)
    var resMargen = this.getNumberFix(Number(this.rowSelectedEdit.strMargenPI) - this.ParametrosZona.fltMargenMax, 0);
    var resConsQ = this.getNumberFix(Number(this.rowSelectedEdit.strConsumoQ) - this.ParametrosZona.fltConsumoMax, 0);

    if(Number(resMaq) < 0) this.colorMsgMaquila = 'red';
    else {  this.colorMsgMaquila = 'green'; resMaq = '+'+resMaq;  }
    if(Number(resMargen) < 0) this.colorMsgMargen = 'red';
    else {  this.colorMsgMargen = 'green'; resMargen = '+'+resMargen; }
    if(Number(resConsQ) < 0) this.colorMsgConsQ = 'red';
    else {  this.colorMsgConsQ = 'green'; resConsQ = '+'+resConsQ;  }
    this.dataMargen[0].Maquila = (Number(this.rowSelectedEdit.strMaquila) === 0)?'+0' : resMaq;
    this.dataMargen[0].Margen = (Number(this.rowSelectedEdit.strMargenPI) === 0)?'+0' : resMargen;
    this.dataMargen[0].ConsQ = (Number(this.rowSelectedEdit.strConsumoQ) === 0)?'+0' : resConsQ;

    //Liq Int(Au)
    this.changeLiqIntAu();
    //Propuesta AG
    this.changeMargenAg();
  }
  changePropAu(){
    this.rowSelectedEdit.strPorcRecup = this.dataMargen[2].REC;
    this.rowSelectedEdit.strMargenPI = this.dataMargen[2].Margen;
    this.rowSelectedEdit.strMaquila = this.dataMargen[2].Maquila;
    this.rowSelectedEdit.strGastAdm = this.dataMargen[2].GastAdm;
    this.changeCostoTotal()
    //LOTES VOLADOS
    this.dataMargen[3].GastAdm = (this.marcarLoteVolado === true)? '0' : this.GastoAdmZona;

    this.cargarDataMargen();
  }
  changeLiqIntAu(){
    this.rowSelectedEdit.strRecIntAu = this.dataMargen[3].REC;
    this.rowSelectedEdit.strMaquilaIntAu = this.dataMargen[3].Maquila;
    this.rowSelectedEdit.strConsQIntAu = this.dataMargen[3].ConsQ;
    this.rowSelectedEdit.strGastAdmIntAu = this.dataMargen[3].GastAdm;
    this.rowSelectedEdit.strRecIntAg = this.dataMargen[6].REC;

    this.dataMargen[3].TMH = (this.checkedCalculoEsp===true)
      ? this.dataMargen[2].TMH
      : this.getNumberFix(this.dataMargen[2].TMH/(this.ParametrosZona.fltTmsLiqMax), 3); //: this.getNumberFix(this.dataMargen[2].TMH/0.99, 3);
    this.dataMargen[3].PorcH2O = Number(this.rowSelectedEdit.strPorcH2O);
    this.dataMargen[3].TMS = this.getNumberFix((this.dataMargen[3].TMH - this.dataMargen[3].PorcH2O*this.dataMargen[3].TMH/100), 3);
    this.dataMargen[3].LeyOz = Math.max(Number(this.rowSelectedEdit.strLeyAuOzTc),Number(this.rowSelectedEdit.strLeyAuREE)); //Add REE
    this.dataMargen[3].Inter = this.dataMargen[2].Inter;
    var _rec3=Number(this.dataMargen[3].REC), _leyozau3=Number(this.dataMargen[3].LeyOz), _inter3=Number(this.dataMargen[3].Inter),
        _factconv=Number(this.dataMargen[0].TMH), _maquila3 = Number(this.dataMargen[3].Maquila), _consq3=Number(this.dataMargen[3].ConsQ),
        _gastadm3=Number(this.dataMargen[3].GastAdm);
    this.dataMargen[3].PrecioxTMS = this.getNumberFix((_rec3 * _leyozau3 * _inter3/100)*_factconv - _maquila3 - _consq3 - _gastadm3, 2);
    this.dataMargen[3].Importe = this.getNumberFix(Number(this.dataMargen[3].PrecioxTMS) * Number(this.dataMargen[3].TMS), 2);

    //CALCULAR FINOS INT
    if(this.rowSelectedEdit.strProcedenciaSt === 'TRUJILLO'){
      this.dataMargen[3].Fino = this.getNumberFix(Number(this.dataMargen[3].TMS)*Number(this.dataMargen[3].LeyOz)*34.285, 2);
    }
    this.changeMargenUtilidad()
  }
  changeMargenAg(){
    //Propuesta AG
    this.dataMargen[5].TMH = Number(this.dataMargen[2].TMH);
    this.dataMargen[5].PorcH2O = this.getNumberFix(Number(this.dataMargen[2].PorcH2O), 2);
    this.dataMargen[5].TMS = this.getNumberFix(Number(this.dataMargen[5].TMH) - Number(this.dataMargen[5].PorcH2O) * Number(this.dataMargen[5].TMH)/100, 3);
    this.dataMargen[5].REC = Number(this.rowSelectedEdit.strRecAg);
    this.dataMargen[5].LeyOz = Number(this.rowSelectedEdit.strLeyAgLiq);
    this.dataMargen[5].Inter = Number(this.rowSelectedEdit.strPrecioAg);
    this.dataMargen[5].Margen = Number(this.rowSelectedEdit.strMargenAg);
    this.dataMargen[5].PrecioxTMS = this.getNumberFix(Number(this.dataMargen[5].LeyOz)*Number(this.dataMargen[5].REC)*(Number(this.dataMargen[5].Inter)-Number(this.dataMargen[5].Margen))/100, 2);
    this.dataMargen[5].Importe = this.getNumberFix(Number(this.dataMargen[5].PrecioxTMS) * Number(this.dataMargen[5].TMS), 2);
    //Liq Int AG
    this.dataMargen[6].TMH = Number(this.dataMargen[3].TMH); //this.getNumberFix(Number(this.dataMargen[3].TMH), 2);
    this.dataMargen[6].PorcH2O = this.getNumberFix(Number(this.dataMargen[3].PorcH2O), 2);
    this.dataMargen[6].TMS = this.getNumberFix(Number(this.dataMargen[6].TMH) - Number(this.dataMargen[6].PorcH2O) * Number(this.dataMargen[6].TMH/100), 3);
    this.dataMargen[6].LeyOz = Number(this.rowSelectedEdit.strLeyAgOzTc);
    this.dataMargen[6].Inter = this.dataMargen[5].Inter;
    this.dataMargen[6].PrecioxTMS = this.getNumberFix(Number(this.dataMargen[6].LeyOz) *Number(this.dataMargen[6].REC) *Number(this.dataMargen[6].Inter)*Number(this.dataMargen[0].TMH) /100, 2);
    this.dataMargen[6].Importe = this.getNumberFix(Number(this.dataMargen[6].PrecioxTMS) * Number(this.dataMargen[6].TMS) * 0.935, 2);
    this.changeMargenUtilidad();
  }
  changeMargenUtilidad(){
    var _inter=Number(this.dataMargen[3].Inter), _leyozau=Number(this.dataMargen[3].LeyOz), _rec=Number(this.dataMargen[3].REC),
        _tms=Number(this.dataMargen[3].TMS), _importe=Number(this.dataMargen[2].Importe), _maquila=Number(this.dataMargen[3].Maquila),
        _consQ=Number(this.dataMargen[3].ConsQ), _gastadm=Number(this.dataMargen[3].GastAdm), _tms=Number(this.dataMargen[3].TMS),
        _factconv=Number(this.dataMargen[0].TMH)
    //Margen Au
    this.dataMargen[10].PorcH2O = this.getNumberFix((_inter* _leyozau* _rec* _factconv/100 *0.985 *_tms) - _importe - (_maquila+_consQ+_gastadm)* _tms, 2);
    var margen_bruAu = this.dataMargen[10].PorcH2O/(_inter * _leyozau * _rec /100 * _factconv * 0.985 * _tms);
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
    this.dataMargen[9].PorcH2O = formatter.format(margen_bruAu);
    //Margen Ag
    this.dataMargen[10].Inter = this.getNumberFix(Number(this.dataMargen[6].Importe) - Number(this.dataMargen[5].Importe), 2);
    var margen_bruAg = Number(this.dataMargen[10].Inter) / Number(this.dataMargen[6].Importe);
    this.dataMargen[9].Inter = formatter.format(margen_bruAg);
    //Margen x Lote
    this.dataMargen[10].GastAdm = this.getNumberFix(Number(this.dataMargen[10].PorcH2O) + Number(this.dataMargen[10].Inter), 2);
    var margen_bruLote = Number(this.dataMargen[10].GastAdm) / (Number(this.dataMargen[6].Importe) + (_inter * _leyozau * _rec * _factconv /100 * 0.985 * _tms));
    this.dataMargen[9].GastAdm = formatter.format(margen_bruLote);

    //Porcentage
    var Aux_PrecioxTMS = this.getNumberFix((Number(this.dataMargen[3].REC) * Number(this.dataMargen[3].LeyOz) * Number(this.dataMargen[3].Inter)/100)*_factconv, 2);
    var Aux_Importe = this.getNumberFix(Number(Aux_PrecioxTMS) * Number(this.dataMargen[3].TMS), 2);
    this.dataMargen[9].Zona = Aux_Importe;
    this.dataMargen[10].Zona = this.getNumberFix(Number(this.dataMargen[2].Importe)/Number(Aux_Importe), 2);
  }

  changeMarcarLoteVolado(){
    this.dataMargen[3].GastAdm = (this.marcarLoteVolado === true)? '0' : this.GastoAdmZona;
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFloat(number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  changeFechaPropAu(){
    this.dataPropuestaAU[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAu);
  }
  changeFechaPropAg(){
    this.dataPropuestaAG[0].FechaRec = GLOBAL.getDateFormat(this.fechaNegociacionAg);
  }
  changeFechaMargen(){
    this.dataMargen[0].FechaRec = GLOBAL.getDateFormat(this.fechaLiqMargen);
  }
  changeLeyAuLiq(){
    if(this.rowSelectedEdit.strLeyAuLiq===''||this.rowSelectedEdit.strLeyAuOzTc===''||this.rowSelectedEdit.strLeyAuLiq===null||this.rowSelectedEdit.strLeyAuOzTc===null){
      this.viewMsgLeyAuLiqError = false;
      this.viewMsgLeyAuLiqEqual=false;
      this.viewMsgLeyAuLiqCheck=false;
    }
    else {
      var leyAuLiq = Number(this.rowSelectedEdit.strLeyAuLiq);
      var leyAuLab = Number(this.rowSelectedEdit.strLeyAuOzTc);
      if(leyAuLiq > leyAuLab) {this.viewMsgLeyAuLiqError=true;this.viewMsgLeyAuLiqEqual=false;this.viewMsgLeyAuLiqCheck=false;}
      else if(leyAuLiq === leyAuLab) {this.viewMsgLeyAuLiqError=false;this.viewMsgLeyAuLiqEqual=true;this.viewMsgLeyAuLiqCheck=false;}
      else {this.viewMsgLeyAuLiqError=false;this.viewMsgLeyAuLiqEqual=false;this.viewMsgLeyAuLiqCheck=true;}
    }
  }
  changeLeyAgLiq(){
    if(this.rowSelectedEdit.strLeyAgLiq===''||this.rowSelectedEdit.strLeyAgOzTc===''||this.rowSelectedEdit.strLeyAgLiq===null||this.rowSelectedEdit.strLeyAgOzTc===null){
      this.viewMsgLeyAgLiqError = false;
      this.viewMsgLeyAgLiqEqual=false;
      this.viewMsgLeyAgLiqCheck=false;
    }
    else {
      var leyAgLiq = Number(this.rowSelectedEdit.strLeyAgLiq);
      var leyAgLab = Number(this.rowSelectedEdit.strLeyAgOzTc);
      if(leyAgLiq > leyAgLab) {this.viewMsgLeyAgLiqError=true;this.viewMsgLeyAgLiqEqual=false;this.viewMsgLeyAgLiqCheck=false;}
      else if(leyAgLiq === leyAgLab) {this.viewMsgLeyAgLiqError=false;this.viewMsgLeyAgLiqEqual=true;this.viewMsgLeyAgLiqCheck=false;}
      else {this.viewMsgLeyAgLiqError=false;this.viewMsgLeyAgLiqEqual=false;this.viewMsgLeyAgLiqCheck=true;}
    }
  }
  changeTipoConsumo(){
    if(this.TipoConsumo === 'si'){
      this.dataPropuestaAU[1].ConsumoKgTm = "Consumo(Kg/Tm)";
      this.dataPropuestaAU[2].ConsumoKgTm = this.getNumberFix((Number(this.rowSelectedEdit.strConsumoQ) + Number(this.rowSelectedEdit.strGastAdm))/7, 2);
    }
    else{
      this.dataPropuestaAU[1].ConsumoKgTm = "Consumo($/Tm)";
      this.dataPropuestaAU[2].ConsumoKgTm = this.getNumberFix(Number(this.rowSelectedEdit.strConsumoQ) + Number(this.rowSelectedEdit.strGastAdm), 2);
    }
  }
  changeLeyMaquilaTM(){
    if(this.ShowLeyMaquilaTM === true){
      this.dataPropuestaAU[1].LeyOz = "Ley(Oz/Tm)";
      this.dataPropuestaAU[2].LeyOz = this.getNumberFix((Number(this.rowSelectedEdit.strLeyAuLiq) * 1.1023), 3);
      this.dataPropuestaAU[1].Maquila = "Maquila";
      this.dataPropuestaAU[2].Maquila = this.getNumberFix((Number(this.rowSelectedEdit.strMaquila) * 1.1023), 2);
    }
    else{
      this.dataPropuestaAU[1].LeyOz = "Ley(Oz/Tc)";
      this.dataPropuestaAU[2].LeyOz = this.rowSelectedEdit.strLeyAuLiq;
      this.dataPropuestaAU[1].Maquila = "Maquila ($)";
      this.dataPropuestaAU[2].Maquila = this.rowSelectedEdit.strMaquila;
    }
  }
  changeLeyTMAG(){
    if(this.ShowLeyTMAG === true){
      this.dataPropuestaAG[1].LeyOz = "Ley Ag(Oz/Tm)";
      this.dataPropuestaAG[2].LeyOz = this.getNumberFix((Number(this.rowSelectedEdit.strLeyAgLiq) * 1.1023), 3);
    }
    else{
      this.dataPropuestaAG[1].LeyOz = "Ley Ag(Oz/Tc)";
      this.dataPropuestaAG[2].LeyOz = this.rowSelectedEdit.strLeyAgLiq;
    }
  }
  changeVerCostoAnalisis(){
    if(this.ShowCostoAMineral === false){
      var valueCostoInt: number = Number(this.dataPropuestaAU[2].CostoIntegro);
      this.dataPropuestaAU[2].CostoIntegro = this.getNumberFix(valueCostoInt - this.dataPropuestaAU[4].CostoIntegro, 2);
      this.dataPropuestaAU[2].CostoTM = this.getNumberFix(Number(this.dataPropuestaAU[2].CostoIntegro) / Number(this.dataPropuestaAU[2].TMS), 2);
    }
    else{
      var _leyoz = Number(this.rowSelectedEdit.strLeyAuLiq), _porcrec=Number(this.rowSelectedEdit.strPorcRecup),
          _precioint = Number(this.rowSelectedEdit.strPrecioAu), _margenpi=Number(this.rowSelectedEdit.strMargenPI),
          _consq = Number(this.rowSelectedEdit.strConsumoQ), _gastadm = Number(this.rowSelectedEdit.strGastAdm),
          _maqui = Number(this.rowSelectedEdit.strMaquila);

      if(this.checkedCalculoEsp === true)
        this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)*1.1023 -_maqui-_consq-_gastadm), 2);
      else
        this.dataPropuestaAU[2].CostoTM = this.getNumberFix((_leyoz*_porcrec/100 *(_precioint-_margenpi)-_maqui-_consq-_gastadm)*1.1023, 2);

      this.dataPropuestaAU[2].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[2].CostoTM * Number(this.PSecoTMS), 2);

      this.dataPropuestaAU[6].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[2].CostoIntegro - Number(this.dataPropuestaAU[4].CostoIntegro), 2); //NBASE
      this.dataPropuestaAU[7].CostoIntegro = this.getNumberFix(this.dataPropuestaAU[6].CostoIntegro * 0.18, 2); //IGV
      this.dataPropuestaAU[8].CostoIntegro = this.getNumberFix(Number(this.dataPropuestaAU[6].CostoIntegro) + Number(this.dataPropuestaAU[7].CostoIntegro), 2);//TOTAL
    }
  }

  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }
  changeCheckCostoAnalisis(){
    debugger;
    this.dataPropuestaAU[4].CostoIntegro = (this.checkedCostoAnalisis == false)?'0.00': this.getNumberFix(this.ParametrosZona.fltGastLabMax, 2);
    this.cargarDataPropuestaAu();
  }

  ExportarMargenPDF(){
    var Zona = this.rowSelectedEdit.strProcedenciaSt;
    var table:any = this.dataMargen;
    var doc = new jsPDF('l', 'pt', 'letter', true);
    doc.setFontType('bold')
    doc.setFontSize(14);
    doc.text(30, 30, 'Liquidación Margen');
    doc.line(30, 35, 765, 35)
    doc.setFontType('normal')
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(i===1 || i ===8){
        doc.setFontSize(7);
        doc.setFontType('bold')
      }
      else{
        doc.setFontType('normal')
        doc.setFontSize(6);
      }
      doc.cell(15,50,54,15,(item.Lote === null)?'':item.Lote.toString(), i);
      doc.cell(15,50,42,15,(item.FechaRec === null)?'':item.FechaRec.toString(), i);
      doc.cell(15,50,52,15,(item.Zona === null)?'':item.Zona.toString(), i);
      doc.cell(15,50,130,15,(item.Proveedor === null)?'':item.Proveedor.toString(), i);
      doc.cell(15,50,37,15,(item.TMH === null)?'':item.TMH.toString(), i);
      doc.cell(15,50,29,15,(item.PorcH2O === null)?'':item.PorcH2O.toString(), i);
      doc.cell(15,50,26,15,(item.TMS === null)?'':item.TMS.toString(), i);
      doc.cell(15,50,22,15,(item.REC === null)?'':item.REC.toString(), i);
      doc.cell(15,50,41,15,(item.LeyOz === null)?'':item.LeyOz.toString(), i);
      doc.cell(15,50,30,15,(item.Inter === null)?'':item.Inter.toString(), i);
      if(i > 0){
        doc.cell(15,50,39,15,(item.Margen === null)?'':item.Margen.toString(), i);
        doc.cell(15,50,40,15,(item.Maquila === null)?'':item.Maquila.toString(), i);
        doc.cell(15,50,57,15,(item.ConsQ === null)?'':item.ConsQ.toString(), i);
      }
      doc.cell(15,50,45,15,(item.GastAdm === null)?'':item.GastAdm.toString(), i);
      doc.cell(15,50,55,15,(item.PrecioxTMS === null)?'':item.PrecioxTMS.toString(), i);
      doc.cell(15,50,40,15,(item.Importe === null)?'':item.Importe.toString(), i);
      if(Zona === 'TRUJILLO'){
        doc.cell(15,50,30,15,(item.Fino === null)?'':item.Fino.toString(), i);
      }
      i++;
    });
    doc.save('Liq_Margen_'+this.rowSelectedEdit.strLote+'.pdf');
  }
  ExportarPropuestaAuPDF(){
    var VerConsumo = this.ShowConsumo;
    var verCostoAnalisis = this.ShowCostoAMineral;
    var PersonaProp = (this.rowSelectedEdit.strPersonaPropuesta==null||this.rowSelectedEdit.strPersonaPropuesta=='') ? this.userDec.strNombre : this.rowSelectedEdit.strPersonaPropuesta.trim();
    var table:any = this.dataPropuestaAU;
    var doc = new jsPDF('p', 'pt', 'letter', true);
    doc.setFontType('bold');
    doc.setFontSize(12);
    doc.text(30, 30, 'Propuesta Au');
    doc.line(30, 35, 580, 35);

    doc.setFontSize(9);
    doc.text(30, 50, 'Proveedor:');
    doc.text(350, 50, 'RUC:');
    doc.text(30, 70, 'Tipo Mineral:');
    doc.text(200, 70, 'N° Sacos:');
    doc.setFontType('normal');
    doc.text(90, 50, this.rowSelectedEdit.strClienteRazonS);
    doc.text(385, 50, this.rowSelectedEdit.strClienteRuc);
    doc.text(100, 70, this.rowSelectedEdit.strMaterial);
    doc.text(250, 70, (this.rowSelectedEdit.intNroSacos).toString());
    doc.text(155,310, 'La Joya Mining SAC', 'center');
    doc.text(455,310, 'Proveedor', 'center');
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(verCostoAnalisis == false && i===4){
        i++;
      }
      else {
        if(i===1 || i ===8){
          doc.setFontSize(7);
          doc.setFontType('bold')
        }
        else{
          doc.setFontType('normal')
          doc.setFontSize(7);
        }
        doc.cell(25,100,75,15,(item.Lote === null)?'': item.Lote.toString(), i);
        doc.cell(25,100,50,15,(item.FechaRec === null)?'': item.FechaRec.toString(), i);
        doc.cell(25,100,27,15,(item.TMH === null)?'': item.TMH.toString(), i);
        doc.cell(25,100,30,15,(item.PorcH2O === null)?'': item.PorcH2O.toString(), i);
        doc.cell(25,100,27,15,(item.TMS === null)?'': item.TMS.toString(), i);
        doc.cell(25,100,47,15,(item.LeyOz === null)?'': item.LeyOz.toString(), i);
        doc.cell(25,100,38,15,(item.PorcRecup === null)?'': item.PorcRecup.toString(), i);
        doc.cell(25,100,48,15,(item.PrecioInt === null)?'': item.PrecioInt.toString(), i);
        doc.cell(25,100,45,15,(item.Maquila === null)?'': item.Maquila.toString(), i);
        if(VerConsumo === true) doc.cell(25,100,60,15,(item.ConsumoKgTm === null)?'': item.ConsumoKgTm.toString(), i);
        doc.cell(25,100,64,15,(item.CostoTM === null)?'': item.CostoTM.toString(), i);
        if(i > 1) doc.setFontSize(8);
        doc.cell(25,100,58,15,(item.CostoIntegro === null)?'': item.CostoIntegro.toString(), i);
        i++;
      }
    });
    doc.line(80, 300, 230, 300);
    doc.text(155, 325, (PersonaProp).toUpperCase(),'center');
    doc.line(380, 300, 530, 300);
    doc.text(455, 325, this.rowSelectedEdit.strClienteRazonS, 'center');
    doc.save('Liq_PropuestaAu_'+this.rowSelectedEdit.strLote+'.pdf');
  }
  ExportarPropuestaAgPDF(){
    var PersonaProp = (this.rowSelectedEdit.strPersonaPropuesta==null||this.rowSelectedEdit.strPersonaPropuesta=='') ? this.userDec.strNombre : this.rowSelectedEdit.strPersonaPropuesta.trim();
    var table:any = this.dataPropuestaAG;
    var doc = new jsPDF('p', 'pt', 'letter', true);
    doc.setFontType('bold');
    doc.setFontSize(12);
    doc.text(30, 30, 'Propuesta AG');
    doc.line(30, 35, 580, 35)

    doc.setFontSize(9);
    doc.text(30, 50, 'Proveedor:');
    doc.text(350, 50, 'RUC:');
    doc.text(30, 70, 'Tipo Mineral:');
    doc.text(200, 70, 'N° Sacos:');
    doc.setFontType('normal');
    doc.text(90, 50, this.rowSelectedEdit.strClienteRazonS);
    doc.text(385, 50, this.rowSelectedEdit.strClienteRuc);
    doc.text(100, 70, this.rowSelectedEdit.strMaterial);
    doc.text(250, 70, (this.rowSelectedEdit.intNroSacos).toString());
    doc.text(155,310, 'La Joya Mining SAC', 'center');
    doc.text(455,310, 'Proveedor', 'center');
    doc.cellInitialize();
    var i=0;
    table.forEach(function(item) {
      if(i===1 || i ===8){
        doc.setFontSize(8);
        doc.setFontType('bold')
      }
      else{
        doc.setFontType('normal')
        doc.setFontSize(8);
      }
      doc.cell(30,100,75,15,(item.Lote === null)?'': item.Lote.toString(), i);
      doc.cell(30,100,55,15,(item.FechaRec === null)?'': item.FechaRec.toString(), i);
      doc.cell(30,100,40,15,(item.TMH === null)?'': item.TMH.toString(), i);
      doc.cell(30,100,40,15,(item.PorcH2O === null)?'': item.PorcH2O.toString(), i);
      doc.cell(30,100,35,15,(item.TMS === null)?'': item.TMS.toString(), i);
      doc.cell(30,100,60,15,(item.LeyOz === null)?'': item.LeyOz.toString(), i);
      doc.cell(30,100,50,15,(item.PorcRecup === null)?'': item.PorcRecup.toString(), i);
      doc.cell(30,100,60,15,(item.PrecioInt === null)?'': item.PrecioInt.toString(), i);
      doc.cell(30,100,60,15,(item.CostoTM === null)?'': item.CostoTM.toString(), i);
      doc.cell(30,100,70,15,(item.CostoIntegro === null)?'': item.CostoIntegro.toString(), i);
      i++;
    });
    doc.line(80, 300, 230, 300);
    doc.text(155, 325, (PersonaProp).toUpperCase(),'center');
    doc.line(380, 300, 530, 300);
    doc.text(455, 325, this.rowSelectedEdit.strClienteRazonS, 'center');
    doc.save('Liq_PropuestaAg_'+this.rowSelectedEdit.strLote+'.pdf');
  }


  //REINTEGROS
  handleReintegro(index, row){
    this.dataReintegro = JSON.parse(JSON.stringify(row));
    this.rowReintegro.IdLote = row.intIdRegistro;
    this.rowReintegro.Lote = row.strLote;
    this.dialogReintegrosVisible = true;
  }
  cerrarDialogReintegro(val){
    this.dialogReintegrosVisible = false;
  }
  //EXPORTACIONES
  handleSelectionChange(val) {
    this.multipleSelection = val;
  }
  GuardarExportacionAny(){
    if(this.multipleSelection.length > 0){
      if(this.nroExportacionAny == '' || this.nroExportacionAny == null) this.openMessageError('Ingrese N° de Exportación.');
      else{
        var aux = this.multipleSelection
        for (let i = 0; i < aux.length; i++) {
          var consulta = "UPDATE tblLiquidacion SET strNroExportacion = '"+this.nroExportacionAny.toString()+
                         "', dtmFechaExportacion = '"+GLOBAL.getParseDate(this.fechaExportacion)+
                         "' WHERE strLote = '"+aux[i].strLote+"'";
          this.ExecuteConsulta(consulta);
        }
        this.BuscarLotes();
        this.nroExportacionAny = '';
      }
    }
    else this.openMessageError('No se seleccionó ningún lote.');
  }
  ActualizarLeyesLiq(){
    this.rowSelectedEdit.strLeyAuLiq = this.rowSelectedEdit.strLeyAuComercial;
    this.rowSelectedEdit.strLeyAgLiq = this.rowSelectedEdit.strLeyAgComercial;
  }

  EnviarCostos(){
    var consulta = "UPDATE tblLiquidacion SET fltCostoTotal = "+Number(this.dataPropuestaAU[2].CostoIntegro) +
                   ", strNBaseAu = '" + this.dataPropuestaAU[6].CostoIntegro +
                   "', strCostoAnalisis = '" + this.dataPropuestaAU[4].CostoIntegro +
                   "' WHERE strLote = '"+this.dataPropuestaAU[2].Lote+"'";
    console.log(consulta);
    this.ExecuteConsulta(consulta);
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickEnviarMargenes(){
    var loadPropAU = this.handlePropuestaAU(-1, this.rowSelectedEdit);
    var loadPropAG = this.handlePropuestaAG(-1, this.rowSelectedEdit);

    var strNBaseAu = (isNaN(this.dataPropuestaAU[6].CostoIntegro)||this.dataPropuestaAU[6].CostoIntegro==undefined||this.dataPropuestaAU[6].CostoIntegro==null)?'':this.dataPropuestaAU[6].CostoIntegro.toString();
    var strNBaseAg = (isNaN(this.dataPropuestaAG[4].CostoIntegro)||this.dataPropuestaAG[4].CostoIntegro==undefined||this.dataPropuestaAG[4].CostoIntegro==null)?'':this.dataPropuestaAG[4].CostoIntegro.toString();

    var consulta = "UPDATE tblLiquidacion "+
			"SET strNBaseAu='"+strNBaseAu+"', strNBaseAg='"+strNBaseAg+"' "+
			"WHERE intIdLiquidacion = "+this.rowSelectedEdit.intIdLiquidacion
    this.ExecuteConsulta(consulta);
  }
  selectRow(row,index){
    this.checkedCostoAnalisis = true;
    this.rowSelectedEdit = {};
    this.rowSelectedEdit = row;
    this.dataMargen[3].GastAdm = ''; //Limpiar gasto
    this.statusLiquid = (row.strEstadoLiq == null)? 'Pendiente' : row.strEstadoLiq;
    this.marcarLoteVolado = (row.strLoteVolado == 'true') ? true : false;
    this.guardarViewButton = ((this.statusLiquid === 'Liquidado'||this.statusLiquid === 'Facturado'||this.viewAccessEscritura==false) && this.userDec.strSuperAdmin != 'true') ? false : true;
    this.fechaLiqMargen = (row.dtmFechaLiquidacion === null)? new Date() : new Date(row.dtmFechaLiquidacion);
    this.fechaNegociacionAu = (row.dtmFechaPropuesta === null)? new Date() : new Date(row.dtmFechaPropuesta);
    this.fechaNegociacionAg = (row.dtmFechaPropuesta === null)? new Date() : new Date(row.dtmFechaPropuesta);
    this.fechaEmisionFactura = (row.dtmFechaFacturacion === null)? new Date() : new Date(row.dtmFechaFacturacion);
    this.rowSelectedEdit.strLeyAuLiq = (this.rowSelectedEdit.strLeyAuLiq==''||this.rowSelectedEdit.strLeyAuLiq==null)?row.strLeyAuComercial : row.strLeyAuLiq;
    this.rowSelectedEdit.strLeyAgLiq = (this.rowSelectedEdit.strLeyAgLiq==''||this.rowSelectedEdit.strLeyAgLiq==null)?row.strLeyAgComercial : row.strLeyAgLiq;
    this.rowSelectedEdit.strLeyAuMargen = (this.rowSelectedEdit.strLeyAuMargen==''||this.rowSelectedEdit.strLeyAuMargen==null)
      ? ((row.strCheckH2O=='true') ? row.strPorcH2O : this.getNumberFix(Number(row.strPorcH2O)+1,2))
      : row.strLeyAuMargen;
    this.PSecoTMS = this.getNumberFix(this.rowSelectedEdit.fltPesoTmh * (100 - Number(this.rowSelectedEdit.strLeyAuMargen))/100, 3);

    this.checkedCostoAnalisis = (this.rowSelectedEdit.strCostoAnalisis == null || Number(this.rowSelectedEdit.strCostoAnalisis) > 0)?true: false;

    this.dataMargen[3].GastAdm = (this.rowSelectedEdit.strGastAdmIntAu==''||this.rowSelectedEdit.strGastAdmIntAu==null)
      ? this.dataMargen[3].GastAdm
      : this.rowSelectedEdit.strGastAdmIntAu;
    // this.dataMargen[6].REC = (this.rowSelectedEdit.strRecIntAg==''||this.rowSelectedEdit.strRecIntAg==null)?'40':this.rowSelectedEdit.strRecIntAg;
    if(row.intIdLiquidacion === null) {
      // this.viewEstadoLoteLiqCheck = false;
      // this.viewEstadoLoteLiqError = true;
    }
    else { 
      // this.viewEstadoLoteLiqCheck=true; this.viewEstadoLoteLiqError=false 
    }

    //Habilitar descarga PDF
    if(this.statusLiquid === 'Liquidado' || this.statusLiquid === 'Facturado'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = false;
    }
    else if(this.statusLiquid === 'Propuesta'){
      this.disableExpPdfPropuesta = false;
      this.disableExpPdfMargen = true;
    }
    else{
      this.disableExpPdfPropuesta = true;
      this.disableExpPdfMargen = true;
    }

    this.activeName = 'first';
    this.TipoConsumo = 'no';
    this.ShowLeyMaquilaTM = false;
    this.ShowLeyTMAG = false;
    this.NroFacGenerado = (row.strNroFactura == null || row.strNroFactura == '')? '' : row.strNroFactura;
    this.checkedFacElect = false;

    this.handleParametros(index, row);
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strProveedor : '',
        strFechaInicio : '',
        strFechaFin : '',
        strEstadoLote : '',
        strFechaLiqInicio: '',
        strFechaLiqFin: '',
      },
      json_fields: {
        "strLote":"LOTE",
        "strEstadoLiq": "Estado",
        "dtmFechaRecepcion":"Fecha Recepción",
        "strProcedenciaSt":"Procedencia",
        "strClienteRazonS":"Proveedor Mineral",
        "fltPesoTmh":"Peso TMH",
        "strPorcH2O":"%H2O",
        "strCheckH2O" : "Check H2O",
        "strPSecoTM": "P.Seco TM",
        "strLeyAuOzTc": "Ley Au Oz/tc",
        "strLeyAuREE": "Ley Au REE",
        "strLeyAuComercial":"Ley Au Comercial",
        "strLeyAgOzTc":"Ley Ag (Oz/tc)",
        "strLeyAgComercial":"Ley Ag Comercial",
        "strLeyAuMargen" : "%H2O Liquidación",
        "strLeyAuLiq" : "Ley Au Liquidación",
        "strLeyAgLiq" : "Ley Ag Liquidación",
        "strPorcRecup":"% Recup.",
        "strPrecioAu":"Precio Au",
        "strMargenPI":"Margen P.I.",
        "strMaquila":"Maquila",
        "strConsumoQ":"Consumo Q",
        "strGastAdm":"Gast Adm.",
        "strRecAg":"Rec Ag",
        "strPrecioAg":"Precio Ag",
        "strMargenAg":"Margen Ag",
        "fltCostoTotal":"Costo Total",
      },
      json_fieldsMargen : {
        "Lote":"",
        "FechaRec":"",
        "Zona":"",
        "Proveedor":"",
        "TMH":"",
        "PorcH2O":"",
        "TMS":"",
        "REC":"",
        "LeyOz":"",
        "Inter":"",
        "Margen":"",
        "Maquila":"",
        "ConsQ":"",
        "GastAdm":"",
        "PrecioxTMS":"",
        "Importe":"",
      },
      json_fieldsPAu:{
        "Lote":"",
        "FechaRec":"",
        "TMH":"",
        "PorcH2O":"",
        "TMS":"",
        "LeyOz":"",
        "PorcRecup":"",
        "PrecioInt":"",
        "Maquila":"",
        "ConsumoKgTm":"",
        "CostoTM":"",
        "CostoIntegro":""
      },
      json_fieldsPAg:{
        "Lote":"",
        "FechaRec":"",
        "TMH":"",
        "PorcH2O":"",
        "TMS":"",
        "LeyOz":"",
        "PorcRecup":"",
        "PrecioInt":"",
        "CostoTM":"",
        "CostoIntegro":""
      },
      dataMargen:[
        {Lote:'Fecha Liquidación',FechaRec:'',Zona:'',Proveedor:'Factor de Conversión',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[0]
        {Lote:'',FechaRec:'Fecha Rec.',Zona:'Zona',Proveedor:'Proveedor',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',REC:'REC',LeyOz:'Ley(Oz/Tc)',Inter:'Inter($)',Margen:'Margen($)',Maquila:'Maquila($)',ConsQ:'Cons Q(Kg/Tm)',GastAdm:'Gast Adm($)',PrecioxTMS:'PrecioxTMS($)',Importe:'Importe($)',Fino:'Finos'}, //[1] Lote - Titulos
        {Lote:'Propuesta(Au)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[2] Propuesta (Au)
        {Lote:'Liq Int(Au)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[3] Liq Int(Au)
        {Lote:'',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[4] Vacio
        {Lote:'Propuesta(Ag)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[5] Propuesta (Ag)
        {Lote:'Liq Int(Ag)',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[6] Liq Int(Ag)
        {Lote:'',FechaRec:'',Zona:'',Proveedor:'',TMH:'',PorcH2O:'',TMS:'',REC:'',LeyOz:'',Inter:'',Margen:'',Maquila:'',ConsQ:'',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[7] Vacio
        {Lote:'',FechaRec:'',Zona:'Importe',Proveedor:'',TMH:'',PorcH2O:'Au',TMS:'',REC:'',LeyOz:'',Inter:'Ag',Margen:'',Maquila:'',ConsQ:'',GastAdm:'Por Lote',PrecioxTMS:'',Importe:'',Fino:''}, //[8] Au - Ag - Por lote
        {Lote:'',FechaRec:'Liq Int(Au)',Zona:'',Proveedor:'',TMH:'Margen Bru',PorcH2O:'',TMS:'',REC:'',LeyOz:'Margen Bru',Inter:'',Margen:'',Maquila:'',ConsQ:'Margen Bru',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[9] Margen Bru
        {Lote:'',FechaRec:'%',Zona:'',Proveedor:'',TMH:'Utilidad Bru',PorcH2O:'',TMS:'',REC:'',LeyOz:'Utilidad Bru',Inter:'',Margen:'',Maquila:'',ConsQ:'Utilidad Bru',GastAdm:'',PrecioxTMS:'',Importe:'',Fino:''}, //[10] Utilidad Bru
      ],
      dataPropuestaAU:[
        {Lote:'Fecha Negociación',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''}, //Fecha Negociacion
        {Lote:'Lote',FechaRec:'Fecha Rec.',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',LeyOz:'Ley(Oz/Tc)',PorcRecup:'% Recup.',PrecioInt:'Precio Inter',Maquila:'Maquila ($)',ConsumoKgTm:'Consumo($/Tm)',CostoTM:'Costo/TM',CostoIntegro:'Costo Integro'}, //Cabeceras Tabla
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''}, //Datos
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'Costo de Analisis',CostoIntegro:''}, //Costo de Analisis
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'NBASE',CostoIntegro:''}, //NBASE
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'IGV. 18%',CostoIntegro:''}, //IGV 18%
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',Maquila:'',ConsumoKgTm:'',CostoTM:'TOTAL',CostoIntegro:''} //Total
      ],
      dataPropuestaAG:[
        {Lote:'Fecha Negociación',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''}, //Fecha Negociacion
        {Lote:'Lote',FechaRec:'Fecha Rec.',TMH:'TMH',PorcH2O:'% H2O',TMS:'TMS',LeyOz:'Ley Ag(Oz/Tc)',PorcRecup:'% Recup.',PrecioInt:'Precio Inter',CostoTM:'Costo/TM',CostoIntegro:'Costo Integro'}, //Cabeceras Tabla
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''}, //Datos
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'NBASE',CostoIntegro:''}, //IGV 18%
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'IGV. 18%',CostoIntegro:''},
        {Lote:'',FechaRec:'',TMH:'',PorcH2O:'',TMS:'',LeyOz:'',PorcRecup:'',PrecioInt:'',CostoTM:'TOTAL',CostoIntegro:''} //Total
      ],
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ],
      DataRecuperacionAu: [
        {id:'80', name:'80'},
        {id:'85', name:'85'},
        {id:'88', name:'88'},
        {id:'89', name:'89'},
        {id:'90', name:'90'}
      ],
      DataRecuperacionAg: [
        {id:'40', name:'40'},
        {id:'45', name:'45'},
        {id:'50', name:'50'},
        {id:'55', name:'55'},
        {id:'60', name:'60'}
      ],
      DataTipoConsumo: [
        {id:'no', name:'Consumo($/Tm)'},
        {id:'si', name:'Consumo(Kg/Tm)'},
      ],
      DataEstado: [
        {id: 'Propuesta', name:'PROPUESTAS'},
        {id: 'Liquidado', name:'LIQUIDADOS'},
        {id: 'Facturado', name:'FACTURADOS'},
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
