import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import UsuarioService from '../../services/usuario.service'
import { Notification } from 'element-ui';
import router from '../../router';
import GLOBAL from '@/Global';
import QueryService from '@/services/query.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'usuario',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class UsuarioComponent extends Vue {
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  dataDominios :any =[];
  dataRoles : any = [];
  rowSelectedEdit :any = [];
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  //PAGINATION
  pagina:number =1;
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';;
  RegistersForPage:number = 15;
  totalRegistros:number = this.RegistersForPage;
  userDec: any = {};
  FormAgregar: any = {};
  loadingGetData: boolean = false;
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  cbSuperAdminAdd: boolean = false;
  cbSuperAdminEdit: boolean = false;
  FormSearch: any = {};
  constructor(){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 11){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.ConsultarUsuarios();
        this.getRoles();
      }
    }
  }
  // loadingData(){
  //   this.loadingGetData = true;
  //   UsuarioService.loadingData()
  //   .then(response => {
  //     this.CompleteData = response.Data;
  //     this.totalRegistros = response.Count;
  //     this.countGridData = response.Count;
  //     this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  //     this.loadingGetData = false;
  //   })
  //   .catch(e =>{
  //     console.log(e);
  //     this.loadingGetData = false;
  //     if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
  //     else  this.openMessageError('Error al cargar usuarios');
  //   })
  // }
  getRoles(){
    UsuarioService.getRoles()
    .then(response => {
      this.dataRoles = response;
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar roles');
    })
  }
  //CONEXION WEB SERVICIO USUARIO.SERVICIO
  AgregarUsuario(FormAgregar){
    FormAgregar.strSuperAdmin = this.cbSuperAdminAdd.toString();
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    UsuarioService.AgregarUsuario(FormAgregar)
    .then(response => {
      this.openMessage(response);
      this.ConsultarUsuarios();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar usuario');
    })
    this.dialogAgregarVisible = false;
  }
  EliminarUsuario(row){
    UsuarioService.EliminarUsuario(row.strCodUsuario, this.userDec.strUsuario)
    .then(response => {
      this.openMessage(response);
      this.ConsultarUsuarios();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al Deshabilitar usuario');
    })
  }
  EditarUsuario(){
    this.rowSelectedEdit.strSuperAdmin = this.cbSuperAdminEdit.toString();
    this.rowSelectedEdit.strUsuarioModif = this.userDec.strUsuario;

    UsuarioService.EditarUsuario(this.rowSelectedEdit)
    .then(response => {
      this.openMessage(response)
      this.ConsultarUsuarios();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al editar usuario');
    })
    this.dialogEditarVisible = false;
  }
  HabilitarUsuario(row){
    let query = "UPDATE tblUsuarios	SET strUsuarioModif = '"+this.userDec.strUsuario+
                "', dtmFechaModif = GETDATE(), chrEstado = 'A'"+
                "	WHERE strCodUsuario = '"+row.strCodUsuario+"'"
    var DataQ = {
      strQuery: query,
      strUsuarioCrea: this.userDec.strUsuario
    }
    QueryService.ExecuteQuery(DataQ)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al habilitar usuario.')
      else this.openMessage('Se habilitó el usuario: '+row.strUsuario+' correctamente.');
      this.ConsultarUsuarios();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al habilitar usuario.');
    })
  }

  ConsultarUsuarios(){
    UsuarioService.ConsultarUsuarios(this.FormSearch)
    .then(response => {
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al consultar usuarios');
    })
  }

  LimpiarFormAgregar(){
    this.FormAgregar.strNombres = '';
    this.FormAgregar.strApellidoPat = '';
    this.FormAgregar.strApellidoMat = '';
    this.FormAgregar.strEmail = '';
    this.FormAgregar.strUsuario = '';
    this.FormAgregar.strPassword = '';
    this.FormAgregar.strCargo = '';
    this.cbSuperAdminAdd = false;
  }

  handleAgregar(){
    this.LimpiarFormAgregar();
    this.getRoles();
    this.dialogAgregarVisible = true
  }
  handleEdit(index, row){
    this.getRoles();
    this.rowSelectedEdit = row;
    // console.log(btoa(this.rowSelectedEdit.strPassword));
    // console.log(atob(this.rowSelectedEdit.strPassword));
    // console.log('clave');
    
    this.cbSuperAdminEdit = (row.strSuperAdmin == 'true')? true : false;
    this.dialogEditarVisible = true
  }
  handleDelete(index, row){
    this.$confirm('Desea Deshabilitar el Usuario: '+row.strUsuario +' ?', 'Deshabilitar', {
      confirmButtonText: 'Deshabilitar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.EliminarUsuario(row);
    }).catch(() => {
    });
  }
  handleHabilitar(index, row){
    this.$confirm('Desea Habilitar el Usuario: '+row.strUsuario +' ?', 'Habilitar', {
      confirmButtonText: 'Habilitar',
      cancelButtonText: 'Cancelar',
      type: 'success'
    }).then(() => {
      this.HabilitarUsuario(row);
    }).catch(() => {
    });
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(newMsg : string) {
    this.$message({
      showClose: true,
      message: newMsg,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
        showClose: true,
        type: 'error',
        message: strMessage
      });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar: {
        strNombres:'',
        strApellidoPat:'',
        strApellidoMat:'',
        strEmail : '',
        strUsuario: '',
        strPassword:'',
        strCargo: '',
        strUsuarioCrea: ''
      },
      FormSearch:{
        strCodUsuario:'A',
        strUsuario: '',
        strCargo:'',
        strNombres:'',
        strApellidoPat:'',
        strApellidoMat:''
      },
      DataEstado: [
        {id: 'A', name: 'Habilitados'},
        {id: 'E', name: 'Deshabilitados'}
      ],
      LimpiarConsulta(){
        this.FormSearch.strCodUsuario = 'A';
        this.FormSearch.strUsuario = '';
        this.FormSearch.strCargo = '',
        this.FormSearch.strNombres = '';
        this.FormSearch.strApellidoPat = '';
        this.FormSearch.strApellidoMat = '';
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
