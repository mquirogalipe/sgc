import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import { Loading } from 'element-ui';
import CuotasService from '@/services/cuotas.service';
import AcopiadorService from '@/services/acopiador.service';
import ZonaService from '@/services/zona.service';
import AcopiadorComponent from '@/components/Acopiador/acopiador.vue';
import ZonaComponent from '@/components/zona/zona.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
  name: 'cuotas',
  components:{
    'module-acopiador':AcopiadorComponent,
    'module-zona' : ZonaComponent,
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class CuotasComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number =1;
  RegistersForPage:number = 10;
  totalRegistros:number = this.RegistersForPage;

  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  dialogDetalleVisible: boolean = false;
  FormAgregar: any = {};
  fechaAddCuota: Date = new Date();
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];

  rowSelectedEdit: any = {};
  fechaDetCuota: any = '';

  DataZonas: any =[];
  DataAcopiadores: any = [];

  verAcopio: boolean = false;
  verAcopiadores: boolean = false;
  verZonas: boolean = false;
  loadingGetData:boolean = false;
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  nameButton: string = '';
  fechaSearch: Date = new Date();
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  sumatoriaPesoEstimado:any = 0;
  selectRow:any=null;
  indexRow:any=null;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1005){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          this.verAcopio = true;
        }
        if(listaAccesos[i].intCodAcceso === 1003){
          flag=true;
          this.verAcopiadores = true;
        }
        if(listaAccesos[i].intCodAcceso === 1004){
          flag=true;
          this.verZonas = true;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.nameButton = ((this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador') && this.userDec.strCargo != 'Control Acopio')? 'Actualizar' : 'Detalle';
        this.BuscarCuotas();
      }
    }
  }

  loadingData(){
    this.loadingGetData = true;
    CuotasService.loadingData()
    .then(response => {
      this.gridData = response.Data;
      this.countGridData = response.Count;
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar cuotas');
    })
  }
  BuscarCuotas(){
    if(this.fechaSearch == null || this.fechaSearch == undefined){
      this.openMessageError('Seleccione mes');
    }
    else{
      this.loadingGetData = true;
      var month = this.monthNames[this.fechaSearch.getMonth()];
      CuotasService.BuscarCuotas(month)
      .then(response =>{
        this.gridData = response.Data;
        this.countGridData = response.Count;
        this.loadingGetData = false;
        this.sumarPesoEstimado(this.gridData);
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar cuotas');
      })
    }
  }
  loadingDataAcopiadores(){
    AcopiadorService.loadingData()
    .then(response => {
      this.DataAcopiadores = response.Data;
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar acopiadores');
    })
  }
  loadingDataZonas(){
    ZonaService.loadingData()
    .then(response => {
      this.DataZonas = response.Data;
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  AgregarCuota(FormAgregar){
    var year = this.fechaAddCuota.getFullYear();
    var month = this.monthNames[this.fechaAddCuota.getMonth()];
    FormAgregar.strAnio = year;
    FormAgregar.strMes = month;
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    var data = {
      intIdAcopiador: Number(FormAgregar.intIdAcopiador),
      intIdZona: Number(FormAgregar.intIdZona),
      fltCuota: Number(FormAgregar.fltCuota),
      strLeyAu:  FormAgregar.strLeyAu.toString(),
      strFinoAu: FormAgregar.strFinoAu.toString(),
      strUnidadMedida: FormAgregar.strUnidadMedida,
      strAnio: year.toString(),
      strMes: month.toLowerCase(),
      strUsuarioCrea: this.userDec.strUsuario
    }
    CuotasService.AgregarCuota(data)
    .then(response => {
      if(response === -1) this.openMessageError("Error al agregar cuota:"+response)
      else if(response === -2) this.openMessageError("Ya existe una cuota del Acopiador con el mismo mes y año.")
      else
        this.openMessage('Cuota guardada correctamente.');
      //this.loadingData();
      this.BuscarCuotas();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Cuota.');
    })
    this.dialogAgregarVisible = false;
  }
  ActualizarCuota(rowEdit){
    rowEdit.strUsuarioModif = this.userDec.strUsuario;
    CuotasService.EditarCuota(rowEdit)
    .then(response => {
      if(response === -1) this.openMessageError("Error al agregar cuota:"+response)
      else if(response === -2) this.openMessageError("Ya existe una cuota del Acopiador con el mismo mes y año.")
      else
        this.openMessage('Cuota actualizada correctamente.');
      //this.loadingData();
      this.BuscarCuotas();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Cuota.');
    })
    this.dialogEditarVisible = false;
  }

  LimpiarFormAgregar(){
    this.FormAgregar.intIdAcopiador = '';
    this.FormAgregar.intIdZona = '';
    this.FormAgregar.fltCuota = '';
    this.FormAgregar.strLeyAu =  '';
    this.FormAgregar.strFinoAu = '';
    this.FormAgregar.strUnidadMedida = '';
    this.FormAgregar.strAnio = '';
    this.FormAgregar.strMes = '';
    this.FormAgregar.strUsuarioCrea = '';
  }

  handleAgregar(){
    this.LimpiarFormAgregar();
    this.loadingDataAcopiadores();
    this.loadingDataZonas();
    this.dialogAgregarVisible = true;
  }
  handleDetalle2(){
    debugger;
    this.handleDetalle(this.seleccionarRow);
  }
  handleDetalle(row){
    this.loadingDataAcopiadores();
    this.loadingDataZonas();
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Control Acopio') {
        this.dialogDetalleVisible = true;
      }
      else this.dialogEditarVisible = true;
    }
    else this.dialogDetalleVisible = true;
  }
  sumarPesoEstimado(data){
    var PesoTotal = 0;
    data.forEach(function(element) {
      if(element.fltCuota != null || element.fltCuota != '')
        PesoTotal += element.fltCuota;
    });
    this.sumatoriaPesoEstimado = this.getNumberFix(PesoTotal, 2);
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  handleClick(tab, event) {
    // console.log(tab, event);
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  seleccionarRow(row,index){
    debugger;
    this.seleccionarRow=row;
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar: {
        intIdAcopiador: '',
        intIdZona: '',
        fltCuota: '',
        strLeyAu: '',
        strFinoAu: '',
        strUnidadMedida: '',
        strAnio: '',
        strMes: '',
        strUsuarioCrea: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }
}
