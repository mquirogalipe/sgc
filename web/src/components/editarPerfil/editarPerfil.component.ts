import { Vue, Component } from 'vue-property-decorator'
import { Notification } from 'element-ui';
import EditarPerfilService from '@/services/editarPerfil.service';
import LoginService from '@/services/login.service';
import { Loading } from 'element-ui';
import GLOBAL from '@/Global';
import router from '../../router';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
@Component({
   name: 'editarPerfil',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
  }
})
export default class EditarPerfilComponent extends Vue {
  active:number = 0;
  showActual: boolean = true;
  showNew: boolean = false;
  FormUpdate: any = {};
  userDec: any = {};
  FormUpdateLogin : any = {};
  FormLogin : any = {};
  constructor (){
    super()
  }

  checkAccesos(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página')
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      if (listaAccesos.length > 0){
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined) ? {} : JSON.parse(GLOBAL.decodeString(session));
        this.bind();
      }
      else{
        Notification.warning('No tiene permisos para acceder a esta página')
        this.$router.push('/');
      }
    }
  }

  bind(){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Cargando datos...'
    });

    EditarPerfilService.loadingPersona(this.userDec.strCodPersona)
    .then(response =>{
      this.FormUpdate.strCodPersona = response.strCodPersona;
      this.FormUpdate.strNombres = response.strNombres;
      this.FormUpdate.strApellidoPat = response.strApellidoPat;
      this.FormUpdate.strApellidoMat = response.strApellidoMat;
      this.FormUpdate.strEmail = response.strEmail;
      this.FormUpdate.strTelefono = response.strTelefono;

      this.FormLogin.strUsuario = this.userDec.strUsuario;
      loadingInstance.close();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar datos del usuario.');
    })
  }
  GuardarCambios(FormUpdate){
    this.$confirm('Desea guardar los cambios?', 'Guardar', {
      confirmButtonText: 'Guardar',
      cancelButtonText: 'Cancelar',
      type: 'success'
    }).then(() => {
      FormUpdate.strUsuarioModif = this.userDec.strUsuario;
      EditarPerfilService.UpdatePersona(FormUpdate)
      .then(response =>{
        this.openMessage(response);
        this.bind();
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al guardar cambios.')
      })
    }).catch(() => {
    });
  }

  handleClick(tab, event) {
    // console.log(tab, event);
  }

  nextStep() {
    LoginService.LoginUser(this.FormLogin)
    .then(response =>{
      if(response != null){
        this.showActual = false;
        this.showNew = true;

        this.FormUpdateLogin.strUsuarioNew = this.FormLogin.strUsuario;
        this.FormUpdateLogin.strPasswordNew = '';
        this.FormUpdateLogin.strPasswordNewConfirm = '';
      }
      else{
        this.openMessageError('Usuario y/o password incorrectos.');
      }

    }).catch(e =>{
      this.showActual = true;
      this.showNew = false;
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al Iniciar Sesión');
    })
    if (this.active++ > 1) this.active = 0;
  }
  finishStep(){
    if(this.FormUpdateLogin.strPasswordNew === this.FormUpdateLogin.strPasswordNewConfirm){
      var Data = {
        strCodUsuario: this.userDec.strCodUsuario,
        strUsuario: this.FormUpdateLogin.strUsuarioNew,
        strPassword: this.FormUpdateLogin.strPasswordNew,
        strUsuarioModif: this.userDec.strUsuario
      }
      EditarPerfilService.ChangeUsuario(Data)
      .then(response =>{
        if(response === 'vacio'){
          this.openMessageError('El usuario ingresado ya existe.')
        }
        else{
          this.openMessage(response);
          this.active = 0;
          this.showNew = false;
          this.showActual = true;
          this.FormLogin.strPassword = '';
        }
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al guardar cambios.');
      })
    }
    else{
      this.openMessageError('Las contraseñas no coinciden.');
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      activeName:'first',
      FormUpdate: {
        strCodPersona: '',
        strNombres: '',
        strApellidoPat: '',
        strApellidoMat: '',
        strEmail: '',
        strTelefono: '',
        strUsuarioModif:  ''
      },
      FormUpdateLogin: {
        strUsuarioNew: '',
        strPasswordNew: '',
        strPasswordNewConfirm: ''
      },
      FormLogin:{
        strUsuario: '',
        strPassword: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccesos();
    }
  }
}
