import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import router from '@/router';
import GLOBAL from '@/Global';
@Component({
  name: 'top-menu',
})
export default class TopMenu extends Vue {
  accesosUser:any = [];
  mostrarConfig:boolean = true;
  userLoged: any = {};
  usuarioLogeado: string = 'SA';
  codigo:string;
  ocultar:boolean=false;
  isActive:boolean=false;
  isCollapse:boolean=false;
  constructor(){
    super()
  }
  bind(){
    var session = window.sessionStorage.getItem("session_user");
    this.userLoged = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
    this.usuarioLogeado = this.userLoged.strUsuario;
  }
  getAccesos(){
    this.bind();
    var decoded:any= window.sessionStorage.getItem('session_access');
    if( decoded === null ){
      this.openMessageError('No tiene acceso');
    }
    else{
      var dataAccesos:any = JSON.parse(GLOBAL.decodeString(decoded));
      for (var i=0; i< dataAccesos.length; i++){
        if(dataAccesos[i].intNivel == 7){
          this.accesosUser.push({
            strNombre: dataAccesos[i].strNombre,
            strIndex: dataAccesos[i].strIndex,
            strIconName: dataAccesos[i].strIconName,
            strEnlace: dataAccesos[i].strEnlace
          });
        }
      }
      if(this.accesosUser.length === 0){
        this.mostrarConfig = false;
      }
    }
  }
  loadBarmenu(){
    this.$emit('getLink',this.codigo);
  }
  fnOcultar(){
    this.ocultar=!this.ocultar;
  }
  linkRoute(route){
    router.push(route)
  }
  clickHamburger () {
    debugger;    
    this.isActive=!GLOBAL.getActive()
    this.isCollapse=!GLOBAL.getCollapse()
    GLOBAL.setActive(this.isActive);
    GLOBAL.setCollapse(this.isCollapse);
    this.$emit('proveedorSeleccionado');
  }
  linkLogout(){
    window.sessionStorage.clear();
    router.push('/')
  }
  clickCollapse(){
    this.$emit('CollapseTopMenu','click collapse');
  }
  openMessage(newMsg : string) {
    this.$message({
      showClose: true,
      message: newMsg,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  data(){
    return{
      dialogTableVisible: false,
      user: {
        authenticated: false
      },
      data:{
        Usuario:localStorage.getItem('User_Nombre'),
      },
      accesosUser: [],
      hours: 0,
      minutos:0,
      seconds:0,
      codigo:'',
    }
  }
  created() {
    if(typeof window != 'undefined') {
      this.getAccesos();
    }
  }
}
