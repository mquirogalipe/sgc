import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LotesService from '@/services/lotes.service';
import ZonaService from '@/services/zona.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'atencionCliente',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class AtencionClienteComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina: number =1;
  RegistersForPage: number = 25;
  totalRegistros: number = this.RegistersForPage;
  FormSearch:any = {}
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';;//'0';
  loadingGetData:boolean = false;

  fechaSearch: Date = new Date();
  fechaInicio: any = '';
  fechaFin: any = '';
  dataZonas: any = [];
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  
  
  blnperiodo:boolean=false;
  blnfiltroavanzado:boolean=false;

  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 2){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página');
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        // this.nameButton = (this.userDec.strCargo === 'Area Balanza' || this.userDec.strCargo === 'Administrador')? 'Actualizar' : 'Detalle';
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        this.BuscarLotes();
        this.getZonas();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
    this.FormSearch.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);

    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = true;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  LimpiarConsulta(){
    this.FormSearch.strLote = '';
    this.FormSearch.strProcedencia = '';
    this.FormSearch.strAcopiador = '';
    this.FormSearch.strProveedor = '';
    this.FormSearch.strMaterial = '';
    this.FormSearch.strEstadoLote = '';
    this.FormSearch.strFechaInicio = '';
    this.FormSearch.strFechaFin = '';
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },

    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
