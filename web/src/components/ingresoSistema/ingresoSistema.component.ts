import { Vue, Component } from 'vue-property-decorator';
import Router from 'vue-router';
import 'bootstrap/dist/css/bootstrap.css'
import LoginService from '@/services/login.service';
import UsuarioService from "@/services/usuario.service";
import GLOBAL from '@/Global';
@Component({
   name: 'ingresoSistema'
})
export default class IngresoSistemaComponent extends Vue {
  FormLogin:any = {strUsuario:'', strPassword:''};
  loadingLogin :boolean =false;
  constructor(){
    super()
  }

  btnIngresarSistema(FormLogin){
    if(FormLogin.strUsuario.trim() != "" && FormLogin.strPassword.trim() != ""){
      this.loadingLogin = true;
      LoginService.LoginUser(FormLogin)
      .then(response =>{
        if(response != null){
          LoginService.Authentification(response)
          .then(res=>{
            UsuarioService.GetUsuarioAccesos(response.strCargo, res)
            .then(success =>{
              var user_session = {
                strCodUsuario: response.strCodUsuario,
                strCodPersona: response.strCodPersona,
                strCargo: response.strCargo,
                strUsuario: response.strUsuario,
                strNombre: response.strNombres +' '+ response.strApellidoPat +' '+response.strApellidoMat,
                strSuperAdmin: response.strSuperAdmin
              }
              var encoded_userS = GLOBAL.encodeString(JSON.stringify(user_session));
              window.sessionStorage.setItem("session_user", encoded_userS);

              var encoded_acc = GLOBAL.encodeString(JSON.stringify(success.Data));
              window.sessionStorage.setItem('session_access', encoded_acc);
              this.loadingLogin = false;
              this.$router.push('/menu/inicio');
            }).catch(ex =>{
              console.log(ex);
              this.loadingLogin = false;
              this.openMessageError('Error al obtener accesos.');
            })
          }).catch(e =>{
            this.loadingLogin = false;
            this.openMessageError('Error al autentificar el usuario');
            console.log(e);
          })
        }
        else{
          this.loadingLogin = false;
          this.openMessageError('Usuario y/o password incorrectos.');
        }

      }).catch(e =>{
        console.log(e);
        this.loadingLogin = false;
        this.openMessageError('Error al Iniciar Sesión.');
      })
    }
    else{
      this.openMessageError('Ingrese Usuario y/o Contraseña.');
    }

  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  data() {
    return {

    };
  }
}
