import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import router from '../../router';
import Roles from '@components/roles/roles.vue';
import RolesService from '../../services/roles.service';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'roles',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})

export default class RolesComponent extends Vue {
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  FormAgregar : any = {};
  listaAccesos:any = [];
  listaAccesosEdit: any =[];
  RolAccesoSelected:any;
  rowSelectedEdit :any;
  defaultSelectEdit:any = [];
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  //PAGINATION
  pagina:number =1;
  RegistersForPage:number = 20;
  totalRegistros:number = this.RegistersForPage;
  valueLista :any;
  Accesos:{
    intCodAcceso:'',
    strDescripcion:''
  }
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  userDec: any = {};
  // defaultSelectAdd: any = [1];
  loadingGetData: boolean = false;
  loadingGetAccesos: boolean = false;
  loadingGetAccesosEdit: boolean = false;
  multipleSelection:any = [];
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  constructor(){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesosVista:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesosVista.length; i++){
        if(listaAccesosVista[i].intCodAcceso === 10){
          flag=true;
          this.NivelAcceso = listaAccesosVista[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesosVista.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.loadingData();
      }
    }
  }
  loadingData(){
    this.loadingGetData = true;
    RolesService.loadingData()
    .then(response => {
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    })
    .catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar roles');
    })
  }
  getAccesos(){
    this.loadingGetAccesos = true;
    RolesService.getAccesos()
    .then(response => {
      this.listaAccesos = response.Data;
      this.loadingGetAccesos = false;
    })
    .catch(e =>{
      console.log(e);
      this.loadingGetAccesos = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Accesos');
    })
  }

  ObtenerAccesosRol(intIdRol){
    this.loadingGetAccesosEdit = true;
    RolesService.getRolAcceso(intIdRol)
    .then(response =>{
      this.listaAccesosEdit = response.Data;
      this.loadingGetAccesosEdit = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetAccesosEdit = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Accesos del Rol');
    })
    this.dialogEditarVisible = true
  }

  AgregarRol(){
    this.FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    this.FormAgregar.lstAccesos = [];
    for (let i = 0; i < this.multipleSelection.length; i++) {
      this.FormAgregar.lstAccesos.push({
        intCodAcceso: this.multipleSelection[i].intCodAcceso,
        strTipoAcceso: (this.multipleSelection[i].strTipoAcceso===null)?'lectura':this.multipleSelection[i].strTipoAcceso
      });
    }
    RolesService.AgregarRol(this.FormAgregar)
    .then(response => {
      if(response === -1) this.openMessageError("Error al agregar rol:"+response)
      else if(response === -2) this.openMessageError("Ya existe un rol con el mismo nombre.")
      else
        this.openMessage('Rol guardado correctamente.');
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar rol');
    })
    this.dialogAgregarVisible = false;
  }
  EditarRol(){
    this.rowSelectedEdit.lstAccesos = [];
    for (let i = 0; i < this.listaAccesosEdit.length; i++) {
      var flag = this.listaAccesosEdit[i].chkEstado;
      if(flag === 1 && flag != undefined && flag != 0){
        this.rowSelectedEdit.lstAccesos.push({
          intCodAcceso: this.listaAccesosEdit[i].intCodAcceso,
          strTipoAcceso: (this.listaAccesosEdit[i].strTipoAcceso===null)?'lectura':this.listaAccesosEdit[i].strTipoAcceso
        });
      }
    }
    RolesService.EditarRol(this.rowSelectedEdit)
    .then(response => {
      this.openMessage('Rol actualizado correctamente: '+response);
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al editar rol');
    })
    this.dialogEditarVisible = false;
  }
  EliminarRol(row){
    RolesService.EliminarRol(row.intCodRol, this.userDec.strUsuario)
    .then(response => {
      this.openMessage('Rol eliminado correctamente: '+response);
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al eliminar rol');
    })
  }
  LimpiarFormAgregar(){
    this.FormAgregar.strDescripcion = '';
    this.FormAgregar.lstAccesos = [];
  }

  handleAgregar(){
    this.LimpiarFormAgregar();
    this.getAccesos();
    this.dialogAgregarVisible = true
  }
  handleEdit(index, row){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.ObtenerAccesosRol(row.intCodRol)
  }
  handleDelete(index, row){
    this.$confirm('Desea Eliminar el Rol: '+row.strDescripcion +' ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.EliminarRol(row);
    }).catch(() => {
    });
  }
  handleSelectionChange(val) {
    this.multipleSelection = val;
  }
  changeSelectRol(row){
    // console.log('select nivel',row);
  }
  changeSelectRolEdit(row){

  }
  getDateString(fecha:string){
    var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
    return dateString;
  }
  getCheckEstado(row){
    if(row.chkEstado === 1){
      return true;
    }
    else{
      return false;
    }
  }
  handleCheckBox(index, row){
    this.listaAccesosEdit[index].chkEstado = (this.listaAccesosEdit[index].chkEstado === 1) ? 0 : 1;
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      gridData: [],
      listaAccesos:[],
      valueLista:[],
      tituloAccesos : ['Lista Accesos', 'Accesos del Usuario'],
      rowSelectedEdit:[],
      defaultSelectEdit:[],

      FormAgregar: {
        strDescripcion:'',
        lstAccesos:[],
        strUsuarioCrea : ''
      },
      dataNivelAcceso:[
        {id: '1', name: 'lectura'},
        {id: '2', name: 'escritura'}
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }
}
