import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import { Loading } from 'element-ui';
import LotesService from '@/services/lotes.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'trujillo',
   components:{
    'download-excel' : JsonExcel,
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class TrujilloComponent extends Vue {
  FormSearch: any = {};
  fechaSearch: Date = new Date();
  userDec: any = {};
  CompleteData:any = [];
  gridData: any = [];
  countGridData = 0;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  options2 = {  day: '2-digit',month: '2-digit', year: 'numeric', hour:"2-digit", minute:"2-digit" };
  //PAGINATION
  pagina: number =1;
  RegistersForPage: number = 20;
  totalRegistros: number = this.RegistersForPage;
  NivelAcceso: string = '';
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';;
  dialogDetalleVisible: boolean = false;
  loadingGetData: boolean = false;
  rowSelectedEdit: any = {};
  checkedH2O: boolean = false;
  sumatorioPesoTMH:any = 0;
  sumatorioPSecoTM:any = 0;
  titleExcel:string ='Trujillo - '+CONFIG.DATE_STRING+'.xls';
  constructor (){
    super()
  }

  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1021){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.BuscarLotes();
      }
    }
  }

  BuscarLotes(){
    // let loadingInstance = Loading.service({
    //   fullscreen: true,
    //   spinner: 'el-icon-loading',
    //   text:'Cargando lotes...'
    // });
    this.loadingGetData=true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    // this.mesActual = this.monthNames[fSearch.getMonth()];
    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;

    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.sumarPesoTMH(this.CompleteData);
      this.changeDateString(this.CompleteData);
      // loadingInstance.close()
      this.loadingGetData=false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData=false;
      // loadingInstance.close()
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  handleDetalle(index, row){
    this.rowSelectedEdit = row;
    this.checkedH2O = (row.strCheckH2O === null|| row.strCheckH2O === "" || row.strCheckH2O === "false")? false : true;
    this.dialogDetalleVisible = true;
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    data.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        PesoTotal += element.fltPesoTmh;
      if(element.strPSecoTM != null || element.strPSecoTM != '')
        PesoSecoTotal += Number(element.strPSecoTM);
    });
    this.sumatorioPesoTMH = this.getNumberFloat(PesoTotal);
    this.sumatorioPSecoTM = this.getNumberFloat(PesoSecoTotal);
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getDateTime(fecha:string){
    if(fecha === '' || fecha === null || fecha == undefined) return "";
    else{
      var dateString = new Date(fecha).toLocaleString('es-PE', this.options2)
      return dateString;
    }
  }
  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }
  getNumberFloat(number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : 'TRUJILLO',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      dataEstado:[
        {id_estado: 'F', name: 'FACTURADOS'},
        {id_estado: 'E', name: 'ENTREGADOS'},
        {id_estado: 'R', name: 'REINTEGROS'},
        {id_estado: 'P', name: 'PENDIENTES'},
        {id_estado: 'RT', name: 'RETIRADOS'},
      ],
      json_fields : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strEstadoLote" : "Estado",
        "strProcedenciaSt" : "Procedencia",
        "strClienteRuc" : "Ruc Proveedor",
        "strClienteRazonS" : "Proveedor Mineral",
        "strMaterial" : "Material",
        "intNroSacos" : "Nro. Sacos",
        "fltPesoTmh" : "Peso TMH",
        "strPorcH2O" : "%H2O",
        "strCheckH2O" : "Check H2O",
        "strPSecoTM" : "P.Seco TM",
        "strPorcRCPRef12hrs" : "% RCP Ref 12hrs",
        "strPorcRCPAu30hrs" : "% RCP Au 48hrs",
        "strPorcRCPAu70hrs" : "% RCP Au 72hrs",
        "strPorcRCPReeAu72hrs" : "REE % RCP Au 72hrs",
        "strLeyAuOzTc" : "Ley Au Oz/tc",
        "strLeyAuREE" : "Ley Au REE",
        "strLeyAuComercial" : "Ley Au Comercial",
        "strLeyAuGrTm" : "Ley Au Gr/Tm",
        "strLeyAgOzTc" : "Ley Ag (Oz/tc)",
        "strLeyAgREE" : "Ley Ag REE",
        "strLeyAgComercial" : "Ley Ag Comercial",
        "strPorcCobre" : "Cobre %",
        "strNaOHKgTM" : "NaOH (kg/TM)",
        "strNaCNKgTM" : "NaCN (Kg/TM)",
        "strFechaRetiro" : "Fecha Retiro",
        "strNombreTransportista" : "Nombre Fletero",
        "strNroPlaca" : "Nro. Placa",
        "strGuiaTransport" : "Guia Transport.",
        "strGuiaRemitente" : "Guia Remitente",
        "strCodCompromiso" : "Código Compromiso",
        "strCodConcesion" : "Código Concesión",
        "strNombreConcesion" : "Nombre Concesión",
        "strProcedenciaLg" : "Procedencia",
        "strNombreAcopiador" : "Acopiador",
        "strObservacion" : "Observación"
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
