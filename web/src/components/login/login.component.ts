import { Vue, Component } from 'vue-property-decorator';
import Router from 'vue-router';
import '../../assets/css/login.scss';
import 'font-awesome/css/font-awesome.css';
import '../../assets/css/slider.scss';

@Component({
   name: 'login'
})
export default class LoginComponent extends Vue {
  constructor(){
    super()
  }

  navegacion(){
      this.$router.push('/barmenu/hello');
  }
  data() {
    return {
      labelPosition: 'right',
      formLabelAlign: {
        name: '',
        region: ''
      }
    };
  }
}
