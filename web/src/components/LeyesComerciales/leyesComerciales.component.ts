import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LeyComercialService from '@/services/leyComercial.service';
import ZonaService from '@/services/zona.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'leyesComerciales',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class LeyesComercialesComponent extends Vue {
  gridData: any = [];
  dataZonas: any = [];
  loadingGetData: boolean = false;
  sizeScreen:string = (window.innerHeight - 420).toString();
  viewAccessEscritura: boolean = false;
  countGridData = 0;
  NivelAcceso: string = '';
  userDec: any = {};
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  FormAgregar: any = {};
  rowSelectedEdit: any = {};
  seleccionarRows:any=null;
  constructor (){
    super()
  }
  checkAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1020){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.loadingData();
      }
    }
  }
  loadingData(){
    this.loadingGetData = true;
    LeyComercialService.loadingData()
    .then(response => {
      this.gridData = response.Data;
      this.countGridData = response.Count;
      this.loadingGetData = false;
    })
    .catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Leyes Comerciales');
    })
  }
  AgregarLeyComercial(FormAgregar){
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    LeyComercialService.AgregarLeyComercial(FormAgregar)
    .then(response =>{
      if(response === -1) this.openMessageError("Error al agregar ley Comercial:"+response)
      else if(response === -2) this.openMessageError("Ya existe una ley comercial con la misma zona.")
      else this.openMessage('Ley comercial guardada correctamente.');
      this.loadingData();
      this.dialogAgregarVisible = false;
    }).catch(e =>{
      console.log(e);
      this.dialogAgregarVisible = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Ley Comercial');
    })
  }
  ActualizarLeyComercial(rowEdit){
    rowEdit.strUsuarioModif = this.userDec.strUsuario;
    LeyComercialService.EditarLeyComercial(rowEdit)
    .then(response =>{
      if(response === -1) this.openMessageError("Error al editar ley Comercial:"+response);
      else this.openMessage('Ley comercial actualizada correctamente.');
      this.loadingData();
      this.dialogEditarVisible = false;
    }).catch(e =>{
      console.log(e);
      this.dialogEditarVisible = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al editar Ley Comercial');
    })
  }
  handleAgregar(){
    this.LimpiarFormAgregar();
    this.getZonas();
    this.dialogAgregarVisible = true;
  }
  seleccionarRow(row,index){
    this.seleccionarRows=row;
  }
  handleEdit2(){
    this.handleEdit(this.seleccionarRows);
  }
  handleEdit(row){
    this.getZonas();
    this.rowSelectedEdit = row;
    this.dialogEditarVisible = true;
  }
  handleDelete2(){
    this.handleDelete(this.seleccionarRows);
  }
  handleDelete(row){
    this.$confirm('Desea Eliminar ley Comercial de la Zona: '+row.strZona +' ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      LeyComercialService.EliminarLeyComercial(row.intIdValue, this.userDec.strUsuario)
      .then(response =>{
        this.openMessage('Ley Comercial eliminada correctamente: '+response);
        this.loadingData();
      }).catch(e =>{
        console.log(e);
        this.dialogEditarVisible = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al eliminar Ley Comercial');
      })
    }).catch(() => { });
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Zonas');
    })
  }
  LimpiarFormAgregar(){
    this.FormAgregar.intIdZona = '';
    this.FormAgregar.fltPuntoAu = '';
    this.FormAgregar.fltValorAuMenor = '';
    this.FormAgregar.fltValorAuMayor = '';
    this.FormAgregar.fltPuntoAuInter = '',
    this.FormAgregar.fltValueAuInter = '',
    this.FormAgregar.fltPuntoAuMax= '',
    this.FormAgregar.fltValorAuMax= '',
    this.FormAgregar.fltValorAg = '';
    this.FormAgregar.strUsuarioCrea = '';
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      FormAgregar:{
        intIdZona: '',
        fltValorAuMenor: '',
        fltPuntoAu: '',
        fltValorAuMayor: '',
        fltPuntoAuInter: '',
        fltValueAuInter: '',
        fltPuntoAuMax: '',
        fltValorAuMax: '',
        fltValorAg: '',
        strUsuarioCrea: '',
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccess()
    }
  }

}
