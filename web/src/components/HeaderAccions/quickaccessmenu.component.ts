import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import 'font-awesome/css/font-awesome.css';
import GLOBAL from '@/Global';
import router from '@/router';
import 'element-ui/lib/theme-default/index.css';
import { Notification } from 'element-ui';
@Component({
  name: 'quickaccessmenu'
})
export default class QuickAccessMenuComponent extends Vue {
  timer=0;
  hours:number;
  minutos:number;
  seconds:number;
  user:any;
  tiempoagotado:any;
  contador:any=0;
  _10min:boolean=false;
  ocultarConfig:boolean = true;
  nameuser:string;
  namecomplete:string;
  accesosUser:any=[];
  ocultar:boolean=false;
  dialogVisible:boolean=false;
  SendDocument:boolean=false;
  myModalRef:boolean=false;
  visible:boolean=false;
  constructor(){
    super();
  }
  //*test
  proveedorSeleccionado(val){
    debugger;
  }

  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }

  fnOcultar(){
    this.ocultar=!this.ocultar;
  }
  
  guardar(){
    this.dialogVisible=true;
    this.SendDocument=true;
    
  }
  validar(){   
  }
  
  openMessage(newMsg : string) {
    this.$message({
      showClose: true,
      message: newMsg,
      type: 'success'
    });
  }
  submit(){
  }
  openMessageError(strMessage:string){
    this.$message({
        showClose: true,
        type: 'error',
        message: strMessage
      });
  }
  linkLogout(){
   localStorage.clear();
   window.sessionStorage.clear();
    //GLOBAL.limpiarDatosSession();
    router.push('/')
  }
  confirmaraceptar(){
    this.SendDocument=false;    
    this.$emit('guardarTodo');

  }
  BuscarLotes(){
    this.$emit('BuscarLotes');
  }
  linksUser(comand){
    router.push('/barmenu/'+comand)
  }
  linksLogin(){
    router.push('/inicio')
  }
  linkRoute(){
    router.push('/barmenu/inicio')
  }
  linkHelp(){}
  redirectLogin(msg){
    Notification.warning(msg)
    localStorage.clear();
    router.push('/')
  }
  calcular(temp){
    if(temp < 600){
      return { rojo: true,}
    }
    else{
      return { verde: true, }
    }
  }
  ValidarItem(){
    this.$emit('ValidarItem');   
  }
  
  data(){
    return{
      dialogTableVisible: false,
      visible:true,
      user: {
        authenticated: false
      },
      data:{
        Usuario:localStorage.getItem('User_Nombre'),
      },
      accesosUser: [],
      hours: 0,
      minutos:0,
      seconds:0
    }
  }
  
}
