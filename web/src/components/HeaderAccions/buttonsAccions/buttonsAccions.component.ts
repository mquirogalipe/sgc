import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import 'font-awesome/css/font-awesome.css';
import router from '@/router';
import ElementUI from 'element-ui';
import InfiniteScroll from 'vue-infinite-scroll';
import 'element-ui/lib/theme-default/index.css';
import Global from '@/Global';
import { Notification } from 'element-ui';
@Component({
  name: 'buttons-accions',
  
})
export default class ButtonsAccionsComponent extends Vue { 
  filter:boolean=false;
  falseortrue:boolean=false;
  falseortruePO:boolean=false;
  constructor(){
    super();        
    setTimeout(() => {
      this.load();
    }, 400)
  } 
  load(){
  }
  BuscarSome(){
    this.$emit('handleClickInParent');
  }  
  LimpiarConsulta(){
    this.$emit('LimpiarConsulta');
  }
  changeIcon(){  
  }
  ActivaCheck(){
    this.$emit('ActivaCheck');
  }
  ExportarPDF(){
    this.$emit('ExportarPDF');
  }
  Inactivar(){
    this.$emit('EliminarItem');
  }
  ValidarVer(){
    this.$emit('ValidarVer');
  }
  Activar(){
    this.$emit('Activar');
  }
  siguiente(){
    this.$emit('siguiente');
  }
  anterior(){
    this.$emit('anterior');
  }
  AscItem(){
    this.$emit('AscItem');
  }
  DscItem(){
    this.$emit('DscItem');
  }
  Buscar(){
    this.filter=true;
    this.$emit('Buscar');
  }
  Buscar2(){
    this.filter=false;
    alert('ddd')
  }
  Print(){
    this.$emit('Print');
  }
  Limpiar(){
    this.filter=false;
    this.$emit('Limpiar');
  }
  data(){
    return{
      dialogTableVisible: false,
      user: {
        authenticated: false
      },
      // falseortrue:Global.falseortrue,
      imagenLoad:'',
      data:{
        // Usuario:localStorage.getItem('User_Nombre'),
      },
      accesosUser: [],
      hours: 0,
      minutos:0,
      seconds:0,
      falseortruePO:false,
      falseortrue:false
    }
  }
  
}
