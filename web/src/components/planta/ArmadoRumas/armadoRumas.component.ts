import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ArmadoRumasService from '@/services/armadoRumas.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'armadoRumas',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ArmadoRumasComponent extends Vue {
  fechaSearch: Date = new Date();
  gridData: any = [];
  CompleteData: any = [];
  countGridData = 0;
  loadingGetData: boolean = false;
  sizeScreen: string = (window.innerHeight - 350).toString();
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  FormSearch:any = {};
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  checkedSearch: boolean = true;
  sumatorioPesoTMH: any = 0;
  sumatorioPSecoTM: any = 0;
  multipleSelection:any = [];
  nroRumaAny: string = '';
  nroCampaniaAny: string = '';
  dateAny: Date = new Date();
  datePeriodoAny: Date = new Date();
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1022){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.BuscarRumas();
      }
    }
  }
  // BuscarLotes(){
  //   this.loadingGetData = true;
  //   var fSearch = this.fechaSearch;
  //   var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
  //   var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
  //   this.FormSearch.strFechaInicio = bFechaIni;
  //   this.FormSearch.strFechaFin = bFechaFin;
  //   ArmadoRumasService.SearchRumas(this.FormSearch)
  //   .then(response =>{
  //     this.CompleteData = response.Data;
  //     this.totalRegistros = response.Count;
  //     this.countGridData = response.Count;
  //     this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  //     this.loadingGetData = false;
  //   }).catch(e =>{
  //     console.log(e);
  //     this.loadingGetData = false;
  //     if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
  //     else  this.openMessageError('Error al cargar lotes.');
  //   })
  // }
  BuscarRumas(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else{
      this.FormSearch.strFechaInicio = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
      this.FormSearch.strFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    }
    ArmadoRumasService.BuscarRumas(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.sumarPesoTMH(this.CompleteData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    data.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        PesoTotal += element.fltPesoTmh;
      if(element.strPSecoTM != null || element.strPSecoTM != '')
        PesoSecoTotal += Number(element.strPSecoTM);
    });
    this.sumatorioPesoTMH = this.getNumberFix(PesoTotal, 3);
    this.sumatorioPSecoTM = this.getNumberFix(PesoSecoTotal, 3);
  }

  EnterGuardarRuma(row){
    if(row.strNroRuma == '' || row.strNroRuma == null) this.openMessageError('Ingrese N° de Ruma');
    else{
      var Data = {
        intIdRegistro: row.intIdRegistro,
        strLote: row.strLote,
        strNroRuma: (row.strNroRuma == null)? '': row.strNroRuma,
        dtmFecha: GLOBAL.getParseDate(new Date()),
        strMesContable: this.monthNames[(new Date()).getMonth()],
        strNroCampania: row.strNroCampania,
        strUsuarioCrea: this.userDec.strUsuario,
      };
      ArmadoRumasService.AgregarRuma(Data)
      .then(response =>{
        if(response === -1) this.openMessageError("SERVICE: Error al guardar Ruma:"+response);
        else if(response === -2) {
          row.dtmFecha = Data.dtmFecha;
          row.strMesContable = Data.strMesContable;
          this.openMessage("Se actualizó correctamente.");
        }
        else {
          row.dtmFecha = Data.dtmFecha;
          row.strMesContable = Data.strMesContable;
          this.openMessage('Se guardó correctamente.');
        }
      }).catch(e =>{
        console.log(e);
        // if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        this.openMessageError('Error al guardar ruma.');
      })
    }
  }
  GuardarRumasAny(){
    if(this.multipleSelection.length > 0){
      if(this.nroRumaAny==''||this.nroRumaAny==null||this.nroCampaniaAny==''||this.nroCampaniaAny==null)
        this.openMessageError('Ingrese N° de Ruma y N° de Campaña.');
      else{
        var aux = this.multipleSelection
        for (let i = 0; i < aux.length; i++) {
          var Data = {
            intIdRegistro: aux[i].intIdRegistro,
            strLote: aux[i].strLote,
            strNroRuma: (this.nroRumaAny == null)? '': this.nroRumaAny,
            dtmFecha: GLOBAL.getParseDate(this.dateAny),
            strMesContable: GLOBAL.getPeriodoFormat(this.datePeriodoAny),//this.monthNames[(this.dateAny).getMonth()],
            dtmFechaPerContable: GLOBAL.getParseDate(this.datePeriodoAny),
            strNroCampania: this.nroCampaniaAny,
            strUsuarioCrea: this.userDec.strUsuario,
          };
          ArmadoRumasService.AgregarRuma(Data)
          .then(response =>{
          }).catch(e =>{
            console.log(e);
            // if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
            this.openMessageError('Error al guardar ruma.');
          })
        }
        this.openMessage('Se guardó correctamente.');
        this.BuscarRumas();
        // this.nroRumaAny = '';
      }
    }
    else this.openMessageError('No se seleccionó ningún lote.');
  }

  handleSelectionChange(val) {
    this.multipleSelection = val;
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strFechaInicio : '',
        strFechaFin : '',
        strNroRuma : '',
        strEstadoRuma : 'stock'
      },
      dataEstado: [
        {id: 'stock', name: 'En Stock'},
        {id: 'ruma', name: 'En Ruma'},
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
