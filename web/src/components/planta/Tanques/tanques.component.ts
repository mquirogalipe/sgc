import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import TanquesService from '@/services/tanques.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'tanques',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class TanquesComponent extends Vue {
  fechaSearch: Date = new Date();
  gridData: any = [];
  CompleteData: any = [];
  countGridData = 0;
  loadingGetData: boolean = false;
  sizeScreen: string = (window.innerHeight - 350).toString();
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  FormAgregar: any = {};
  rowSelectedEdit: any = {};
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1025){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.BuscarTanques();
      }
    }
  }
  BuscarTanques(){
    TanquesService.getTanques()
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }

  //AGREGAR TANQUE
  handleAgregar(){
    this.LimpiarAgregar();
    this.dialogAgregarVisible = true;
  }
  AgregarTanque(FormAgregar){
    FormAgregar.intIdTanque = -1;
    FormAgregar.dtmFecha = GLOBAL.getParseDate(new Date());
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    console.log(FormAgregar);
    TanquesService.PutTanque(FormAgregar)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar tanque:");
      // else if(response === -2) this.openMessage("Se actualizó correctamente.");
      else this.openMessage('Se guardó correctamente.')
      this.BuscarTanques();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar tanque.');
    })
    this.dialogAgregarVisible = false;
  }
  LimpiarAgregar(){
    this.FormAgregar.intIdTanque = '',
    this.FormAgregar.strNroTanque = '',
    this.FormAgregar.strCarbonSeco = '',
    this.FormAgregar.strGrAuKgC = '',
    this.FormAgregar.strGrAgKgC = '',
    this.FormAgregar.strDensPulpa = '',
    this.FormAgregar.strVolPulpa = '',
    this.FormAgregar.strGAum3 = '',
    this.FormAgregar.strGAut = '',
    this.FormAgregar.strGAgm3 = '',
    this.FormAgregar.strGAgt = '',
    this.FormAgregar.strUsuarioCrea = ''
  }

  //EDITAR TANQUE
  handleEdit(index, row){
    this.rowSelectedEdit = row;
    this.dialogEditarVisible = true;
  }
  EditarTanque(rowEdit){
    rowEdit.dtmFecha = GLOBAL.getParseDate(rowEdit.dtmFecha)
    rowEdit.strUsuarioCrea = this.userDec.strUsuario;
    TanquesService.PutTanque(rowEdit)
    .then(response =>{
      if(response === -1) this.openMessageError('"SERVICE: Error al guardar tanque:"')
      else this.openMessage("Se actualizó correctamente.");
      this.BuscarTanques();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar tanque.');
    })
    this.dialogEditarVisible = false;
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  data() {
    return {
      FormAgregar: {
        intIdTanque: '',
        dtmFecha: '',
        strNroTanque: '',
        strCarbonSeco: '',
        strGrAuKgC: '',
        strGrAgKgC: '',
        strDensPulpa: '',
        strVolPulpa: '',
        strGAum3: '',
        strGAut: '',
        strGAgm3: '',
        strGAgt: '',
        strUsuarioCrea: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
