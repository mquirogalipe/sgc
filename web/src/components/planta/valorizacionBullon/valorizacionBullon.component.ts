import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import BarraBullonService from '@/services/barraBullon.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reportePrueba',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
  'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ValorizacionBullonComponent extends Vue {
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();
  loadingGetData: boolean = false;

  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  FormAgregar: any = {};
  FormSearch: any = {};
  rowSelectedEdit: any = {};
  FormTotales: any = { totalAuExp: 0, totalAgExp: 0, subTotal: 0, totalEnviar: 0, diferencia: 0 }
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else {
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1040){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
      }
    }
  }

  BuscarValorizacion(){
    if (this.FormSearch.strNroCampania.trim() === ""){
      this.openMessageError('Ingrese número de campaña');
    }
    else{
      this.loadingGetData = true;
      BarraBullonService.getBarrasBullon(this.FormSearch)
      .then( response => {
        this.gridData = response.Data;
        this.CalcularTotales(this.gridData);
        this.loadingGetData = false;
      }).catch( e => {
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar barras.');
      })
    }
  }
  AgregarBarraBullon(FormAgregar){
    var _campania = FormAgregar.strNroCampania;
    var split= _campania.split('-');
    FormAgregar.strNroCampania = (split.length === 1) ? 'C-'+FormAgregar.strNroCampania : FormAgregar.strNroCampania;

    FormAgregar.strFecha = GLOBAL.getParseDate(new Date());
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    BarraBullonService.AgregarBarraBullon(FormAgregar)
    .then( response => {
      if(response === -1) this.openMessageError("SERVICE: Error al agregar barra:"+response);
      else this.openMessage('Se guardó correctamente.');
    }).catch( e => {
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar barra.');
    })
    this.dialogAgregarVisible = false;
  }
  EditarBarraBullon(rowEdit){
    var _campania = rowEdit.strNroCampania;
    var split= _campania.split('-');
    rowEdit.strNroCampania = (split.length === 1) ? 'C-'+rowEdit.strNroCampania : rowEdit.strNroCampania;

    rowEdit.strFecha = GLOBAL.getParseDate(new Date());
    rowEdit.strUsuarioCreaModif = this.userDec.strUsuario;
    BarraBullonService.EditarBarraBullon(rowEdit)
    .then( response => {
      if(response === -1) this.openMessageError("SERVICE: Error al editar barra:"+response);
      else this.openMessage('Se guardó correctamente.');
      this.BuscarValorizacion();
    }).catch( e => {
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar barra.');
    })
    this.dialogEditarVisible = false;
  }

  CalcularTotales( Data ){
    let _tAUexp=0, _tAGexp=0, _subT = 0, _tEnviar = 0;
    for (let i = 0; i < Data.length; i++) {
        _tAUexp += Data[i].fltFinoAuMerma;
        _tAGexp += Data[i].fltFinoAgMerma;
        _subT += Data[i].fltSubTotal;
        _tEnviar += Data[i].fltPagar;
    }
    this.FormTotales.totalAuExp = _tAUexp;
    this.FormTotales.totalAgExp = _tAGexp
    this.FormTotales.subTotal = _subT;
    this.FormTotales.totalEnviar = _tEnviar;
    this.FormTotales.diferencia = this.FormTotales.subTotal - this.FormTotales.totalEnviar;
  }

  handleAgregar(){
    this.FormAgregar.fltPeso = '',
    this.FormAgregar.fltLeyAu = '',
    this.FormAgregar.fltLeyAg = '',
    this.changeFinos();
    this.dialogAgregarVisible = true;
  }
  handleEditar(index, row){
    this.rowSelectedEdit = row;
    this.dialogEditarVisible = true;
  }
  changeFinos(){
    this.FormAgregar.fltFinoAu = this.FormAgregar.fltPeso * this.FormAgregar.fltLeyAu;
    this.FormAgregar.fltFinoAg = this.FormAgregar.fltPeso * this.FormAgregar.fltLeyAg;
  }
  changeFinosEdit(){
    this.rowSelectedEdit.fltFinoAu = this.rowSelectedEdit.fltPeso * this.rowSelectedEdit.fltLeyAu;
    this.rowSelectedEdit.fltFinoAg = this.rowSelectedEdit.fltPeso * this.rowSelectedEdit.fltLeyAg;
  }

  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, '', '', 0, 0, ''];
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.fltPeso != null || element.fltPeso != '')
        Total[2] += Number(element.fltPeso);
      if(element.fltFinoAu != null || element.fltFinoAu != '')
        Total[5] += Number(element.fltFinoAu);
      if(element.fltFinoAg != null || element.fltFinoAg != '')
        Total[6] += Number(element.fltFinoAg);
    });
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 1 || index === 3 || index === 4 || index === 7){
        sums[index] = ''
        return;
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 3,
          maximumFractionDigits: 3
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }

  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getPorcentageFix(numero, fix:number){
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: fix, maximumFractionDigits: fix });
    var num =  formatter.format(numero);
    return num;
  }
  getNumberFormatFix(numero, fix:number){
    var formatter = new Intl.NumberFormat('es-PE', { minimumFractionDigits: fix, maximumFractionDigits: fix});
    var num =  formatter.format(numero);
    return num;
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch: {
        strNroCampania: ''
      },
      FormAgregar: {
        strNroCampania: '',
        strFecha: '',
        fltPeso: '',
        fltLeyAu: '',
        fltLeyAg: '',
        fltFinoAu: '',
        fltFinoAg: '',
        strUsuarioCrea: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
