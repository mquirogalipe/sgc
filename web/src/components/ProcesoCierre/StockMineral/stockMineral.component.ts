import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ArmadoRumasService from '@/services/armadoRumas.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'stockMineral',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class StockMineralComponent extends Vue {
  fechaSearch: Date = new Date();
  gridData: any = [];
  CompleteData: any = [];
  countGridData = 0;
  loadingGetData: boolean = false;
  sizeScreen: string = (window.innerHeight - 350).toString();
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };

  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  sumatorioPesoTMH: any = 0;
  sumatorioPSecoTM: any = 0;
  titleExcel:string ='StockMineral_'+CONFIG.DATE_STRING+'.xls';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1038){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.BuscarStockMineral();
      }
    }
  }

  BuscarStockMineral(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    var fIni= (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var fFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    var FormSearch = {
      strLote: '',
      strNroRuma: '',
      strNroCampania: '',
      strMesContable: '',
      strFechaInicio: fIni,
      strFechaFin: fFin,
    }
    ArmadoRumasService.GetStockMineral(FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.sumarPesoTMH(this.CompleteData);
      this.changeDateString(this.CompleteData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    data.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        PesoTotal += element.fltPesoTmh;
      if(element.strPSecoTM != null || element.strPSecoTM != '')
        PesoSecoTotal += Number(element.strPSecoTM);
    });
    this.sumatorioPesoTMH = this.getNumberFix(PesoTotal, 3);
    this.sumatorioPSecoTM = this.getNumberFix(PesoSecoTotal, 3);
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
        this.CompleteData[i].dtmFecha = (Data[i].dtmFecha==null ||Data[i].dtmFecha=='')?'':new Date(Data[i].dtmFecha).toLocaleDateString('es-PE', this.options);
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  data() {
    return {
      json_fields : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion": "Fecha Recepción",
        "strProcedenciaSt" : "Procedencia",
        "strClienteRazonS" : "Proveedor Mineral",
        "strMaterial" : "Tipo Mineral",
        "fltPesoTmh" : "P.Humedo TM",
        "strPorcH2O" : "% H2O",
        "strPSecoTM" : "P.Seco TM",
        "fltFino" : "Fino(Au/Gr)",
        "strNroRuma" : "N° Ruma",
        "strNroCampania" : "N° Campaña",
        "dtmFecha" : "Fecha",
        "strMesContable" : "Periodo Contable"
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {

    }
  }

}
