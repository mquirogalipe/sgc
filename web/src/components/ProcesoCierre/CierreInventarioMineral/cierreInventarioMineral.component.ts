import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import CierreInventarioService from '@/services/CierreInventario.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'cierreInventarioMineral',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class CierreInventarioMineralComponent extends Vue {
  RumasSelected: any = [];
  fechaAddCierreInv: Date = new Date();
  dialogAgregarVisible: boolean = false;
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();
  loadingGetData: boolean = false;
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  userDec: any = {};
  DataRumas: any = [];
  FormAgregar: any = {};
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null) {
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else {
      var listaAccesos: any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1027){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.BuscarCierresInventario();
      }
    }
  }
  BuscarCierresInventario(){
    this.loadingGetData = true;
    CierreInventarioService.loadingData()
    .then(response =>{
      this.gridData = response.Data;
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Datos.');
    })
  }
  GetPesoInventario(){
    var dateCierre: Date = new Date(this.fechaAddCierreInv.getFullYear(), this.fechaAddCierreInv.getMonth() + 1, 0);;
    CierreInventarioService.getPesoInventario(GLOBAL.getParseDate(dateCierre))
    .then(response =>{
      if(response.Count > 0){
        this.FormAgregar.fltPesoTMHAcum = this.getNumberFix(response.Data[0].fltPesoTmh, 3);
        this.FormAgregar.fltPesoTMSAcum = this.getNumberFix(response.Data[0].fltPesoSecoTM, 3);
        this.FormAgregar.trLotesDesc = (response.Data[0].strLotesDesc==null) ? '' : response.Data[0].strLotesDesc;
      }
      else{
        this.FormAgregar.fltPesoTMHAcum = 0;
        this.FormAgregar.fltPesoTMSAcum = 0;
        this.FormAgregar.trLotesDesc = '';
      }
      this.CalcularTotal()
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener pesos del Inventario.');
    })
  }
  GetRumasInventario(){
    CierreInventarioService.getRumasInventario()
    .then(response =>{
      this.DataRumas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtner rumas.');
    })
  }
  AgregarCierreInventario(FormAgregar){
    var month = this.monthNames[this.fechaAddCierreInv.getMonth()];
    var year = this.fechaAddCierreInv.getFullYear();
    var Data = {
      fltPesoTmh: Number(FormAgregar.fltTotalTMH),
      fltPesoSecoTM: Number(FormAgregar.fltTotalTMS),
      strRumasDesc: this.RumasSelected.join(';'),
      strLotesDesc: this.FormAgregar.strLotesDesc.trim(),
      dtmFecha: GLOBAL.getParseDate(this.fechaAddCierreInv),
      strMes: month.toLowerCase(),
      strAnio: year.toString(),
      strUsuarioCrea: this.userDec.strUsuario
    }
    CierreInventarioService.AgregarCierreInventario(Data)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al procesar Inventario:"+response)
      else if(response === -2) this.openMessageError("Ya existe un registro con el mismo mes y año.")
      else this.openMessage('Se guardó correctamente.');
      this.BuscarCierresInventario();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al procesar Inventario.');
    })
    this.dialogAgregarVisible = false;
  }
  handleAgregar(){
    this.GetPesoInventario();
    this.GetRumasInventario();
    this.dialogAgregarVisible = true;
  }
  CalcularTotal(){
    this.FormAgregar.fltTotalTMH = this.getNumberFix(Number(this.FormAgregar.fltPesoRumaTMH)+Number(this.FormAgregar.fltPesoTMHAcum),3);
    this.FormAgregar.fltTotalTMS = this.getNumberFix(Number(this.FormAgregar.fltPesoRumaTMS)+Number(this.FormAgregar.fltPesoTMSAcum),3);
  }
  changeRumasInv(){
    var totalTmh = 0, totalTms = 0;
    for (let i = 0; i < this.RumasSelected.length; i++) {
      var objRuma = this.DataRumas.find( item => item.strNroRuma === this.RumasSelected[i])
      if(objRuma != undefined || objRuma != null || objRuma != {}){
        totalTmh += Number(objRuma.fltSumPesoTmh);
        totalTms += Number(objRuma.fltSumPSecoTM);
      }
    }
    this.FormAgregar.fltPesoRumaTMH = this.getNumberFix(totalTmh, 3);
    this.FormAgregar.fltPesoRumaTMS = this.getNumberFix(totalTms, 3);
    this.CalcularTotal();
  }
  changeFechaCierre(){
    this.GetPesoInventario()
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar:{
        fltPesoRumaTMH: '0',
        fltPesoRumaTMS: '0',
        fltPesoTMHAcum: '0',
        fltPesoTMSAcum: '0',
        fltTotalTMH: '0',
        fltTotalTMS: '0',
        strLotesDesc: ''
      },
      DataRumas:[
        {id:'1', name: 'F-254'},
        {id:'2', name: 'F-255'},
        {id:'3', name: 'F-256'},
        {id:'4', name: 'F-257'}
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
