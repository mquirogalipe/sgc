import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LotesService from '@/services/lotes.service';
import LeyesLab from '@/services/leyesLaboratorio.service';
import ConsumoService from '@/services/consumo.service';
import RecuperacionService from '@/services/recuperacion.service';
import { Loading } from 'element-ui';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
import * as CONFIG from '@/Config';
@Component({
   name: 'laboratorio',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class LaboratorioComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  gridDataAddLey: any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;

  FormSearch: any = {}
  fechaSearch: Date = new Date();
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  options2 = {  day: '2-digit',month: '2-digit', year: 'numeric', hour:"2-digit", minute:"2-digit" };

  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  NivelAcceso: string = '';

  //DATA EDITAR
  rowSelectedEdit: any = {};
  editViewButton: boolean = true;
  dialogDetalleVisible: boolean = false;
  dialogEditarLabVisible: boolean = false;
  dialogEditarMetVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  dialogLeyesVisible: boolean = false;
  dialogConsumosVisible: boolean = false;
  checkedH2O: boolean = false;
  FechaHoraMuestreo: any = '';
  nameButton: string = '';
  titleExcel:string ='Laboratorio_'+CONFIG.DATE_STRING+'.xls';
  viewAccessEscritura: boolean = false;
  viewLeyesLab: boolean = false;
  viewConsumos: boolean = false;
  viewRecuperacion: boolean = false;
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  loadingGetData:boolean = false;
  //Leyes
  FechaLeyes: Date = new Date();
  FormAgregarLey: any = {};
  loadingGetLeyes: boolean = false;
  reporteLeyes:any = { leyAuOz: '', leyAgOz: '', leyAuRee: '', leyAgRee: '', leyAuRmi: ''};
  activeNameLeyes: any='first';
  //Consumos
  FormAgregarConsumo: any = {};
  //Recuperacion
  FormAgregarRcp: any = {};
  sizeOperations: string = '180';
  PesosM: any = {PesoMuestra: '', PesoAgua: ''}
  viewErrorPesos: boolean = false;
  msgRecuperacionRipio: string = '';
  viewMsgLeyRipio: boolean = false;
  strH2OComercial: string = '0.0';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 3){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.nameButton = ((this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador') && this.userDec.strCargo!='Area Metalurgica')? 'Actualizar' : 'Detalle';
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.viewLeyesLab = ((this.NivelAcceso=='escritura'||this.userDec.strCargo=='Administrador'))
        this.viewConsumos = ((this.NivelAcceso=='escritura'||this.userDec.strCargo=='Administrador'))
        this.sizeOperations = (this.NivelAcceso=='escritura'||this.userDec.strCargo=='Administrador') ? '250' : '100'
        this.BuscarLotes();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;
    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  EditarLote(rowEdit){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Actualizando lote...'
    });

    var FormUpdate = {
      intIdRegistro: rowEdit.intIdRegistro,
      strLote: rowEdit.strLote,
      strPorcH2O: (rowEdit.strPorcH2O === null)? "" : rowEdit.strPorcH2O,
      strCheckH2O: this.checkedH2O.toString(),
      strPSecoTM: "",
      dtmFechaHoraMuestreo: (this.FechaHoraMuestreo===undefined||this.FechaHoraMuestreo.toString()==='')? '': GLOBAL.getDateTimeString(this.FechaHoraMuestreo),
      strPorcRCPRef12hrs: (rowEdit.strPorcRCPRef12hrs === null)? "" : rowEdit.strPorcRCPRef12hrs,
      strPorcRCPAu30hrs: (rowEdit.strPorcRCPAu30hrs === null)? "" : rowEdit.strPorcRCPAu30hrs,
      strPorcRCPAu70hrs: (rowEdit.strPorcRCPAu70hrs === null)? "" : rowEdit.strPorcRCPAu70hrs,
      strPorcRCPReeAu72hrs: (rowEdit.strPorcRCPReeAu72hrs === null)? "" : rowEdit.strPorcRCPReeAu72hrs,
      strLeyAuOzTc: (rowEdit.strLeyAuOzTc === null)? "" : rowEdit.strLeyAuOzTc,
      strLeyAuREE: (rowEdit.strLeyAuREE === null)? "" : rowEdit.strLeyAuREE,
      strLeyAuGrTm: (rowEdit.strLeyAuGrTm === null)? "" : rowEdit.strLeyAuGrTm.toString(),
      strFinoAuGr: (rowEdit.strFinoAuGr === null)? "" : rowEdit.strFinoAuGr.toString(),
      strPorcRCPAg30hrs: (rowEdit.strPorcRCPAg30hrs === null)? "" : rowEdit.strPorcRCPAg30hrs,
      strPorcRCPAg70hrs: (rowEdit.strPorcRCPAg70hrs === null)? "" : rowEdit.strPorcRCPAg70hrs,
      strLeyAgOzTc: (rowEdit.strLeyAgOzTc === null)? "" : rowEdit.strLeyAgOzTc,
      strLeyAgREE: (rowEdit.strLeyAgREE === null)? "" : rowEdit.strLeyAgREE,
      strPorcCobre: (rowEdit.strPorcCobre === null)? "" : rowEdit.strPorcCobre,
      strNaOHKgTM: (rowEdit.strNaOHKgTM === null)? "" : rowEdit.strNaOHKgTM,
      strNaCNKgTM: (rowEdit.strNaCNKgTM === null)? "" : rowEdit.strNaCNKgTM,
      strHorasAgitacion:(rowEdit.strHorasAgitacion === null)? "" : rowEdit.strHorasAgitacion,
      strPenalidad: (rowEdit.strPenalidad === null)? "" : rowEdit.strPenalidad,
      strLeyAuComercial: (rowEdit.strLeyAuComercial === null)? "" : rowEdit.strLeyAuComercial,
      strLeyAgComercial: (rowEdit.strLeyAgComercial === null)? "" : rowEdit.strLeyAgComercial,
      strUsuarioModif: this.userDec.strUsuario,
    }
    LotesService.EditarLoteLaboratorio(FormUpdate)
    .then(response =>{
      loadingInstance.close();
      if(response == -1) this.openMessageError('SERVICE: Error al actualizar lote.');
      else this.openMessage('Se actualizó el lote correctamente');
      this.BuscarLotes();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar lote.');
    })
    this.dialogEditarVisible = false;
    this.dialogEditarLabVisible = false;
    this.dialogEditarMetVisible = false;
    this.dialogDetalleVisible = false;
  }

  handleEdit(index, row){
    this.rowSelectedEdit = {};
    this.rowSelectedEdit = row;
    this.checkedH2O = (row.strCheckH2O === null || row.strCheckH2O === "" || row.strCheckH2O === "false")? false : true;
    this.FechaHoraMuestreo = row.dtmFechaHoraMuestreo;
    if(this.rowSelectedEdit.strPorcH2O=='' || this.rowSelectedEdit.strPorcH2O==null) this.strH2OComercial = ''
    else{
      var pH2o = Number(this.rowSelectedEdit.strPorcH2O);
      if(this.checkedH2O==false) this.strH2OComercial = this.getNumberFix(pH2o +1, 2);
      else this.strH2OComercial = this.getNumberFix(pH2o, 2);
    }

    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Area Laboratorio') {
        this.editViewButton = (row.chrEstadoModifLab === 'F' || row.chrEstadoModifLab === null) ? true : false;
        this.dialogEditarLabVisible = true;
      }
      else if(this.userDec.strCargo === 'Area Metalurgica') {
        // this.editViewButton = (row.chrEstadoModifMet === 'F' || row.chrEstadoModifMet === null) ? true : false;
        this.dialogDetalleVisible = true;
      }
      else this.dialogEditarVisible = true;
    }
    else this.dialogDetalleVisible = true;
  }
  handleLeyes2(){
    if(this.rowSelectedEdit.strLote!="" && this.rowSelectedEdit.strLote!=undefined)
    {
      this.handleLeyes(1,this.rowSelectedEdit);
    }
  }
  handleLeyes(index, row){
    this.activeNameLeyes = 'first';
    this.rowSelectedEdit = {};
    this.LimpiarFormAgregarLey();
    this.FormAgregarLey.strTipoMineral = row.strMaterial,
    this.FormAgregarLey.strTipoProceso = 'normal',
    this.FormAgregarLey.strBk = '0.202',

    this.rowSelectedEdit = row;
    this.reporteLeyes.leyAuOz = row.strLeyAuOzTc;
    this.reporteLeyes.leyAuRee = row.strLeyAuREE;
    this.reporteLeyes.leyAgOz = row.strLeyAgOzTc;
    this.reporteLeyes.leyAgRee = row.strLeyAgREE;
    this.reporteLeyes.leyAuRmi = row.strLeyAuRemuestreo;
    this.loadingGetLeyes = true;
    this.gridDataAddLey = [];
    LeyesLab.SearchLeyesLaboratorio(row.strLote)
    .then(response =>{
      if(response.Count === 0) this.gridDataAddLey = [];
      else {
        this.gridDataAddLey = response.Data;
        // this.FormAgregarLey.strTipoMineral = response.Data[0].strTipoMineral;
      }
      this.loadingGetLeyes = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetLeyes = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar leyes.');
    })
    this.dialogLeyesVisible = true;
  }
  AgregarLeyLab(FormAgregarLey){
    var newItem = {
      intIdLeyes: 0,
      strTipoProceso: FormAgregarLey.strTipoProceso,
      strItem: '',
      strBk: FormAgregarLey.strBk,
      dtmFecha: GLOBAL.getParseDate(this.FechaLeyes),
      strTipoMineral: FormAgregarLey.strTipoMineral,
      strLote: this.rowSelectedEdit.strLote,
      strPMFino: FormAgregarLey.strPMFino,
      strPMGrueso: FormAgregarLey.strPMGrueso,
      strPesoAuMasAg: FormAgregarLey.strPesoAuMasAg,
      strPesoAuFino1: FormAgregarLey.strPesoAuFino1,
      strPesoAuFino2: FormAgregarLey.strPesoAuFino2,
      strPesoAuGrueso: FormAgregarLey.strPesoAuGrueso,
      strLeyOzTcAuFino: '', //v
      strLeyOzTcAuGrueso: '', //v
      strLeyOzTcAuFinal: '',
      strLeyOzTcAgfinal1: '',
      strLeyFinalAu: '', //v
      strLeyFinalAg: '', //v
      strPorcAuFino: '', //v
      strPorcAuGrueso: '', //v
      chrCheckStatus: 'T',
      strUsuarioCrea: this.userDec.strUsuario
    }
    var factorLey: number = 34.28205;
    var leyOzAuFino = '', leyOzAugrueso = '';

    if(newItem.strPesoAuFino1 != '') { //CALCULO LEY(Oz/Tc) AU FINO
      leyOzAuFino = (((Number(newItem.strPesoAuFino1)+Number(newItem.strPesoAuFino2))/2)/Number(newItem.strPMFino)*1000/factorLey*(200-Number(newItem.strPMGrueso))/200).toString();
      newItem.strLeyOzTcAuFino = this.getNumberFix(leyOzAuFino, 3);
    }
    if(newItem.strPMGrueso != '') { //CALCULO LEY(Oz/Tc) AU GRUESO
      leyOzAugrueso = (Number(newItem.strPesoAuGrueso)/Number(newItem.strPMGrueso)*1000/factorLey*Number(newItem.strPMGrueso)/200).toString();
      newItem.strLeyOzTcAuGrueso = this.getNumberFix(leyOzAugrueso, 3);
    }
    if(newItem.strLeyOzTcAuGrueso == '') //CALCULO LEY FINAL AU
      newItem.strLeyFinalAu = this.getNumberFix(leyOzAuFino, 3);
    else
      newItem.strLeyFinalAu = this.getNumberFix(Number(leyOzAuFino) + Number(leyOzAugrueso), 3);
    if(newItem.strPesoAuMasAg != ''){ //CALCULO LEY FINAL AG
      newItem.strLeyFinalAg = this.getNumberFix((Number(newItem.strPesoAuMasAg)-Number(newItem.strBk))/Number(newItem.strPMFino)*1000/factorLey - Number(newItem.strLeyFinalAu), 3);
    }
    newItem.strPorcAuFino = this.getNumberFix(Number(leyOzAuFino)*100/Number(newItem.strLeyFinalAu), 2);
    newItem.strPorcAuGrueso = this.getNumberFix(100 - Number(newItem.strPorcAuFino), 2);
    this.gridDataAddLey.push(newItem);

    this.LimpiarFormAgregarLey();
    this.CalcularReporteFinal();
  }
  LimpiarFormAgregarLey(){
    this.FormAgregarLey.strPMFino = '';
    this.FormAgregarLey.strPMGrueso = '';
    this.FormAgregarLey.strPesoAuMasAg = '';
    this.FormAgregarLey.strPesoAuFino1 = '';
    this.FormAgregarLey.strPesoAuFino2 = '';
    this.FormAgregarLey.strPesoAuGrueso = '';
  }
  GuardarLeyes(){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando leyes...'
    });
    LeyesLab.AgregarLeyesLaboratorio(this.gridDataAddLey)
    .then(response =>{
      if(response === null) this.openMessageError("SERVICE: Error al guardar leyes.");
      else{
        var DataL = {
          strLote: this.rowSelectedEdit.strLote,//this.gridDataAddLey[0].strLote,
          strLeyAuOzTc: this.reporteLeyes.leyAuOz,
          strLeyAgOzTc: this.reporteLeyes.leyAgOz,
          strLeyAuREE: this.reporteLeyes.leyAuRee,
          strLeyAgREE: this.reporteLeyes.leyAgRee,
          strLeyAuRemuestreo: this.reporteLeyes.leyAuRmi,
          strUsuarioModif: this.userDec.strUsuario,
        }
        LeyesLab.AgregarLeyesLote(DataL)
        .then(success =>{
          if(response === null) this.openMessageError("SERVICE: Error al asignar leyes al Lote");
          else{
            this.openMessage(success);
          }
          this.BuscarLotes();
          loadingInstance.close();
        }).catch(err =>{
          console.log(err);
          loadingInstance.close();
          if(err.response.status === 401) this.redirectLogin(err.response.statusText+', Vuelva a Iniciar Sesion');
          else  this.openMessageError('Error al guardar leyes.');
        })
      }

    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar leyes.');
    })
    // this.dialogLeyesVisible = false;
  }
  CalcularReporteFinal(){
    var data:any = this.gridDataAddLey;
    var totalLeyOzAu:number=0, minLeyOzAg:number=0, totalLeyAuRee:number=0, minLeyAgRee:number=0, totalLeyAuRmi:number=0;
    var countLeyOzAu:number=0, countLeyAuRee:number=0, countLeyAuRmi:number=0;
    for(var i = 0; i< data.length;i++){
      if(data[i].strTipoProceso === 'normal' && data[i].chrCheckStatus=='T' && data[i].strLeyFinalAu !=''){
        //totalLeyOzAu += Number(data[i].strLeyFinalAu);
        totalLeyOzAu += (Number(data[i].strPMFino) + Number(data[i].strPMGrueso)) * Number(data[i].strLeyFinalAu);
        minLeyOzAg = (minLeyOzAg === 0)? Number(data[i].strLeyFinalAg) : Math.min(minLeyOzAg, Number(data[i].strLeyFinalAg))
        // countLeyOzAu++;
        countLeyOzAu += Number(data[i].strPMFino) + Number(data[i].strPMGrueso)
      }
      else if(data[i].strTipoProceso === 'REE' && data[i].chrCheckStatus=='T' && data[i].strLeyFinalAu !=''){
        totalLeyAuRee += (Number(data[i].strPMFino) + Number(data[i].strPMGrueso)) * Number(data[i].strLeyFinalAu);
        minLeyAgRee = (minLeyAgRee === 0)? Number(data[i].strLeyFinalAg) : Math.min(minLeyAgRee, Number(data[i].strLeyFinalAg))
        countLeyAuRee += Number(data[i].strPMFino) + Number(data[i].strPMGrueso);
      }
      else if(data[i].strTipoProceso === 'RMI' && data[i].chrCheckStatus=='T' && data[i].strLeyFinalAu !=''){
        totalLeyAuRmi += (Number(data[i].strPMFino) + Number(data[i].strPMGrueso)) * Number(data[i].strLeyFinalAu);
        countLeyAuRmi += Number(data[i].strPMFino) + Number(data[i].strPMGrueso)
      }
    }
    this.reporteLeyes.leyAuOz = (countLeyOzAu === 0)? '' : this.getNumberFix(totalLeyOzAu/countLeyOzAu, 3);
    this.reporteLeyes.leyAuRee = (countLeyAuRee === 0)? '' : this.getNumberFix(totalLeyAuRee/countLeyAuRee, 3);
    this.reporteLeyes.leyAuRmi = (countLeyAuRmi === 0)? '' : this.getNumberFix(totalLeyAuRmi/countLeyAuRmi, 3);
    this.reporteLeyes.leyAgOz = (minLeyOzAg === 0)? '' : this.getNumberFix(minLeyOzAg, 3);
    this.reporteLeyes.leyAgRee = (minLeyAgRee === 0)? '' : this.getNumberFix(minLeyAgRee, 3);
  }
  handleDeleteLey(index, row) {
    this.$confirm('Desea Eliminar este análisis ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.gridDataAddLey = this.gridDataAddLey.filter(item => item !== row);
      this.CalcularReporteFinal();
    }).catch(e => {
      // if(e.response.status === 404){ // token no valido
      //   this.redirectLogin('Tiempo de session a expirado, Vuelva a Iniciar Sesion');
      // }
    });
  }
  disabledDeleteLey(status){
    if(status == 'A') return true;
    else return false;
  }

  handleConsumos2(){
    debugger;
    if(this.rowSelectedEdit.strLote!="" && this.rowSelectedEdit.strLote!=undefined){
      this.handleConsumos(1,this.rowSelectedEdit)
    };
  }
  /* ||======================||
     ||       CONSUMOS       ||
     ||======================|| */
  handleConsumos(index, row){
    this.rowSelectedEdit = {};
    this.rowSelectedEdit = row;
    this.LimpiarFormAgregarConsumo();
    ConsumoService.SearchConsumos(row.strLote)
    .then(response =>{
      if(response != null){
        this.FormAgregarConsumo.strGeP1 = response.strGeP1;
        this.FormAgregarConsumo.strGeP2 = response.strGeP2;
        this.FormAgregarConsumo.strGeP3 = response.strGeP3;
        this.FormAgregarConsumo.strGeP4 = response.strGeP4;
        this.FormAgregarConsumo.strGeGEspecif2 = response.strGeEspecif2;
        this.FormAgregarConsumo.strPlDensidad = response.strPlDensidad;
        this.FormAgregarConsumo.strPlPorcMalla = response.strPlPorcMalla;
        this.FormAgregarConsumo.strPlPeso = response.strPlPeso;
        this.FormAgregarConsumo.str0HrsLixCN = response.str0HrsLixCN;
        this.FormAgregarConsumo.str2HrsLixCN = response.str2HrsLixCN;
        this.FormAgregarConsumo.str4HrsLixCN = response.str4HrsLixCN;
        this.FormAgregarConsumo.str8HrsLixCN = response.str8HrsLixCN;
        this.FormAgregarConsumo.str12HrsLixCN = response.str12HrsLixCN;
        this.FormAgregarConsumo.str24HrsLixCN = response.str24HrsLixCN;
        this.FormAgregarConsumo.str48HrsLixCN = response.str48HrsLixCN;
        this.FormAgregarConsumo.str72HrsLixCN = response.str72HrsLixCN;
        this.FormAgregarConsumo.str0HrsLixPH = response.str0HrsLixPH;
        this.FormAgregarConsumo.str2HrsLixPH = response.str2HrsLixPH;
        this.FormAgregarConsumo.str4HrsLixPH = response.str4HrsLixPH;
        this.FormAgregarConsumo.str8HrsLixPH = response.str8HrsLixPH;
        this.FormAgregarConsumo.str12HrsLixPH = response.str12HrsLixPH;
        this.FormAgregarConsumo.str24HrsLixPH = response.str24HrsLixPH;
        this.FormAgregarConsumo.str48HrsLixPH = response.str48HrsLixPH;
        this.FormAgregarConsumo.str72HrsLixPH = response.str72HrsLixPH;
        this.FormAgregarConsumo.strHrsLixReactOH0 = response.strHrsLixReactOH0;
        this.FormAgregarConsumo.strHrsLixReactOH2 = response.strHrsLixReactOH2;
        this.FormAgregarConsumo.strHrsLixReactOH4 = response.strHrsLixReactOH4;
        this.FormAgregarConsumo.strHrsLixReactOH8 = response.strHrsLixReactOH8;
        this.FormAgregarConsumo.strHrsLixReactOH12 = response.strHrsLixReactOH12;
        this.FormAgregarConsumo.strHrsLixReactOH24 = response.strHrsLixReactOH24;
        this.FormAgregarConsumo.strHrsLixReactOH48 = response.strHrsLixReactOH48;
        this.FormAgregarConsumo.strHrsLixReactOH72 = response.strHrsLixReactOH72;
        this.FormAgregarConsumo.strVolDes2 = response.strVolDes2;
        this.FormAgregarConsumo.strVolDes4 = response.strVolDes4;
        this.FormAgregarConsumo.strVolDes8 = response.strVolDes8;
        this.FormAgregarConsumo.strVolDes12 = response.strVolDes12;
        this.FormAgregarConsumo.strVolDes24 = response.strVolDes24;
        this.FormAgregarConsumo.strVolDes48 = response.strVolDes48;
        this.FormAgregarConsumo.strVolDes72 = response.strVolDes72;
      }
      this.CalcularConsumos();
      this.dialogConsumosVisible = true;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar consumo.');
    })
  }
  CalcularConsumos(){
    var dataCons: any = this.FormAgregarConsumo;
    this.FormAgregarConsumo.strGeGEspecif = (dataCons.strGeP3)/((dataCons.strGeP2-dataCons.strGeP1)-(dataCons.strGeP4-dataCons.strGeP1-dataCons.strGeP3));
    this.FormAgregarConsumo.strPlConsK = (this.FormAgregarConsumo.strGeGEspecif2-1)/this.FormAgregarConsumo.strGeGEspecif2;
    this.FormAgregarConsumo.strPlPorcSolido = (this.FormAgregarConsumo.strPlDensidad-1000)/(this.FormAgregarConsumo.strPlDensidad*this.FormAgregarConsumo.strPlConsK)*100;
    this.FormAgregarConsumo.strPlPorcLiquido = 100-this.FormAgregarConsumo.strPlPorcSolido;
    this.FormAgregarConsumo.strPlDilusion = this.FormAgregarConsumo.strPlPorcLiquido/this.FormAgregarConsumo.strPlPorcSolido;
    this.FormAgregarConsumo.strPlMineralGr = this.getNumberFix(this.FormAgregarConsumo.strPlPorcSolido * this.FormAgregarConsumo.strPlPeso/100, 0)
    this.FormAgregarConsumo.strPlAguaMl = this.FormAgregarConsumo.strPlPeso-this.FormAgregarConsumo.strPlMineralGr;

    this.FormAgregarConsumo.strHrsLixReactCN0 = ((this.FormAgregarConsumo.str0HrsLixCN*this.FormAgregarConsumo.strPlAguaMl/98)===0)? '' : (this.FormAgregarConsumo.str0HrsLixCN*this.FormAgregarConsumo.strPlAguaMl/98)
    var auxLixCN2 = 0, auxLixCN4 = 0, auxLixCN8 = 0, auxLixCN12 = 0, auxLixCN24 = 0, auxLixCN48 = 0, auxLixCN72 = 0;
    if(this.FormAgregarConsumo.str2HrsLixCN != '')
      auxLixCN2=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes2)*this.FormAgregarConsumo.str2HrsLixCN/98);

    if(this.FormAgregarConsumo.str4HrsLixCN != '')
      auxLixCN4=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes4)*this.FormAgregarConsumo.str4HrsLixCN/98);

    if(this.FormAgregarConsumo.str8HrsLixCN != '')
      auxLixCN8=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes8)*this.FormAgregarConsumo.str8HrsLixCN/98);

    if(this.FormAgregarConsumo.str12HrsLixCN != '')
      auxLixCN12=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes12)*this.FormAgregarConsumo.str12HrsLixCN/98);

    if(this.FormAgregarConsumo.str24HrsLixCN != '')
      auxLixCN24=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes24)*this.FormAgregarConsumo.str24HrsLixCN/98);

    if(this.FormAgregarConsumo.str48HrsLixCN != '')
      auxLixCN48=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes48)*this.FormAgregarConsumo.str48HrsLixCN/98);

    if(this.FormAgregarConsumo.str72HrsLixCN != '')
      auxLixCN72=this.FormAgregarConsumo.strHrsLixReactCN0 - ((this.FormAgregarConsumo.strPlAguaMl - this.FormAgregarConsumo.strVolDes72)*this.FormAgregarConsumo.str72HrsLixCN/98);

    this.FormAgregarConsumo.strHrsLixReactCN2 = (auxLixCN2 === 0)? '' : auxLixCN2.toString();
    this.FormAgregarConsumo.strHrsLixReactCN4 = (auxLixCN4 === 0)? '' : auxLixCN4.toString();
    this.FormAgregarConsumo.strHrsLixReactCN8 = (auxLixCN8 === 0)? '' : auxLixCN8.toString();
    this.FormAgregarConsumo.strHrsLixReactCN12 = (auxLixCN12 === 0)? '' : auxLixCN12.toString();
    this.FormAgregarConsumo.strHrsLixReactCN24 = (auxLixCN24 === 0)? '' : auxLixCN24.toString();
    this.FormAgregarConsumo.strHrsLixReactCN48 = (auxLixCN48 === 0)? '' : auxLixCN48.toString();
    this.FormAgregarConsumo.strHrsLixReactCN72 = (auxLixCN72 === 0)? '' : auxLixCN72.toString();

    var auxNaOH = 0, auxNaCN = 0;
    auxNaOH = Number(this.FormAgregarConsumo.strHrsLixReactOH0) + Number(this.FormAgregarConsumo.strHrsLixReactOH2)
              + Number(this.FormAgregarConsumo.strHrsLixReactOH4) + Number(this.FormAgregarConsumo.strHrsLixReactOH8)
              + Number(this.FormAgregarConsumo.strHrsLixReactOH12) + Number(this.FormAgregarConsumo.strHrsLixReactOH24)
              + Number(this.FormAgregarConsumo.strHrsLixReactOH48) + Number(this.FormAgregarConsumo.strHrsLixReactOH72);
    // if(this.FormAgregarConsumo.strHrsLixReactCN0 === '' || this.FormAgregarConsumo.strHrsLixReactCN2 === '' ||
    //   this.FormAgregarConsumo.strHrsLixReactCN4 === '' || this.FormAgregarConsumo.strHrsLixReactCN8 === '' ||
    //   this.FormAgregarConsumo.strHrsLixReactCN12 === '' || this.FormAgregarConsumo.strHrsLixReactCN24 === '' ||
    //   this.FormAgregarConsumo.strHrsLixReactCN48 === '' || this.FormAgregarConsumo.strHrsLixReactCN72 === ''){
    //
    //   auxNaCN = 0;
    // }
    // else{
      auxNaCN = Number(this.FormAgregarConsumo.strHrsLixReactCN0) + Number(this.FormAgregarConsumo.strHrsLixReactCN2)
                + Number(this.FormAgregarConsumo.strHrsLixReactCN4) + Number(this.FormAgregarConsumo.strHrsLixReactCN8)
                + Number(this.FormAgregarConsumo.strHrsLixReactCN12) + Number(this.FormAgregarConsumo.strHrsLixReactCN24)
                + Number(this.FormAgregarConsumo.strHrsLixReactCN48) + Number(this.FormAgregarConsumo.strHrsLixReactCN72)/Number(this.FormAgregarConsumo.strPlMineralGr);
    // }
    this.FormAgregarConsumo.strConsReactNaOH = (auxNaOH * 1000 === 0)? '' : auxNaOH;
    this.FormAgregarConsumo.strConsReactNaCN = (auxNaCN === 0)? '' : auxNaCN;
  }
  AgregarConsumo(FormAdd){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando consumos...'
    });
    var DataAdd: any = {
      strLote: this.rowSelectedEdit.strLote,
      strGeP1: FormAdd.strGeP1,
      strGeP2: FormAdd.strGeP2,
      strGeP3: FormAdd.strGeP3,
      strGeP4: FormAdd.strGeP4,
      strGeEspecif2: FormAdd.strGeGEspecif2,
      strPlDensidad: FormAdd.strPlDensidad,
      strPlPorcMalla: FormAdd.strPlPorcMalla,
      strPlPeso: FormAdd.strPlPeso,
      str0HrsLixCN: FormAdd.str0HrsLixCN,
      str2HrsLixCN: FormAdd.str2HrsLixCN,
      str4HrsLixCN: FormAdd.str4HrsLixCN,
      str8HrsLixCN: FormAdd.str8HrsLixCN,
      str12HrsLixCN: FormAdd.str12HrsLixCN,
      str24HrsLixCN: FormAdd.str24HrsLixCN,
      str48HrsLixCN: FormAdd.str48HrsLixCN,
      str72HrsLixCN: FormAdd.str72HrsLixCN,
      str0HrsLixPH: FormAdd.str0HrsLixPH,
      str2HrsLixPH: FormAdd.str2HrsLixPH,
      str4HrsLixPH: FormAdd.str4HrsLixPH,
      str8HrsLixPH: FormAdd.str8HrsLixPH,
      str12HrsLixPH: FormAdd.str12HrsLixPH,
      str24HrsLixPH: FormAdd.str24HrsLixPH,
      str48HrsLixPH: FormAdd.str48HrsLixPH,
      str72HrsLixPH: FormAdd.str72HrsLixPH,
      strHrsLixReactOH0: FormAdd.strHrsLixReactOH0,
      strHrsLixReactOH2: FormAdd.strHrsLixReactOH2,
      strHrsLixReactOH4: FormAdd.strHrsLixReactOH4,
      strHrsLixReactOH8: FormAdd.strHrsLixReactOH8,
      strHrsLixReactOH12: FormAdd.strHrsLixReactOH12,
      strHrsLixReactOH24: FormAdd.strHrsLixReactOH24,
      strHrsLixReactOH48: FormAdd.strHrsLixReactOH48,
      strHrsLixReactOH72: FormAdd.strHrsLixReactOH72,
      strVolDes2: FormAdd.strVolDes2,
      strVolDes4: FormAdd.strVolDes4,
      strVolDes8: FormAdd.strVolDes8,
      strVolDes12: FormAdd.strVolDes12,
      strVolDes24: FormAdd.strVolDes24,
      strVolDes48: FormAdd.strVolDes48,
      strVolDes72: FormAdd.strVolDes72,
      strNaOH: this.getNumberFix(FormAdd.strConsReactNaOH, 2),
	    strNaCN: this.getNumberFix(FormAdd.strConsReactNaCN, 2),
      strUsuarioCrea: this.userDec.strUsuario
    }
    ConsumoService.AgregarConsumo(DataAdd)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar consumos:"+response);
      else if(response === -2) this.openMessage("Se actualizó correctamente.");
      else this.openMessage('Consumos guardados correctamente.');
      loadingInstance.close();
      this.BuscarLotes();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar consumo.');
    })
    this.dialogConsumosVisible = false;
  }
  LimpiarFormAgregarConsumo(){
    this.FormAgregarConsumo.strGeP1 = '155.5',
    this.FormAgregarConsumo.strGeP2 = '255.6',
    this.FormAgregarConsumo.strGeP3 = '13',
    this.FormAgregarConsumo.strGeP4 = '264',
    this.FormAgregarConsumo.strGeGEspecif = '',
    this.FormAgregarConsumo.strGeGEspecif2 = '2.90',
    this.FormAgregarConsumo.strPlDensidad = '1300',
    this.FormAgregarConsumo.strPlConsK = '',
    this.FormAgregarConsumo.strPlPorcSolido = '',
    this.FormAgregarConsumo.strPlPorcLiquido = '',
    this.FormAgregarConsumo.strPlDilusion = '',
    this.FormAgregarConsumo.strPlPorcMalla = '80.00',
    this.FormAgregarConsumo.strPlMineralGr = '',
    this.FormAgregarConsumo.strPlAguaMl = '',
    this.FormAgregarConsumo.strPlPeso = '3000',

    this.FormAgregarConsumo.str0HrsLixCN = '',
    this.FormAgregarConsumo.str2HrsLixCN = '',
    this.FormAgregarConsumo.str4HrsLixCN = '',
    this.FormAgregarConsumo.str8HrsLixCN = '',
    this.FormAgregarConsumo.str12HrsLixCN = '',
    this.FormAgregarConsumo.str24HrsLixCN = '',
    this.FormAgregarConsumo.str48HrsLixCN = '',
    this.FormAgregarConsumo.str72HrsLixCN = '',

    this.FormAgregarConsumo.str0HrsLixPH = '',
    this.FormAgregarConsumo.str2HrsLixPH = '',
    this.FormAgregarConsumo.str4HrsLixPH = '',
    this.FormAgregarConsumo.str8HrsLixPH = '',
    this.FormAgregarConsumo.str12HrsLixPH = '',
    this.FormAgregarConsumo.str24HrsLixPH = '',
    this.FormAgregarConsumo.str48HrsLixPH = '',
    this.FormAgregarConsumo.str72HrsLixPH = '',

    this.FormAgregarConsumo.strHrsLixReactCN0 = '',
    this.FormAgregarConsumo.strHrsLixReactOH0 ='',
    this.FormAgregarConsumo.strHrsLixReactCN2 = '',
    this.FormAgregarConsumo.strHrsLixReactOH2 ='',
    this.FormAgregarConsumo.strHrsLixReactCN4 = '',
    this.FormAgregarConsumo.strHrsLixReactOH4 ='',
    this.FormAgregarConsumo.strHrsLixReactCN8 = '',
    this.FormAgregarConsumo.strHrsLixReactOH8 ='',
    this.FormAgregarConsumo.strHrsLixReactCN12 = '',
    this.FormAgregarConsumo.strHrsLixReactOH12 ='',
    this.FormAgregarConsumo.strHrsLixReactCN24 = '',
    this.FormAgregarConsumo.strHrsLixReactOH24 ='',
    this.FormAgregarConsumo.strHrsLixReactCN48 = '',
    this.FormAgregarConsumo.strHrsLixReactOH48 ='',
    this.FormAgregarConsumo.strHrsLixReactCN72 = '',
    this.FormAgregarConsumo.strHrsLixReactOH72 = '',

    this.FormAgregarConsumo.strVolDes2 = '25',
    this.FormAgregarConsumo.strVolDes4 = '25',
    this.FormAgregarConsumo.strVolDes8 = '25',
    this.FormAgregarConsumo.strVolDes12 = '25',
    this.FormAgregarConsumo.strVolDes24 = '25',
    this.FormAgregarConsumo.strVolDes48 = '25',
    this.FormAgregarConsumo.strVolDes72 = '25',

    this.FormAgregarConsumo.strConsReactNaOH = '',
    this.FormAgregarConsumo.strConsReactNaCN = '',
    this.FormAgregarConsumo.strOxDis48 = '',
    this.FormAgregarConsumo.strOxDis72 = ''
  }

  // ||========================||
  // ||      RECUPERACION      ||
  // ||========================||
  handleTabClick(tab, event) {
    if(tab.name=="first"){

    }
    if(tab.name=='second'){
      this.FormAgregarRcp.strTipoProceso = 'normal';
      //aki
      ConsumoService.SearchConsumos(this.rowSelectedEdit.strLote)
      .then(response =>{
        if(response === null){
          this.PesosM.PesoMuestra = '0';
          this.PesosM.PesoAgua = '0';
          this.viewErrorPesos = true;
        }
        else{
          this.viewErrorPesos = false;
          var Peso = Number(response.strPlPeso);
          var consK = (Number(response.strGeEspecif2) - 1)/Number(response.strGeEspecif2);
          var PorcSolido = (Number(response.strPlDensidad)-1000)/(Number(response.strPlDensidad)*consK)*100;
          this.PesosM.PesoMuestra = PorcSolido * Peso/100;
          this.PesosM.PesoAgua = Peso - Number(this.PesosM.PesoMuestra);
        }
        this.ObtenerRecuperacion(this.FormAgregarRcp.strTipoProceso);
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar pesos.');
      })

    }
  }
  CalculoRecuperacion(){
    //Save Values
    var CoefTestAu1: number = Number(this.PesosM.PesoAgua) /*1943.31983805668*/,
        CoefTestAu2:number = 25, CoefResAu:number = 34.285,
        CoefResAu2:number = Number(this.PesosM.PesoMuestra) /*1056.68016194332*/;
    var _oro2=this.FormAgregarRcp.strOroPpm2, _oro4=this.FormAgregarRcp.strOroPpm4,
    _oro8=this.FormAgregarRcp.strOroPpm8, _oro12=this.FormAgregarRcp.strOroPpm12,
    _oro24=this.FormAgregarRcp.strOroPpm24, _oro48=this.FormAgregarRcp.strOroPpm48,
    _oro72=this.FormAgregarRcp.strOroPpm72, _oro90=this.FormAgregarRcp.strOroPpm90,

    _plata2=this.FormAgregarRcp.strPlataPpm2, _plata4=this.FormAgregarRcp.strPlataPpm4,
    _plata8=this.FormAgregarRcp.strPlataPpm8, _plata12=this.FormAgregarRcp.strPlataPpm12,
    _plata24=this.FormAgregarRcp.strPlataPpm24, _plata48=this.FormAgregarRcp.strPlataPpm48,
    _plata72=this.FormAgregarRcp.strPlataPpm72, _plata90=this.FormAgregarRcp.strPlataPpm90,

    _auRipio=this.FormAgregarRcp.strLeyAuRipio, _agRipio=this.FormAgregarRcp.strLeyAgRipio,
    _cobre48=this.FormAgregarRcp.strCobrePpm48, _cobre72=this.FormAgregarRcp.strCobrePpm72,
    _cobre90=this.FormAgregarRcp.strCobrePpm90

    if(_auRipio==='' || _agRipio==='') {
      this.msgRecuperacionRipio = 'Faltan leyes de Ripio, no se calculará la recuperacion Final';
      this.viewMsgLeyRipio = true;
    }
    else{ this.viewMsgLeyRipio = false; }

    this.FormAgregarRcp.strCabEnsayOro = CoefResAu * Number(this.reporteLeyes.leyAuOz);
    this.FormAgregarRcp.strColaEnsayOro = CoefResAu * Number(_auRipio);
    this.FormAgregarRcp.strMetalCabOro = Number(this.FormAgregarRcp.strCabEnsayOro)*CoefResAu2/1000;

    this.FormAgregarRcp.str1PruebaOro2 = (_oro2 === '')? '' : CoefTestAu1 * Number(_oro2)/1000;
    this.FormAgregarRcp.str1PruebaOro4 = (_oro4 === '')? '' : CoefTestAu1 * Number(_oro4)/1000;
    this.FormAgregarRcp.str1PruebaOro8 = (_oro8 === '')? '' : CoefTestAu1 * Number(_oro8)/1000;
    this.FormAgregarRcp.str1PruebaOro12 = (_oro12 === '')? '' : CoefTestAu1 * Number(_oro12)/1000;
    this.FormAgregarRcp.str1PruebaOro24 = (_oro24 === '')? '' : CoefTestAu1 * Number(_oro24)/1000;
    this.FormAgregarRcp.str1PruebaOro48 = (_oro48 === '')? '' : CoefTestAu1 * Number(_oro48)/1000;
    this.FormAgregarRcp.str1PruebaOro72 = (_oro72 === '')? '' : CoefTestAu1 * Number(_oro72)/1000;
    this.FormAgregarRcp.str1PruebaOro90 = (_oro90 === '')? '' : CoefTestAu1 * Number(_oro90)/1000;

    this.FormAgregarRcp.str2PruebaOro2 = (_oro2 === '')? '' : CoefTestAu2 * Number(_oro2)/1000;
    this.FormAgregarRcp.str2PruebaOro4 = (_oro4 === '')? '' : CoefTestAu2 * Number(_oro4)/1000 + Number(this.FormAgregarRcp.str2PruebaOro2);
    this.FormAgregarRcp.str2PruebaOro8 = (_oro8 === '')? '' : CoefTestAu2 * Number(_oro8)/1000 + Number(this.FormAgregarRcp.str2PruebaOro4);
    this.FormAgregarRcp.str2PruebaOro12 = (_oro12 === '')? '' : CoefTestAu2 * Number(_oro12)/1000 + Number(this.FormAgregarRcp.str2PruebaOro8);
    this.FormAgregarRcp.str2PruebaOro24 = (_oro24 === '')? '' : CoefTestAu2 * Number(_oro24)/1000 + Number(this.FormAgregarRcp.str2PruebaOro12);
    this.FormAgregarRcp.str2PruebaOro48 = (_oro48 === '')? '' : CoefTestAu2 * Number(_oro48)/1000 + Number(this.FormAgregarRcp.str2PruebaOro24);
    this.FormAgregarRcp.str2PruebaOro72 = (_oro72 === '')? '' : CoefTestAu2 * Number(_oro72)/1000 + Number(this.FormAgregarRcp.str2PruebaOro48);
    this.FormAgregarRcp.str2PruebaOro90 = (_oro90 === '')? '' : CoefTestAu2 * Number(_oro90)/1000 + Number(this.FormAgregarRcp.str2PruebaOro72);

    this.FormAgregarRcp.str3PruebaOro2 = this.FormAgregarRcp.str1PruebaOro2;
    this.FormAgregarRcp.str3PruebaOro4 = Number(this.FormAgregarRcp.str1PruebaOro4)+Number(this.FormAgregarRcp.str2PruebaOro2);
    this.FormAgregarRcp.str3PruebaOro8 = Number(this.FormAgregarRcp.str1PruebaOro8)+Number(this.FormAgregarRcp.str2PruebaOro4);
    this.FormAgregarRcp.str3PruebaOro12 = Number(this.FormAgregarRcp.str1PruebaOro12)+Number(this.FormAgregarRcp.str2PruebaOro8);
    this.FormAgregarRcp.str3PruebaOro24 = Number(this.FormAgregarRcp.str1PruebaOro24)+Number(this.FormAgregarRcp.str2PruebaOro12);
    this.FormAgregarRcp.str3PruebaOro48 = Number(this.FormAgregarRcp.str1PruebaOro48)+Number(this.FormAgregarRcp.str2PruebaOro24);
    this.FormAgregarRcp.str3PruebaOro72 = Number(this.FormAgregarRcp.str1PruebaOro72)+Number(this.FormAgregarRcp.str2PruebaOro48);
    this.FormAgregarRcp.str3PruebaOro90 = Number(this.FormAgregarRcp.str1PruebaOro90)+Number(this.FormAgregarRcp.str2PruebaOro72);

    this.FormAgregarRcp.str4PruebaOro2 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro2)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro4 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro4)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro8 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro8)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro12 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro12)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro24 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro24)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro48 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro48)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro72 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro72)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.str4PruebaOro90 = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.str3PruebaOro90)/Number(this.FormAgregarRcp.strMetalCabOro)*100;

    this.FormAgregarRcp.str1PruebaPlata2 = CoefTestAu1 * Number(_plata2)/1000;
    this.FormAgregarRcp.str1PruebaPlata4 = CoefTestAu1 * Number(_plata4)/1000;
    this.FormAgregarRcp.str1PruebaPlata8 = CoefTestAu1 * Number(_plata8)/1000;
    this.FormAgregarRcp.str1PruebaPlata12 = CoefTestAu1 * Number(_plata12)/1000;
    this.FormAgregarRcp.str1PruebaPlata24 = CoefTestAu1 * Number(_plata24)/1000;
    this.FormAgregarRcp.str1PruebaPlata48 = CoefTestAu1 * Number(_plata48)/1000;
    this.FormAgregarRcp.str1PruebaPlata72 = CoefTestAu1 * Number(_plata72)/1000;
    this.FormAgregarRcp.str1PruebaPlata90 = CoefTestAu1 * Number(_plata90)/1000;

    this.FormAgregarRcp.str2PruebaPlata2 = (_plata2 === '')? '' : CoefTestAu2 * Number(_plata2)/1000;
    this.FormAgregarRcp.str2PruebaPlata4 = (_plata4 === '')? '' : CoefTestAu2 * Number(_plata4)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata2);
    this.FormAgregarRcp.str2PruebaPlata8 = (_plata8 === '')? '' : CoefTestAu2 * Number(_plata8)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata4);
    this.FormAgregarRcp.str2PruebaPlata12 = (_plata12 === '')? '' : CoefTestAu2 * Number(_plata12)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata8);
    this.FormAgregarRcp.str2PruebaPlata24 = (_plata24 === '')? '' : CoefTestAu2 * Number(_plata24)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata12);
    this.FormAgregarRcp.str2PruebaPlata48 = (_plata48 === '')? '' : CoefTestAu2 * Number(_plata48)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata24);
    this.FormAgregarRcp.str2PruebaPlata72 = (_plata72 === '')? '' : CoefTestAu2 * Number(_plata72)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata48);
    this.FormAgregarRcp.str2PruebaPlata90 = (_plata90 === '')? '' : CoefTestAu2 * Number(_plata90)/1000 + Number(this.FormAgregarRcp.str2PruebaPlata72);

    this.FormAgregarRcp.str3PruebaPlata2 = this.FormAgregarRcp.str1PruebaPlata2;
    this.FormAgregarRcp.str3PruebaPlata4 = Number(this.FormAgregarRcp.str1PruebaPlata4)+Number(this.FormAgregarRcp.str2PruebaPlata2);
    this.FormAgregarRcp.str3PruebaPlata8 = Number(this.FormAgregarRcp.str1PruebaPlata8)+Number(this.FormAgregarRcp.str2PruebaPlata4);
    this.FormAgregarRcp.str3PruebaPlata12 = Number(this.FormAgregarRcp.str1PruebaPlata12)+Number(this.FormAgregarRcp.str2PruebaPlata8);
    this.FormAgregarRcp.str3PruebaPlata24 = Number(this.FormAgregarRcp.str1PruebaPlata24)+Number(this.FormAgregarRcp.str2PruebaPlata12);
    this.FormAgregarRcp.str3PruebaPlata48 = Number(this.FormAgregarRcp.str1PruebaPlata48)+Number(this.FormAgregarRcp.str2PruebaPlata24);
    this.FormAgregarRcp.str3PruebaPlata72 = Number(this.FormAgregarRcp.str1PruebaPlata72)+Number(this.FormAgregarRcp.str2PruebaPlata48);
    this.FormAgregarRcp.str3PruebaPlata90 = Number(this.FormAgregarRcp.str1PruebaPlata90)+Number(this.FormAgregarRcp.str2PruebaPlata72);

    this.FormAgregarRcp.str4PruebaPlata2 = '';
    this.FormAgregarRcp.str4PruebaPlata4 = '';
    this.FormAgregarRcp.str4PruebaPlata8 = '';
    this.FormAgregarRcp.str4PruebaPlata12 = '';
    this.FormAgregarRcp.str4PruebaPlata24 = '';
    this.FormAgregarRcp.str4PruebaPlata48 = '';
    this.FormAgregarRcp.str4PruebaPlata72 = '';
    this.FormAgregarRcp.str4PruebaPlata90 = '';


    this.FormAgregarRcp.strMetalSolOro = Math.max(Number(this.FormAgregarRcp.str3PruebaOro2),Number(this.FormAgregarRcp.str3PruebaOro4),
                                          Number(this.FormAgregarRcp.str3PruebaOro8),Number(this.FormAgregarRcp.str3PruebaOro12),
                                          Number(this.FormAgregarRcp.str3PruebaOro24),Number(this.FormAgregarRcp.str3PruebaOro48),
                                          Number(this.FormAgregarRcp.str3PruebaOro72),Number(this.FormAgregarRcp.str3PruebaOro90));
    this.FormAgregarRcp.strMetalColaOro = Number(this.FormAgregarRcp.strColaEnsayOro)*CoefResAu2/1000;
    this.FormAgregarRcp.strMetalTotalOro = Number(this.FormAgregarRcp.strMetalSolOro)+Number(this.FormAgregarRcp.strMetalColaOro);
    this.FormAgregarRcp.strCabCalculaOro = Number(this.FormAgregarRcp.strMetalTotalOro)/(CoefResAu2/1000);
    this.FormAgregarRcp.strRcpEnsayOro = (this.FormAgregarRcp.strCabEnsayOro=='')?'':(Number(this.FormAgregarRcp.strCabEnsayOro)-Number(this.FormAgregarRcp.strColaEnsayOro))/Number(this.FormAgregarRcp.strCabEnsayOro)*100;
    this.FormAgregarRcp.strRcpCalculaOro = (this.FormAgregarRcp.strMetalTotalOro=='')?'':Number(this.FormAgregarRcp.strMetalSolOro)/Number(this.FormAgregarRcp.strMetalTotalOro)*100;

    this.FormAgregarRcp.strCabEnsayPlata = Number(this.reporteLeyes.leyAgOz)*CoefResAu;
    this.FormAgregarRcp.strColaEnsayPlata = Number(_agRipio)*CoefResAu;
    this.FormAgregarRcp.strMetalColaPlata = Number(this.FormAgregarRcp.strColaEnsayPlata)*CoefResAu2/1000;
    this.FormAgregarRcp.strMetalSolPlata = Math.max(Number(this.FormAgregarRcp.str3PruebaPlata2),Number(this.FormAgregarRcp.str3PruebaPlata4),
                                          Number(this.FormAgregarRcp.str3PruebaPlata8),Number(this.FormAgregarRcp.str3PruebaPlata12),
                                          Number(this.FormAgregarRcp.str3PruebaPlata24),Number(this.FormAgregarRcp.str3PruebaPlata48),
                                          Number(this.FormAgregarRcp.str3PruebaPlata72),Number(this.FormAgregarRcp.str3PruebaPlata90));
    this.FormAgregarRcp.strMetalTotalPlata = Number(this.FormAgregarRcp.strMetalSolPlata)+Number(this.FormAgregarRcp.strMetalColaPlata);
    this.FormAgregarRcp.strCabCalculaPlata = Number(this.FormAgregarRcp.strMetalTotalPlata)/(CoefResAu2/1000);
    this.FormAgregarRcp.strRcpEnsayPlata = (this.FormAgregarRcp.strCabEnsayPlata=='')?'':(Number(this.FormAgregarRcp.strCabEnsayPlata)-Number(this.FormAgregarRcp.strColaEnsayPlata))/Number(this.FormAgregarRcp.strCabEnsayPlata)*100;
    this.FormAgregarRcp.strRcpCalculaPlata = (this.FormAgregarRcp.strMetalTotalPlata=='')?'':Number(this.FormAgregarRcp.strMetalSolPlata)/Number(this.FormAgregarRcp.strMetalTotalPlata)*100;
    this.FormAgregarRcp.strMetalCabPlata = Number(this.FormAgregarRcp.strCabEnsayPlata)*CoefResAu2/1000;

    this.FormAgregarRcp.strRcpArtifFinal = (this.FormAgregarRcp.strMetalCabOro=='')?'':Number(this.FormAgregarRcp.strMetalSolOro)/Number(this.FormAgregarRcp.strMetalCabOro)*100;
    this.FormAgregarRcp.strRcpOroFinal = (_auRipio === '')?'' : this.getNumberFix(this.FormAgregarRcp.strRcpCalculaOro, 0);
    this.FormAgregarRcp.strRcpPlataFinal = (_agRipio === '')?'' : this.getNumberFix(this.FormAgregarRcp.strRcpCalculaPlata, 0);
    this.FormAgregarRcp.strPorcCobreFinal = this.getNumberFix((Math.max(Number(_cobre48), Number(_cobre72), Number(_cobre90))*1.84*1.057)/10570, 3);
    this.FormAgregarRcp.strConsumoNaOH = this.rowSelectedEdit.strNaOHKgTM;
    this.FormAgregarRcp.strConsumoNaCN = this.rowSelectedEdit.strNaCNKgTM;
    this.FormAgregarRcp.strREEDrive = (Number(this.FormAgregarRcp.strRcpArtifFinal)>=100)?'Alta':(Number(this.FormAgregarRcp.strRcpArtifFinal)==0)?'':Number(this.FormAgregarRcp.strRcpArtifFinal)
    this.FormAgregarRcp.strHorasAgitacion = (this.FormAgregarRcp.strREEDrive=='')?'':(_oro4=='0')?2:(_oro8=='0')?4:(_oro12=='0')?8:(_oro24=='0')?12:(_oro48=='0')?24:(_oro72=='0')?48:72
    this.FormAgregarRcp.strREEDrive = (this.FormAgregarRcp.strREEDrive ==='Alta')?this.FormAgregarRcp.strREEDrive : this.getNumberFix(this.FormAgregarRcp.strREEDrive, 2)
  }
  ObtenerRecuperacion(strTipoProceso){
    RecuperacionService.SearchRecuperacion(this.rowSelectedEdit.strLote, strTipoProceso)
    .then(response =>{
      if(response == null){
        this.FormAgregarRcp.strOroPpm2 = '0';
        this.FormAgregarRcp.strOroPpm4 = '0';
        this.FormAgregarRcp.strOroPpm8 = '0';
        this.FormAgregarRcp.strOroPpm12 = '0';
        this.FormAgregarRcp.strOroPpm24 = '0';
        this.FormAgregarRcp.strOroPpm48 = '0';
        this.FormAgregarRcp.strOroPpm72 = '0';
        this.FormAgregarRcp.strOroPpm90 = '0';
        this.FormAgregarRcp.strPlataPpm2 = '0';
        this.FormAgregarRcp.strPlataPpm4 = '0';
        this.FormAgregarRcp.strPlataPpm8 = '0';
        this.FormAgregarRcp.strPlataPpm12 = '0';
        this.FormAgregarRcp.strPlataPpm24 = '0';
        this.FormAgregarRcp.strPlataPpm48 = '0';
        this.FormAgregarRcp.strPlataPpm72 = '0';
        this.FormAgregarRcp.strPlataPpm90 = '0';
        this.FormAgregarRcp.strLeyAuRipio = '';
        this.FormAgregarRcp.strLeyAgRipio = '';
        this.FormAgregarRcp.strCobrePpm48 = '0';
        this.FormAgregarRcp.strCobrePpm72 = '0';
        this.FormAgregarRcp.strCobrePpm90 = '0';
      }
      else{
        this.FormAgregarRcp.strOroPpm2 = (response.strOroPpm2==null)? '' : response.strOroPpm2;
        this.FormAgregarRcp.strOroPpm4 = (response.strOroPpm4==null)? '' : response.strOroPpm4;
        this.FormAgregarRcp.strOroPpm8 = (response.strOroPpm8==null)? '' : response.strOroPpm8;
        this.FormAgregarRcp.strOroPpm12 = (response.strOroPpm12==null)? '' : response.strOroPpm12;
        this.FormAgregarRcp.strOroPpm24 = (response.strOroPpm24==null)? '' : response.strOroPpm24;
        this.FormAgregarRcp.strOroPpm48 = (response.strOroPpm48==null)? '' : response.strOroPpm48;
        this.FormAgregarRcp.strOroPpm72 = (response.strOroPpm72==null)? '' : response.strOroPpm72;
        this.FormAgregarRcp.strOroPpm90 = (response.strOroPpm90==null)? '' : response.strOroPpm90;
        this.FormAgregarRcp.strPlataPpm2 = (response.strPlataPpm2==null)? '' : response.strPlataPpm2;
        this.FormAgregarRcp.strPlataPpm4 = (response.strPlataPpm4==null)? '' : response.strPlataPpm4;
        this.FormAgregarRcp.strPlataPpm8 = (response.strPlataPpm8==null)? '' : response.strPlataPpm8;
        this.FormAgregarRcp.strPlataPpm12 = (response.strPlataPpm12==null)? '' : response.strPlataPpm12;
        this.FormAgregarRcp.strPlataPpm24 = (response.strPlataPpm24==null)? '' : response.strPlataPpm24;
        this.FormAgregarRcp.strPlataPpm48 = (response.strPlataPpm48==null)? '' : response.strPlataPpm48;
        this.FormAgregarRcp.strPlataPpm72 = (response.strPlataPpm72==null)? '' : response.strPlataPpm72;
        this.FormAgregarRcp.strPlataPpm90 = (response.strPlataPpm90==null)? '' : response.strPlataPpm90;
        this.FormAgregarRcp.strLeyAuRipio = (response.strLeyAuRipio==null)? '' : response.strLeyAuRipio;
        this.FormAgregarRcp.strLeyAgRipio = (response.strLeyAgRipio==null)? '' : response.strLeyAgRipio;
        this.FormAgregarRcp.strCobrePpm48 = (response.strCobrePpm48==null)? '' : response.strCobrePpm48;
        this.FormAgregarRcp.strCobrePpm72 = (response.strCobrePpm72==null)? '' : response.strCobrePpm72;
        this.FormAgregarRcp.strCobrePpm90 = (response.strCobrePpm90==null)? '' : response.strCobrePpm90;
      }
      this.CalculoRecuperacion();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener recuperacion.');
    })
  }
  changeRCPTipoAnalisis(){
    this.ObtenerRecuperacion(this.FormAgregarRcp.strTipoProceso);
  }
  GuardarRecuperacion(){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando leyes...'
    });
    var Data: any = {
      strLote: this.rowSelectedEdit.strLote,
      strTipoProceso: this.FormAgregarRcp.strTipoProceso,
      strOroPpm2: this.FormAgregarRcp.strOroPpm2,
      strOroPpm4: this.FormAgregarRcp.strOroPpm4,
      strOroPpm8: this.FormAgregarRcp.strOroPpm8,
      strOroPpm12: this.FormAgregarRcp.strOroPpm12,
      strOroPpm24: this.FormAgregarRcp.strOroPpm24,
      strOroPpm48: this.FormAgregarRcp.strOroPpm48,
      strOroPpm72: this.FormAgregarRcp.strOroPpm72,
      strOroPpm90: this.FormAgregarRcp.strOroPpm90,
      strPlataPpm2: this.FormAgregarRcp.strPlataPpm2,
      strPlataPpm4: this.FormAgregarRcp.strPlataPpm4,
      strPlataPpm8: this.FormAgregarRcp.strPlataPpm8,
      strPlataPpm12: this.FormAgregarRcp.strPlataPpm12,
      strPlataPpm24: this.FormAgregarRcp.strPlataPpm24,
      strPlataPpm48: this.FormAgregarRcp.strPlataPpm48,
      strPlataPpm72: this.FormAgregarRcp.strPlataPpm72,
      strPlataPpm90: this.FormAgregarRcp.strPlataPpm90,
      strLeyAuRipio: this.FormAgregarRcp.strLeyAuRipio,
      strLeyAgRipio: this.FormAgregarRcp.strLeyAgRipio,
      strCobrePpm48: this.FormAgregarRcp.strCobrePpm48,
      strCobrePpm72: this.FormAgregarRcp.strCobrePpm72,
      strCobrePpm90: this.FormAgregarRcp.strCobrePpm90,
      strRcpArtifFinal: this.FormAgregarRcp.strRcpArtifFinal,
      strRcpOroFinal: this.FormAgregarRcp.strRcpOroFinal,
      strRcpPlataFinal: this.FormAgregarRcp.strRcpPlataFinal,
      strPorcCobreFinal: this.FormAgregarRcp.strPorcCobreFinal,
      strREEDrive: this.FormAgregarRcp.strREEDrive,
      strHorasAgitacion: this.FormAgregarRcp.strHorasAgitacion,
      strUsuarioCrea: this.userDec.strUsuario
    }
    RecuperacionService.AgregarRecuperacion(Data)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar recuperacion:"+response);
      else if(response === -2) this.openMessage("Se actualizó correctamente.");
      else this.openMessage('Recuperacion guardada correctamente.');
      loadingInstance.close();
      this.BuscarLotes();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar recuperacion.');
    })
  }

  changeH2O(){
    // var pesotmh = row.fltPesoTmh;

    // if(pesotmh === null || (pH2o === ""||pH2o === null)) this.rowSelectedEdit.strPSecoTM = "";
    // else{
    //   var porcH2o: number = Number(this.rowSelectedEdit.strPorcH2O);
    //   var success = pesotmh * (100 - porcH2o)/100;
    //   this.rowSelectedEdit.strPSecoTM = success.toFixed(3);
    // }
    if(this.rowSelectedEdit.strPorcH2O=='' || this.rowSelectedEdit.strPorcH2O==null) this.strH2OComercial = ''
    else{
      var pH2o = Number(this.rowSelectedEdit.strPorcH2O);
      if(this.checkedH2O==false) this.strH2OComercial = this.getNumberFix(pH2o +1, 2);
      else this.strH2OComercial = this.getNumberFix(pH2o, 2);
    }
    this.changeFinoAuGr();
  }
  changeLeyAuOzTc(value){
    if(value === null || value === "") this.rowSelectedEdit.strLeyAuGrTm = "";
    else{
      var success = Number(value)*34.286;
      this.rowSelectedEdit.strLeyAuGrTm = success;//.toFixed(3);
    }
    this.changeFinoAuGr();
  }
  changeFinoAuGr(){
    var Pseco = this.rowSelectedEdit.strPSecoTM;
    var AuOZTC = this.rowSelectedEdit.strLeyAuOzTc;
    if((AuOZTC === "" || Pseco === "" ) || (AuOZTC === null || Pseco === null)){
      this.rowSelectedEdit.strFinoAuGr = ""
    }
    else {
      var success = Number(Pseco) * Number(AuOZTC) * 34.285;
      this.rowSelectedEdit.strFinoAuGr = success;//.toFixed(3);
    }
  }
  handleCheckStatus(index, row){
    this.gridDataAddLey[index].chrCheckStatus = (this.gridDataAddLey[index].chrCheckStatus === 'T') ? 'F' : 'T';
    this.CalcularReporteFinal();
  }
  getCheckStatusLey(row){
    if(row.chrCheckStatus === 'T'){
      return true;
    }
    else{
      return false;
    }
  }
  getTipoProceso(TProceso){
    if(TProceso === 'normal') return 'newmon';
    else return TProceso;
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha == undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getDateTime(fecha:string){
    if(fecha === '' || fecha === null || fecha == undefined) return "";
    else{
      var dateString = new Date(fecha).toLocaleString('es-PE', this.options2)
      return dateString;
    }
  }
  getNumberFloat(number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string) {
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  selectRow(row,index){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    
    this.rowSelectedEdit = row;
    this.checkedH2O = (row.strCheckH2O === null || row.strCheckH2O === "" || row.strCheckH2O === "false")? false : true;
    this.FechaHoraMuestreo = row.dtmFechaHoraMuestreo;
    if(this.rowSelectedEdit.strPorcH2O=='' || this.rowSelectedEdit.strPorcH2O==null) this.strH2OComercial = ''
    else{
      var pH2o = Number(this.rowSelectedEdit.strPorcH2O);
      if(this.checkedH2O==false) this.strH2OComercial = this.getNumberFix(pH2o +1, 2);
      else this.strH2OComercial = this.getNumberFix(pH2o, 2);
    }

    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Area Laboratorio') {
        this.editViewButton = (row.chrEstadoModifLab === 'F' || row.chrEstadoModifLab === null) ? true : false;
      }
      else if(this.userDec.strCargo === 'Area Metalurgica') {
        // this.editViewButton = (row.chrEstadoModifMet === 'F' || row.chrEstadoModifMet === null) ? true : false;
      }
    }
  }
  // clickactive(event){
  //   if(event=='A'){
  //     this.blnfiltroavanzado=true;
  //   }
  //   if(event=='B'){
  //     this.blnfiltroavanzado=false;
  //   }
  // }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      FormAgregarLey: {
        strTipoMineral: 'MINERAL',
        strTipoProceso: 'normal',
        strBk: '0.202',
        strPMFino: '',
        strPMGrueso: '',
        strPesoAuMasAg: '',
        strPesoAuFino1: '',
        strPesoAuFino2: '',
        strPesoAuGrueso: '',
      },
      FormAgregarConsumo: {
        strGeP1: '155.5',
        strGeP2: '255.6',
        strGeP3: '13',
        strGeP4: '264',
        strGeGEspecif: '',
        strGeGEspecif2: '2.90',
        strPlDensidad: '1300',
        strPlConsK: '',
        strPlPorcSolido: '',
        strPlPorcLiquido: '',
        strPlDilusion: '',
        strPlPorcMalla: '80.00',
        strPlMineralGr: '',
        strPlAguaMl: '',
        strPlPeso: '3000',

        str0HrsLixCN: '',
        str2HrsLixCN: '',
        str4HrsLixCN: '',
        str8HrsLixCN: '',
        str12HrsLixCN: '',
        str24HrsLixCN: '',
        str48HrsLixCN: '',
        str72HrsLixCN: '',

        str0HrsLixPH: '',
        str2HrsLixPH: '',
        str4HrsLixPH: '',
        str8HrsLixPH: '',
        str12HrsLixPH: '',
        str24HrsLixPH: '',
        str48HrsLixPH: '',
        str72HrsLixPH: '',

        strHrsLixReactCN0: '',
        strHrsLixReactOH0:'',
        strHrsLixReactCN2: '',
        strHrsLixReactOH2:'',
        strHrsLixReactCN4: '',
        strHrsLixReactOH4:'',
        strHrsLixReactCN8: '',
        strHrsLixReactOH8:'',
        strHrsLixReactCN12: '',
        strHrsLixReactOH12:'',
        strHrsLixReactCN24: '',
        strHrsLixReactOH24:'',
        strHrsLixReactCN48: '',
        strHrsLixReactOH48:'',
        strHrsLixReactCN72: '',
        strHrsLixReactOH72: '',

        strVolDes2: '25',
        strVolDes4: '25',
        strVolDes8: '25',
        strVolDes12: '25',
        strVolDes24: '25',
        strVolDes48: '25',
        strVolDes72: '25',

        strConsReactNaOH: '',
        strConsReactNaCN: '',
        strOxDis48: '',
        strOxDis72: ''
      },
      FormAgregarRcp: {
        strTipoProceso: 'normal',
        strOroPpm2: '0',
        strOroPpm4: '0',
        strOroPpm8: '0',
        strOroPpm12: '0',
        strOroPpm24: '0',
        strOroPpm48: '0',
        strOroPpm72: '0',
        strOroPpm90: '0',
        strPlataPpm2: '0',
        strPlataPpm4: '0',
        strPlataPpm8: '0',
        strPlataPpm12: '0',
        strPlataPpm24: '0',
        strPlataPpm48: '0',
        strPlataPpm72: '0',
        strPlataPpm90: '0',
        strLeyAuRipio: '0',
        strLeyAgRipio: '0',
        strCobrePpm48: '0',
        strCobrePpm72: '0',
        strCobrePpm90: '0',

        str1PruebaOro2: '',
        str1PruebaOro4: '',
        str1PruebaOro8: '',
        str1PruebaOro12: '',
        str1PruebaOro24: '',
        str1PruebaOro48: '',
        str1PruebaOro72: '',
        str1PruebaOro90: '',
        str2PruebaOro2: '',
        str2PruebaOro4: '',
        str2PruebaOro8: '',
        str2PruebaOro12: '',
        str2PruebaOro24: '',
        str2PruebaOro48: '',
        str2PruebaOro72: '',
        str2PruebaOro90: '',
        str3PruebaOro2: '',
        str3PruebaOro4: '',
        str3PruebaOro8: '',
        str3PruebaOro12: '',
        str3PruebaOro24: '',
        str3PruebaOro48: '',
        str3PruebaOro72: '',
        str3PruebaOro90: '',
        str4PruebaOro2: '',
        str4PruebaOro4: '',
        str4PruebaOro8: '',
        str4PruebaOro12: '',
        str4PruebaOro24: '',
        str4PruebaOro48: '',
        str4PruebaOro72: '',
        str4PruebaOro90: '',

        str1PruebaPlata2: '',
        str1PruebaPlata4: '',
        str1PruebaPlata8: '',
        str1PruebaPlata12: '',
        str1PruebaPlata24: '',
        str1PruebaPlata48: '',
        str1PruebaPlata72: '',
        str1PruebaPlata90: '',
        str2PruebaPlata2: '',
        str2PruebaPlata4: '',
        str2PruebaPlata8: '',
        str2PruebaPlata12: '',
        str2PruebaPlata24: '',
        str2PruebaPlata48: '',
        str2PruebaPlata72: '',
        str2PruebaPlata90: '',
        str3PruebaPlata2: '',
        str3PruebaPlata4: '',
        str3PruebaPlata8: '',
        str3PruebaPlata12: '',
        str3PruebaPlata24: '',
        str3PruebaPlata48: '',
        str3PruebaPlata72: '',
        str3PruebaPlata90: '',
        str4PruebaPlata2: '',
        str4PruebaPlata4: '',
        str4PruebaPlata8: '',
        str4PruebaPlata12: '',
        str4PruebaPlata24: '',
        str4PruebaPlata48: '',
        str4PruebaPlata72: '',
        str4PruebaPlata90: '',

        strCabEnsayOro: '',
        strColaEnsayOro: '',
        strCabCalculaOro: '',
        strRcpEnsayOro:'',
        strRcpCalculaOro: '',
        strMetalCabOro: '',
        strMetalSolOro: '',
        strMetalColaOro: '',
        strMetalTotalOro: '',
        strCabEnsayPlata: '',
        strColaEnsayPlata: '',
        strCabCalculaPlata: '',
        strRcpEnsayPlata:'',
        strRcpCalculaPlata: '',
        strMetalCabPlata: '',
        strMetalSolPlata: '',
        strMetalColaPlata: '',
        strMetalTotalPlata: '',

        strRcpArtifFinal: '',
        strRcpOroFinal: '',
        strRcpPlataFinal: '',
        strPorcCobreFinal: '',
        strConsumoNaOH: '',
        strConsumoNaCN: '',
        strREEDrive: '',
        strHorasAgitacion: ''
      },
      dataTMineral:[
        {id:'1', name:'MINERAL'},
        {id:'2', name:'LLAMPO'},
      ],
      dataTAnalisisLey: [
        {id: 'normal', name: 'Newmon'},
        {id: 'REE', name: 'REE'},
        {id: 'RMI', name: 'RMI'},
      ],
      dataTAnalisis: [
        {id: 'normal', name: 'Newmon'},
        {id: 'REE', name: 'REE'}
      ],
      json_fields : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strMaterial" : "Material",
        "strPorcH2O" : "%H2O",
        "strCheckH2O" : "Check H2O",
        "strPSecoTM" : "P.Seco TM",
        "dtmFechaHoraMuestreo" : "Fecha y Hora de Ingreso Muestra LQ",
        "strPorcRCPAu30hrs" : "% RCP Au 48hrs",
        "strPorcRCPAu70hrs" : "% RCP Au 72hrs",
        "strPorcRCPReeAu72hrs" : "REE % RCP Au 72hrs",
        "strLeyAuOzTc" : "Ley Au Oz/tc",
        "strLeyAuREE" : "Ley Au REE",
        "strLeyAuGrTm" : "Ley Au Gr/Tm",
        "strLeyAgOzTc" : "Ley Ag (Oz/tc)",
        "strLeyAgREE" : "Ley Ag REE",
        "strPorcCobre" : "Cobre %",
        "strNaOHKgTM" : "NaOH (kg/TM)",
        "strNaCNKgTM" : "NaCN (Kg/TM)",
        "strPorcRCPRef12hrs" : "% RCP Ref",
        "strHorasAgitacion" : "Horas Agitación",
        "strPenalidad" : "Penalidad ($/Tms)",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ],
      visible2: false,
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
      GLOBAL.setComponent('laboratorio');
    }
  }

}
