import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import { Loading } from 'element-ui';
import LotesService from '@/services/lotes.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'secocha',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class SecochaComponent extends Vue {
  FormSearch: any = {};
  fechaSearch: Date = new Date();
  userDec: any = {};
  CompleteData:any = [];
  gridData: any = [];
  countGridData = 0;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  options2 = {  day: '2-digit',month: '2-digit', year: 'numeric', hour:"2-digit", minute:"2-digit" };
  //PAGINATION
  pagina: number =1;
  RegistersForPage: number = 20;
  totalRegistros: number = this.RegistersForPage;
  NivelAcceso: string = '';
  loadingGetData:boolean=false;
  // sizeScreen:string = (window.innerHeight - 200).toString();//'0';
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  constructor (){
    super()
  }

  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1014){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.BuscarLotes();
      }
    }
  }

  BuscarLotes(){
    // let loadingInstance = Loading.service({
    //   fullscreen: true,
    //   spinner: 'el-icon-loading',
    //   text:'Cargando lotes...'
    // });
    this.loadingGetData=true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    // this.mesActual = this.monthNames[fSearch.getMonth()];
    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;

    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      // loadingInstance.close()
      this.loadingGetData=false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData=false;
      // loadingInstance.close()
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getDateTime(fecha:string){
    if(fecha === '' || fecha === null || fecha == undefined) return "";
    else{
      var dateString = new Date(fecha).toLocaleString('es-PE', this.options2)
      return dateString;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : 'M.N.VALCARCEL',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
