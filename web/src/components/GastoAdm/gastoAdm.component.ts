import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import GastoAdmService from '@/services/GastoAdm.service';
import ZonaService from '@/services/zona.service';
import AcopiadorService from '@/services/acopiador.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'gastoAdm',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class GastoAdmComponent extends Vue {
  fechaSearch: Date = new Date();
  gridData: any = [];
  CompleteData: any = [];
  DataZonas: any = [];
  DataProveedor: any = [];
  DataAcopiador: any = [];
  countGridData = 0;
  loadingGetData: boolean = false;
  sizeScreen: string = (window.innerHeight - 260).toString();
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 20;
  totalRegistros:number = this.RegistersForPage;
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  fechaAddCuota: Date = new Date();
  fechaEditCuota: any = '';
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  FormAgregar: any = {};
  rowSelectedEdit: any = {};
  selectRow:any=null;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else {
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1023){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.loadingGastosAdmPorMes();
      }
    }
  }
  loadingGastosAdmPorMes(){
    var fSearch = this.fechaSearch;
    if(fSearch===null || fSearch === undefined || fSearch.toString() === '') this.openMessageError('Seleccione Año');
    else{
      this.loadingGetData = true;
      var year = this.fechaSearch.getFullYear();
      GastoAdmService.ObtenerGastoAdmPorMes(year)
      .then(response =>{
        this.CompleteData = response.Data;
        this.totalRegistros = response.Count;
        this.countGridData = response.Count;
        this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        this.openMessageError('Error al cargar gastos.');
      })
    }
  }
  BuscarZonas(){
    ZonaService.loadingData()
    .then(response => {
      this.DataZonas = response.Data;
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  BuscarAcopiadores(){
    GastoAdmService.GetAcopiadores()
    .then(response =>{
      this.DataAcopiador = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  changeZonaAdd(){
    GastoAdmService.GetProveedores(this.FormAgregar.strZona)
    .then(response =>{
      this.DataProveedor = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar proveedores');
    })
  }
  AgregarGastoAdm(FormAgregar){
    if(FormAgregar.strZona === '') this.openMessageError('Seleccione Zona');
    else{
      var Data: any = {
        strZona: FormAgregar.strZona,
        strAcopiador: (FormAgregar.strAcopiador === '')? 'All': FormAgregar.strAcopiador,
        strProveedor: (FormAgregar.strProveedor === '')? 'All': FormAgregar.strProveedor,
        fltGasto: Number(FormAgregar.fltGasto),
        dtmFecha: GLOBAL.getParseDate(this.fechaAddCuota),
        strMes: this.monthNames[this.fechaAddCuota.getMonth()],
        strUsuarioCrea: this.userDec.strUsuario,
      }
      GastoAdmService.AgregarGastoAdm(Data)
      .then(response =>{
        if(response === -1) this.openMessageError("SERVICE: Error al guardar Gasto:"+response);
        else if(response === -2) this.openMessage("Ya existe este Gasto.");
        else this.openMessage('Se guardó correctamente.');
        this.loadingGastosAdmPorMes();
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al guardar Gasto.');
      })
      this.dialogAgregarVisible = false;
    }
  }
  EditarGastoAdm(rowEdit, gasto, mes){
    var iMonth = this.monthNames.indexOf(mes);
    var date = new Date(rowEdit.strAnio, iMonth, 1);
    var Data: any = {
      intIdGastosAdm: -1,
      strZona: rowEdit.strZona,
      strAcopiador: rowEdit.strAcopiador,
      strProveedor: rowEdit.strProveedor,
      fltGasto: Number(gasto),
      dtmFecha: GLOBAL.getParseDate(date),
      strMes: this.monthNames[(iMonth)],
      strUsuarioModif: this.userDec.strUsuario,
    }

    GastoAdmService.EditarGastoAdm(Data)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar Gasto:"+response);
      else this.openMessage('Se guardó correctamente.');
      this.loadingGastosAdmPorMes();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar Gasto.');
    })
    // this.dialogEditarVisible = false;
  }
  EliminarGastoAdm(row){
    GastoAdmService.EliminarGastoAdm(row.intIdGastosAdm, this.userDec.strUsuario)
    .then(response => {
      if(response == -1) this.openMessageError('SERVICE: Error al eliminar Gasto.');
      else this.openMessage('Gasto eliminado correctamente.');
      this.loadingGastosAdmPorMes();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al eliminar Gasto.');
    })
  }
  Duplicar(row, gasto, mes){
    this.$confirm('Desea Duplicar este Gasto ?', 'Duplicar', {
      confirmButtonText: 'Duplicar',
      cancelButtonText: 'Cancelar',
      type: 'success'
    }).then(() => {
      this.DuplicarGastoAdm(row, gasto, mes);
    }).catch(() => { });
  }
  DuplicarGastoAdm(row, gasto, mes){
    var iMonth = this.monthNames.indexOf(mes);
    var date = new Date(row.strAnio, iMonth+1, 1);

    var Data: any = {
      strZona: row.strZona,
      strAcopiador: row.strAcopiador,
      strProveedor: row.strProveedor,
      fltGasto: Number(gasto),
      dtmFecha: GLOBAL.getParseDate(date),
      strMes: this.monthNames[(iMonth+1)],
      strUsuarioCrea: this.userDec.strUsuario,
    }
    GastoAdmService.AgregarGastoAdm(Data)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al duplicar Gasto:"+response);
      else if(response === -2) this.openMessage("Ya existe este Gasto.");
      else this.openMessage('Se guardó correctamente.');
      this.loadingGastosAdmPorMes();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar Gasto.');
    })
  }
  handleAgregar(){
    this.LimpiarAgregar();
    this.BuscarZonas();
    this.BuscarAcopiadores();
    this.dialogAgregarVisible = true;
  }
  handleEdit2(){
    this.handleEdit(this.seleccionarRow);
  }
  handleEdit(row){
    this.BuscarZonas();
    this.rowSelectedEdit = row;
    this.fechaEditCuota = new Date();
    this.dialogEditarVisible = true;
  }
  handleDelete(index, row){
    this.$confirm('Desea Eliminar este gasto: '+row.strZona+', '+row.strProveedor+', '+row.fltGasto, 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.EliminarGastoAdm(row);
    }).catch(() => { });
  }
  LimpiarAgregar(){
    this.FormAgregar.intIdGastosAdm= '';
    this.FormAgregar.strZona= '';
    this.FormAgregar.strProveedor= '';
    this.FormAgregar.strAcopiador= '';
    this.FormAgregar.fltGasto= '';
    this.FormAgregar.dtmFecha= '';
    this.FormAgregar.strMes= '';
    this.FormAgregar.strUsuarioCrea= '';
    this.fechaAddCuota = new Date();
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  seleccionarRow(row,index){
    debugger;
    this.seleccionarRow=row;
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar:{
        intIdGastosAdm: '',
        strZona: '',
        strAcopiador: '',
        strProveedor: '',
        fltGasto: '',
        dtmFecha: '',
        strMes: '',
        strUsuarioCrea: '',
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
