import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LotesTrujilloService from '@/services/lotesTrujillo.service';
import ProveedorService from '@/services/proveedor.service';
import LeyRefService from '@/services/leyReferencial.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'lotesTrujillo',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class LotesTrujilloComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  checkedSearch: boolean = false;
  dataProveedores: any = [];
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  FormSearch: any ={};
  fechaSearch: Date = new Date();
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  sizeScreen:string = (window.innerHeight - 200).toString();
  loadingGetData:boolean = false;
  fechaInicio: any = '';
  fechaFin: any = '';
  dialogAgregarVisible: boolean = false;
  dialogAgregarLote: boolean = false;
  fechaAgregar: Date = new Date();
  //AGREGAR
  //FormAgregar: any = {};
  FormAgregarLote: any ={};
  multipleSelection:any = [];
  loadingGetLotes:boolean = false;
  dataLotes: any = [];
  lotesAsociados: string = '';
  CodigoLeyRef: string = '';
  dialogBuscarLeyesRefVisible: boolean = false;
  dataLeyesRef: any = [];
  loadingGetLeyesRef: boolean = false;
  NroParticiones: number = 1;

  //BUSCAR LOTES
  fechaSearchLotes: Date = new Date();
  dialogBuscarLotesVisible: boolean = false;
  FormSearchLote: any = {};
  checkedSearchLotes: boolean = false;
  countLotesLJ = 0;

  //VER LOTES ASOCIADOS
  dataVerLotes: any = [];
  loadingVerLotes: boolean = false;
  dialogVerLotesAsoc: boolean = false;
  //EDITAR
  rowSelectedEdit: any = {};
  fechaEditar: any = '';
  dialogEditarLote: boolean = false;
  lotesAsociadosEdit: string = '';
  checkEditLJ: boolean = false;

  listaLetras: any = ['A','B','C','D','E','F','G','H','I','J'];

  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1028){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        this.BuscarLotesTrujillo();
      }
    }
  }
  BuscarLotesTrujillo(){
    this.loadingGetData = true;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else{
      this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearch.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    LotesTrujilloService.SearchLotesAS(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  ObtenerLotes(){
    this.loadingGetLotes = true;
    var fechaS = this.fechaSearchLotes;
    this.FormSearchLote.strProcedencia = 'TRUJILLO';
    if(this.checkedSearchLotes == true){
      this.FormSearchLote.strFechaInicio = '*'
      this.FormSearchLote.strFechaFin = '*'
    }
    else{
      this.FormSearchLote.strFechaInicio = (fechaS==null ||fechaS==undefined||fechaS.toString()=='')?'*':GLOBAL.getParseDate(new Date(fechaS.getFullYear(), fechaS.getMonth(), 1));
      this.FormSearchLote.strFechaFin = (fechaS==null ||fechaS==undefined||fechaS.toString()=='')?'*':GLOBAL.getParseDate(new Date(fechaS.getFullYear(), fechaS.getMonth() + 1, 0));
    }
    this.lotesAsociadosEdit = '';
    LotesTrujilloService.SearchLotesLJ(this.FormSearchLote)
    .then(response =>{
      this.dataLotes = response.Data;
      this.countLotesLJ = response.Count;
      this.loadingGetLotes = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetLotes = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener lotes.');
    })
  }
  AgregarLotesTrujillo(FormAgregarLote){
    if(FormAgregarLote.strLote == '')
      this.openMessageError('Ingrese N° de Lote AS');
    else{
      FormAgregarLote.strUsuarioCrea = this.userDec.strUsuario;
      FormAgregarLote.fltPesoTmh = Number(FormAgregarLote.fltPesoTmh);
      FormAgregarLote.fltPesoTmhHistorico = Number(FormAgregarLote.fltPesoTmhHistorico);
      FormAgregarLote.dtmFechaRecepcion = GLOBAL.getParseDate(this.fechaAgregar);
      FormAgregarLote.strDniAcopiador = '12345678';
      FormAgregarLote.lstLotes = [];
      for (let i = 0; i < this.multipleSelection.length; i++) {
        FormAgregarLote.lstLotes.push({
          intIdRegistro: this.multipleSelection[i].intIdRegistro,
          strLote: this.multipleSelection[i].strLote
        });
      }
      var PesoNew:number = FormAgregarLote.fltPesoTmh / this.NroParticiones;
      var TMH:number = GLOBAL.truncateNumber(PesoNew, 3);
      var Lote: string = FormAgregarLote.strLote;
      var listaLotes: any = [];
      for (let i = 0; i < this.NroParticiones; i++) {
        var Data: any = FormAgregarLote;
        var _lote = (this.NroParticiones === 1)? Lote : Lote + this.listaLetras[i];
        var LoteAS = {
          strLote: _lote,
          dtmFechaRecepcion: Data.dtmFechaRecepcion,
          strProcedenciaSt: Data.strProcedenciaSt,
          strProcedenciaLg: Data.strProcedenciaLg,
          strClienteRuc: Data.strClienteRuc,
          strClienteRazonS: Data.strClienteRazonS,
          strMaterial: Data.strMaterial,
          intNroSacos: Data.intNroSacos,
          fltPesoTmh: TMH,
          fltPesoTmhHistorico: TMH,
          strDuenioMineral: Data.strDuenioMineral,
          strObservacion: Data.strObservacion,
          strNombreTransportista: Data.strNombreTransportista,
          strNroPlaca: Data.strNroPlaca,
          strGuiaTransport: Data.strGuiaTransport,
          strDniAcopiador: Data.strDniAcopiador,
          strNombreAcopiador: Data.strNombreAcopiador,
          strTurnoRegistro: (Data.strTurnoRegistro==='')?'Dia' : Data.strTurnoRegistro,
          strCodConcesion: Data.strCodConcesion,
          strNombreConcesion: Data.strNombreConcesion,
          strGuiaRemitente: Data.strGuiaRemitente,
          strCodCompromiso: Data.strCodCompromiso,
          strPorcH2O: Data.strPorcH2O,
          strPSecoTM: Data.strPSecoTM,
          strLeyAuOzTc: Data.strLeyAuOzTc,
          strLeyAuREE: Data.strLeyAuREE,
          strLeyAgOzTc: Data.strLeyAgOzTc,
          strLeyAgREE: Data.strLeyAgREE,
          strPorcRCPRef12hrs: Data.strPorcRCPRef12hrs,
          strPorcRCPAu30hrs: Data.strPorcRCPAu30hrs,
          strPorcRCPAu70hrs: Data.strPorcRCPAu70hrs,
          strPorcRCPReeAu72hrs: Data.strPorcRCPReeAu72hrs,
          strPorcRCPAg30hrs: Data.strPorcRCPAg30hrs,
          strNaCNKgTM: Data.strNaCNKgTM,
          strNaOHKgTM: Data.strNaOHKgTM,
          strPorcCobre: Data.strPorcCobre,
          strUsuarioCrea: Data.strUsuarioCrea,
          lstLotes: Data.lstLotes
        }
        listaLotes.push(LoteAS);
      }
      LotesTrujilloService.AgregarAnyLotesAS(listaLotes)
      .then(response =>{
        this.openMessage('Se guardó correctamente.');
        this.BuscarLotesTrujillo();
      }).catch(e =>{
        console.log(e);
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al Guardar lote.');
      })
      this.dialogAgregarLote = false;
    }
  }

  ActualizarLoteTrujillo(rowEdit){
    rowEdit.strUsuarioModif = this.userDec.strUsuario;
    rowEdit.fltPesoTmh = Number(rowEdit.fltPesoTmh);
    rowEdit.fltPesoTmhHistorico = Number(rowEdit.fltPesoTmhHistorico);
    rowEdit.dtmFechaRecepcion = GLOBAL.getParseDate(this.fechaEditar);
    rowEdit.lstLotes = [];
    var lotesAsoc = this.lotesAsociadosEdit.split(';')
    for (let i = 0; i < lotesAsoc.length; i++) {
      if(lotesAsoc[i].trim() != ''){
        rowEdit.lstLotes.push({
          intIdRegistro: -1,
          strLote: lotesAsoc[i]
        });
      }
    }
    LotesTrujilloService.UpdateLotesAS(rowEdit)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al guardar Lote.');
      else this.openMessage('Se actualizó correctamente.');
      this.BuscarLotesTrujillo();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al Guardar lote.');
    })
    this.dialogEditarLote = false;
  }
  TruncateNumber(){
    var test = GLOBAL.truncateNumber(12322589.187, 4);
    console.log(test);
  }
  getProveedores(){
    var Query = {
      strRazonS: '',
      strRuc: ''
    }
    ProveedorService.SearchProveedor(Query)
    .then(response =>{
      this.dataProveedores = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar proveedores');
    })
  }
  handleAgregar(){
    this.LimpiarFormAgregar();
    this.getProveedores();
    this.lotesAsociados = '';
    this.NroParticiones = 1;
    this.checkEditLJ = false;
    this.dialogAgregarLote = true;
  }
  handleEditarLote(index, row){
    this.rowSelectedEdit = row;
    this.getProveedores();
    this.checkEditLJ = true;;
    this.fechaEditar = new Date(row.dtmFechaRecepcion);
    this.dialogEditarLote = true;
    var query = {
      strLote : '',
      strSubLote : row.strLote,
      strProcedencia : 'TRUJILLO',
      strFechaInicio : '*',
      strFechaFin : '*',
    }
    this.lotesAsociadosEdit = '';
    LotesTrujilloService.SearchLotesLJ(query)
    .then(response =>{
      for (let i = 0; i < response.Data.length; i++) {
          this.lotesAsociadosEdit += response.Data[i].strLote + ';';
      }
    }).catch(e =>{
      console.log(e);
      this.lotesAsociadosEdit = '';
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener lotes asociados.');
    })
  }
  handleSelectionChangeEdit(val){
    this.multipleSelection = val;
    this.lotesAsociadosEdit = '';
    for (let i = 0; i < this.multipleSelection.length; i++) {
        this.lotesAsociadosEdit += this.multipleSelection[i].strLote+';';
    }
  }
  handleSelectionChangeAdd(val){
    this.multipleSelection = val;
    var total = 0, nroSacos = 0;
    this.lotesAsociados = '';
    for (let i = 0; i < this.multipleSelection.length; i++) {
        total += this.multipleSelection[i].fltPesoTmh;
        nroSacos += this.multipleSelection[i].intNroSacos;
        this.lotesAsociados += this.multipleSelection[i].strLote+';';
    }
    this.FormAgregarLote.strProcedenciaLg = (this.multipleSelection.length == 0)? '' : this.multipleSelection[0].strProcedenciaLg;
    this.FormAgregarLote.strProcedenciaSt = (this.multipleSelection.length == 0)? '' : this.multipleSelection[0].strProcedenciaSt;
    this.FormAgregarLote.strMaterial = (this.multipleSelection.length == 0)? '' : this.multipleSelection[0].strMaterial;
    // this.FormAgregarLote.intNroSacos = (this.multipleSelection.length == 0)? 0 : nroSacos;
    this.FormAgregarLote.strNombreAcopiador = (this.multipleSelection.length == 0)? '' :this.multipleSelection[0].strNombreAcopiador;
    this.FormAgregarLote.strTurnoRegistro = (this.multipleSelection.length == 0)? '' :this.multipleSelection[0].strTurnoRegistro;
    // this.FormAgregarLote.fltPesoTmh = (this.multipleSelection.length == 0)? 0 :this.getNumberFix(total, 3);
  }
  handleSelectionChange(val) {
    if(this.checkEditLJ === true)
      this.handleSelectionChangeEdit(val);
    else
      this.handleSelectionChangeAdd(val);
  }
  handleBuscarLotesAsociados(){
    this.ObtenerLotes();
    this.dialogBuscarLotesVisible = true;
  }
  handleVerLotes(index, row){
    this.dialogVerLotesAsoc = true;

    this.loadingVerLotes = true;
    var query = {
      strLote : '',
      strSubLote : row.strLote,
      strProcedencia : 'TRUJILLO',
      strFechaInicio : '*',
      strFechaFin : '*',
    }
    LotesTrujilloService.SearchLotesLJ(query)
    .then(response =>{
      this.dataVerLotes = response.Data;
      this.loadingVerLotes = false;
    }).catch(e =>{
      console.log(e);
      this.loadingVerLotes = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al obtener lotes.');
    })
  }
  LimpiarFormAgregar(){
    this.FormAgregarLote.strLote ='';
    this.FormAgregarLote.dtmFechaRecepcion = '';
    this.FormAgregarLote.strProcedenciaSt = '';
    this.FormAgregarLote.strProcedenciaLg = '';
    this.FormAgregarLote.strClienteRuc = '';
    this.FormAgregarLote.strClienteRazonS = '';
    this.FormAgregarLote.strMaterial = 'MINERAL';
    this.FormAgregarLote.intNroSacos = '';
    this.FormAgregarLote.fltPesoTmh = '';
    this.FormAgregarLote.fltPesoTmhHistorico = '';
    this.FormAgregarLote.strDuenioMineral = '';
    this.FormAgregarLote.strObservacion = '';
    this.FormAgregarLote.strNombreTransportista = '';
    this.FormAgregarLote.strNroPlaca = '';
    this.FormAgregarLote.strGuiaTransport = '';
    this.FormAgregarLote.strDniAcopiador = '';
    this.FormAgregarLote.strNombreAcopiador = '';
    this.FormAgregarLote.strTurnoRegistro = '';
    this.FormAgregarLote.strCodConcesion = '';
    this.FormAgregarLote.strNombreConcesion = '';
    this.FormAgregarLote.strGuiaRemitente = '';
    this.FormAgregarLote.strCodCompromiso = '';
    this.FormAgregarLote.strPorcH2O = '';
    this.FormAgregarLote.strPSecoTM = '';
    this.FormAgregarLote.strLeyAuOzTc = '';
    this.FormAgregarLote.strLeyAuREE = '';
    this.FormAgregarLote.strLeyAgOzTc = '';
    this.FormAgregarLote.strLeyAgREE = '';
    this.FormAgregarLote.strPorcRCPRef12hrs = '';
    this.FormAgregarLote.strPorcRCPAu30hrs = '';
    this.FormAgregarLote.strPorcRCPAu70hrs = '';
    this.FormAgregarLote.strPorcRCPReeAu72hrs = '';
    this.FormAgregarLote.strPorcRCPAg30hrs = '';
    this.FormAgregarLote.strNaCNKgTM = '';
    this.FormAgregarLote.strNaOHKgTM = '';
    this.FormAgregarLote.strPorcCobre = '';
    this.FormAgregarLote.strUsuarioCrea = '';
    this.FormAgregarLote.lstLotes = [];
  }

  //BUSQUEDA LEYES REFERENCIALES
  handleLeyesRef(){
    this.CodigoLeyRef = '';
    this.dataLeyesRef = [];
    this.dialogBuscarLeyesRefVisible = true;
  }
  BuscarLeyesReferenciales(){
    if(this.CodigoLeyRef.trim() === '') this.openMessageError('Ingrese código');
    else {
      this.loadingGetLeyesRef = true;
      var SearchData = {
        strCorrelativo: '',
        strDescripcion: this.CodigoLeyRef,
        strTipoMineral: '',
        strZona: 'TRUJILLO',
        strFechaInicio: '*',
        strFechaFin: '*',
      }
      LeyRefService.SearchLeyesReferenciales(SearchData)
      .then(response =>{
        this.dataLeyesRef = response.Data;
        this.loadingGetLeyesRef = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetLeyesRef = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar Leyes referenciales.');
      })
    }
  }
  SeleccionarLeyRef(index, row){
    var codigo = row.strDescripcion;
    var split= codigo.split('-');
    this.FormAgregarLote.strLote = (split.length === 1) ? 'AS-'+row.strDescripcion : row.strDescripcion;
    this.FormAgregarLote.strLeyAuOzTc = (row.strLeyAuOzTc===null)? '' : row.strLeyAuOzTc;
    this.FormAgregarLote.strLeyAgOzTc = (row.strLeyAgOzTc===null)? '' : row.strLeyAgOzTc;
    this.dialogBuscarLeyesRefVisible = false;
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch: {
        strFechaInicio: '',
      	strFechaFin: '',
      	strNroAS: '',
      	strLote: '',
      },
      FormSearchLote:{
        strLote : '',
        strSubLote : '',
        strProcedencia : '',
        strFechaInicio : '',
        strFechaFin : '',
      },
      FormAgregarLote:{
        strLote:'',
        dtmFechaRecepcion: '',
        strProcedenciaSt: '',
        strProcedenciaLg: '',
        strClienteRuc: '',
        strClienteRazonS: '',
        strMaterial: 'MINERAL',
        intNroSacos: '',
        fltPesoTmh: '',
        fltPesoTmhHistorico: '',
        strDuenioMineral: '',
        strObservacion: '',
        strNombreTransportista: '',
        strNroPlaca: '',
        strGuiaTransport: '',
        strDniAcopiador: '',
        strNombreAcopiador: '',
        strTurnoRegistro: '',
        strCodConcesion: '',
        strNombreConcesion: '',
        strGuiaRemitente: '',
        strCodCompromiso: '',
        strPorcH2O: '',
        strPSecoTM: '',
        strLeyAuOzTc: '',
        strLeyAuREE: '',
        strLeyAgOzTc: '',
        strLeyAgREE: '',
        strPorcRCPRef12hrs: '',
        strPorcRCPAu30hrs: '',
        strPorcRCPAu70hrs: '',
        strPorcRCPReeAu72hrs: '',
        strPorcRCPAg30hrs: '',
        strNaCNKgTM: '',
        strNaOHKgTM: '',
        strPorcCobre: '',
        strUsuarioCrea: '',
        lstLotes: []
      },
      dataMaterial:[
        {id_material: 1, name: 'MINERAL'},
        {id_material: 2, name: 'LLAMPO'},
      ],
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
