import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import TopMenu from '../customs/top-menu/TopMenu.vue';
import router from '../../router';
import GLOBAL from '@/Global';
// import QuickAccessMenuComponent from '@/components/quickaccessmenu/quickaccessmenu.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';

@Component({
   name: 'barmenu',
   components:{
     'top-menu':TopMenu,     
    //  'quickaccessmenu':QuickAccessMenuComponent,
   }
})
export default class BarmenuComponent extends Vue {
  isActive:boolean;
  isCollapse:boolean;
  dimensionContent = 21;
  accesosBarMenu:any = [];
  accesosGeneral: any = [];
  accesosConsultas: any = [];
  accesosIngDatos: any = [];
  accesosIDSubAcopio: any = [];
  accesosConfig: any = [];
  accesosReportes: any = [];
  accesosRepAcopio: any = [];
  accesosRepMargen: any = [];
  accesosPlanta: any = [];
  accesosRecepcionPL: any = [];
  accesosLixiviacionPL: any = [];
  accesosProcesoCierre: any = [];
  accesosRepPlanta: any = [];

  verDatosGenerales: boolean = false;
  verConsultas: boolean = false;
  verIngDatos: boolean = false;
  verConfig: boolean = false;
  verReportes: boolean = false;
  verIDAcopio: boolean = false;
  verRepAcopio: boolean = false;
  verRepMargen: boolean = false;
  verRepPlanta: boolean = false;
  verPlanta: boolean = false;
  verRecepcionPL: boolean = false;
  verLixiviacionPL: boolean = false;
  verProcesoCierre: boolean = false;


  constructor(){
    super()
    this.isActive=true;
    this.isCollapse=false;
  }
  handleOpen (key, keyPath) {
    // console.log(key, keyPath)
  }

  handleClose (key, keyPath) {
    // console.log(key, keyPath)
  }
  clickHamburger () {
    this.isActive = !this.isActive
    this.isCollapse = !this.isCollapse
  }
  linkRoute(route){
    router.push(route)
  }
  linkRoute1(){
    console.log("algo");
    
    alert('clic me!1');
  }
  clickCollapse(value){
    this.isActive = !this.isActive
    this.isCollapse = !this.isCollapse
    if(this.isActive==true){
      //this.$emit('redimensionar',18);
      this.dimensionContent = 21;
    }
    else{
      //this.$emit('redimensionar',21);
      this.dimensionContent = 23;
    }
  }
  getAccesos(){
    var decoded:any= window.sessionStorage.getItem('session_access');
    if( decoded === null ){
      this.openMessageError('No tiene acceso');
    }
    else{
      var dataAccesos:any = JSON.parse(GLOBAL.decodeString(decoded));
      for (var i=0; i< dataAccesos.length; i++){
        //Accesos INICIO
        if(dataAccesos[i].intNivel === 0){
          this.accesosBarMenu.push({
            strNombre: dataAccesos[i].strNombre,
            strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
            strIconName: dataAccesos[i].strIconName,
            strEnlace: dataAccesos[i].strEnlace
          });
        }

        //Accesos DATOS GENERALES
        else if(dataAccesos[i].intNivel === 1){
          this.accesosGeneral.push({
            strNombre: dataAccesos[i].strNombre,
            strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
            strIconName: dataAccesos[i].strIconName,
            strEnlace: dataAccesos[i].strEnlace
          });
        }

        //Accesos CONSULTAS
        else if(dataAccesos[i].intNivel === 2){
          this.accesosConsultas.push({
            strNombre: dataAccesos[i].strNombre,
            strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
            strIconName: dataAccesos[i].strIconName,
            strEnlace: dataAccesos[i].strEnlace
          })
        }

        //Accesos INGRESO DATOS
        else if(dataAccesos[i].intNivel === 3){
          if(dataAccesos[i].intSubNivel === 0){
            this.accesosIngDatos.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          else if(dataAccesos[i].intSubNivel === 1){
            this.accesosIDSubAcopio.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }

        }

        //Accesos CONFIGURACION
        else if(dataAccesos[i].intNivel === 4){
          this.accesosConfig.push({
            strNombre: dataAccesos[i].strNombre,
            strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
            strIconName: dataAccesos[i].strIconName,
            strEnlace: dataAccesos[i].strEnlace
          })
        }

        //Accesos PLANTA
        else if(dataAccesos[i].intNivel === 5){
          if(dataAccesos[i].intSubNivel === 0){
            this.accesosPlanta.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          else if(dataAccesos[i].intSubNivel === 1){
            this.accesosRecepcionPL.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          else if(dataAccesos[i].intSubNivel === 2){
            this.accesosLixiviacionPL.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
        }

        //Accesos PROCESO CIERRE
        else if(dataAccesos[i].intNivel === 6){
          if(dataAccesos[i].intSubNivel === 0){
            this.accesosProcesoCierre.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
        }

        //Accesos REPORTES
        else if(dataAccesos[i].intNivel === 10){
          if(dataAccesos[i].intSubNivel === 0){
            this.accesosReportes.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          if(dataAccesos[i].intSubNivel === 1){
            this.accesosRepAcopio.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          if(dataAccesos[i].intSubNivel === 2){
            this.accesosRepMargen.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
          if(dataAccesos[i].intSubNivel === 3){
            this.accesosRepPlanta.push({
              strNombre: dataAccesos[i].strNombre,
              strIndex: dataAccesos[i].intNivel +"-"+ dataAccesos[i].intSubNivel +"-"+ dataAccesos[i].strIndex,
              strIconName: dataAccesos[i].strIconName,
              strEnlace: dataAccesos[i].strEnlace
            })
          }
        }
      }
      this.verDatosGenerales = (this.accesosGeneral.length === 0) ? false : true;
      this.verConsultas = (this.accesosConsultas.length === 0) ? false : true;

      this.verConfig = (this.accesosConfig.length === 0) ? false : true;
      this.verReportes = (this.accesosReportes.length === 0) ? false : true;
      this.verIDAcopio = (this.accesosIDSubAcopio.length === 0) ? false : true;
      this.verRepAcopio = (this.accesosRepAcopio.length === 0) ? false : true;
      this.verRepMargen = (this.accesosRepMargen.length === 0) ? false : true;
      this.verRepPlanta = (this.accesosRepPlanta.length === 0) ? false : true;
      this.verIngDatos = (this.accesosIngDatos.length > 0 || this.verIDAcopio === true) ? true : false;
      this.verPlanta = (this.accesosPlanta.length === 0) ? false : true;
      this.verRecepcionPL = (this.accesosRecepcionPL.length === 0) ? false : true;
      this.verLixiviacionPL = (this.accesosLixiviacionPL.length === 0) ? false : true;
      this.verProcesoCierre = (this.accesosProcesoCierre.length === 0) ? false : true;

      // this.verIDAcopio = (this.verIDAcopio === true) ? true: false;
      this.verReportes = (this.verRepAcopio === true || this.verRepMargen === true || this.verRepPlanta === true) ? true: false;
      this.verPlanta = (this.verRecepcionPL === true || this.verLixiviacionPL === true) ? true : false;
    }
  }
  openMessage(newMsg : string) {
    this.$message({
      showClose: true,
      message: newMsg,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }

  created() {
    if(typeof window != 'undefined') {
      this.getAccesos();
    }
  }
}
