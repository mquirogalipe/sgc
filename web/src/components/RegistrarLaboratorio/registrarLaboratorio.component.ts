import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LaboratorioService from '@/services/laboratorio.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'registrarLaboratorio',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class RegistrarLaboratorioComponent extends Vue {
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  FormAgregar : any = {};
  rowSelectedEdit: any = {};
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 15;
  totalRegistros:number = this.RegistersForPage;
  sizeScreen:string = (window.innerHeight - 420).toString();
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  loadingGetData:boolean = false;
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  userDec: any = {};
  FormSearch: any = {};
  seleccionarRows:any=null;
  constructor (){
    super()
  }

  checkAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1033){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.buscarLaboratorios();
      }
    }
  }
  buscarLaboratorios(){
    this.loadingGetData = true;
    LaboratorioService.SearchLaboratorios(this.FormSearch)
    .then(response => {
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    })
    .catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar laboratorios');
    })
  }
  AgregarLaboratorio(FormAgregar){
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    LaboratorioService.AgregarLaboratorio(FormAgregar)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al guardar Laboratorio: "+response);
      else if(response === -2) this.openMessage("Ya existe un laboratorio con el mismo nombre.");
      else this.openMessage('Se guardó correctamente.');
      this.buscarLaboratorios();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar Laboratorio.');
    })
    this.dialogAgregarVisible = false;
  }
  EditarLaboratorio(rowEdit){
    rowEdit.strUsuarioModif = this.userDec.strUsuario;
    LaboratorioService.EditarLaboratorio(rowEdit)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al modificar Laboratorio: "+response);
      else this.openMessage('Se guardó correctamente.');
      this.buscarLaboratorios();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al modificar Laboraotorio.');
    })
    this.dialogEditarVisible = false;
  }

  handleAgregar(){
    this.FormAgregar.strNombre = '';
    this.FormAgregar.strDescripcion = '';
    this.dialogAgregarVisible = true;
  }
  handleEdit2(){
    this.handleEdit(this.seleccionarRows);
  }
  handleEdit( row){
    this.rowSelectedEdit = row;
    this.dialogEditarVisible = true;
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  seleccionarRow(row,index){
    this.seleccionarRows=row;
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch: {
        strNombre : '',
        strDescripcion: ''
      },
      FormAgregar: {
        strNombre: '',
        strDescripcion : '',
        strUsuarioCrea: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccess();
    }
  }

}
