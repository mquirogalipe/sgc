import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import VentasService from '@/services/ventas.service';
import { Loading } from 'element-ui';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'ventas',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class VentasComponent extends Vue {
  userDec: any ={};
  fechaSearch: Date = new Date();
  fechaInicio: any = '';
  fechaFin: any = '';
  gridData: any = [];
  gridDataAdd: any =[];
  FormAgregar: any = {};
  FormSearch: any = {};
  FormAddCamp: any = {};
  rowEditCamp: any = {};
  loadingGetData:boolean = false;
  sizeScreen:string = (window.innerHeight - 150).toString();
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  DialogAgregarVisible: boolean = false;
  DialogAgregarCampVisible: boolean = false;
  DialogEditarCampVisible: boolean = false;
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  fechaMonthAdd: Date = new Date();
  fechaAdd: Date = new Date();
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;

  //EDITAR
  fechaMonthEdit: any = '';
  fechaEdit: any = '';
  DialogEditarVisible: boolean = false;
  rowSelectedEdit: any = {};
  gridDataEdit: any = [];
  DialogAgregarCampEditVisible: boolean = false;
  DialogEditarCampEditVisible: boolean = false;
  FormAddCampEdit: any = {};
  rowEditCampEdit: any = {};
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1012){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else {
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
      }
    }
  }
  BuscarVentas(){
    var fSearch = this.fechaSearch;
    if(this.fechaInicio==='' || this.fechaInicio===undefined || this.fechaFin==='' || this.fechaFin===undefined)
      this.openMessageError('Seleccione Fecha')
    else {
      this.gridData = [];
      this.loadingGetData = true;
      this.FormSearch.strFechaInicio= (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearch.strFechaFin= (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
      VentasService.GetVentas(this.FormSearch)
      .then(response =>{
        this.gridData = response.Data;
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar ventas.');
      })
    }
  }
  AgregarVenta(FormAgregar){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando venta...'
    });
    FormAgregar.dtmFecha = GLOBAL.getParseDate(this.fechaAdd);
    FormAgregar.fltDolares = Number(FormAgregar.fltDolares);
    FormAgregar.fltKgProducidos = Number(FormAgregar.fltKgProducidos);
    FormAgregar.fltTotal = Number(FormAgregar.fltTotal);
    FormAgregar.fltValorUS = Number(FormAgregar.fltValorUS);
    FormAgregar.lstDetVentas = this.gridDataAdd;
    FormAgregar.strAnio = this.fechaMonthAdd.getFullYear().toString();
    FormAgregar.strMes = this.monthNames[this.fechaMonthAdd.getMonth()];
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    VentasService.PutVenta(FormAgregar)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al agregar venta:"+response);
      else if(response === -2) this.openMessageError("Ya existe una venta con el mismo numero de operación.");
      else this.openMessage('Venta guardada correctamente.');
      loadingInstance.close();
      this.BuscarVentas();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar venta.');
    })
    this.DialogAgregarVisible = false;
  }
  AgregarCampania(FormAddCamp){
    var newItem: any = {
      strPro: FormAddCamp.strPro,
      strPeriodo: FormAddCamp.strPeriodo,
      strOperacion: FormAddCamp.strOperacion,
      fltPeso: FormAddCamp.fltPeso,
      strLeyOro: FormAddCamp.strLeyOro,
      strGrOro: FormAddCamp.strGrOro,
      fltKlgOro: FormAddCamp.fltKlgOro,
    };
    this.gridDataAdd.push(newItem);
    this.DialogAgregarCampVisible = false;
  }
  AgregarCampaniaEdit(FormAddCampEdit){
    var newItem: any = {
      strPro: FormAddCampEdit.strPro,
      strPeriodo: FormAddCampEdit.strPeriodo,
      strOperacion: FormAddCampEdit.strOperacion,
      fltPeso: FormAddCampEdit.fltPeso,
      strLeyOro: FormAddCampEdit.strLeyOro,
      strGrOro: FormAddCampEdit.strGrOro,
      fltKlgOro: FormAddCampEdit.fltKlgOro,
    };
    this.gridDataEdit.push(newItem);
    this.DialogAgregarCampEditVisible = false;
  }

  handleAgregar(){
    this.FormAgregar.strMes = '';
    this.FormAgregar.strAnio = '';
    this.FormAgregar.strOperacion = '';
    this.FormAgregar.dtmFecha = '';
    this.FormAgregar.strNroDoc = '';
    this.FormAgregar.strImpFac = '';
    this.FormAgregar.strNroDocND = '';
    this.FormAgregar.strImpND = '';
    this.FormAgregar.fltTotal = '';
    this.FormAgregar.strKgVenta = '';
    this.FormAgregar.strPenVenta = '';
    this.FormAgregar.fltDolares = '';
    this.FormAgregar.fltKgProducidos = '';
    this.FormAgregar.fltValorUS = '';
    this.FormAgregar.strUsuarioCrea = '';
    this.FormAgregar.lstDetVentas = [];
    this.gridDataAdd = [];
    this.DialogAgregarVisible = true;
  }
  handleEditVenta(index, row){
    this.rowSelectedEdit = row;
    this.gridDataEdit = row.lstDetVentas;
    this.fechaEdit = new Date(row.dtmFecha);
    this.DialogEditarVisible = true;
  }
  handleAddCamp(){
    this.FormAddCamp.strPro= '',
    this.FormAddCamp.strPeriodo= '',
    this.FormAddCamp.strOperacion= '',
    this.FormAddCamp.fltPeso= '',
    this.FormAddCamp.strLeyOro= '',
    this.FormAddCamp.strGrOro= '',
    this.FormAddCamp.fltKlgOro= '',
    this.DialogAgregarCampVisible = true;
  }
  handleAddCampEdit(){
    this.FormAddCampEdit.strPro= '',
    this.FormAddCampEdit.strPeriodo= '',
    this.FormAddCampEdit.strOperacion= '',
    this.FormAddCampEdit.fltPeso= '',
    this.FormAddCampEdit.strLeyOro= '',
    this.FormAddCampEdit.strGrOro= '',
    this.FormAddCampEdit.fltKlgOro= '',
    this.DialogAgregarCampEditVisible = true;
  }
  handleEditCamp(index, row){
    this.rowEditCamp = row;
    this.DialogEditarCampVisible = true;
  }
  handleEditCampEdit(index, row){
    this.rowEditCampEdit = row;
    this.DialogEditarCampEditVisible = true;
  }
  handleDeleteCamp(index, row) {
    this.$confirm('Desea Eliminar esta campaña ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.gridDataAdd = this.gridDataAdd.filter(item => item !== row);
    }).catch(e => {
    });
  }
  handleDeleteCampEdit(index, row) {
    this.$confirm('Desea Eliminar esta campaña ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.gridDataEdit = this.gridDataEdit.filter(item => item !== row);
    }).catch(e => {
    });
  }

  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }
  changeTotal(){
    this.FormAgregar.fltTotal = Number(this.FormAgregar.strImpFac) + Number(this.FormAgregar.strImpND)
  }
  changeGrOro(){
    this.FormAddCamp.strGrOro = Number(this.FormAddCamp.fltPeso)*Number(this.FormAddCamp.strLeyOro);
    this.FormAddCamp.fltKlgOro = this.FormAddCamp.strGrOro/1000;
  }
  changeGrOroEdit(){
    this.rowEditCamp.strGrOro = Number(this.rowEditCamp.fltPeso)*Number(this.rowEditCamp.strLeyOro);
    this.rowEditCamp.fltKlgOro = this.rowEditCamp.strGrOro/1000;
  }
  changeGrOroAddEdit(){
    this.FormAddCampEdit.strGrOro = Number(this.FormAddCampEdit.fltPeso)*Number(this.FormAddCampEdit.strLeyOro);
    this.FormAddCampEdit.fltKlgOro = this.FormAddCampEdit.strGrOro/1000;
  }
  changeGrOroEditEnEdit(){
    this.rowEditCampEdit.strGrOro = Number(this.rowEditCampEdit.fltPeso)*Number(this.rowEditCampEdit.strLeyOro);
    this.rowEditCampEdit.fltKlgOro = this.rowEditCampEdit.strGrOro/1000;
  }

  ExpandChange(){
    // console.log('Se expandio');
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha == undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFloat(number, fix){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      FormSearch:{
        strFechaInicio:'',
        strFechaFin:''
      },
      FormAgregar:{
        strPro : '',
        strPeriodo : '',
        strMes: '',
        strAnio: '',
        strOperacion: '',
        dtmFecha: '',
        strNroDoc: '',
        strImpFac: '',
        strNroDocND: '',
        strImpND: '',
        fltTotal: '',
        strKgVenta: '',
        strPenVenta: '',
        fltDolares: '',
        fltKgProducidos: '',
        fltValorUS: '',
        strUsuarioCrea: '',
        lstDetVentas: [],
      },
      FormAddCamp:{
        strPro: '',
        strPeriodo: '',
        strOperacion: '',
        fltPeso: '',
        strLeyOro: '',
        strGrOro: '',
        fltKlgOro: '',
      },
      FormAddCampEdit:{
        strPro: '',
        strPeriodo: '',
        strOperacion: '',
        fltPeso: '',
        strLeyOro: '',
        strGrOro: '',
        fltKlgOro: '',
      },
      json_fields : {
        "strZona" : "Zona",
        "strAcopiador" : "Acopiador",
        "strProveedor" : "Proveedor",
        "fltSumPesoTmh" : "Peso TMH Acumulado",
        "fltFinalLeyAuOzTc" : "Prom. Ley Au Oz/tc",
        "fltFinalLeyAgOzTc" : "Prom. Ley Ag Oz/Tc",
        "fltPromFinoAuGr" : "Prom. Fino(Au/Gr)",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
