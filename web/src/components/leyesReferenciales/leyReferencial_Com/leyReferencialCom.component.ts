import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LeyRefService from '@/services/leyReferencial.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'leyReferencialCom',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class LeyReferencialComComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina: number = 1;
  RegistersForPage: number = 25;
  totalRegistros: number = this.RegistersForPage;
  FormSearch: any = {};
  rowSelectedEdit: any = {};
  fechaEditar: any = '';
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  fechaSearch: Date = new Date();
  dialogEditarVisible: boolean = false;
  dialogDetalleVisible: boolean = false;
  nameButton: string = '';
  loadingGetData:boolean = false;
  titleExcel:string ='Leyes Referenciales_'+CONFIG.DATE_STRING+'.xls';
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';;//'0';
  constructor (){
    super()
  }
  checkAccesos(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1009){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.nameButton = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? 'Actualizar' : 'Detalle';
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.SearchLeyesReferenciales();
      }
    }
  }
  SearchLeyesReferenciales(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));

    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;
    LeyRefService.SearchLeyesReferenciales(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.changeDateString(this.CompleteData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Leyes referenciales.');
    })
  }
  ActualizarLeyReferencial(rowEdit){
    var FormUpdate = {
      intIdLeyRef: rowEdit.intIdLeyRef,
      strLeyAuCom: (rowEdit.strLeyAuCom === null) ? '': rowEdit.strLeyAuCom,
      strLeyAgCom: (rowEdit.strLeyAgCom === null) ? '': rowEdit.strLeyAgCom,
      strUsuarioModif: this.userDec.strUsuario
    }
    LeyRefService.UpdateLeyReferencialCom(FormUpdate)
    .then(response =>{
      if(response === -1) this.openMessageError('SERVICE: Error al actualizar ley referencial.');
      else this.openMessage('Se actualizó correctamente.');
      this.SearchLeyesReferenciales();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar Ley referencial.');
    })
    this.dialogEditarVisible = false;
  }
  selectRow(row,index){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.fechaEditar = row.dtmFechaRecepcion;
  }

  handleEdit(index, row){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.fechaEditar = row.dtmFechaRecepcion;
    if(this.userDec.strCargo === 'Administrador' || this.userDec.strCargo === 'Area Comercial') this.dialogEditarVisible = true;
    else this.dialogDetalleVisible = true;
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      // if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string) {
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch: {
        strCorrelativo: '',
        strDescripcion: '',
        strTipoMineral: '',
        strZona: '',
        strFechaInicio: '*',
        strFechaFin: '*',
      },
      json_fields : {
        "strCorrelativo" : "Item",
        "dtmFechaRecepcion" : "Fecha Recepcion",
        "strDescripcion" : "Código",
        "strTipoMineral" : "Tipo Mineral",
        "strZona" : "Proveedor",
        "strAcopiador": "Responsable",
        "strPorcRcpLab" : "% RCP LAB",
        "strLeyAuOzTc" : "Ley Au Oz/Tc",
        "strLeyAuReeOzTc" : "Ley Au REE Oz/Tc",
        "strLeyAuCom" : "Ley Au Comercial",
        "strLeyAgOzTc" : "Ley Ag Oz/Tc",
        "strLeyAgCom" : "Ley Ag Comercial",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccesos();
    }
  }
}
