import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LeyRefService from '@/services/leyReferencial.service';
import LiquidacionService from '@/services/liquidacion.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import * as CONFIG from '@/Config';
@Component({
   name: 'leyReferencialBal',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
   }
})
export default class LeyReferencialBalComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number =1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  FormSearch: any = {};
  FormAgregar: any = {};
  fechaAgregar: Date = new Date();
  rowSelectedEdit: any = {};
  fechaEditar: any = '';
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  fechaSearch: Date = new Date();
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  loadingGetData:boolean = false;
  titleExcel:string ='Leyes Referenciales_'+CONFIG.DATE_STRING+'.xls';
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  sizeScreen:string = (window.innerHeight - 230).toString();//'0';
  constructor (){
    super()
  }
  checkAccesos(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1007){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.SearchLeyesReferenciales();
      }
    }
  }

  SearchLeyesReferenciales(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));

    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;
    LeyRefService.SearchLeyesReferenciales(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar Leyes referenciales.');
    })
  }
  AgregarLeyReferencial(FormAgregar){
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    FormAgregar.dtmFechaRecepcion = GLOBAL.getParseDate(this.fechaAgregar);
    LeyRefService.AgregarLeyReferencial(FormAgregar)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al agregar Ley Referencial:"+response);
      else if(response === -2) this.openMessageError("La ley insertada ya existe.");
      else this.openMessage('Se guardo correctamente.');
      this.SearchLeyesReferenciales();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Ley referencial.');
    })
    this.dialogAgregarVisible = false;
  }
  ActualizarLeyReferencial(rowSelectedEdit){
    rowSelectedEdit.strUsuarioModif = this.userDec.strUsuario;
    rowSelectedEdit.dtmFechaRecepcion = GLOBAL.getParseDate(this.fechaEditar);
    LeyRefService.UpdateLeyReferencialBal(rowSelectedEdit)
    .then(response =>{
      if(response === -1) this.openMessageError('SERVICE: Error al actualizar ley referencial');
      else this.openMessage('Se actualizó correctamente.');
      this.SearchLeyesReferenciales();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar Ley referencial.');
    })
    this.dialogEditarVisible = false;
  }

  handleAgregar(){
    this.limpiarAgregar()
    LiquidacionService.searchMaestro('CorrLeyReferencial')
    .then(response =>{
      this.FormAgregar.strCorrelativo = Number(response.strValor)+1;
      this.dialogAgregarVisible = true;
    }).catch(e =>{
      console.log(e);
      this.dialogAgregarVisible = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar correlativo');
    })
  }

  handleEdit(index, row){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.fechaEditar = row.dtmFechaRecepcion;
    this.dialogEditarVisible = true;
  }
  limpiarAgregar(){
    this.FormAgregar.dtmFechaRecepcion = '';
    this.FormAgregar.strDescripcion = '';
    this.FormAgregar.strTipoMineral = 'MRL';
    this.FormAgregar.strZona = '';
    this.FormAgregar.strAcopiador = '';
    this.FormAgregar.strUsuarioCrea = '';
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string) {
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      FormSearch: {
        strCorrelativo: '',
        strDescripcion: '',
        strTipoMineral: '',
        strZona: '',
        strFechaInicio: '*',
        strFechaFin: '*',
      },
      FormAgregar: {
        strCorrelativo: '',
        dtmFechaRecepcion: '',
        strDescripcion: '',
        strTipoMineral: 'MRL',
        strZona: '',
        strAcopiador: '',
        strUsuarioCrea: '',
      },
      TipoMineral:[
        {id:'MRL', name:'MRL'},
        {id:'RLV', name:'RLV'}
      ],
      json_fields : {
        "strCorrelativo" : "Item",
        "dtmFechaRecepcion" : "Fecha Recepcion",
        "strDescripcion" : "Descripción",
        "strTipoMineral" : "Tipo Mineral",
        "strZona" : "Zona",
        "strPruebaMetalurgica" : "Prueba Metalurgica",
        "strLeyAuCom" : "Ley Au Comercial",
        "strLeyAgCom" : "Ley Ag Comercial",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccesos();
    }
  }
}
