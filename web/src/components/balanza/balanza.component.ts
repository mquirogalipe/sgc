import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import moment from 'moment'
import LotesService from '@/services/lotes.service';
import ZonaService from '@/services/zona.service';
import { Loading } from 'element-ui';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
import * as CONFIG from '@/Config';
import EventBus from '@/bus';
import Global from '@/Global';

@Component({
   name: 'balanza',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class BalanzaComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina: number =1;
  RegistersForPage: number = 25;
  totalRegistros: number = this.RegistersForPage;
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  dialogEditarBalVisible: boolean = false;
  dialogDetalleVisible: boolean = false;
  dialogEditarLiqVisible: boolean = false;
  //DATA AGREGAR
  FormAgregar: any ={};
  fechaRecepcion: Date = new Date();
  horaIngreso: Date = new Date();
  horaSalida: Date = new Date();
  fechaMuestreo: any = '';
  fechaRetiro: any = '';

  FormSearch:any = {}
  fechaSearch: Date = new Date();
  fechaInicio: any = '';
  fechaFin: any = '';
  dataZonas: any = [];
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  NivelAcceso: string = '';

  //DATA EDITAR
  rowSelectedEdit: any = {};
  editViewButton: boolean = true;
  fechaRecepcionEdit: Date = new Date();
  horaIngresoEdit: any = '';
  horaSalidaEdit: any = '';
  fechaMuestreoEdit: any = '';
  fechaRetiroEdit: any = '';
  nameButton: string = '';
  titleExcel:string ='Balanza_'+CONFIG.DATE_STRING+'.xls';
  viewAccessEscritura: boolean = false;
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  loadingGetData:boolean = false;

  blnperiodo:boolean=false;
  blnfiltroavanzado:boolean=false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 2){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página');
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.nameButton = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? 'Actualizar' : 'Detalle';
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        this.BuscarLotes();
        this.getZonas();
      }
    }
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  AgregarLote(FormAgregar){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando lote...'
    });
    var data:any = FormAgregar;
    data.fltPesoTmh = (FormAgregar.fltPesoTmh === '') ? '0.0': FormAgregar.fltPesoTmh;
    data.dtmFechaRecepcion = GLOBAL.getParseDate(this.fechaRecepcion);
    data.strFechaMuestreo = (this.fechaMuestreo === '') ? "": GLOBAL.getParseDate(this.fechaMuestreo);
    data.strFechaRetiro = (this.fechaRetiro === '') ? "": GLOBAL.getParseDate(this.fechaRetiro);
    data.strHoraIngreso = this.getTimeString(this.horaIngreso);
    data.strHoraSalida = this.getTimeString(this.horaSalida);
    data.strUsuarioCrea = this.userDec.strUsuario;

    LotesService.AgregarLote(data)
    .then(response =>{
      if(response === -1) this.openMessageError("SERVICE: Error al agregar lote:"+response);
      else if(response === -2) this.openMessageError("Ya existe un registro con el mismo numero de lote.");
      else this.openMessage('Lote guardado correctamente.');
      loadingInstance.close();
      this.BuscarLotes();
    }).catch(e =>{
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar lote.');
    })
    this.dialogAgregarVisible = false;
  }
  BuscarLotes(){
    this.loadingGetData = true;
    // var fSearch = this.fechaSearch;
    // var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    // var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
    this.FormSearch.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);

    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      this.loadingGetData = true;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }

  EditarLote(rowSelectedEdit){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Guardando lote...'
    });
    var FormUpdate: any = {
      intIdRegistro: rowSelectedEdit.intIdRegistro,
      strLote: rowSelectedEdit.strLote,
      strSubLote: (rowSelectedEdit.strSubLote===null)? "" : rowSelectedEdit.strSubLote.trim(),
      dtmFechaRecepcion: GLOBAL.getParseDate(this.fechaRecepcionEdit),
      strProcedenciaLg: (rowSelectedEdit.strProcedenciaLg===null)? "" : rowSelectedEdit.strProcedenciaLg.trim(),
      strProcedenciaSt: (rowSelectedEdit.strProcedenciaSt===null)? "" : rowSelectedEdit.strProcedenciaSt.trim(),
      strRegion: (rowSelectedEdit.strRegion===null)? "" : rowSelectedEdit.strRegion.trim(),
      strClienteRuc: (rowSelectedEdit.strClienteRuc===null)? "" : rowSelectedEdit.strClienteRuc.trim(),
      strClienteRazonS: (rowSelectedEdit.strClienteRazonS===null)? "" : rowSelectedEdit.strClienteRazonS.trim(),
      strCodConcesion: (rowSelectedEdit.strCodConcesion===null)? "" : rowSelectedEdit.strCodConcesion.trim(),
      strNombreConcesion: (rowSelectedEdit.strNombreConcesion===null)? "" : rowSelectedEdit.strNombreConcesion.trim(),
      strGuiaRemitente: (rowSelectedEdit.strGuiaRemitente===null)? "" : rowSelectedEdit.strGuiaRemitente.trim(),
      strCodCompromiso: (rowSelectedEdit.strCodCompromiso===null)? "" : rowSelectedEdit.strCodCompromiso.trim(),
      strMaterial: (rowSelectedEdit.strMaterial===null)? "" : rowSelectedEdit.strMaterial.trim(),
      intNroSacos: (rowSelectedEdit.intNroSacos === null)? 0 : rowSelectedEdit.intNroSacos,
      fltPesoTmh: (rowSelectedEdit.fltPesoTmh==='' || rowSelectedEdit.fltPesoTmh===null)?'0.0' : rowSelectedEdit.fltPesoTmh,
      strHoraIngreso: (this.horaIngresoEdit === undefined || this.horaIngresoEdit === '') ? '': this.getTimeString(this.horaIngresoEdit),
      strHoraSalida: (this.horaSalidaEdit === undefined || this.horaSalidaEdit === '') ? '': this.getTimeString(this.horaSalidaEdit),
      strTiempoDescarga: (rowSelectedEdit.strTiempoDescarga === null)? "" : rowSelectedEdit.strTiempoDescarga,
      strDuenioMineral: (rowSelectedEdit.strDuenioMineral === null)? "" : rowSelectedEdit.strDuenioMineral,
      strFechaMuestreo: (this.fechaMuestreoEdit === '' || this.fechaMuestreoEdit === null) ? "": GLOBAL.getParseDate(this.fechaMuestreoEdit),
      strTurnoMuestreo: (rowSelectedEdit.strTurnoMuestreo === null)?"" : rowSelectedEdit.strTurnoMuestreo,
      strFechaRetiro: (this.fechaRetiroEdit === '') ? "": GLOBAL.getParseDate(this.fechaRetiroEdit),
      strNombreTransportista: (rowSelectedEdit.strNombreTransportista=== null)? "" : rowSelectedEdit.strNombreTransportista,
      strNroPlaca: (rowSelectedEdit.strNroPlaca=== null)? "" : rowSelectedEdit.strNroPlaca,
      strGuiaTransport: (rowSelectedEdit.strGuiaTransport=== null)? "" : rowSelectedEdit.strGuiaTransport,
      strNombreAcopiador: (rowSelectedEdit.strNombreAcopiador=== null)? "" : rowSelectedEdit.strNombreAcopiador,
      strTurnoRegistro: (rowSelectedEdit.strTurnoRegistro=== null)? "" : rowSelectedEdit.strTurnoRegistro,
      strObservacion: (rowSelectedEdit.strObservacion=== null)? "" : rowSelectedEdit.strObservacion,
      strUsuarioModif: this.userDec.strUsuario,
    }
    LotesService.EditarLoteBalanza(FormUpdate)
    .then(response =>{
      loadingInstance.close();
      if(response === -1) this.openMessageError('SERVICE: Error al actualizar lote.');
      else this.openMessage('Se actualizó el lote correctamente');
      this.BuscarLotes();
    }).catch(e =>{
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar lote.');
    })
    this.dialogEditarVisible = false;
    this.dialogEditarBalVisible = false;
  }

  changeHora(){
    if(this.horaIngreso.toString() === "" || this.horaSalida.toString() === "")
      this.FormAgregar.strTiempoDescarga = "0:00:00"
    else
      this.FormAgregar.strTiempoDescarga = this.getDiffTime(this.horaIngreso, this.horaSalida);
  }
  changeHoraEdit(){
    if(this.horaIngresoEdit.toString() === "" || this.horaSalidaEdit.toString() === "")
      this.rowSelectedEdit.strTiempoDescarga = "0:00:00"
    else
      this.rowSelectedEdit.strTiempoDescarga = this.getDiffTime(this.horaIngresoEdit, this.horaSalidaEdit);
  }
  getTimeString(value:Date){
    var hour = value.getHours()
    var mins = value.getMinutes();
    var secs = value.getSeconds();
    var HH:string = '', MM:string = '', SS:string = '';
    HH = (hour < 10) ? '0'+hour : hour.toString();
    MM = (mins < 10) ? '0'+mins : mins.toString();
    SS = (secs < 10) ? '0'+secs : secs.toString();
    return HH+':'+MM+':'+SS;
  }
  getDiffTime(hIngreso, hSalida):string {
    var fecha1: any = moment(hIngreso, "YYYY-MM-DD HH:mm:ss");
    var fecha2: any = moment(hSalida, "YYYY-MM-DD HH:mm:ss");

    var diffTime = fecha2.diff(fecha1);
    var duration = moment.duration(diffTime);
    var hrs = duration.hours(),
      mins = duration.minutes(),
      secs = duration.seconds();
    var HH:string = '', MM:string = '', SS:string = '';
    HH = (hrs < 10) ? '0'+hrs : hrs.toString();
    MM = (mins < 10) ? '0'+mins : mins.toString();
    SS = (secs < 10) ? '0'+secs : secs.toString();
    return hrs+':'+MM+':'+SS;
  }

  handleAgregar(){
    this.LimpiarFormAgregar();
    this.dialogAgregarVisible = true;
  }
  handleEdit(index, row){
    this.rowSelectedEdit = [];
    // this.rowSelectedEdit = row;
    // this.fechaRecepcionEdit = row.dtmFechaRecepcion;
    // this.fechaMuestreoEdit = (row.strFechaMuestreo === '' || row.strFechaMuestreo === null) ? '': row.strFechaMuestreo+'T00:00:00';
    // this.fechaRetiroEdit = (row.strFechaRetiro === '' || row.strFechaRetiro === null) ? '': row.strFechaRetiro+'T00:00:00';
    // this.horaIngresoEdit = (row.strHoraIngreso === '' || row.strHoraIngreso===null ) ? '': new Date(GLOBAL.getParseDate(this.fechaRecepcionEdit) +'T'+row.strHoraIngreso);
    // this.horaSalidaEdit = (row.strHoraSalida === '' || row.strHoraSalida===null ) ? '': new Date(GLOBAL.getParseDate(this.fechaRecepcionEdit) +'T'+row.strHoraSalida);

    // if(this.NivelAcceso === 'escritura'){
    //   if(this.userDec.strCargo === 'Area Balanza') {
    //     this.dialogEditarBalVisible = true;
    //     this.editViewButton = (row.chrEstadoModifBal === 'F' || row.chrEstadoModifBal === null) ? true : false;
    //   }
    //   if(this.userDec.strCargo === 'Liquidacion') {
    //     this.dialogEditarLiqVisible = true;
    //   }
    //   else this.dialogEditarVisible = true;
    // }
    // else this.dialogDetalleVisible = true;
    
    //EventBus.$emit('add-comment', 'hola mundo') 
    GLOBAL.setDataGuardar('balanza',row);
  }
  handleDelete(index, row){

  }
  LimpiarConsulta(){
    this.FormSearch.strLote = '';
    this.FormSearch.strProcedencia = '';
    this.FormSearch.strAcopiador = '';
    this.FormSearch.strProveedor = '';
    this.FormSearch.strMaterial = '';
    this.FormSearch.strEstadoLote = '';
    this.FormSearch.strFechaInicio = '';
    this.FormSearch.strFechaFin = '';
  }

  LimpiarFormAgregar(){
    this.FormAgregar.strLote = '',
    this.FormAgregar.dtmFechaRecepcion = '',
    this.FormAgregar.strProcedenciaLg = '',
    this.FormAgregar.strProcedenciaSt = '',
    this.FormAgregar.strClienteRuc = '',
    this.FormAgregar.strClienteRazonS = '',
    this.FormAgregar.strCodConcesion = '',
    this.FormAgregar.strNombreConcesion = '',
    this.FormAgregar.strGuiaRemitente = '',
    this.FormAgregar.strCodCompromiso = '',
    this.FormAgregar.strMaterial = '',
    this.FormAgregar.intNroSacos = 10,
    this.FormAgregar.fltPesoTmh = '',
    this.FormAgregar.strHoraIngreso = '',
    this.FormAgregar.strHoraSalida = '',
    this.FormAgregar.strTiempoDescarga = '',
    this.FormAgregar.strDuenioMineral = '',
    this.FormAgregar.strFechaMuestreo = '',
    this.FormAgregar.strTurnoMuestreo = '',
    this.FormAgregar.strFechaRetiro = '',
    this.FormAgregar.strNombreTransportista = '',
    this.FormAgregar.strNroPlaca = '',
    this.FormAgregar.strGuiaTransport = '',
    this.FormAgregar.strNombreAcopiador = '',
    this.FormAgregar.strTurnoRegistro = 'Dia',
    this.FormAgregar.strObservacion = '',
    this.FormAgregar.strUsuarioCrea = '',
    this.fechaRecepcion = new Date();
    this.horaIngreso = new Date();
    this.horaSalida = new Date();
    this.fechaMuestreo = '';
    this.fechaRetiro = '';
  }
  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  getSummaries(param) {
    const { columns, data } = param;
    const sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'Total';
        return;
      }
      const values = data.map(item => Number(item[column.property]));
      if (!values.every(value => isNaN(value))) {
        sums[index] = values.reduce((prev, curr) => {
          const value = Number(curr);
          if (!isNaN(value)) {
            return prev + curr;
          } else {
            return prev;
          }
        }, 0);
      } else {
        sums[index] = '';
      }
    });
    if(sums!=''){
      //var a=parseFloat(sums).toFixed(2);
      sums[7]=parseFloat(sums[7]).toFixed(2);
      return sums;
    }
    return sums; 
  }
  selectRow(row,index){
    //alert("Hola Mundo");
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.fechaRecepcionEdit = row.dtmFechaRecepcion;
    this.fechaMuestreoEdit = (row.strFechaMuestreo === '' || row.strFechaMuestreo === null) ? '': row.strFechaMuestreo+'T00:00:00';
    this.fechaRetiroEdit = (row.strFechaRetiro === '' || row.strFechaRetiro === null) ? '': row.strFechaRetiro+'T00:00:00';
    this.horaIngresoEdit = (row.strHoraIngreso === '' || row.strHoraIngreso===null ) ? '': new Date(GLOBAL.getParseDate(this.fechaRecepcionEdit) +'T'+row.strHoraIngreso);
    this.horaSalidaEdit = (row.strHoraSalida === '' || row.strHoraSalida===null ) ? '': new Date(GLOBAL.getParseDate(this.fechaRecepcionEdit) +'T'+row.strHoraSalida);

    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Area Balanza') {
        this.editViewButton = (row.chrEstadoModifBal === 'F' || row.chrEstadoModifBal === null) ? true : false;
      }
      if(this.userDec.strCargo === 'Liquidacion') {
      }
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar:{
        strLote: '',
        dtmFechaRecepcion: '',
        strProcedenciaLg: '',
        strProcedenciaSt: '',
        strClienteRuc: '',
        strClienteRazonS: '',
        strCodConcesion: '',
        strNombreConcesion: '',
        strGuiaRemitente: '',
        strCodCompromiso: '',
        strMaterial: '',
        intNroSacos: 10,
        fltPesoTmh: '',
        strHoraIngreso: '',
        strHoraSalida: '',
        strTiempoDescarga: '',
        strDuenioMineral: '',
        strFechaMuestreo: '',
        strTurnoMuestreo: '',
        strFechaRetiro: '',
        strNombreTransportista: '',
        strNroPlaca: '',
        strGuiaTransport: '',
        strNombreAcopiador: '',
        strTurnoRegistro: 'Dia',
        strObservacion: '',
        strUsuarioCrea: ''
      },
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      dataMaterial:[
        {id_material: 1, name: 'MINERAL'},
        {id_material: 2, name: 'LLAMPO'},
      ],
      json_fields : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strProcedenciaLg" : "Procedencia",
        "strProcedenciaSt" : "Procedencia Mineral",
        "strClienteRazonS" : "Proveedor Mineral",
        "strClienteRuc" : "Ruc",
        "strNombreConcesion" : "Nombre Concesion",
        "strGuiaRemitente" : "Guia Remitente",
        "strCodCompromiso" : "Código Compromiso",
        "strCodConcesion" : "Código Concesión",
        "strMaterial" : "Material",
        "intNroSacos" : "Nro. Sacos",
        "fltPesoTmh" : "Peso TMH",
        "strHoraIngreso" : "Hora Ingreso",
        "strHoraSalida" : "Hora Salida",
        "strTiempoDescarga" : "Tiempo Descarga",
        "strDuenioMineral" : "Dueño Mineral",
        "strTurnoMuestreo" : "Turno Muestreo",
        "strFechaRetiro" : "Fecha Retiro",
        "strNombreTransportista" : "Nombre Fletero",
        "strNroPlaca" : "Nro. Placa",
        "strGuiaTransport" : "Guia Transportista",
        "strNombreAcopiador" : "Acopiador",
        "strUsuarioCrea" : "Balancero",
        "strTurnoRegistro" : "Turno",
        "strObservacion" : "Observaciones",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  guarda:string='add-comment';
  blnguarda:boolean=false;
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
      GLOBAL.setComponent('balanza');
    }
    
    EventBus.$on(this.guarda, (item) => {
      if(item=="balanza" && !this.blnguarda){
        alert(item)
        this.guarda='';
        this.blnguarda=true;
      }
    });
  }
}
