import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LiquidacionService from '@/services/liquidacion.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
@Component({
   name: 'liquidacionLote',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
  }
   
})
export default class LiquidacionLoteComponent extends Vue {
  fechaLiquidacion: Date = new Date();
  factorConver: number = 0;
  userDec: any = {};
  constructor (){
    super()
  }
  checkAccesos(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      if (listaAccesos.length > 0){
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined) ? {} : JSON.parse(GLOBAL.decodeString(session));
        this.getFactorConversion();
      }
      else{
        Notification.warning('No tiene permisos para acceder a esta página');
        this.$router.push('/');
      }
    }
  }
  getFactorConversion(){
    LiquidacionService.searchMaestro('FactorConversion')
    .then(response =>{
      this.factorConver = Number(response.strValor);
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar factor de conversión');
    })
  }
  changeTMS(){
    // =REDONDEAR(G6-H6*G6/100;3)
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  data() {
    return {
      FormAgregar: {
        strLote: '',
        strCodLiquidacion: '',
        dtmProAuFecRecepcion: '',
        dtmProAuFecLiquidacion: '',
        strProAuZona: '',
        strProAuAcopiador: '',
        fltProAuTmh: '',
        fltProAuPorcH2O: '',
        fltProAuTms: '',
        fltProAuRec: '',
        fltProAuLeyOzTc: '',
        fltProAuInter: '',
        fltProAuMargen: '',
        fltProAuMaquila: '',
        fltProAuConsQ: '',
        fltProAuGastAdm: '',
        fltProAuPUTms: '',
        fltProAuImporte: '',
        fltProAuIGV: '',
        fltProAuTotal: '',
        fltLiqAuTmh: '',
        fltLiqAuPorcH2O: '',
        fltfltLiqAuTms: '',
        fltLiqAuRec: '',
        fltLiqAuLeyOzTc: '',
        fltLiqAuInter: '',
        fltLiqAuMaquila: '',
        fltLiqAuConsQ: '',
        fltLiqAuGastAdm: '',
        fltLiqAuPUTms: '',
        fltLiqAuImporte: '',
        fltVenAuTmh: '',
        fltVenAuPorcH2O: '',
        fltVenAuTms: '',
        fltVenAuRec: '',
        fltVenAuLeyOzTc: '',
        fltVenAuInter: '',
        fltVenAuPUTms: '',
        fltVenAuImporte: '',
        fltProAgTmh: '',
        fltProAgPorcH2O: '',
        fltProAgTms: '',
        fltProAgRec: '',
        fltProAgLeyOzTc: '',
        fltProAgInter: '',
        fltProAgMargen: '',
        fltProAgPUTms: '',
        fltProAgImporte: '',
        fltProAgIGV: '',
        fltProAgTotal: '',
        fltLiqAgTmh: '',
        fltLiqAgTPorcH2O: '',
        fltLiqAgTms: '',
        fltLiqAgRec: '',
        fltLiqAgLeyOzTc: '',
        fltLiqAgLeyOzTc2: '',
        fltLiqAgInter: '',
        fltLiqAgPUTms: '',
        fltLiqAgImporte: '',
        fltAuMargenBruto: '',
        fltAuUtilidadBruta: '',
        fltAgMargenBruto: '',
        fltAgUtilidadBruta: '',
        fltUtLoteMargenCompra: '',
        fltUtLoteMargenBruto: '',
        fltUtLoteUtilidadBruta: '',
        fltOtro: '',
        strUsuarioCrea: ''
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.checkAccesos();
    }
  }
}
