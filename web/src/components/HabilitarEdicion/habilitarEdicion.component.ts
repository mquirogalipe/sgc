import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LotesService from '@/services/lotes.service';
import QueryService from '@/services/query.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
@Component({
   name: 'habilitarEdicion',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
  }
})
export default class HabilitarEdicionComponent extends Vue {
  FormSearch: any = {};
  fechaSearch: Date = new Date();
  userDec: any = {};
  CompleteData:any = [];
  gridData: any = [];
  countGridData = 0;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  //PAGINATION
  pagina: number = 1;
  RegistersForPage: number = 20;
  totalRegistros: number = this.RegistersForPage;
  NivelAcceso: string = '';
  sizeScreen:string = (window.innerHeight - 420).toString();//'0';;
  loadingGetData: boolean = false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1031){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.BuscarLotes();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    var fSearch = this.fechaSearch;
    var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
    var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    this.FormSearch.strFechaInicio = bFechaIni;
    this.FormSearch.strFechaFin = bFechaFin;

    LotesService.SearchLotes(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }

  ExecuteConsulta(query){
    var DataQ = {
      strQuery: query,
      strUsuarioCrea: this.userDec.strUsuario
    }
    QueryService.ExecuteQuery(DataQ)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al guardar.')
      else this.openMessage('Se habilió correctamente.');
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar.');
    })
  }

  handleEnableBal(index, row){
    this.$confirm('Habilitar edición en Balanza del lote: '+row.strLote +' ?', 'Habilitar', {
      confirmButtonText: 'Habilitar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      let query = "UPDATE tblRegistro SET chrEstadoModifBal = 'F' WHERE intIdRegistro = "+row.intIdRegistro;
      this.ExecuteConsulta(query);
    }).catch(() => { });
  }
  handleEnableLab(index, row){
    this.$confirm('Habilitar edición en Laboratorio del lote: '+row.strLote +' ?', 'Habilitar', {
      confirmButtonText: 'Habilitar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      let query = "UPDATE tblRegistro SET chrEstadoModifLab = 'F' WHERE intIdRegistro = "+row.intIdRegistro;
      this.ExecuteConsulta(query);
    }).catch(() => { });
  }
  handleEnableCom(index, row){
    this.$confirm('Habilitar edición en Comercial del lote: '+row.strLote +' ?', 'Habilitar', {
      confirmButtonText: 'Habilitar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      let query = "UPDATE tblRegistro SET chrEstadoModifCom = 'F' WHERE intIdRegistro = "+row.intIdRegistro;
      this.ExecuteConsulta(query);
    }).catch(() => { });
  }

  handleEnableLiq(index, row){
    this.$confirm('Desea habilitar edición de la Liquidación: '+row.strLote +' ?', 'Habilitar', {
      confirmButtonText: 'Habilitar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      let query = "UPDATE tblLiquidacion SET strEstadoLiq = 'Propuesta' WHERE strLote = '"+row.strLote+"' AND strTipoLiquid = 'LIQ'";
      this.ExecuteConsulta(query);
    }).catch(() => { });
  }

  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
