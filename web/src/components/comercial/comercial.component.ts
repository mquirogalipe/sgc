import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import LotesService from '@/services/lotes.service';
import { Loading } from 'element-ui';
import ZonaService from '@/services/zona.service';
import QueryService from '@/services/query.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
import * as CONFIG from '@/Config';
import moment from 'moment'
import EventBus from '@/bus';

@Component({
   name: 'comercial',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class ComercialComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  FormSearch: any ={};
  fechaSearch: Date = new Date();
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  NivelAcceso: string = '';

  //DATA EDITAR
  dialogEditarVisible: boolean = false;
  dialogEditarComVisible: boolean = false;
  dialogDetalleVisible: boolean = false;
  dialogLeyIncorrecta: boolean = false;
  rowSelectedEdit: any ={};
  editViewButton: boolean = true;
  checkedH2O: boolean = false;

  nameButton: string = '';
  sumatorioPesoTMH:any = 0;
  sumatorioPSecoTM:any = 0;
  promPrecioInter: any = 0;
  fechaInicio: any = '';
  fechaFin: any = '';
  dataZonas: any = [];
  titleExcel:string ='Comercial_'+CONFIG.DATE_STRING+'.xls';
  checkedSearch: boolean = false;

  viewMsgLeyAuComError: boolean = false;
  viewMsgLeyAuComCheck: boolean = false;
  viewMsgLeyAuComEqual: boolean = false;
  viewMsgLeyAgComError: boolean = false;
  viewMsgLeyAgComCheck: boolean = false;
  viewMsgLeyAgComEqual: boolean = false;

  viewAccessEscritura: boolean = false;

  sizeScreen:string = (window.innerHeight - 420).toString();//'0';
  loadingGetData:boolean = false;
  showModal: boolean = false;
  tipoLote: string = 'lote';
  
  blnperiodo:boolean=false;
  blnfiltroavanzado:boolean=false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 4){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.nameButton = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? 'Actualizar' : 'Detalle';
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        this.BuscarLotes();
        this.getZonas();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else{
      this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearch.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    LotesService.SearchLotesComercial(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.sumarPesoTMH(this.CompleteData);
      this.changeDateString(this.CompleteData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  EditarLote(rowEdit){
    let loadingInstance = Loading.service({
      fullscreen: true ,
      spinner: 'el-icon-loading',
      text:'Actualizando lote...'
    });
    var FormUpdate = {
      intIdRegistro: rowEdit.intIdRegistro,
      strLote: rowEdit.strLote,
      strLeyAuComercial: (rowEdit.strLeyAuComercial === null) ? '' : rowEdit.strLeyAuComercial,
      strLeyAgComercial: (rowEdit.strLeyAgComercial === null) ? '' : rowEdit.strLeyAgComercial,
      strEstadoLote: (rowEdit.strEstadoLote === null) ? '' : rowEdit.strEstadoLote,
      strLeyCertificado: (rowEdit.strLeyCertificado === null) ? '' : rowEdit.strLeyCertificado,
      strLeyDirimencia: (rowEdit.strLeyDirimencia === null) ? '' : rowEdit.strLeyDirimencia,
      strUsuarioModif: this.userDec.strUsuario
    }
    LotesService.EditarLoteComercial(FormUpdate)
    .then(response =>{
      loadingInstance.close();
      if(response === -1) this.openMessageError('SERVICE: Error al actualizar lote.');
      else this.openMessage('Se actualizó el lote correctamente');
      this.BuscarLotes();
    }).catch(e =>{
      console.log(e);
      loadingInstance.close();
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar lote.');
    })
    this.dialogEditarVisible = false;
    this.dialogEditarComVisible = false;
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }

  LimpiarConsulta(){
    this.FormSearch.strLote = '';
    this.FormSearch.strProcedencia = '';
    this.FormSearch.strAcopiador = '';
    this.FormSearch.strProveedor = '';
    this.FormSearch.strMaterial = '';
    this.FormSearch.strEstadoLote = '';
    this.FormSearch.strFechaInicio = '';
    this.FormSearch.strFechaFin = '';
  }

  handleEdit(index, row){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.checkedH2O = (row.strCheckH2O === null|| row.strCheckH2O === "" || row.strCheckH2O === "false")? false : true;

    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Area Comercial'){
        this.editViewButton = (row.chrEstadoModifCom === 'F' || row.chrEstadoModifCom === null) ? true : false;
        this.dialogEditarComVisible = true;
      }
      else this.dialogEditarVisible = true;
    }
    else this.dialogDetalleVisible = true;
  }
  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }
  changeLeyAuCom(){
    if(this.rowSelectedEdit.strLeyAuComercial===''||this.rowSelectedEdit.strLeyAuOzTc===''||this.rowSelectedEdit.strLeyAuComercial===null||this.rowSelectedEdit.strLeyAuOzTc===null){
      this.viewMsgLeyAuComError = false;
    }
    else {
      var leyAuCom = Number(this.rowSelectedEdit.strLeyAuComercial);
      var leyAuLab = Number(this.rowSelectedEdit.strLeyAuOzTc);
      if(leyAuCom > leyAuLab) {this.viewMsgLeyAuComError=true;this.viewMsgLeyAuComEqual=false;this.viewMsgLeyAuComCheck=false;}
      else if(leyAuCom === leyAuLab) {this.viewMsgLeyAuComError=false;this.viewMsgLeyAuComEqual=true;this.viewMsgLeyAuComCheck=false;}
      else {this.viewMsgLeyAuComError=false;this.viewMsgLeyAuComEqual=false;this.viewMsgLeyAuComCheck=true;}
    }
  }
  changeLeyAgCom(){
    if(this.rowSelectedEdit.strLeyAgComercial===''||this.rowSelectedEdit.strLeyAgOzTc===''||this.rowSelectedEdit.strLeyAgComercial===null||this.rowSelectedEdit.strLeyAgOzTc===null){
      this.viewMsgLeyAgComError = false;
      this.viewMsgLeyAgComEqual=false;
      this.viewMsgLeyAgComCheck=false;
    }
    else {
      var leyAgCom = Number(this.rowSelectedEdit.strLeyAgComercial);
      var leyAgLab = Number(this.rowSelectedEdit.strLeyAgOzTc);
      if(leyAgCom > leyAgLab) {this.viewMsgLeyAgComError=true;this.viewMsgLeyAgComEqual=false;this.viewMsgLeyAgComCheck=false;}
      else if(leyAgCom === leyAgLab) {this.viewMsgLeyAgComError=false;this.viewMsgLeyAgComEqual=true;this.viewMsgLeyAgComCheck=false;}
      else {this.viewMsgLeyAgComError=false;this.viewMsgLeyAgComEqual=false;this.viewMsgLeyAgComCheck=true;}
    }
  }

  EnterLeyAuComercial(row){
    var leyEstim = 0;
    if(row.strProcedenciaSt === 'UNTUCA') leyEstim = Number(row.strLeyAuOzTc) - 0.03;
    else leyEstim = Number((Number(row.strLeyAuOzTc) - 0.04).toFixed(3));

    var leyAuCom = Number(row.strLeyAuComercial);
    if(leyAuCom > leyEstim || leyAuCom <=0) this.showModal = true;
    else {
      this.showModal = false;
      var consulta = "UPDATE tblRegistro SET strLeyAuComercial = '"+leyAuCom.toString()+"' WHERE strLote = '"+row.strLote+"'";
      this.ExecuteConsultaLeyes(consulta);
    }
  }
  EnterLeyAgComercial(row){
    var leyEstim = Number(row.strLeyAgOzTc);
    var leyAgCom = Number(row.strLeyAgComercial);
    if(leyAgCom > leyEstim) this.showModal = true;
    else{
      this.showModal = false;
      var consulta = "UPDATE tblRegistro SET strLeyAgComercial = '"+leyAgCom.toString()+"' WHERE strLote = '"+row.strLote+"'";
      this.ExecuteConsultaLeyes(consulta);
    }
  }
  ExecuteConsultaLeyes(query){
    var DataQ = {
      strQuery: query,
      strUsuarioCrea: this.userDec.strUsuario
    }
    QueryService.ExecuteQuery(DataQ)
    .then(response =>{
      if(response == -1) this.openMessageError('SERVICE: Error al guardar leyes.')
      else this.openMessage('Se guardo correctamente.');
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al guardar leyes.');
    })
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getNumberFloat(number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }
  changeCheckSearch(){
    console.log(this.checkedSearch);
    if(this.checkedSearch === true)
      this.FormSearch.strEstadoLote = '*'
    else
      this.FormSearch.strEstadoLote = ''
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    var PrecioInterAcum:number = 0;
    var countPI: number = 0;
    // var PesoSecoComercialTotal = 0;
    data.forEach(function(element) {
      if((element.fltPesoTmh != null || element.fltPesoTmh != '')) // && (element.strSubLote==='AS' || element.strSubLote===''))
        PesoTotal += element.fltPesoTmh;
      if((element.strPSecoCom != null || element.strPSecoCom != '')) // && (element.strSubLote==='AS' || element.strSubLote===''))
        PesoSecoTotal += Number(element.strPSecoCom);
      if((element.strPrecioInter != null && element.strPrecioInter != '')){// && (element.strSubLote==='AS' || element.strSubLote==='')){
        PrecioInterAcum += Number(element.strPrecioInter);
        countPI++;
      }
    });
    this.sumatorioPesoTMH = this.getNumberFloat(PesoTotal);
    this.sumatorioPSecoTM = this.getNumberFloat(PesoSecoTotal);
    this.promPrecioInter = this.getNumberFix(PrecioInterAcum/countPI, 2);
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  selectRow(row,index){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.checkedH2O = (row.strCheckH2O === null|| row.strCheckH2O === "" || row.strCheckH2O === "false")? false : true;

    if(this.NivelAcceso === 'escritura'){
      if(this.userDec.strCargo === 'Area Comercial'){
        this.editViewButton = (row.chrEstadoModifCom === 'F' || row.chrEstadoModifCom === null) ? true : false;
        // this.dialogEditarComVisible = true;
      }
      // else this.dialogEditarVisible = true;
    }
    // else this.dialogDetalleVisible = true;
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      dataEstado:[
        {id_estado: 'F', name: 'FACTURADOS'},
        {id_estado: 'E', name: 'ENTREGADOS'},
        {id_estado: 'R', name: 'REINTEGROS'},
        {id_estado: 'P', name: 'PENDIENTES'},
        {id_estado: 'RT', name: 'RETIRADOS'},
        {id_estado: '*', name: 'TODOS'}
      ],
      FormSearch:{
        strLote : '',
        strSubLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      dataMaterial:[
        {id_material: 1, name: 'MINERAL'},
        {id_material: 2, name: 'LLAMPO'},
      ],
      json_fields : {
        "strLote" : "LOTE",
        "strSubLote": "Sub Lote",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strEstadoLote" : "Estado",
        "strProcedenciaSt" : "Procedencia",
        "strClienteRuc" : "Ruc Proveedor",
        "strClienteRazonS" : "Proveedor Mineral",
        "strMaterial" : "Material",
        "intNroSacos" : "Nro. Sacos",
        "fltPesoTmh" : "Peso TMH",
        "strH2OCom" : "%H2O Com",
        "strCheckH2O" : "Check H2O",
        "strPSecoCom" : "P.Seco Com",
        "strPorcRCPRef12hrs" : "% RCP Ref 12hrs",
        "strPorcRCPAu30hrs" : "% RCP Au 48hrs",
        "strPorcRCPAu70hrs" : "% RCP Au 72hrs",
        "strPorcRCPReeAu72hrs" : "REE % RCP Au 72hrs",
        "strLeyAuOzTc" : "Ley Au Oz/tc",
        "strLeyAuREE" : "Ley Au REE",
        "strLeyAuComercial" : "Ley Au Comercial",
        "strLeyAuGrTm" : "Ley Au Gr/Tm",
        "strLeyAgOzTc" : "Ley Ag (Oz/tc)",
        "strLeyAgREE" : "Ley Ag REE",
        "strLeyAgComercial" : "Ley Ag Comercial",
        "strPorcCobre" : "Cobre %",
        "strNaOHKgTM" : "NaOH (kg/TM)",
        "strNaCNKgTM" : "NaCN (Kg/TM)",
        "strFechaRetiro" : "Fecha Retiro",
        "strNombreTransportista" : "Nombre Fletero",
        "strNroPlaca" : "Nro. Placa",
        "strGuiaTransport" : "Guia Transport.",
        "strGuiaRemitente" : "Guia Remitente",
        "strCodCompromiso" : "Código Compromiso",
        "strCodConcesion" : "Código Concesión",
        "strNombreConcesion" : "Nombre Concesión",
        "strProcedenciaLg" : "Procedencia",
        "strNombreAcopiador" : "Acopiador",
        "strObservacion" : "Observación"
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }
}
