import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ZonaService from '@/services/zona.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'zona',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ZonaComponent extends Vue {
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  FormAgregar : any = {};
  rowSelectedEdit: any = {};
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 15;
  totalRegistros:number = this.RegistersForPage;
  sizeScreen:string = (window.innerHeight - 420).toString();
  dialogAgregarVisible: boolean = false;
  dialogEditarVisible: boolean = false;
  userDec: any = {};
  loadingGetData:boolean = false;
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  checkCalcEspecial: boolean = false;
  checkCalcEspecialEdit: boolean = false;
  seleccionarRows:any=null;
  constructor (){
    super()
  }
  checkAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1004){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.loadingData();
      }
    }
  }
  loadingData(){
    this.loadingGetData = true;
    ZonaService.loadingData()
    .then(response => {
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.loadingGetData = false;
    })
    .catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  AgregarZona(FormAgregar){
    FormAgregar.strCalculoEspecial = 'false'; //this.checkCalcEspecial.toString()
    FormAgregar.strUsuarioCrea = this.userDec.strUsuario;
    ZonaService.AgregarZona(FormAgregar)
    .then(response => {
      if(response === -1) this.openMessageError("Error al agregar zona:"+response)
      else if(response === -2) this.openMessageError("Ya existe una zona con el mismo nombre.")
      else
        this.openMessage('Zona guardada correctamente.');
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar zona.');
    })
    this.dialogAgregarVisible = false;
  }

  EditarZona(rowSelectedEdit){
    rowSelectedEdit.strCalculoEspecial = 'false'; //this.checkCalcEspecialEdit.toString();
    rowSelectedEdit.strUsuarioModif = this.userDec.strUsuario;
    ZonaService.EditarZona(rowSelectedEdit)
    .then(response => {
      this.openMessage('Zona actualizada correctamente.');
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al actualizar zona.');
    })
    this.dialogEditarVisible = false;
  }

  EliminarZona(row){
    ZonaService.EliminarZona(row.intIdZona, this.userDec.strUsuario)
    .then(response => {
      this.openMessage('Zona eliminada correctamente: '+response);
      this.loadingData();
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al eliminar zona.');
    })
  }

  handleAgregar(){
    this.LimpiarFormAgregar();
    this.dialogAgregarVisible = true
  }
  handleEdit2(){
    this.handleEdit(this.seleccionarRows);
  }
  handleEdit(row){
    this.rowSelectedEdit = [];
    this.rowSelectedEdit = row;
    this.checkCalcEspecialEdit = (row.strCalculoEspecial=== 'true')? true : false;
    this.dialogEditarVisible = true
  }
  handleDelete2(row){
    this.handleDelete(this.seleccionarRows);
  }
  handleDelete(row){
    this.$confirm('Desea Eliminar la zona: '+row.strZona +' ?', 'Eliminar', {
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      type: 'warning'
    }).then(() => {
      this.EliminarZona(row);
    }).catch(() => { });
  }
  LimpiarFormAgregar(){
    this.FormAgregar.strCodZona = '';
    this.FormAgregar.strZona  = '';
    this.FormAgregar.strProcedencia = '';
    this.FormAgregar.strRegion = '';
    this.FormAgregar.strCalculoEspecial = '';
    this.FormAgregar.strUsuarioCrea;
  }
  checkCalculoEspecial(value){
    if(value === 'true') return true;
    else return false;
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  seleccionarRow(row,index){
    this.seleccionarRows=row;
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormAgregar: {
        strCodZona: '',
        strZona: '',
        strProcedencia: '',
        strRegion: '',
        strCalculoEspecial: '',
        strUsuarioCrea: ''
      }
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.checkAccess();
    }
  }

}
