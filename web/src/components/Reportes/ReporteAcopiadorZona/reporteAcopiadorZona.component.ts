import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReportesService from '@/services/reportes.service'
import ZonaService from '@/services/zona.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reportePrueba',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ReporteAcopiadorZonaComponent extends Vue {
  gridData: any = [];
  fechaSearch: Date = new Date();
  FormSearch: any = {};
  loadingGetData: boolean = false;
  dataZonas: any = [];
  sizeScreen:string =  (window.innerHeight - 350).toString();;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1019){
          flag=true;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        this.getZonas();
      }
    }
  }
  BuscarReporte(){
    var fSearch = this.fechaSearch;
    this.gridData = [];
    this.loadingGetData = true;
    if(fSearch === undefined){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else {
      this.FormSearch.strFechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'*': GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
      this.FormSearch.strFechaFin = (fSearch===undefined||fSearch.toString()==='') ?'*': GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));
    }
    ReportesService.GetReporteAcopiadorZona(this.FormSearch)
    .then(response =>{
      this.gridData = response.Data;
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte.');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, 0, 0, 0, 0];
    var TotalPSeco = 0;
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.fltSumPesoTmh != null || element.fltSumPesoTmh != '')
        Total[2] += element.fltSumPesoTmh;
      if(element.fltSumPSecoTM != null || element.fltSumPSecoTM != '')
        Total[3] += element.fltSumPSecoTM;
      if(element.fltPromLeyAuOzTc != null || element.fltPromLeyAuOzTc != '')
        Total[4] += element.fltPromLeyAuOzTc;
      if(element.fltPromLeyAgOzTc != null || element.fltPromLeyAgOzTc != '')
        Total[5] += element.fltPromLeyAgOzTc;
      if(element.fltPromFinoAuGr != null || element.fltPromFinoAuGr != '')
        Total[6] += element.fltPromFinoAuGr;
      if(element.fltSumPSecoTM != null || element.fltSumPSecoTM != '')
        TotalPSeco += element.fltSumPSecoTM;
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1){
        sums[index] = ''
        return;
      }
      else if(index === 4 || index === 5){
        sums[index] = this.getNumberFloat(Total[index]/TotalPSeco);
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getNumberFloat(number){
    if(number === null) return '';
    else {
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  getPromedioLeyAu(row){
    row.fltFinalLeyAuOzTc = row.fltPromLeyAuOzTc / row.fltSumPSecoTM;
    return parseFloat(row.fltFinalLeyAuOzTc).toFixed(3)
  }
  getPromedioLeyAg(row){
    row.fltFinalLeyAgOzTc = row.fltPromLeyAgOzTc / row.fltSumPSecoTM;
    return parseFloat(row.fltFinalLeyAgOzTc).toFixed(3)
  }

  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  data() {
    return {
      FormSearch:{
        strFechaInicio:'',
        strFechaFin:'',
        strAcopiador:'',
        strZona:''
      },
    };
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
