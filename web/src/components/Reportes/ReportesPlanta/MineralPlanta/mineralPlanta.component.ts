import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ZonaService from '@/services/zona.service';
import ReporteService from '@/services/reportes.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'mineralPlanta',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
   'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class MineralPlantaComponent extends Vue {
  userDec: any = {};
  CompleteData:any = [];
  gridData : any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;
  FormSearch: any ={};
  fechaSearch: Date = new Date();
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  NivelAcceso: string = '';
  sizeScreen:string = (window.innerHeight - 350).toString();
  fechaInicio: any = '';
  fechaFin: any = '';
  loadingGetData:boolean = false;
  checkedSearch: boolean = false;
  sumatorioPesoTMH:any = 0;
  sumatorioPSecoTM:any = 0;
  sumatorioFinos: any = 0;
  promedioLeyAu: any = 0;
  promedioLeyAg: any = 0;
  dataZonas: any = [];
  titleExcel:string ='Reporte_Mineral_'+CONFIG.DATE_STRING+'.xls';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1035){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        this.BuscarLotes();
        this.getZonas();
      }
    }
  }
  BuscarLotes(){
    this.loadingGetData = true;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else{
      this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearch.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    ReporteService.GetReporteMineralPlanta(this.FormSearch)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.sumarPesoTMH(this.CompleteData);
      this.changeDateString(this.CompleteData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    var PesoFino = 0;
    var LeyPond = 0;
    var LeyAgPond = 0;
    var PrecioInterAcum:number = 0;
    var countPI: number = 0;
    // var PesoSecoComercialTotal = 0;
    data.forEach(function(element) {
      if(element.fltPesoTmhHistorico != null || element.fltPesoTmhHistorico != '')
        PesoTotal += element.fltPesoTmhHistorico;
      if(element.strPSecoHistorico != null || element.strPSecoHistorico != '')
        PesoSecoTotal += Number(element.strPSecoHistorico);
      if((element.strPSecoHistorico != null || element.strPSecoHistorico != '')&&(element.strLeyAuOzTc != null || element.strLeyAuOzTc != '')){
        PesoFino += Number(element.strPSecoHistorico) * Number(element.strLeyAuOzTc) * 34.285;
        LeyPond += Number(element.strPSecoHistorico) * Number(element.strLeyAuOzTc);
        LeyAgPond += Number(element.strPSecoHistorico) * Number(element.strLeyAgOzTc);
      }
      // data.strFinoAuGr = Number(element.strPSecoTM) * Number(element.strLeyAuOzTc) * 34.285;
      // strPSecoTM * strLeyAuOzTc * 34.285
    });
    this.sumatorioPesoTMH = this.getNumberFix(PesoTotal, 3);
    this.sumatorioPSecoTM = this.getNumberFix(PesoSecoTotal, 3);
    this.sumatorioFinos = this.getNumberFix(PesoFino, 3);
    this.promedioLeyAu = this.getNumberFix(LeyPond/PesoSecoTotal, 3);
    this.promedioLeyAg = this.getNumberFix(LeyAgPond/PesoSecoTotal, 3);
  }
  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }
  getFinoAu(PSeco, Ley){
    var finos = Number(PSeco) * Number(Ley) * 34.285;
    return this.getNumberFix(finos, 3);
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  cambioPagina(){
    this.gridData = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      json_fields : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strProcedenciaSt" : "Zona",
        "strClienteRuc" : "Ruc Proveedor",
        "strClienteRazonS" : "Proveedor Mineral",
        "strMaterial" : "Material",
        "intNroSacos" : "Nro. Sacos",
        "fltPesoTmhHistorico" : "Peso TMH",
        "strPorcH2O" : "% H2O",
        "strPSecoHistorico" : "P.Seco TM",
        "strPorcRCPRef12hrs" : "% RCP Ref 12hrs",
        "strPorcRCPAu30hrs" : "% RCP Au 48hrs",
        "strPorcRCPAu70hrs" : "% RCP Au 72hrs",
        "strPorcRCPReeAu72hrs" : "REE % RCP Au 72hrs",
        "strLeyAuOzTc" : "Ley Au Oz/tc",
        "strLeyAuREE" : "Ley Au REE",
        "strLeyAuGrTm" : "Ley Au Gr/Tm",
        "fltFinoAu" : "Fino (Au/Gr)",
        "strLeyAgOzTc" : "Ley Ag (Oz/tc)",
        "strLeyAgREE" : "Ley Ag REE",
        "strPorcCobre" : "Cobre %",
        "strNaOHKgTM" : "NaOH (kg/TM)",
        "strNaCNKgTM" : "NaCN (Kg/TM)",
        "strFechaRetiro" : "Fecha Retiro",
        "strNombreTransportista" : "Nombre Fletero",
        "strNroPlaca" : "Nro. Placa",
        "strGuiaTransport" : "Guia Transport.",
        "strGuiaRemitente" : "Guia Remitente",
        "strCodCompromiso" : "Código Compromiso",
        "strCodConcesion" : "Código Concesión",
        "strNombreConcesion" : "Nombre Concesión",
        "strProcedenciaLg" : "Procedencia",
        "strNombreAcopiador" : "Acopiador",
        "strObservacion" : "Observación"
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
