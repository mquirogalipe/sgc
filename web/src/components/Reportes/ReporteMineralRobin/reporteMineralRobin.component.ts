import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReporteService from '@/services/reportes.service';
import ZonaService from '@/services/zona.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reporteMineralRobin',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class ReporteMineralRobinComponent extends Vue {
  //Reporte Mineral
  fechaSearch: any = new Date();
  strZonaRep: string = '';
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 255).toString();//'0';
  userDec: any = {};
  loadingGetData:boolean = false;
  DataSubTotal: any = [];
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  titleExcel:string ='Reporte Zona_Mes_'+CONFIG.DATE_STRING+'.xls';
  titleExcelDet:string ='Reporte Diario_'+CONFIG.DATE_STRING+'.xls';

  //Reporte Diario
  CompleteData:any = [];
  gridDataRepDiario: any = [];
  countGridData = 0;
  //PAGINATION
  pagina:number = 1;
  RegistersForPage:number = 25;
  totalRegistros:number = this.RegistersForPage;

  FormSearchRD: any ={};
  fechaSearchRD: Date = new Date();
  date_actual: Date = new Date();
  fechaInicio: any = '';
  fechaFin: any = '';
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  checkedSearch: boolean = false;
  dataZonas: any = [];
  loadingRepDiario: boolean = false;
  sizeScreenRepDiario:string = (window.innerHeight - 275).toString();
  sumatorioPesoTMH:any = 0;
  sumatorioPSecoTM:any = 0;
  constructor (){
    super()
  }
  CheckAccess(){
    var date_actual = new Date();
    this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
    this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
    this.getZonas();
    // this.bind()
  }
  bind(){
    // this.BuscarReporte();
    // this.BuscarLotes();
    // setTimeout(this.bind, 300000);
  }
  BuscarReporte(){
    this.loadingGetData = true;
    var year = (this.fechaSearch===undefined||this.fechaSearch.toString()==='') ?'*' : this.fechaSearch.getFullYear().toString();
    ReporteService.GetReporteRobin(year, this.strZonaRep)
    .then(response =>{
      this.gridData = response.Data;
      this.getSubTotales(this.gridData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte');
    })
  }
  getSubTotales(Data){
    var DataTemp: any = [];
    var subTAux: any = [
      {strZona: '',strAnio: '',enero: '',febrero: '',marzo: '',abril: '',mayo: '',junio: '',julio: '',agosto: '',septiembre: '',octubre: '',noviembre: '',diciembre: ''},
      {strZona: 'SUBTOTAL',strAnio: '',enero: '',febrero: '',marzo: '',abril: '',mayo: '',junio: '',julio: '',agosto: '',septiembre: '',octubre: '',noviembre: '',diciembre: ''},
    ]
    this.DataSubTotal = this.getDefaultSubTotal();
    DataTemp = Data.concat(subTAux);
    for (let i = 0; i < Data.length; i++) {
      if(Data[i].strRegion === 'SUR'){
        this.DataSubTotal[1].enero = Number(this.DataSubTotal[1].enero) + Data[i].enero;
        this.DataSubTotal[1].eneroFino = Number(this.DataSubTotal[1].eneroFino) + Data[i].eneroActual;
        this.DataSubTotal[1].febrero = Number(this.DataSubTotal[1].febrero) + Data[i].febrero;
        this.DataSubTotal[1].febreroFino = Number(this.DataSubTotal[1].febreroFino) + Data[i].febreroActual;
        this.DataSubTotal[1].marzo = Number(this.DataSubTotal[1].marzo) + Data[i].marzo;
        this.DataSubTotal[1].marzoFino = Number(this.DataSubTotal[1].marzoFino) + Data[i].marzoActual;
        this.DataSubTotal[1].abril = Number(this.DataSubTotal[1].abril) + Data[i].abril;
        this.DataSubTotal[1].abrilFino = Number(this.DataSubTotal[1].abrilFino) + Data[i].abrilActual;
        this.DataSubTotal[1].mayo = Number(this.DataSubTotal[1].mayo) + Data[i].mayo;
        this.DataSubTotal[1].mayoFino = Number(this.DataSubTotal[1].mayoFino) + Data[i].mayoActual;
        this.DataSubTotal[1].junio = Number(this.DataSubTotal[1].junio) + Data[i].junio;
        this.DataSubTotal[1].junioFino = Number(this.DataSubTotal[1].junioFino) + Data[i].junioActual;
        this.DataSubTotal[1].julio = Number(this.DataSubTotal[1].julio) + Data[i].julio;
        this.DataSubTotal[1].julioFino = Number(this.DataSubTotal[1].julioFino) + Data[i].julioActual;
        this.DataSubTotal[1].agosto = Number(this.DataSubTotal[1].agosto) + Data[i].agosto;
        this.DataSubTotal[1].agostoFino = Number(this.DataSubTotal[1].agostoFino) + Data[i].agostoActual;
        this.DataSubTotal[1].septiembre = Number(this.DataSubTotal[1].septiembre) + Data[i].septiembre;
        this.DataSubTotal[1].septiembreFino = Number(this.DataSubTotal[1].septiembreFino) + Data[i].septiembreActual;
        this.DataSubTotal[1].octubre = Number(this.DataSubTotal[1].octubre) + Data[i].octubre;
        this.DataSubTotal[1].octubreFino = Number(this.DataSubTotal[1].octubreFino) + Data[i].octubreActual;
        this.DataSubTotal[1].noviembre = Number(this.DataSubTotal[1].noviembre) + Data[i].noviembre;
        this.DataSubTotal[1].noviembreFino = Number(this.DataSubTotal[1].noviembreFino) + Data[i].noviembreActual;
        this.DataSubTotal[1].diciembre = Number(this.DataSubTotal[1].diciembre) + Data[i].diciembre;
        this.DataSubTotal[1].diciembreFino = Number(this.DataSubTotal[1].diciembreFino) + Data[i].diciembreActual;
      }
      else if(Data[i].strRegion === 'TRUJILLO'){
        this.DataSubTotal[2].enero = Number(this.DataSubTotal[2].enero) + Data[i].enero;
        this.DataSubTotal[2].eneroFino = Number(this.DataSubTotal[2].eneroFino) + Data[i].eneroActual;
        this.DataSubTotal[2].febrero = Number(this.DataSubTotal[2].febrero) + Data[i].febrero;
        this.DataSubTotal[2].febreroFino = Number(this.DataSubTotal[2].febreroFino) + Data[i].febreroActual;
        this.DataSubTotal[2].marzo = Number(this.DataSubTotal[2].marzo) + Data[i].marzo;
        this.DataSubTotal[2].marzoFino = Number(this.DataSubTotal[2].marzoFino) + Data[i].marzoActual;
        this.DataSubTotal[2].abril = Number(this.DataSubTotal[2].abril) + Data[i].abril;
        this.DataSubTotal[2].abrilFino = Number(this.DataSubTotal[2].abrilFino) + Data[i].abrilActual;
        this.DataSubTotal[2].mayo = Number(this.DataSubTotal[2].mayo) + Data[i].mayo;
        this.DataSubTotal[2].mayoFino = Number(this.DataSubTotal[2].mayoFino) + Data[i].mayoActual;
        this.DataSubTotal[2].junio = Number(this.DataSubTotal[2].junio) + Data[i].junio;
        this.DataSubTotal[2].junioFino = Number(this.DataSubTotal[2].junioFino) + Data[i].junioActual;
        this.DataSubTotal[2].julio = Number(this.DataSubTotal[2].julio) + Data[i].julio;
        this.DataSubTotal[2].julioFino = Number(this.DataSubTotal[2].julioFino) + Data[i].julioActual;
        this.DataSubTotal[2].agosto = Number(this.DataSubTotal[2].agosto) + Data[i].agosto;
        this.DataSubTotal[2].agostoFino = Number(this.DataSubTotal[2].agostoFino) + Data[i].agostoActual;
        this.DataSubTotal[2].septiembre = Number(this.DataSubTotal[2].septiembre) + Data[i].septiembre;
        this.DataSubTotal[2].septiembreFino = Number(this.DataSubTotal[2].septiembreFino) + Data[i].septiembreActual;
        this.DataSubTotal[2].octubre = Number(this.DataSubTotal[2].octubre) + Data[i].octubre;
        this.DataSubTotal[2].octubreFino = Number(this.DataSubTotal[2].octubreFino) + Data[i].octubreActual;
        this.DataSubTotal[2].noviembre = Number(this.DataSubTotal[2].noviembre) + Data[i].noviembre;
        this.DataSubTotal[2].noviembreFino = Number(this.DataSubTotal[2].noviembreFino) + Data[i].noviembreActual;
        this.DataSubTotal[2].diciembre = Number(this.DataSubTotal[2].diciembre) + Data[i].diciembre;
        this.DataSubTotal[2].diciembreFino = Number(this.DataSubTotal[2].diciembreFino) + Data[i].diciembreActual;
      }
      else if(Data[i].strRegion === 'SECOCHA'){
        this.DataSubTotal[3].enero = Number(this.DataSubTotal[3].enero) + Data[i].enero;
        this.DataSubTotal[3].eneroFino = Number(this.DataSubTotal[3].eneroFino) + Data[i].eneroActual;
        this.DataSubTotal[3].febrero = Number(this.DataSubTotal[3].febrero) + Data[i].febrero;
        this.DataSubTotal[3].febreroFino = Number(this.DataSubTotal[3].febreroFino) + Data[i].febreroActual;
        this.DataSubTotal[3].marzo = Number(this.DataSubTotal[3].marzo) + Data[i].marzo;
        this.DataSubTotal[3].marzoFino = Number(this.DataSubTotal[3].marzoFino) + Data[i].marzoActual;
        this.DataSubTotal[3].abril = Number(this.DataSubTotal[3].abril) + Data[i].abril;
        this.DataSubTotal[3].abrilFino = Number(this.DataSubTotal[3].abrilFino) + Data[i].abrilActual;
        this.DataSubTotal[3].mayo = Number(this.DataSubTotal[3].mayo) + Data[i].mayo;
        this.DataSubTotal[3].mayoFino = Number(this.DataSubTotal[3].mayoFino) + Data[i].mayoActual;
        this.DataSubTotal[3].junio = Number(this.DataSubTotal[3].junio) + Data[i].junio;
        this.DataSubTotal[3].junioFino = Number(this.DataSubTotal[3].junioFino) + Data[i].junioActual;
        this.DataSubTotal[3].julio = Number(this.DataSubTotal[3].julio) + Data[i].julio;
        this.DataSubTotal[3].julioFino = Number(this.DataSubTotal[3].julioFino) + Data[i].julioActual;
        this.DataSubTotal[3].agosto = Number(this.DataSubTotal[3].agosto) + Data[i].agosto;
        this.DataSubTotal[3].agostoFino = Number(this.DataSubTotal[3].agostoFino) + Data[i].agostoActual;
        this.DataSubTotal[3].septiembre = Number(this.DataSubTotal[3].septiembre) + Data[i].septiembre;
        this.DataSubTotal[3].septiembreFino = Number(this.DataSubTotal[3].septiembreFino) + Data[i].septiembreActual;
        this.DataSubTotal[3].octubre = Number(this.DataSubTotal[3].octubre) + Data[i].octubre;
        this.DataSubTotal[3].octubreFino = Number(this.DataSubTotal[3].octubreFino) + Data[i].octubreActual;
        this.DataSubTotal[3].noviembre = Number(this.DataSubTotal[3].noviembre) + Data[i].noviembre;
        this.DataSubTotal[3].noviembreFino = Number(this.DataSubTotal[3].noviembreFino) + Data[i].noviembreActual;
        this.DataSubTotal[3].diciembre = Number(this.DataSubTotal[3].diciembre) + Data[i].diciembre;
        this.DataSubTotal[3].diciembreFino = Number(this.DataSubTotal[3].diciembreFino) + Data[i].diciembreActual;
      }
    }

    //this.CompleteData = DataTemp.concat(this.DataSubTotal.slice(1, 4));

    // if(this.viewAccessEscritura === true){
    //   for (let i = 0; i < this.gridData.length; i++) {
    //     this.CompleteData[i].enero = (this.CompleteData[i].enero === null)? '' : this.CompleteData[i].enero;
    //     this.CompleteData[i].febrero = (this.CompleteData[i].febrero === null)? '' : this.CompleteData[i].febrero;
    //     this.CompleteData[i].marzo = (this.CompleteData[i].marzo === null)? '' : this.CompleteData[i].marzo;
    //     this.CompleteData[i].abril = (this.CompleteData[i].abril === null)? '' : this.CompleteData[i].abril;
    //     this.CompleteData[i].mayo = (this.CompleteData[i].mayo === null)? '' : this.CompleteData[i].mayo;
    //     this.CompleteData[i].junio = (this.CompleteData[i].junio === null)? '' : this.CompleteData[i].junio;
    //     this.CompleteData[i].julio = (this.CompleteData[i].julio === null)? '' : this.CompleteData[i].julio;
    //     this.CompleteData[i].agosto = (this.CompleteData[i].agosto === null)? '' : this.CompleteData[i].agosto;
    //     this.CompleteData[i].septiembre = (this.CompleteData[i].septiembre === null)? '' : this.CompleteData[i].septiembre;
    //     this.CompleteData[i].octubre = (this.CompleteData[i].octubre === null)? '' : this.CompleteData[i].octubre;
    //     this.CompleteData[i].noviembre = (this.CompleteData[i].noviembre === null)? '' : this.CompleteData[i].noviembre;
    //     this.CompleteData[i].diciembre = (this.CompleteData[i].diciembre === null)? '' : this.CompleteData[i].diciembre;
    //   }
    // }
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.enero != null || element.enero != '')
        Total[2] += Number(element.enero);
      if(element.eneroActual != null || element.eneroActual != '')
        Total[3] += Number(element.eneroActual);
      if(element.febrero != null || element.febrero != '')
        Total[4] += Number(element.febrero);
      if(element.febreroActual != null || element.febreroActual != '')
        Total[5] += Number(element.febreroActual);
      if(element.marzo != null || element.marzo != '')
        Total[6] += Number(element.marzo);
      if(element.marzoActual != null || element.marzoActual != '')
        Total[7] += Number(element.marzoActual);
      if(element.abril != null || element.abril != '')
        Total[8] += Number(element.abril);
      if(element.abrilActual != null || element.abrilActual != '')
        Total[9] += Number(element.abrilActual);
      if(element.mayo != null || element.mayo != '')
        Total[10] += Number(element.mayo);
      if(element.mayoActual != null || element.mayoActual != '')
        Total[11] += Number(element.mayoActual);
      if(element.junio != null || element.junio != '')
        Total[12] += Number(element.junio);
      if(element.junioActual != null || element.junioActual != '')
        Total[13] += Number(element.junioActual);
      if(element.julio != null || element.julio != '')
        Total[14] += Number(element.julio);
      if(element.julioActual != null || element.julioActual != '')
        Total[15] += Number(element.julioActual);
      if(element.agosto != null || element.agosto != '')
        Total[16] += Number(element.agosto);
      if(element.agostoActual != null || element.agostoActual != '')
        Total[17] += Number(element.agostoActual);
      if(element.septiembre != null || element.septiembre != '')
        Total[18] += Number(element.septiembre);
      if(element.septiembreActual != null || element.septiembreActual != '')
        Total[19] += Number(element.septiembreActual);
      if(element.octubre != null || element.octubre != '')
        Total[20] += Number(element.octubre);
      if(element.octubreActual != null || element.octubreActual != '')
        Total[21] += Number(element.octubreActual);
      if(element.noviembre != null || element.noviembre != '')
        Total[22] += Number(element.noviembre);
      if(element.noviembreActual != null || element.noviembreActual != '')
        Total[23] += Number(element.noviembreActual);
      if(element.diciembre != null || element.diciembre != '')
        Total[24] += Number(element.diciembre);
      if(element.diciembreActual != null || element.diciembreActual != '')
        Total[25] += Number(element.diciembreActual);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1){
        sums[index] = ''
        return;
      }
      else if(index==3||index==5||index==7||index==9 ||index==11||index==13||index==15||index===17||index==19||
        index==21||index==23||index ==25){
          var formatter = new Intl.NumberFormat('es-PE', {
            maximumFractionDigits: 1,
          });

          sums[index] = formatter.format(Total[index]/1000);
          return;
        }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          // style: 'currency',
          // currency: 'USD',
          maximumFractionDigits: 2,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  handleClick(tab, event) {
    if(tab.name=="first"){
      // this.BuscarReporte();
    }
    if(tab.name=='second'){
      // this.BuscarLotes();
    }
  }

  // REPORTE DIARIO
  BuscarLotes(){
    this.loadingRepDiario = true;
    if(this.checkedSearch === true){
      this.FormSearchRD.strFechaInicio = '*';
      this.FormSearchRD.strFechaFin = '*';
    }
    else {
      this.FormSearchRD.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearchRD.strFechaFin = (this.fechaFin===undefined||this.fechaFin.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    ReporteService.GetReporteDiarioRobin(this.FormSearchRD)
    .then(response =>{
      this.CompleteData = response.Data;
      this.totalRegistros = response.Count;
      this.countGridData = response.Count;
      this.gridDataRepDiario = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
      this.changeDateString(this.CompleteData);
      this.sumarPesoTMH(this.CompleteData);
      this.loadingRepDiario = false;
    }).catch(e =>{
      console.log(e);
      this.loadingRepDiario = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar lotes.');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  sumarPesoTMH(data){
    var PesoTotal = 0;
    var PesoSecoTotal = 0;
    data.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        PesoTotal += element.fltPesoTmh;
      if(element.fltPSecoTM != null || element.fltPSecoTM != '')
        PesoSecoTotal += element.fltPSecoTM;
    });
    this.sumatorioPesoTMH = this.getNumberFix(PesoTotal, 3);
    this.sumatorioPSecoTM = this.getNumberFix(PesoSecoTotal, 3);
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.CompleteData[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }
  cambioPagina(){
    this.gridDataRepDiario = this.CompleteData.slice(this.RegistersForPage*(this.pagina-1), this.RegistersForPage*(this.pagina));
  }
  LimpiarConsulta(){
    this.FormSearchRD.strLote = '';
    this.FormSearchRD.strProcedencia = '';
    this.FormSearchRD.strAcopiador = '';
    this.FormSearchRD.strProveedor = '';
    this.FormSearchRD.strMaterial = '';
    this.FormSearchRD.strEstadoLote = '';
    this.FormSearchRD.strFechaInicio = '';
    this.FormSearchRD.strFechaFin = '';
  }
  getCheckEstado(estado){
    if(estado === 'true') return true;
    else return false;
  }

  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  changeMonth(){
    var fSearch = this.fechaSearchRD;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }
  getDefaultSubTotal(){
    var DataSubTotal:any = [
      {strZona: '', strAnio:'SUB TOTAL', enero:'ENERO',eneroFino:'', febrero:'FEBRERO',febreroFino:'', marzo:'MARZO',marzoFino:'', abril:'ABRIL',abrilFino:'', mayo:'MAYO', mayoFino:'', junio:'JUNIO',junioFino:'', julio:'JULIO',julioFino:'',agosto:'AGOSTO',agostoFino:'', septiembre:'SEPTIEMBRE',septiembreFino:'', octubre:'OCTUBRE',octubreFino:'', noviembre:'NOVIEMBRE',noviembreFino:'', diciembre:'DICIEMBRE',diciembreFino:''},
      {strZona: '', strAnio:'SUR', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
      {strZona: '', strAnio:'NORTE', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
      {strZona: '', strAnio:'SECOCHA', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
    ]
    return DataSubTotal;
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      DataSubTotal:[
        {strZona: '', strAnio:'SUB TOTAL', enero:'ENERO',eneroFino:'', febrero:'FEBRERO',febreroFino:'', marzo:'MARZO',marzoFino:'', abril:'ABRIL',abrilFino:'', mayo:'MAYO', mayoFino:'', junio:'JUNIO',junioFino:'', julio:'JULIO',julioFino:'',agosto:'AGOSTO',agostoFino:'', septiembre:'SEPTIEMBRE',septiembreFino:'', octubre:'OCTUBRE',octubreFino:'', noviembre:'NOVIEMBRE',noviembreFino:'', diciembre:'DICIEMBRE',diciembreFino:''},
        {strZona: '', strAnio:'SUR', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
        {strZona: '', strAnio:'NORTE', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
        {strZona: '', strAnio:'SECOCHA', enero:'',eneroFino:'', febrero:'',febreroFino:'', marzo:'',marzoFino:'', abril:'',abrilFino:'', mayo:'', mayoFino:'', junio:'',junioFino:'', julio:'',julioFino:'', agosto:'',agostoFino:'', septiembre:'',septiembreFino:'', octubre:'',octubreFino:'', noviembre:'',noviembreFino:'', diciembre:'',diciembreFino:''},
      ],
      FormSearchRD:{
        strLote : '',
        strProcedencia : '',
        strAcopiador : '',
        strProveedor : '',
        strMaterial : '',
        strEstadoLote : '',
        strFechaInicio : '',
        strFechaFin : ''
      },
      dataEstado:[
        {id_estado: 'F', name: 'FACTURADOS'},
        {id_estado: 'E', name: 'ENTREGADOS'},
        {id_estado: 'R', name: 'REINTEGROS'},
        {id_estado: 'P', name: 'PENDIENTES'},
        {id_estado: 'RT', name: 'RETIRADOS'},
      ],
      json_fields : {
        "strZona" : "Zona",
        "strAnio" : "Año",
        "enero" : "ENERO",
        "febrero" : "FEBRERO",
        "marzo" : "MARZO",
        "abril" : "ABRIL",
        "mayo" : "MAYO",
        "junio" : "JUNIO",
        "julio" : "JULIO",
        "agosto" : "AGOSTO",
        "septiembre" : "SEPTIEMBRE",
        "octubre" : "OCTUBRE",
        "noviembre" : "NOVIEMBRE",
        "diciembre" : "DICIEMBRE",
      },
      json_fieldsDet : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strZona" : "Zona",
        "strAcopiador" : "Acopiador",
        "strProveedor" : "Proveedor",
        "fltPesoTmh" : "Peso TMH",
        "fltPSecoTM" : "P.Seco TM",
        "fltLeyAuOz" : "Ley Au Oz/tc",
        "fltLeyAgOz" : "Ley Ag Oz/Tc",
        "fltFinoAuGr" : "Fino(Au/Gr)",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    }
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
