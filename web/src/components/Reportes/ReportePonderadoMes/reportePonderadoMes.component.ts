import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReporteService from '@/services/reportes.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reportePonderadoMes',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ReportePonderadoMesComponent extends Vue {
  fechaSearch: any = new Date();
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';
  userDec: any = {};
  loadingGetData:boolean = false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1017){
          flag=true;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.sizeScreen = (window.innerHeight - 200).toString();
      }
    }
  }

  BuscarReporte(){
    if(this.fechaSearch===undefined || this.fechaSearch.toString()===''){
      this.openMessageError('Seleccione año');
    }
    else{
      this.loadingGetData = true;
      var year = (this.fechaSearch===undefined||this.fechaSearch.toString()==='') ?'*' : this.fechaSearch.getFullYear().toString();
      ReporteService.GetReportePonderadoMes(year)
      .then(response =>{
        this.gridData = response.Data
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte');
      })
    }
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {

    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
