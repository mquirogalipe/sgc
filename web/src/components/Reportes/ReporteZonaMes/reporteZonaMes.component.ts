import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReporteService from '@/services/reportes.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reporteZonaMes',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
     'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class ReporteZonaMesComponent extends Vue {
  fechaSearch: any = new Date();
  gridData: any = [];
  CompleteData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';
  userDec: any = {};
  loadingGetData:boolean = false;
  DataSubTotal: any = [];
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  titleExcel:string ='Reporte Zona_Mes_'+CONFIG.DATE_STRING+'.xls';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1016){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.sizeScreen = (window.innerHeight - 200).toString();
      }
    }
  }

  BuscarReporte(){
    this.loadingGetData = true;
    var year = (this.fechaSearch===undefined||this.fechaSearch.toString()==='') ?'*' : this.fechaSearch.getFullYear().toString();
    ReporteService.GetReporteZonaMes(year)
    .then(response =>{
      this.CompleteData = [];
      this.gridData = response.Data;
      // this.CompleteData = response.Data;
      this.getSubTotales(this.gridData);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte');
    })
  }
  getSubTotales(Data){
    var DataTemp: any = [];
    var subTAux: any = [
      {strZona: '',strAnio: '',enero: '',febrero: '',marzo: '',abril: '',mayo: '',junio: '',julio: '',agosto: '',septiembre: '',octubre: '',noviembre: '',diciembre: ''},
      {strZona: 'SUBTOTAL',strAnio: '',enero: '',febrero: '',marzo: '',abril: '',mayo: '',junio: '',julio: '',agosto: '',septiembre: '',octubre: '',noviembre: '',diciembre: ''},
    ]
    DataTemp = Data.concat(subTAux);
    this.DataSubTotal = [
      {strZona: '', strAnio:'SUB TOTAL', enero:'ENERO', febrero:'FEBRERO', marzo:'MARZO', abril:'ABRIL', mayo:'MAYO', junio:'JUNIO', julio:'JULIO', agosto:'AGOSTO', septiembre:'SEPTIEMBRE', octubre:'OCTUBRE', noviembre:'NOVIEMBRE', diciembre:'DICIEMBRE'},
      {strZona: '', strAnio:'SUR', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''},
      {strZona: '', strAnio:'NORTE', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''},
      {strZona: '', strAnio:'SECOCHA', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''}
    ];
    for (let i = 0; i < Data.length; i++) {
      if(Data[i].strRegion === 'SUR'){
        this.DataSubTotal[1].enero = Number(this.DataSubTotal[1].enero) + Data[i].enero;
        this.DataSubTotal[1].febrero = Number(this.DataSubTotal[1].febrero) + Data[i].febrero;
        this.DataSubTotal[1].marzo = Number(this.DataSubTotal[1].marzo) + Data[i].marzo;
        this.DataSubTotal[1].abril = Number(this.DataSubTotal[1].abril) + Data[i].abril;
        this.DataSubTotal[1].mayo = Number(this.DataSubTotal[1].mayo) + Data[i].mayo;
        this.DataSubTotal[1].junio = Number(this.DataSubTotal[1].junio) + Data[i].junio;
        this.DataSubTotal[1].julio = Number(this.DataSubTotal[1].julio) + Data[i].julio;
        this.DataSubTotal[1].agosto = Number(this.DataSubTotal[1].agosto) + Data[i].agosto;
        this.DataSubTotal[1].septiembre = Number(this.DataSubTotal[1].septiembre) + Data[i].septiembre;
        this.DataSubTotal[1].octubre = Number(this.DataSubTotal[1].octubre) + Data[i].octubre;
        this.DataSubTotal[1].noviembre = Number(this.DataSubTotal[1].noviembre) + Data[i].noviembre;
        this.DataSubTotal[1].diciembre = Number(this.DataSubTotal[1].diciembre) + Data[i].diciembre;
      }
      else if(Data[i].strRegion === 'TRUJILLO'){
        this.DataSubTotal[2].enero = Number(this.DataSubTotal[2].enero) + Data[i].enero;
        this.DataSubTotal[2].febrero = Number(this.DataSubTotal[2].febrero) + Data[i].febrero;
        this.DataSubTotal[2].marzo = Number(this.DataSubTotal[2].marzo) + Data[i].marzo;
        this.DataSubTotal[2].abril = Number(this.DataSubTotal[2].abril) + Data[i].abril;
        this.DataSubTotal[2].mayo = Number(this.DataSubTotal[2].mayo) + Data[i].mayo;
        this.DataSubTotal[2].junio = Number(this.DataSubTotal[2].junio) + Data[i].junio;
        this.DataSubTotal[2].julio = Number(this.DataSubTotal[2].julio) + Data[i].julio;
        this.DataSubTotal[2].agosto = Number(this.DataSubTotal[2].agosto) + Data[i].agosto;
        this.DataSubTotal[2].septiembre = Number(this.DataSubTotal[2].septiembre) + Data[i].septiembre;
        this.DataSubTotal[2].octubre = Number(this.DataSubTotal[2].octubre) + Data[i].octubre;
        this.DataSubTotal[2].noviembre = Number(this.DataSubTotal[2].noviembre) + Data[i].noviembre;
        this.DataSubTotal[2].diciembre = Number(this.DataSubTotal[2].diciembre) + Data[i].diciembre;
      }
      else if(Data[i].strRegion === 'SECOCHA'){
        this.DataSubTotal[3].enero = Number(this.DataSubTotal[3].enero) + Data[i].enero;
        this.DataSubTotal[3].febrero = Number(this.DataSubTotal[3].febrero) + Data[i].febrero;
        this.DataSubTotal[3].marzo = Number(this.DataSubTotal[3].marzo) + Data[i].marzo;
        this.DataSubTotal[3].abril = Number(this.DataSubTotal[3].abril) + Data[i].abril;
        this.DataSubTotal[3].mayo = Number(this.DataSubTotal[3].mayo) + Data[i].mayo;
        this.DataSubTotal[3].junio = Number(this.DataSubTotal[3].junio) + Data[i].junio;
        this.DataSubTotal[3].julio = Number(this.DataSubTotal[3].julio) + Data[i].julio;
        this.DataSubTotal[3].agosto = Number(this.DataSubTotal[3].agosto) + Data[i].agosto;
        this.DataSubTotal[3].septiembre = Number(this.DataSubTotal[3].septiembre) + Data[i].septiembre;
        this.DataSubTotal[3].octubre = Number(this.DataSubTotal[3].octubre) + Data[i].octubre;
        this.DataSubTotal[3].noviembre = Number(this.DataSubTotal[3].noviembre) + Data[i].noviembre;
        this.DataSubTotal[3].diciembre = Number(this.DataSubTotal[3].diciembre) + Data[i].diciembre;
      }
    }
    this.CompleteData = DataTemp.concat(this.DataSubTotal.slice(1, 4));

    if(this.viewAccessEscritura === true){
      for (let i = 0; i < this.gridData.length; i++) {
        this.CompleteData[i].enero = (this.CompleteData[i].enero === null)? '' : this.CompleteData[i].enero;
        this.CompleteData[i].febrero = (this.CompleteData[i].febrero === null)? '' : this.CompleteData[i].febrero;
        this.CompleteData[i].marzo = (this.CompleteData[i].marzo === null)? '' : this.CompleteData[i].marzo;
        this.CompleteData[i].abril = (this.CompleteData[i].abril === null)? '' : this.CompleteData[i].abril;
        this.CompleteData[i].mayo = (this.CompleteData[i].mayo === null)? '' : this.CompleteData[i].mayo;
        this.CompleteData[i].junio = (this.CompleteData[i].junio === null)? '' : this.CompleteData[i].junio;
        this.CompleteData[i].julio = (this.CompleteData[i].julio === null)? '' : this.CompleteData[i].julio;
        this.CompleteData[i].agosto = (this.CompleteData[i].agosto === null)? '' : this.CompleteData[i].agosto;
        this.CompleteData[i].septiembre = (this.CompleteData[i].septiembre === null)? '' : this.CompleteData[i].septiembre;
        this.CompleteData[i].octubre = (this.CompleteData[i].octubre === null)? '' : this.CompleteData[i].octubre;
        this.CompleteData[i].noviembre = (this.CompleteData[i].noviembre === null)? '' : this.CompleteData[i].noviembre;
        this.CompleteData[i].diciembre = (this.CompleteData[i].diciembre === null)? '' : this.CompleteData[i].diciembre;
      }
    }
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.enero != null || element.enero != '')
        Total[2] += Number(element.enero);
      if(element.febrero != null || element.febrero != '')
        Total[3] += Number(element.febrero);
      if(element.marzo != null || element.marzo != '')
        Total[4] += Number(element.marzo);
      if(element.abril != null || element.abril != '')
        Total[5] += Number(element.abril);
      if(element.mayo != null || element.mayo != '')
        Total[6] += Number(element.mayo);
      if(element.junio != null || element.junio != '')
        Total[7] += Number(element.junio);
      if(element.julio != null || element.julio != '')
        Total[8] += Number(element.julio);
      if(element.agosto != null || element.agosto != '')
        Total[9] += Number(element.agosto);
      if(element.septiembre != null || element.septiembre != '')
        Total[10] += Number(element.septiembre);
      if(element.octubre != null || element.octubre != '')
        Total[11] += Number(element.octubre);
      if(element.noviembre != null || element.noviembre != '')
        Total[12] += Number(element.noviembre);
      if(element.diciembre != null || element.diciembre != '')
        Total[13] += Number(element.diciembre);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1){
        sums[index] = ''
        return;
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          // style: 'currency',
          // currency: 'USD',
          minimumFractionDigits: 2,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      DataSubTotal:[
        {strZona: '', strAnio:'SUB TOTAL', enero:'ENERO', febrero:'FEBRERO', marzo:'MARZO', abril:'ABRIL', mayo:'MAYO', junio:'JUNIO', julio:'JULIO', agosto:'AGOSTO', septiembre:'SEPTIEMBRE', octubre:'OCTUBRE', noviembre:'NOVIEMBRE', diciembre:'DICIEMBRE'},
        {strZona: '', strAnio:'SUR', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''},
        {strZona: '', strAnio:'NORTE', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''},
        {strZona: '', strAnio:'SECOCHA', enero:'', febrero:'', marzo:'', abril:'', mayo:'', junio:'', julio:'', agosto:'', septiembre:'', octubre:'', noviembre:'', diciembre:''}
      ],
      json_fields : {
        "strZona" : "Zona",
        "strAnio" : "Año",
        "enero" : "ENERO",
        "febrero" : "FEBRERO",
        "marzo" : "MARZO",
        "abril" : "ABRIL",
        "mayo" : "MAYO",
        "junio" : "JUNIO",
        "julio" : "JULIO",
        "agosto" : "AGOSTO",
        "septiembre" : "SEPTIEMBRE",
        "octubre" : "OCTUBRE",
        "noviembre" : "NOVIEMBRE",
        "diciembre" : "DICIEMBRE",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    }
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
