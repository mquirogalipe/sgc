import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReportesService from '@/services/reportes.service';
import ZonaService from '@/services/zona.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'margenLiquidacion',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
   'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class MargenLiquidacionComponent extends Vue {
  gridData: any = [];
  loadingGetData: boolean = false;
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';;
  fechaSearch: Date = new Date();
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;

  //DETALLADO
  gridDataDetallado: any = [];
  loadingGetDataDet: boolean = false;
  fechaSearchDet: Date = new Date();
  FormSearchDet: any = {};
  dataZonas: any = [];
  DataSubTotal: any = [
    {Total:'', PesoTmh:'PESO TMH', Utilidad:'UTILIDAD', MaquilaAu:'MAQUILA', ConsQAu:'CONS.Q', GastoAdmAu: 'GASTO ADM.', MaquilaIntAu: 'MAQUILA INT', ConsQIntAu: 'CONS.Q INT', GastoAdmIntAu: 'GASTO ADM. INT'},
    {Total:'SUMA:', PesoTmh:'', Utilidad:'', MaquilaAu:'', ConsQAu:'', GastoAdmAu: '', MaquilaIntAu: '', ConsQIntAu: '', GastoAdmIntAu: ''}
  ];
  
  blnperiodo:boolean=false;
  blnfiltroavanzado:boolean=false;
  constructor () {
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1029){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else {
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.getZonas();
      }
    }
  }
  BuscarReporte(){
    var fSearch = this.fechaSearch;
    if(fSearch ==null || fSearch == undefined || fSearch.toString() == '')
      this.openMessageError('Seleccione Mes');
    else{
      this.loadingGetData = true;
      var bFechaIni = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1));
      var bFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0));

      var query = {
        strFechaInicio: bFechaIni,
        strFechaFin: bFechaFin
      }
      ReportesService.GetReporteMargenes(query)
      .then(response =>{
        this.gridData = response.Data;
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte.');
      })
    }
  }
  BuscarReporteDetallado(){
    var fSearch = this.fechaSearchDet;
    if(fSearch ==null || fSearch == undefined || fSearch.toString() == '')
      this.openMessageError('Seleccione Mes');
    else{
      this.loadingGetDataDet = true;

      this.FormSearchDet.strFechaInicio =  (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1)),
      this.FormSearchDet.strFechaFin = (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0))
      ReportesService.GetReporteMargenDetallado(this.FormSearchDet)
      .then(response =>{
        this.gridDataDetallado = response.Data;
        this.CalcularSubTotales(response.Data);
        this.loadingGetDataDet = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetDataDet = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte.');
      })
    }
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  CalcularSubTotales(Data){
    this.DataSubTotal[1].PesoTmh = 0;
    this.DataSubTotal[1].Utilidad = 0;
    this.DataSubTotal[1].MaquilaAu = 0;
    this.DataSubTotal[1].ConsQAu = 0;
    this.DataSubTotal[1].GastoAdmAu = 0;
    this.DataSubTotal[1].MaquilaIntAu = 0;
    this.DataSubTotal[1].ConsQIntAu = 0;
    this.DataSubTotal[1].GastoAdmIntAu = 0;
    for (let i = 0; i < Data.length; i++) {
        this.DataSubTotal[1].PesoTmh += Data[i].fltPesoTmh;
        this.DataSubTotal[1].Utilidad += Data[i].fltUtilidadBru;
        this.DataSubTotal[1].MaquilaAu += Data[i].fltMaquila;
        this.DataSubTotal[1].ConsQAu += Data[i].fltConsumoQ;
        this.DataSubTotal[1].GastoAdmAu += Data[i].fltGastoAdm
        this.DataSubTotal[1].MaquilaIntAu += Data[i].fltMaquilaIntAu
        this.DataSubTotal[1].ConsQIntAu += Data[i].fltConsQIntAu
        this.DataSubTotal[1].GastoAdmIntAu += Data[i].fltGastoAdmIntAu
    }
  }

  handleClick(tab, event) {
    // console.log(tab, event);
  }

  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getPorcentageFix(numero, fix:number){
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
    var num =  formatter.format(numero);
    return num;
  }

  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    dataReport.forEach(function(element) {
      if(element.fltPesoTMHSum != null || element.fltPesoTMHSum != '')
        Total[1] += element.fltPesoTMHSum;
      if(element.fltUtilidadBruSum != null || element.fltUtilidadBruSum != '')
        Total[2] += element.fltUtilidadBruSum;
      if(element.fltMargenBruProm != null || element.fltMargenBruProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[3] += (element.fltUtilidadBruSum * element.fltMargenBruProm);
      if(element.fltPrecioInterAu != null || element.fltPrecioInterAu != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[4] += (element.fltPesoTMHSum * element.fltPrecioInterAu);
      if(element.fltRecuperacionProm != null || element.fltRecuperacionProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[5] += (element.fltPesoTMHSum * element.fltRecuperacionProm);
      if(element.fltMaquilaProm != null || element.fltMaquilaProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[6] += (element.fltPesoTMHSum * element.fltMaquilaProm);
      if(element.fltMargenPIProm != null || element.fltMargenPIProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[7] += (element.fltPesoTMHSum * element.fltMargenPIProm);
      if(element.fltConsumoQProm != null || element.fltConsumoQProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[8] += (element.fltPesoTMHSum * element.fltConsumoQProm);
      if(element.fltGastoAdmProm != null || element.fltGastoAdmProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[9] += (element.fltPesoTMHSum * element.fltGastoAdmProm);
      // if(element.fltCostoTotalSum != null || element.fltCostoTotalSum != '')
      //   Total[10] += element.fltCostoTotalSum;
      if(element.fltRecIntAuProm != null || element.fltRecIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[10] += (element.fltPesoTMHSum * element.fltRecIntAuProm);
      if(element.fltMaquilaIntAuProm != null || element.fltMaquilaIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[11] += (element.fltPesoTMHSum * element.fltMaquilaIntAuProm);
      if(element.fltConsQIntAuProm != null || element.fltConsQIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[12] += (element.fltPesoTMHSum * element.fltConsQIntAuProm);
      if(element.fltGastoAdmIntAuProm != null || element.fltGastoAdmIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[13] += (element.fltPesoTMHSum * element.fltGastoAdmIntAuProm);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1 || index === 2){
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
      else if(index === 3){
        var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
        sums[index] = formatter.format(Total[index]/Total[2]);
        return;
      }
      else{
        sums[index] = this.getNumberFix(Total[index]/Total[1], 2);
        return;
      }
    });
    return sums;
  }
  getSummariesDet(param) {
    var dataReport: any = this.gridDataDetallado;
    var Total: any = ['', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    dataReport.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        Total[4] += element.fltPesoTmh;
      if(element.strPSecoTM != null || element.strPSecoTM != '')
        Total[5] += Number(element.strPSecoTM);
      if(element.fltUtilidadBru != null || element.fltUtilidadBru != '')
        Total[6] += element.fltUtilidadBru;
      if(element.fltMargenBru != null || element.fltMargenBru != '' && (element.fltUtilidadBru != null || element.fltUtilidadBru != ''))
        Total[7] += (element.fltUtilidadBru * element.fltMargenBru);
      if(element.fltPrecioInterAu != null || element.fltPrecioInterAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[8] += (element.fltPesoTmh * element.fltPrecioInterAu);
      if(element.fltRecuperacion != null || element.fltRecuperacion != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[9] += (element.fltPesoTmh * element.fltRecuperacion);
      if(element.fltMaquila != null || element.fltMaquila != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[10] += (element.fltPesoTmh * element.fltMaquila);
      if(element.fltMargenPI != null || element.fltMargenPI != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[11] += (element.fltPesoTmh * element.fltMargenPI);
      if(element.fltConsumoQ != null || element.fltConsumoQ != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[12] += (element.fltPesoTmh * element.fltConsumoQ);
      if(element.fltGastoAdm != null || element.fltGastoAdm != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[13] += (element.fltPesoTmh * element.fltGastoAdm);
      if(element.fltCostoTotal != null || element.fltCostoTotal != '')
        Total[14] += element.fltCostoTotal;
      if(element.fltRecIntAu != null || element.fltRecIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[15] += (element.fltPesoTmh * element.fltRecIntAu);
      if(element.fltMaquilaIntAu != null || element.fltMaquilaIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[16] += (element.fltPesoTmh * element.fltMaquilaIntAu);
      if(element.fltConsQIntAu != null || element.fltConsQIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[17] += (element.fltPesoTmh * element.fltConsQIntAu);
      if(element.fltGastoAdmIntAu != null || element.fltGastoAdmIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[18] += (element.fltPesoTmh * element.fltGastoAdmIntAu);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 1 || index === 2 || index === 3) {
        sums[index] = '';
        return;
      }
      else if(index === 4 || index === 5 || index === 6 || index === 14){
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
      else if(index === 7){
        var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
        sums[index] = formatter.format(Total[index]/Total[6]);
        return;
      }
      else{
        sums[index] = this.getNumberFix(Total[index]/Total[4], 2);
        return;
      }
    });
    return sums;
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {
      FormSearchDet: {
        strLote: '',
        strProcedenciaSt: '',
        strRegion: '',
        strFechaInicio: '',
        strFechaFin: '',
      },
    };
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
