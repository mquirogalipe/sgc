import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReportesService from '@/services/reportes.service';
import RumasService from '@/services/armadoRumas.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'margenProductivo',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
   'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class MargenProductivoComponent extends Vue {
  gridData: any = [];
  loadingGetData: boolean = false;
  sizeScreen:string = (window.innerHeight - 350).toString();
  fechaSearch: Date = new Date();
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  RumasSelected: any = [];
  DataRumas: any = [];
  MargenPeriodo: Date = new Date();
  MargenCampania: string = '';

  //DETALLADO
  gridDataDetallado: any = [];
  loadingGetDataDet: boolean = false;
  fechaSearchDet: Date = new Date();
  FormSearchDet: any = {};
  dataZonas: any = [];
  DataSubTotal: any = [
    {Total:'', PesoTmh:'PESO TMH', Utilidad:'UTILIDAD', MaquilaAu:'MAQUILA', ConsQAu:'CONS.Q', GastoAdmAu: 'GASTO ADM.', MaquilaIntAu: 'MAQUILA INT', ConsQIntAu: 'CONS.Q INT', GastoAdmIntAu: 'GASTO ADM. INT', CostoTotal: 'COSTO'},
    {Total:'SUMA:', PesoTmh:'', Utilidad:'', MaquilaAu:'', ConsQAu:'', GastoAdmAu: '', MaquilaIntAu: '', ConsQIntAu: '', GastoAdmIntAu: '', CostoTotal: ''}
  ];
  RumasSelectedDet: any = [];
  MargenPeriodoDet: Date = new Date();
  MargenCampaniaDet: string = '';
  constructor () {
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1030){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else {
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.ObtenerRumas();
      }
    }
  }
  BuscarReporte(){
    var fSearch = this.fechaSearch;
    if(fSearch ==null || fSearch == undefined || fSearch.toString() == '')
      this.openMessageError('Seleccione fecha');
    else{
      this.loadingGetData = true;
      var _per = (this.MargenPeriodo==null||this.MargenPeriodo==undefined||this.MargenPeriodo.toString()=='')?'' : GLOBAL.getPeriodoFormat(this.MargenPeriodo)
      var abc = " AND (dbo.tblRumas.strMesContable LIKE '%"+ _per +
                "%') AND (dbo.tblRumas.strNroCampania LIKE '%"+this.MargenCampania+"%')"
      var _rumas = " AND dbo.tblRumas.strNroRuma IN ('" + this.RumasSelected.join("','") + "')"
      var query = {
        strQuery: (this.RumasSelected.length ===0)? abc : abc + _rumas
      }
      ReportesService.GetReporteMargenProductivo(query)
      .then(response =>{
        this.gridData = response.Data;
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte.');
      })
    }
  }
  BuscarReporteDetallado(){
    var fSearch = this.fechaSearchDet;
    if(fSearch ==null || fSearch == undefined || fSearch.toString() == '')
      this.openMessageError('Seleccione Mes');
    else{
      this.loadingGetDataDet = true;
      var _per = (this.MargenPeriodoDet==null||this.MargenPeriodoDet==undefined||this.MargenPeriodoDet.toString()=='')?'' : GLOBAL.getPeriodoFormat(this.MargenPeriodoDet)
      var abc = " AND (dbo.tblRumas.strMesContable LIKE '%"+ _per+
                "%') AND (dbo.tblRumas.strNroCampania LIKE '%"+this.MargenCampaniaDet+"%')"
      var _rumas = " AND dbo.tblRumas.strNroRuma IN ('" + this.RumasSelectedDet.join("','") + "')"
      var query = {
        strQuery: (this.RumasSelectedDet.length ===0)? abc : abc + _rumas
      }
      ReportesService.GetReporteMargenProdDetallado(query)
      .then(response =>{
        this.gridDataDetallado = response.Data;
        this.CalcularSubTotales(response.Data);
        this.loadingGetDataDet = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetDataDet = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte.');
      })
    }
  }
  ObtenerRumas(){
    var Data = {
      strFechaInicio: '*',
      strFechaFin: '*'
    }
    RumasService.GetAllRumas(Data)
    .then(response =>{
      this.DataRumas = response.Data;
    })
    .catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte.');
    })
  }
  CalcularSubTotales(Data){
    this.DataSubTotal[1].PesoTmh = 0;
    this.DataSubTotal[1].Utilidad = 0;
    this.DataSubTotal[1].MaquilaAu = 0;
    this.DataSubTotal[1].ConsQAu = 0;
    this.DataSubTotal[1].GastoAdmAu = 0;
    this.DataSubTotal[1].MaquilaIntAu = 0;
    this.DataSubTotal[1].ConsQIntAu = 0;
    this.DataSubTotal[1].GastoAdmIntAu = 0;
    this.DataSubTotal[1].CostoTotal = 0;
    for (let i = 0; i < Data.length; i++) {
        this.DataSubTotal[1].PesoTmh += Data[i].fltPesoTmh;
        this.DataSubTotal[1].Utilidad += Data[i].fltUtilidadBru;
        this.DataSubTotal[1].MaquilaAu += Data[i].fltMaquila;
        this.DataSubTotal[1].ConsQAu += Data[i].fltConsumoQ;
        this.DataSubTotal[1].GastoAdmAu += Data[i].fltGastoAdm;
        this.DataSubTotal[1].MaquilaIntAu += Data[i].fltMaquilaIntAu;
        this.DataSubTotal[1].ConsQIntAu += Data[i].fltConsQIntAu;
        this.DataSubTotal[1].GastoAdmIntAu += Data[i].fltGastoAdmIntAu;
        this.DataSubTotal[1].CostoTotal += Data[i].fltCostoTotal;
    }
  }

  handleClick(tab, event) {
    // console.log(tab, event);
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['',0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    dataReport.forEach(function(element) {
      if(element.fltPesoTMHSum != null || element.fltPesoTMHSum != '')
        Total[1] += element.fltPesoTMHSum;
      if(element.fltUtilidadBruSum != null || element.fltUtilidadBruSum != '')
        Total[2] += element.fltUtilidadBruSum;
      if(element.fltMargenBruProm != null || element.fltMargenBruProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[3] += (element.fltUtilidadBruSum * element.fltMargenBruProm);
      if(element.fltPrecioInterAu != null || element.fltPrecioInterAu != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[4] += (element.fltPesoTMHSum * element.fltPrecioInterAu);
      if(element.fltRecuperacionProm != null || element.fltRecuperacionProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[5] += (element.fltPesoTMHSum * element.fltRecuperacionProm);
      if(element.fltMaquilaProm != null || element.fltMaquilaProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[6] += (element.fltPesoTMHSum * element.fltMaquilaProm);
      if(element.fltMargenPIProm != null || element.fltMargenPIProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[7] += (element.fltPesoTMHSum * element.fltMargenPIProm);
      if(element.fltConsumoQProm != null || element.fltConsumoQProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[8] += (element.fltPesoTMHSum * element.fltConsumoQProm);
      if(element.fltGastoAdmProm != null || element.fltGastoAdmProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[9] += (element.fltPesoTMHSum * element.fltGastoAdmProm);
      if(element.fltCostoTotalSum != null || element.fltCostoTotalSum != '')
        Total[10] += element.fltCostoTotalSum;
      if(element.fltRecIntAuProm != null || element.fltRecIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[11] += (element.fltPesoTMHSum * element.fltRecIntAuProm);
      if(element.fltMaquilaIntAuProm != null || element.fltMaquilaIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[12] += (element.fltPesoTMHSum * element.fltMaquilaIntAuProm);
      if(element.fltConsQIntAuProm != null || element.fltConsQIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[13] += (element.fltPesoTMHSum * element.fltConsQIntAuProm);
      if(element.fltGastoAdmIntAuProm != null || element.fltGastoAdmIntAuProm != '' && (element.fltPesoTMHSum != null || element.fltPesoTMHSum != ''))
        Total[14] += (element.fltPesoTMHSum * element.fltGastoAdmIntAuProm);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1 || index === 2 || index === 10){
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
      else if(index === 3){
        var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
        sums[index] = formatter.format(Total[index]/Total[2]);
        return;
      }
      else{
        sums[index] = this.getNumberFix(Total[index]/Total[1], 2);
        return;
      }
    });
    return sums;
  }
  getSummariesDet(param) {
    var dataReport: any = this.gridDataDetallado;
    var Total: any = ['', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    dataReport.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        Total[7] += element.fltPesoTmh;
      if(element.fltUtilidadBru != null || element.fltUtilidadBru != '')
        Total[8] += element.fltUtilidadBru;
      if(element.fltMargenBru != null || element.fltMargenBru != '' && (element.fltUtilidadBru != null || element.fltUtilidadBru != ''))
        Total[9] += (element.fltUtilidadBru * element.fltMargenBru);
      if(element.fltPrecioInterAu != null || element.fltPrecioInterAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[10] += (element.fltPesoTmh * element.fltPrecioInterAu);
      if(element.fltRecuperacion != null || element.fltRecuperacion != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[11] += (element.fltPesoTmh * element.fltRecuperacion);
      if(element.fltMaquila != null || element.fltMaquila != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[12] += (element.fltPesoTmh * element.fltMaquila);
      if(element.fltMargenPI != null || element.fltMargenPI != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[13] += (element.fltPesoTmh * element.fltMargenPI);
      if(element.fltConsumoQ != null || element.fltConsumoQ != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[14] += (element.fltPesoTmh * element.fltConsumoQ);
      if(element.fltGastoAdm != null || element.fltGastoAdm != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[15] += (element.fltPesoTmh * element.fltGastoAdm);
      if(element.fltCostoTotal != null || element.fltCostoTotal != '')
        Total[16] += element.fltCostoTotal;
      if(element.fltRecIntAu != null || element.fltRecIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[17] += (element.fltPesoTmh * element.fltRecIntAu);
      if(element.fltMaquilaIntAu != null || element.fltMaquilaIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[18] += (element.fltPesoTmh * element.fltMaquilaIntAu);
      if(element.fltConsQIntAu != null || element.fltConsQIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[19] += (element.fltPesoTmh * element.fltConsQIntAu);
      if(element.fltGastoAdmIntAu != null || element.fltGastoAdmIntAu != '' && (element.fltPesoTmh != null || element.fltPesoTmh != ''))
        Total[20] += (element.fltPesoTmh * element.fltGastoAdmIntAu);
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 1 || index === 2 || index === 3 || index === 4 || index === 5 || index === 6) {
        sums[index] = '';
        return;
      }
      else if(index === 7 || index === 8 || index === 16){
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
      else if(index === 9){
        var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
        sums[index] = formatter.format(Total[index]/Total[8]);
        return;
      }
      else{
        sums[index] = this.getNumberFix(Total[index]/Total[7], 2);
        return;
      }
    });
    return sums;
  }

  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getPorcentageFix(numero, fix:number){
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 });
    var num =  formatter.format(numero);
    return num;
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }

  data() {
    return {

    };
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
