import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReportesService from '@/services/reportes.service'
import ZonaService from '@/services/zona.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
  name: 'ReporteAcopioMineral',
  components:{
    'download-excel' : JsonExcel,
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ReporteAcopioMineralComponent extends Vue {
  gridData: any = [];
  gridDataDetalle: any = [];
  gridReporteDetallado: any = [];
  FormSearch: any = {};
  FormSearchDet: any = {};
  fechaSearch: Date = new Date();
  fechaInicio: any = '';
  fechaFin: any = '';
  loadingGetData: boolean = false;
  loadingDetalleData: boolean = false;
  loadingReporteDetallado: boolean = false;
  dataZonas: any = [];
  titleExcel:string ='Reporte Acopio_'+CONFIG.DATE_STRING+'.xls';
  titleExcelDet:string ='Reporte AcopioDet_'+CONFIG.DATE_STRING+'.xls';
  sizeScreen:string = (window.innerHeight - 350).toString();
  checkedSearch: boolean = false;
  checkedSearchDetallado: boolean = false;
  dialogDetalleVisible: boolean = false;
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  NivelAcceso: string = '';
  userDec: any = {};
  viewAccessEscritura: boolean = false;
  subTotalSur: number = 0;
  subTotalNorte: number = 0;
  subTotalSecocha: number = 0;
  
  blnperiodo:boolean=false;
  blnfiltroavanzado:boolean=false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1006){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var date_actual = new Date();
        this.fechaInicio = new Date(date_actual.getFullYear(), date_actual.getMonth(), 1);
        this.fechaFin = new Date(date_actual.getFullYear(), date_actual.getMonth() + 1, 0);
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.getZonas();
      }
    }
  }
  BuscarReporte(){
    var fSearch = this.fechaSearch;
    this.gridData = [];
    this.loadingGetData = true;
    if(this.checkedSearch === true){
      this.FormSearch.strFechaInicio = '*';
      this.FormSearch.strFechaFin = '*';
    }
    else {
      this.FormSearch.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearch.strFechaFin = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    ReportesService.GetReporteAcopioMineral(this.FormSearch)
    .then(response =>{
      this.gridData = response.Data;
      this.getSubTotales(response.Data);
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte.');
    })
  }
  getSubTotales(Data){
    var _subSur:number = 0, _subNorte:number = 0, _subSecocha:number = 0;
    for (let i = 0; i < Data.length; i++) {
        if(Data[i].strRegion === 'SUR') { _subSur += Data[i].fltSumPesoTmh; }
        else if(Data[i].strRegion === 'TRUJILLO') { _subNorte += Data[i].fltSumPesoTmh; }
        else if(Data[i].strRegion === 'SECOCHA') { _subSecocha += Data[i].fltSumPesoTmh; }
    }
    this.subTotalSur = _subSur;
    this.subTotalNorte = _subNorte;
    this.subTotalSecocha = _subSecocha;
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  getDetalleReporte(acop, prov, zona){
    this.loadingDetalleData = true;
    var FormS: any = {
      strFechaInicio: '',
      strFechaFin: '',
      strAcopiador: acop,
      strProveedor: prov,
      strZona: zona,
    }
    if(this.checkedSearch === true){
      FormS.strFechaInicio = '*';
      FormS.strFechaFin = '*';
    }
    else {
      FormS.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      FormS.strFechaFin = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    ReportesService.GetDetalleReporteAcopio(FormS)
    .then(response =>{
      console.log(response);
      this.gridDataDetalle = response.Data;
      this.loadingDetalleData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingDetalleData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar detalle.');
    })
  }

  BuscarReporteDetallado(){
    this.loadingReporteDetallado = true;
    this.gridReporteDetallado = [];
    if(this.checkedSearchDetallado === true){
      this.FormSearchDet.strFechaInicio = '*';
      this.FormSearchDet.strFechaFin = '*';
    }
    else {
      this.FormSearchDet.strFechaInicio = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaInicio);
      this.FormSearchDet.strFechaFin = (this.fechaInicio===undefined||this.fechaInicio.toString()==='')?'*':GLOBAL.getParseDate(this.fechaFin);
    }
    ReportesService.GetReporteAcopioMRL_Detallado(this.FormSearchDet)
    .then(response =>{
      this.gridReporteDetallado = response.Data;
      this.changeDateString(this.gridReporteDetallado);
      this.loadingReporteDetallado = false;
    }).catch(e =>{
      console.log(e);
      this.loadingReporteDetallado = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar detalle.');
    })
  }
  changeDateString(Data){
    for (let i = 0; i < Data.length; i++) {
        this.gridReporteDetallado[i].dtmFechaRecepcion = new Date(Data[i].dtmFechaRecepcion).toLocaleDateString('es-PE', this.options);
    }
  }

  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  handleClick(tab, event) {
    // console.log(tab, event);
  }
  LimpiarConsulta(){
    this.FormSearch.strFechaInicio = '',
    this.FormSearch.strFechaFin = '',
    this.FormSearch.strAcopiador = '',
    this.FormSearch.strProveedor = '',
    this.FormSearch.strZona = ''
  }
  LimpiarConsultaDet(){
    this.FormSearchDet.strFechaInicio = '',
    this.FormSearchDet.strFechaFin = '',
    this.FormSearchDet.strAcopiador = '',
    this.FormSearchDet.strProveedor = '',
    this.FormSearchDet.strZona = ''
  }
  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','','','', 0, 0, 0, 0, 0];
    var TotalPSeco = 0;
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.fltSumPesoTmh != null || element.fltSumPesoTmh != '')
        Total[4] += element.fltSumPesoTmh;
      if(element.fltSumPSecoTM != null || element.fltSumPSecoTM != '')
        Total[5] += element.fltSumPSecoTM;
      if(element.fltPromLeyAuOzTc != null || element.fltPromLeyAuOzTc != '')
        Total[6] += element.fltPromLeyAuOzTc;
      if(element.fltPromLeyAgOzTc != null || element.fltPromLeyAgOzTc != '')
        Total[7] += element.fltPromLeyAgOzTc;
      if(element.fltPromFinoAuGr != null || element.fltPromFinoAuGr != '')
        Total[8] += element.fltPromFinoAuGr;
      if(element.fltSumPSecoTM != null || element.fltSumPSecoTM != '')
        TotalPSeco += element.fltSumPSecoTM;
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 1) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 0 || index === 2 || index === 3){
        sums[index] = ''
        return;
      }
      else if(index === 6 || index === 7){
        sums[index] = this.getNumberFloat(Total[index]/TotalPSeco);
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          // style: 'currency',
          // currency: 'USD',
          minimumFractionDigits: 2,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getSummDetalle(param){
    var dataReport: any = this.gridDataDetalle;
    var Total: any = ['','','','','','', 0, 0, 0, 0, 0];
    var TotalPSeco = 0;
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        Total[6] += element.fltPesoTmh;
      if(element.fltPSecoTM != null || element.fltPSecoTM != '')
        Total[7] += element.fltPSecoTM;
      if(element.fltLeyAuOz != null || element.fltLeyAuOz != '')
        Total[8] += element.fltLeyAuOz * element.fltPSecoTM;
      if(element.fltLeyAgOz != null || element.fltLeyAgOz != '')
        Total[9] += element.fltLeyAgOz * element.fltPSecoTM;
      if(element.fltFinoAuGr != null || element.fltFinoAuGr != '')
        Total[10] += element.fltFinoAuGr;
      if(element.fltPSecoTM != null || element.fltPSecoTM != '')
        TotalPSeco += element.fltPSecoTM;
    });
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 1 || index === 2 || index === 3 || index === 4 || index === 5){
        sums[index] = ''
        return;
      }
      else if(index === 8 || index === 9){
        sums[index] = this.getNumberFloat(Total[index]/TotalPSeco);
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getSummReporteDetallado(param){
    var dataReport: any = this.gridReporteDetallado;
    var Total: any = ['','','','','', 0, 0, 0, 0, 0];
    var TotalPSeco = 0;
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.fltPesoTmh != null || element.fltPesoTmh != '')
        Total[5] += element.fltPesoTmh;
      if(element.fltPSecoTM != null || element.fltPSecoTM != '')
        Total[6] += element.fltPSecoTM;
      if(element.fltLeyAuOz != null || element.fltLeyAuOz != '')
        Total[7] += element.fltLeyAuOz * element.fltPSecoTM;
      if(element.fltLeyAgOz != null || element.fltLeyAgOz != '')
        Total[8] += element.fltLeyAgOz * element.fltPSecoTM;
      if(element.fltFinoAuGr != null || element.fltFinoAuGr != '')
        Total[9] += element.fltFinoAuGr;
      if(element.fltPSecoTM != null || element.fltPSecoTM != '')
        TotalPSeco += element.fltPSecoTM;
    });
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      else if(index === 1 || index === 2 || index === 3 || index === 4){
        sums[index] = ''
        return;
      }
      else if(index === 7 || index === 8){
        sums[index] = this.getNumberFloat(Total[index]/TotalPSeco);
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          minimumFractionDigits: 2,
        });
        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  handleDetalleClick(index, row){
    this.getDetalleReporte(row.strAcopiador, row.strProveedor, row.strZona);
    this.dialogDetalleVisible = true;
  }

  getNumberFloat(number){
    if(number === null) return '';
    else {
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }
  getPromedioLeyAu(row){
    row.fltFinalLeyAuOzTc = row.fltPromLeyAuOzTc / row.fltSumPSecoTM;
    return parseFloat(row.fltFinalLeyAuOzTc).toFixed(3)
  }
  getPromedioLeyAg(row){
    row.fltFinalLeyAgOzTc = row.fltPromLeyAgOzTc / row.fltSumPSecoTM;
    return parseFloat(row.fltFinalLeyAgOzTc).toFixed(3)
  }

  changeMonth(){
    var fSearch = this.fechaSearch;
    this.fechaInicio = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth(), 1);
    this.fechaFin = (fSearch===undefined||fSearch.toString()==='') ?'': new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0);
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    if(event=='A'){
      this.blnfiltroavanzado=true;
    }
    if(event=='B'){
      this.blnfiltroavanzado=false;
    }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strFechaInicio:'',
        strFechaFin:'',
        strAcopiador:'',
        strProveedor:'',
        strZona:'',
        strEstadoLote:''
      },
      FormSearchDet:{
        strFechaInicio:'',
        strFechaFin:'',
        strLote: '',
        strAcopiador:'',
        strProveedor:'',
        strZona:''
      },
      dataEstado:[
        {id_estado: 'F', name: 'FACTURADOS'},
        {id_estado: 'E', name: 'ENTREGADOS'},
        {id_estado: 'R', name: 'REINTEGROS'},
        {id_estado: 'P', name: 'PENDIENTES'},
        {id_estado: 'RT', name: 'RETIRADOS'},
      ],
      json_fields : {
        "strZona" : "Zona",
        "strAcopiador" : "Acopiador",
        "strProveedor" : "Proveedor",
        "fltSumPesoTmh" : "Peso TMH Acumulado",
        "fltFinalLeyAuOzTc" : "Prom. Ley Au Oz/tc",
        "fltFinalLeyAgOzTc" : "Prom. Ley Ag Oz/Tc",
        "fltPromFinoAuGr" : "Prom. Fino(Au/Gr)",
      },
      json_fieldsDet : {
        "strLote" : "LOTE",
        "dtmFechaRecepcion" : "Fecha Recepción",
        "strZona" : "Zona",
        "strAcopiador" : "Acopiador",
        "strProveedor" : "Proveedor",
        "fltPesoTmh" : "Peso TMH",
        "fltPSecoTM" : "P.Seco TM",
        "fltLeyAuOz" : "Ley Au Oz/tc",
        "fltLeyAgOz" : "Ley Ag Oz/Tc",
        "fltFinoAuGr" : "Fino(Au/Gr)",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
