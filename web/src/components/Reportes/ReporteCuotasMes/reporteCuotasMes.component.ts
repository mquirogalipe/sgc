import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReporteService from '@/services/reportes.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reporteAcopioMes',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class ReporteAcopioMesComponent extends Vue {
  fechaSearch: any = new Date();
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';
  userDec: any = {};
  loadingGetData:boolean = false;
  titleExcel: string = 'Reporte_Cuotas_Mes_'+CONFIG.DATE_STRING+'.xls';;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1015){
          flag=true;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
      	this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.sizeScreen = (window.innerHeight - 200).toString();
      }
    }
  }

  BuscarReporte(){
    this.loadingGetData = true;
    if(this.fechaSearch == null || this.fechaSearch == undefined){
      this.openMessageError('Seleccione año');
    }
    else{
      // var year = this.fechaSearch.getFullYear().toString();
      var year = (this.fechaSearch===undefined||this.fechaSearch.toString()==='') ?'*' : this.fechaSearch.getFullYear().toString();
      ReporteService.GetReporteAcopiadorMes(year)
      .then(response =>{
        this.gridData = response.Data;
        ReporteService.GetReporteAcopioMes(year, 'estimado')
        .then(success =>{
          var dEstim:any = success.Data;
          for (let i = 0; i < dEstim.length; i++) {
            var pos = this.gridData.map(function(e) { return e.strDni; }).indexOf(dEstim[i].strDni);
            if(pos != -1){
              this.gridData[pos].eneroActual = dEstim[i].enero;
              this.gridData[pos].febreroActual = dEstim[i].febrero;
              this.gridData[pos].marzoActual = dEstim[i].marzo;
              this.gridData[pos].abrilActual = dEstim[i].abril;
              this.gridData[pos].mayoActual = dEstim[i].mayo;
              this.gridData[pos].junioActual = dEstim[i].junio;
              this.gridData[pos].julioActual = dEstim[i].julio;
              this.gridData[pos].agostoActual = dEstim[i].agosto;
              this.gridData[pos].septiembreActual = dEstim[i].septiembre;
              this.gridData[pos].octubreActual = dEstim[i].octubre;
              this.gridData[pos].noviembreActual = dEstim[i].noviembre;
              this.gridData[pos].diciembreActual = dEstim[i].diciembre;
            }
          }
          this.getDiferencia(this.gridData);
          this.loadingGetData = false;
        }).catch(e =>{
          console.log(e);
          this.loadingGetData = false;
          if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
          else  this.openMessageError('Error al cargar reporte');
        })
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte');
      })
    }
  }
  getDiferencia(Data){
    for (let i = 0; i < Data.length; i++) {
      //Math.abs(number) -> Valor Absoluto
      this.gridData[i].eneroDiff = this.getNumberFix(Number(Data[i].enero) - Number(Data[i].eneroActual), 0);
      this.gridData[i].febreroDiff = this.getNumberFix(Number(Data[i].febrero) - Number(Data[i].febreroActual), 0);
      this.gridData[i].marzoDiff = this.getNumberFix(Number(Data[i].marzo) - Number(Data[i].marzoActual), 0);
      this.gridData[i].abrilDiff = this.getNumberFix(Number(Data[i].abril) - Number(Data[i].abrilActual), 0);
      this.gridData[i].mayoDiff = this.getNumberFix(Number(Data[i].mayo) - Number(Data[i].mayoActual), 0);
      this.gridData[i].junioDiff = this.getNumberFix(Number(Data[i].junio) - Number(Data[i].junioActual), 0);
      this.gridData[i].julioDiff = this.getNumberFix(Number(Data[i].julio) - Number(Data[i].julioActual), 0);
      this.gridData[i].agostoDiff = this.getNumberFix(Number(Data[i].agosto) - Number(Data[i].agostoActual), 0);
      this.gridData[i].septiembreDiff = this.getNumberFix(Number(Data[i].septiembre) - Number(Data[i].septiembreActual), 0);
      this.gridData[i].octubreDiff = this.getNumberFix(Number(Data[i].octubre) - Number(Data[i].octubreActual), 0);
      this.gridData[i].noviembreDiff = this.getNumberFix(Number(Data[i].noviembre) - Number(Data[i].noviembreActual), 0);
      this.gridData[i].diciembreDiff = this.getNumberFix(Number(Data[i].diciembre) - Number(Data[i].diciembreActual), 0);

      this.gridData[i].enero = (this.gridData[i].enero === null)? '' : this.getNumberFix(this.gridData[i].enero,0);
      this.gridData[i].febrero = (this.gridData[i].febrero === null)? '' : this.getNumberFix(this.gridData[i].febrero,0);
      this.gridData[i].marzo = (this.gridData[i].marzo === null)? '' : this.getNumberFix(this.gridData[i].marzo,0);
      this.gridData[i].abril = (this.gridData[i].abril === null)? '' : this.getNumberFix(this.gridData[i].abril,0);
      this.gridData[i].mayo = (this.gridData[i].mayo === null)? '' : this.getNumberFix(this.gridData[i].mayo,0);
      this.gridData[i].junio = (this.gridData[i].junio === null)? '' : this.getNumberFix(this.gridData[i].junio,0);
      this.gridData[i].julio = (this.gridData[i].julio === null)? '' : this.getNumberFix(this.gridData[i].julio,0);
      this.gridData[i].agosto = (this.gridData[i].agosto === null)? '' : this.getNumberFix(this.gridData[i].agosto,0);
      this.gridData[i].septiembre = (this.gridData[i].septiembre === null)? '' : this.getNumberFix(this.gridData[i].septiembre,0);
      this.gridData[i].octubre = (this.gridData[i].octubre === null)? '' : this.getNumberFix(this.gridData[i].octubre,0);
      this.gridData[i].noviembre = (this.gridData[i].noviembre === null)? '' : this.getNumberFix(this.gridData[i].noviembre,0);
      this.gridData[i].diciembre = (this.gridData[i].diciembre === null)? '' : this.getNumberFix(this.gridData[i].diciembre,0);

      this.gridData[i].eneroActual = (this.gridData[i].eneroActual === null)? '' : this.getNumberFix(this.gridData[i].eneroActual,0);
      this.gridData[i].febreroActual = (this.gridData[i].febreroActual === null)? '' : this.getNumberFix(this.gridData[i].febreroActual,0);
      this.gridData[i].marzoActual = (this.gridData[i].marzoActual === null)? '' : this.getNumberFix(this.gridData[i].marzoActual,0);
      this.gridData[i].abrilActual = (this.gridData[i].abrilActual === null)? '' : this.getNumberFix(this.gridData[i].abrilActual,0);
      this.gridData[i].mayoActual = (this.gridData[i].mayoActual === null)? '' : this.getNumberFix(this.gridData[i].mayoActual,0);
      this.gridData[i].junioActual = (this.gridData[i].junioActual === null)? '' : this.getNumberFix(this.gridData[i].junioActual,0);
      this.gridData[i].julioActual = (this.gridData[i].julioActual === null)? '' : this.getNumberFix(this.gridData[i].julioActual,0);
      this.gridData[i].agostoActual = (this.gridData[i].agostoActual === null)? '' : this.getNumberFix(this.gridData[i].agostoActual,0);
      this.gridData[i].septiembreActual = (this.gridData[i].septiembreActual === null)? '' : this.getNumberFix(this.gridData[i].septiembreActual,0);
      this.gridData[i].octubreActual = (this.gridData[i].octubreActual === null)? '' : this.getNumberFix(this.gridData[i].octubreActual,0);
      this.gridData[i].noviembreActual = (this.gridData[i].noviembreActual === null)? '' : this.getNumberFix(this.gridData[i].noviembreActual,0);
      this.gridData[i].diciembreActual = (this.gridData[i].diciembreActual === null)? '' : this.getNumberFix(this.gridData[i].diciembreActual,0);
    }
  }

  getNameCompleteAcop(row){
    row.strAcopiador = row.strNombres + ', '+ row.strApellidos;
    return row.strAcopiador;
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.eneroActual != null || element.eneroActual != '')
        Total[2] += Number(element.eneroActual);
      if(element.enero != null || element.enero != '')
        Total[3] += Number(element.enero);

      if(element.febreroActual != null || element.febreroActual != '')
        Total[5] += Number(element.febreroActual);
      if(element.febrero != null || element.febrero != '')
        Total[6] += Number(element.febrero);

      if(element.marzoActual != null || element.marzoActual != '')
        Total[8] += Number(element.marzoActual);
      if(element.marzo != null || element.marzo != '')
        Total[9] += Number(element.marzo);

      if(element.abrilActual != null || element.abrilActual != '')
        Total[11] += Number(element.abrilActual);
      if(element.abril != null || element.abril != '')
        Total[12] += Number(element.abril);

      if(element.mayoActual != null || element.mayoActual != '')
        Total[14] += Number(element.mayoActual);
      if(element.mayo != null || element.mayo != '')
        Total[15] += Number(element.mayo);

      if(element.junioActual != null || element.junioActual != '')
        Total[17] += Number(element.junioActual);
      if(element.junio != null || element.junio != '')
        Total[18] += Number(element.junio);

      if(element.julioActual != null || element.julioActual != '')
        Total[20] += Number(element.julioActual);
      if(element.julio != null || element.julio != '')
        Total[21] += Number(element.julio);

      if(element.agostoActual != null || element.agostoActual != '')
        Total[23] += Number(element.agostoActual);
      if(element.agosto != null || element.agosto != '')
        Total[24] += Number(element.agosto);

      if(element.septiembreActual != null || element.septiembreActual != '')
        Total[26] += Number(element.septiembreActual);
      if(element.septiembre != null || element.septiembre != '')
        Total[27] += Number(element.septiembre);

      if(element.octubreActual != null || element.octubreActual != '')
        Total[29] += Number(element.octubreActual);
      if(element.octubre != null || element.octubre != '')
        Total[30] += Number(element.octubre);

      if(element.noviembreActual != null || element.noviembreActual != '')
        Total[32] += Number(element.noviembreActual);
      if(element.noviembre != null || element.noviembre != '')
        Total[33] += Number(element.noviembre);

      if(element.diciembreActual != null || element.diciembreActual != '')
        Total[35] += Number(element.diciembreActual);
      if(element.diciembre != null || element.diciembre != '')
        Total[36] += Number(element.diciembre);
    });
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1){
        sums[index] = ''
        return;
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          // style: 'currency',
          // currency: 'USD',
          minimumFractionDigits: 3,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      json_fields : {
        "strAcopiador" : "Acopiador",
        "strAnio" : "Proveedor",
        "eneroActual" : "Enero Estim.",
        "enero" : "Enero Actual",
        "eneroDiff" : "Diff",
        "febreroActual" : "Febrero Estim.",
        "febrero" : "Febrero Actual",
        "febreroDiff" : "Diff",
        "marzoActual" : "Marzo Estim.",
        "marzo" : "Marzo Actual",
        "marzoDiff" : "Diff",
        "abrilActual" : "Abril Estim.",
        "abril" : "Abril Actual",
        "abrilDiff" : "Diff",
        "mayoActual" : "Mayo Estim.",
        "mayo" : "Mayo Actual",
        "mayoDiff" : "Diff",
        "junioActual" : "Junio Estim.",
        "junio" : "Junio Actual",
        "junioDiff" : "Diff",
        "julioActual" : "Julio Estim.",
        "julio" : "Julio Actual",
        "julioDiff" : "Diff",
        "agostoActual" : "Agosto Estim.",
        "agosto" : "Agosto Actual",
        "agostoDiff" : "Diff",
        "septiembreActual" : "Septiembre Estim.",
        "septiembre" : "Septiembre Actual",
        "septiembreDiff" : "Diff",
        "octubreActual" : "Octubre Estim.",
        "octubre" : "Octubre Actual",
        "octubreDiff" : "Diff",
        "noviembreActual" : "Noviembre Estim.",
        "noviembre" : "Noviembre Actual",
        "noviembreDiff" : "Diff",
        "diciembreActual" : "Diciembre Estim.",
        "diciembre" : "Diciembre Actual",
        "diciembreDiff" : "Diff",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]

    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
