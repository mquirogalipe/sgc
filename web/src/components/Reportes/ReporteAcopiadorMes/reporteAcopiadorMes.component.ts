import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReporteService from '@/services/reportes.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'reportePrueba',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ReportePruebaComponent extends Vue {
  fechaSearch: any = new Date();
  gridData: any = [];
  sizeScreen:string = (window.innerHeight - 350).toString();//'0';
  userDec: any = {};
  loadingGetData:boolean = false;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1018){
          flag=true;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.sizeScreen = (window.innerHeight - 200).toString();
      }
    }
  }

  BuscarReporte(){
    this.loadingGetData = true;
    var year = (this.fechaSearch===undefined||this.fechaSearch.toString()==='') ?'*' : this.fechaSearch.getFullYear().toString();
    ReporteService.GetReporteAcopiadorMes(year)
    .then(response =>{
      this.gridData = response.Data
      this.loadingGetData = false;
    }).catch(e =>{
      console.log(e);
      this.loadingGetData = false;
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar reporte');
    })
  }
  getSummaries(param) {
    var dataReport: any = this.gridData;
    var Total: any = ['','', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // var PesoTotalTMH = 0, PromLeyAuOzTc=0, PromLeyAuComercial=0;
    dataReport.forEach(function(element) {
      if(element.enero != null || element.enero != '')
        Total[2] += element.enero;
      if(element.febrero != null || element.febrero != '')
        Total[3] += element.febrero;
      if(element.marzo != null || element.marzo != '')
        Total[4] += element.marzo;
      if(element.abril != null || element.abril != '')
        Total[5] += element.abril;
      if(element.mayo != null || element.mayo != '')
        Total[6] += element.mayo;
      if(element.junio != null || element.junio != '')
        Total[7] += element.junio;
      if(element.julio != null || element.julio != '')
        Total[8] += element.julio;
      if(element.agosto != null || element.agosto != '')
        Total[9] += element.agosto;
      if(element.septiembre != null || element.septiembre != '')
        Total[10] += element.septiembre;
      if(element.octubre != null || element.octubre != '')
        Total[11] += element.octubre;
      if(element.noviembre != null || element.noviembre != '')
        Total[12] += element.noviembre;
      if(element.diciembre != null || element.diciembre != '')
        Total[13] += element.diciembre;
    });
    // console.log(TotalPSeco);
    const { columns, data } = param;
    var sums:any = [];
    columns.forEach((column, index) => {
      if (index === 0) {
        sums[index] = 'TOTAL';
        return;
      }
      if(index === 1){
        sums[index] = ''
        return;
      }
      else{
        var formatter = new Intl.NumberFormat('es-PE', {
          // style: 'currency',
          // currency: 'USD',
          minimumFractionDigits: 2,
        });

        sums[index] = formatter.format(Total[index]);
        return;
      }
    });
    return sums;
  }
  getNumberFix(number, fix:number){
    if(number === "" || number === null || number === undefined) return "";
    else{
      var num = parseFloat(number).toFixed(fix);
      return num;
    }
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {

    };
  }

  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }
}
