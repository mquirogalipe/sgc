import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ReportesService from '@/services/reportes.service';
import JsonExcel from '@/components/vue-json-excel/JsonExcel.vue';
import * as CONFIG from '@/Config';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
@Component({
   name: 'controlCuotas',
   components:{
     'download-excel' : JsonExcel,
     'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
   }
})
export default class ControlCuotasComponent extends Vue {
  fechaSearch: Date = new Date();
  gridData: any = [];
  FormSearch: any = {};
  loadingGetData:boolean = false;
  monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
  ];
  sizeScreen:string = (window.innerHeight - 350).toString();
  titleExcel:string ='Reporte_Control Cuotas_'+CONFIG.DATE_STRING+'.xls';
  colorStatus: any = 'green';
  // sizeScreen:string = '0';
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1011){
          flag=true;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else{
        this.sizeScreen = (window.innerHeight - 250).toString();
      }
    }
  }
  BuscarReporte(){
    var fSearch:any = this.fechaSearch;
    if(fSearch === '' || fSearch===undefined)
      this.openMessageError('Seleccione Fecha')
    else{
      this.loadingGetData = true;
      this.FormSearch.strMesCuota = this.monthNames[this.fechaSearch.getMonth()];
      this.FormSearch.strAnioCuota = this.fechaSearch.getFullYear().toString();
      this.FormSearch.strFecha = GLOBAL.getParseDate(new Date(this.fechaSearch.getFullYear(), this.fechaSearch.getMonth(), 1));
      ReportesService.GetReporteControlCuotas(this.FormSearch)
      .then(response =>{
        this.gridData = response.Data;
        this.loadingGetData = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetData = false;
        if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
        else  this.openMessageError('Error al cargar reporte.');
      })
    }
  }
  LimpiarConsulta(){

  }
  getNameCompleteAcop(row){
    row.strAcopiador = row.strNombres + ', '+ row.strApellidos;
    return row.strAcopiador;
  }
  getEstadoDiff(number){
    if(number > 0) this.colorStatus = 'red'
    else this.colorStatus = 'green'
    var num = parseFloat(number).toFixed(3);
    return num;
  }
  getNumberFloat(number){
    if(number === null) return '';
    else {
      var num = parseFloat(number).toFixed(3);
      return num;
    }
  }

  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  clickactive(event){
    // if(event=='A'){
    //   this.blnfiltroavanzado=true;
    // }
    // if(event=='B'){
    //   this.blnfiltroavanzado=false;
    // }
  }
  guardarTodo(){

  }
  backPage(){
    window.history.back();
  }
  reloadpage(){
    window.location.reload();
  }
  AscItem(){

  }
  DscItem(){

  }
  data() {
    return {
      FormSearch:{
        strMesCuota: '',
        strAnioCuota: '',
        strFecha: '',
        strAcopiador: '',
        strZona: ''
      },
      json_fields : {
        "strAcopiador": "Acopiador",
        "fltCuota" : "Cuota Estimada",
        "fltCuotaActual" : "Cuota Actual",
        "fltDiffCuota" : "Restante",
      },
      json_meta:[
        [{
          "key": "charset",
          "value": "utf-8"
        }]
      ]
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }
}
