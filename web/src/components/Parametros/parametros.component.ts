import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import ParametroService from '@/services/parametro.service';
import ZonaService from '@/services/zona.service';
import ProveedorService from '@/services/proveedor.service';
import QuickAccessMenuComponent from '@/components/HeaderAccions/quickaccessmenu.vue';
import ButtonsAccionsComponent from '@/components/HeaderAccions/buttonsAccions/buttonsAccions.vue';
import * as CONFIG from '@/Config';
@Component({
   name: 'parametros',
   components:{
    'quickaccessmenu':QuickAccessMenuComponent,
    'buttons-accions': ButtonsAccionsComponent,
  }
})
export default class ParametrosComponent extends Vue {
  activeName:any='first';
  FormPrecioInter: any = {};
  gridDataZona: any = [];
  gridDataProveedor: any = [];
  gridDataPrecioInter: any = [];
  dataZonas: any = [];
  FormAgregarPZona: any = [];
  FormAgregarPProv: any =[];
  rowEditZona: any = {};
  rowEditProveedor: any = {};
  NivelAcceso: string = '';
  viewAccessEscritura: boolean = false;
  userDec: any ={};
  sizeScreen:string = (window.innerHeight - 420).toString();
  Factor_Conversion: string = '';
  dialogAgregarPZonaVisible: boolean = false;
  dialogEditarPZonaVisible: boolean = false;
  dialogAgregarPProVisible: boolean = false;
  dialogEditarPProVisible: boolean = false;
  dialogAgregarPIVisible: boolean = false;
  dialogEditarPIVisible: boolean = false;

  //PROVEEDOR
  dataProveedores: any = [];
  //PRECIO INTER
  fechaPrecioInter: Date = new Date()
  options = {  day: '2-digit',month: '2-digit', year: 'numeric' };
  loadingGetDataPI: boolean = false;
  fechaAgregarPI: Date = new Date();
  FormAgregarPI: any = {};
  fechaEditarPI: any = '';
  rowEditPI: any = {};
  seleccionarRows:any=null;
  seleccionarRowsProveedor:any=null;
  seleccionarRowsInternacional:any=null;
  constructor (){
    super()
  }
  CheckAccess(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      var flag:boolean = false;
      for(var i=0; i<listaAccesos.length; i++){
        if(listaAccesos[i].intCodAcceso === 1013){
          flag=true;
          this.NivelAcceso = listaAccesos[i].strTipoAcceso;
          break;
        }
      }
      if (flag == false){
        if(listaAccesos.length === 0) {
          this.$router.push('/');
        }
        else{
          Notification.warning('No tiene permisos para acceder a esta página')
          this.$router.push('/menu/inicio');
        }
      }
      else {
        this.activeName = 'first';
        var session = window.sessionStorage.getItem("session_user");
        this.userDec = (session === null || session === undefined)?{}:JSON.parse(GLOBAL.decodeString(session));
        this.viewAccessEscritura = (this.NivelAcceso === 'escritura' || this.userDec.strCargo === 'Administrador')? true : false;
        this.getParametrosZona();
      }
    }
  }
  AgregarParametroPZona(FormAgregarPZona){
    FormAgregarPZona.fltRecComMax = (FormAgregarPZona.fltRecComMax === '')? 0 : FormAgregarPZona.fltRecComMax;
    FormAgregarPZona.fltMargenMax = (FormAgregarPZona.fltMargenMax === '')? 0 : FormAgregarPZona.fltMargenMax;
    FormAgregarPZona.fltMaquilaMin = (FormAgregarPZona.fltMaquilaMin === '')? 0 : FormAgregarPZona.fltMaquilaMin;
    FormAgregarPZona.fltMaquilaMax = (FormAgregarPZona.fltMaquilaMax === '')? 0 : FormAgregarPZona.fltMaquilaMax;
    FormAgregarPZona.fltConsumoMax = (FormAgregarPZona.fltConsumoMax === '')? 0 : FormAgregarPZona.fltConsumoMax;
    FormAgregarPZona.fltGastAdmMax = (FormAgregarPZona.fltGastAdmMax === '')? 0 : FormAgregarPZona.fltGastAdmMax;
    FormAgregarPZona.fltGastLabMax = (FormAgregarPZona.fltGastLabMax === '')? 0 : FormAgregarPZona.fltGastLabMax;
    FormAgregarPZona.fltPorcHumedMax = (FormAgregarPZona.fltPorcHumedMax === '')? 0 : FormAgregarPZona.fltPorcHumedMax;
    FormAgregarPZona.fltPesoMax = (FormAgregarPZona.fltPesoMax === '')? 0 : FormAgregarPZona.fltPesoMax;
    FormAgregarPZona.fltRecAgMax = (FormAgregarPZona.fltRecAgMax === '')? 0 : FormAgregarPZona.fltRecAgMax;
    FormAgregarPZona.fltLeyAgMax = (FormAgregarPZona.fltLeyAgMax === '')? 0 : FormAgregarPZona.fltLeyAgMax;
    FormAgregarPZona.fltLeyAgComMax = (FormAgregarPZona.fltLeyAgComMax === '')? 0 : FormAgregarPZona.fltLeyAgComMax;
    FormAgregarPZona.fltMargenAgMax = (FormAgregarPZona.fltMargenAgMax === '')? 0 : FormAgregarPZona.fltMargenAgMax;
    FormAgregarPZona.fltRecAgComMax = (FormAgregarPZona.fltRecAgComMax === '')? 0 : FormAgregarPZona.fltRecAgComMax;
    FormAgregarPZona.fltRecIntMax = (FormAgregarPZona.fltRecIntMax === '')? 0 : FormAgregarPZona.fltRecIntMax;
    FormAgregarPZona.fltMaquilaIntMax = (FormAgregarPZona.fltMaquilaIntMax === '')? 0 : FormAgregarPZona.fltMaquilaIntMax;
    FormAgregarPZona.fltConsumoIntMax = (FormAgregarPZona.fltConsumoIntMax === '')? 0 : FormAgregarPZona.fltConsumoIntMax;
    FormAgregarPZona.fltTmsLiqMax = (FormAgregarPZona.fltTmsLiqMax === '')? 0 : FormAgregarPZona.fltTmsLiqMax;
    FormAgregarPZona.fltTmsIntMax = (FormAgregarPZona.fltTmsIntMax === '')? 0 : FormAgregarPZona.fltTmsIntMax;
    FormAgregarPZona.strUsuarioCrea = this.userDec.strUsuario;

    ParametroService.PutParametroZona(FormAgregarPZona)
    .then(response =>{
      if(response === -1) this.openMessageError("Error al agregar parametro:"+response)
      else if(response === -2) this.openMessageError("Ya existe un parametro para esa zona.")
      else
        this.openMessage('Parametro guardado correctamente.');
      this.getParametrosZona();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Parametro.');
    })
    this.dialogAgregarPZonaVisible = false;
  }
  EditarParametroPZona(rowEditZona){
    var FormUpdate = {
      intParamCom: rowEditZona.intParamCom,
      intIdZona : rowEditZona.intIdZona,
      fltRecComMax : (rowEditZona.fltRecComMax === '')? 0 : rowEditZona.fltRecComMax,
      fltMargenMax : (rowEditZona.fltMargenMax === '')? 0 : rowEditZona.fltMargenMax,
      fltMaquilaMin : (rowEditZona.fltMaquilaMin === '')? 0 : rowEditZona.fltMaquilaMin,
      fltMaquilaMax : (rowEditZona.fltMaquilaMax === '')? 0 : rowEditZona.fltMaquilaMax,
      fltConsumoMax : (rowEditZona.fltConsumoMax === '')? 0 : rowEditZona.fltConsumoMax,
      fltGastAdmMax : (rowEditZona.fltGastAdmMax === '')? 0 : rowEditZona.fltGastAdmMax,
      fltGastLabMax : (rowEditZona.fltGastLabMax === '')? 0 : rowEditZona.fltGastLabMax,
      fltPorcHumedMax : (rowEditZona.fltPorcHumedMax === '')? 0 : rowEditZona.fltPorcHumedMax,
      fltPesoMax : (rowEditZona.fltPesoMax === '')? 0 : rowEditZona.fltPesoMax,
      fltRecAgMax : (rowEditZona.fltRecAgMax === '')? 0 : rowEditZona.fltRecAgMax,
      fltLeyAgMax : (rowEditZona.fltLeyAgMax === '')? 0 : rowEditZona.fltLeyAgMax,
      fltLeyAgComMax : (rowEditZona.fltLeyAgComMax === '')? 0 : rowEditZona.fltLeyAgComMax,
      fltMargenAgMax : (rowEditZona.fltMargenAgMax === '')? 0 : rowEditZona.fltMargenAgMax,
      fltRecAgComMax : (rowEditZona.fltRecAgComMax === '')? 0 : rowEditZona.fltRecAgComMax,
      fltRecIntMax : (rowEditZona.fltRecIntMax === '')? 0 : rowEditZona.fltRecIntMax,
      fltMaquilaIntMax : (rowEditZona.fltMaquilaIntMax === '')? 0 : rowEditZona.fltMaquilaIntMax,
      fltConsumoIntMax : (rowEditZona.fltConsumoIntMax === '')? 0 : rowEditZona.fltConsumoIntMax,
      fltTmsLiqMax : (rowEditZona.fltTmsLiqMax === '')? 0 : rowEditZona.fltTmsLiqMax,
      fltTmsIntMax : (rowEditZona.fltTmsIntMax === '')? 0 : rowEditZona.fltTmsIntMax,
      strUsuarioModif : this.userDec.strUsuario
    }
    ParametroService.UpdateParametroZona(FormUpdate)
    .then(response =>{
      this.openMessage('Parametro guardado correctamente.');
      this.getParametrosZona();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al editar Parametro.');
    })
    this.dialogEditarPZonaVisible = false;
  }

  //PRECIO INTER
  handleAgregarPI(){
    this.limpiarAgregarPI();
    this.dialogAgregarPIVisible = true;
  }
  limpiarAgregarPI(){
    this.fechaAgregarPI = new Date();
    this.FormAgregarPI.strGoldAM = '';
    this.FormAgregarPI.strGoldPM = '';
    this.FormAgregarPI.strSilver = '';
  }
  handleEditPI2(){
    this.handleEditPI(this.seleccionarRowsInternacional);
  }
  handleEditPI(row){
    this.fechaEditarPI = new Date(row.dtmFecha);
    this.rowEditPI = row;
    this.dialogEditarPIVisible = true;
  }
  EditarPrecioInter(rowEditPI){
    var Data: any = {
      intIdPrecioInter: rowEditPI.intIdPrecioInter,
      dtmFecha: GLOBAL.getParseDate(this.fechaEditarPI),
      strGoldAM: rowEditPI.strGoldAM,
      strGoldPM: rowEditPI.strGoldPM,
      strSilver: rowEditPI.strSilver,
      strUsuarioModif: this.userDec.strUsuario
    }
    ParametroService.UpdatePrecioInter(Data)
    .then(response =>{
      if(response === null) this.openMessageError('SERVICE: Error al editar Precio');
      else this.openMessage(response);
      this.dialogEditarPIVisible = false;
      this.getPrecioInternacional();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error actualizar precio');
    })
  }
  GuardarPrecioInter(FormAgregarPI){
    FormAgregarPI.dtmFecha = GLOBAL.getParseDate(this.fechaAgregarPI);
    FormAgregarPI.strUsuarioCrea = this.userDec.strUsuario;
    ParametroService.PutPrecioInter(FormAgregarPI)
    .then(response =>{
      if(response == null){
        this.openMessageError('SERVICE: Error al guardar Precio')
      }
      else{
        this.openMessage(response);
      }
      this.dialogAgregarPIVisible = false;
      this.getPrecioInternacional()
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error guardar precio');
    })
  }
  getPrecioInternacional(){
    this.loadingGetDataPI = true;
    if(this.fechaPrecioInter === null || this.fechaPrecioInter === undefined){
      this.openMessageError('Seleccione mes');
    }
    else{
      var fSearch = this.fechaPrecioInter;
      var Data: any = {
        strFechaInicio: (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth(), 1)),
        strFechaFin: (fSearch===undefined||fSearch.toString()==='')?'*':GLOBAL.getParseDate(new Date(fSearch.getFullYear(), fSearch.getMonth() + 1, 0)),
        strType: 'config'
      }
      ParametroService.SearchPrecioInter(Data)
      .then(response =>{
        this.gridDataPrecioInter = response.Data;
        this.loadingGetDataPI = false;
      }).catch(e =>{
        console.log(e);
        this.loadingGetDataPI = false;
        this.openMessageError('Error al obtener precio Internacional');
      })
    }
  }


  // FACTOR DE CONVERSIÓN
  ActualizarFactorConv(){
    var DataUpFactor: any = {
      strNombre: 'FactorConversion',
      strValor: this.Factor_Conversion.toString(),
      strUsuarioModif: this.userDec.strUsuario
    }
    ParametroService.UpdateFactorConvencion(DataUpFactor)
    .then(response =>{
      this.openMessage(response);
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error actualizar precio por zona');
    })
  }

  getParametrosZona(){
    ParametroService.ObtenerParametros('zona')
    .then(response =>{
      this.gridDataZona = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error cargar parametros por zona');
    })
  }
  getParametrosProveedor(){
    ParametroService.ObtenerParametros('proveedor')
    .then(response =>{
      this.gridDataProveedor = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error cargar parametros por proveedor');
    })
  }

  getFactorConversion(){
    ParametroService.searchMaestro('FactorConversion')
    .then(response =>{
      this.Factor_Conversion = response.strValor;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar factor de conversión');
    })
  }
  getZonas(){
    ZonaService.loadingData()
    .then(response =>{
      this.dataZonas = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar zonas');
    })
  }
  getProveedores(){
    var Query = {
      strRazonS: '',
      strRuc: ''
    }
    ProveedorService.SearchProveedor(Query)
    .then(response =>{
      this.dataProveedores = response.Data;
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al cargar proveedores');
    })
  }
  AgregarParametroPProv(FormAgregarPProv){
    FormAgregarPProv.fltRecComMax = (FormAgregarPProv.fltRecComMax === '')? 0 : FormAgregarPProv.fltRecComMax;
    FormAgregarPProv.fltMargenMax = (FormAgregarPProv.fltMargenMax === '')? 0 : FormAgregarPProv.fltMargenMax;
    FormAgregarPProv.fltMaquilaMin = (FormAgregarPProv.fltMaquilaMin === '')? 0 : FormAgregarPProv.fltMaquilaMin;
    FormAgregarPProv.fltMaquilaMax = (FormAgregarPProv.fltMaquilaMax === '')? 0 : FormAgregarPProv.fltMaquilaMax;
    FormAgregarPProv.fltConsumoMax = (FormAgregarPProv.fltConsumoMax === '')? 0 : FormAgregarPProv.fltConsumoMax;
    FormAgregarPProv.fltGastAdmMax = (FormAgregarPProv.fltGastAdmMax === '')? 0 : FormAgregarPProv.fltGastAdmMax;
    FormAgregarPProv.fltGastLabMax = (FormAgregarPProv.fltGastLabMax === '')? 0 : FormAgregarPProv.fltGastLabMax;
    FormAgregarPProv.fltPorcHumedMax = (FormAgregarPProv.fltPorcHumedMax === '')? 0 : FormAgregarPProv.fltPorcHumedMax;
    FormAgregarPProv.fltPesoMax = (FormAgregarPProv.fltPesoMax === '')? 0 : FormAgregarPProv.fltPesoMax;
    FormAgregarPProv.fltRecAgMax = (FormAgregarPProv.fltRecAgMax === '')? 0 : FormAgregarPProv.fltRecAgMax;
    FormAgregarPProv.fltLeyAgMax = (FormAgregarPProv.fltLeyAgMax === '')? 0 : FormAgregarPProv.fltLeyAgMax;
    FormAgregarPProv.fltLeyAgComMax = (FormAgregarPProv.fltLeyAgComMax === '')? 0 : FormAgregarPProv.fltLeyAgComMax;
    FormAgregarPProv.fltMargenAgMax = (FormAgregarPProv.fltMargenAgMax === '')? 0 : FormAgregarPProv.fltMargenAgMax;
    FormAgregarPProv.fltRecAgComMax = (FormAgregarPProv.fltRecAgComMax === '')? 0 : FormAgregarPProv.fltRecAgComMax;
    FormAgregarPProv.fltRecIntMax = (FormAgregarPProv.fltRecIntMax === '')? 0 : FormAgregarPProv.fltRecIntMax;
    FormAgregarPProv.fltMaquilaIntMax = (FormAgregarPProv.fltMaquilaIntMax === '')? 0 : FormAgregarPProv.fltMaquilaIntMax;
    FormAgregarPProv.fltConsumoIntMax = (FormAgregarPProv.fltConsumoIntMax === '')? 0 : FormAgregarPProv.fltConsumoIntMax;
    FormAgregarPProv.fltTmsLiqMax = (FormAgregarPProv.fltTmsLiqMax === '')? 0 : FormAgregarPProv.fltTmsLiqMax;
    FormAgregarPProv.fltTmsIntMax = (FormAgregarPProv.fltTmsIntMax === '')? 0 : FormAgregarPProv.fltTmsIntMax;
    FormAgregarPProv.strUsuarioCrea = this.userDec.strUsuario;

    ParametroService.PutParametroProv(FormAgregarPProv)
    .then(response =>{
      if(response === -1) this.openMessageError("Error al agregar parametro:"+response)
      else if(response === -2) this.openMessageError("Ya existe un parametro para este proveedor.")
      else
        this.openMessage('Parametro guardado correctamente.');
      this.getParametrosProveedor();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al agregar Parametro.');
    })
    this.dialogAgregarPProVisible = false;
  }
  EditarParametroPProv(rowEditZona){
    var FormUpdate = {
      intIdParamComProv: rowEditZona.intIdParamComProv,
      intIdProveedor : rowEditZona.intIdProveedor,
      fltRecComMax : (rowEditZona.fltRecComMax === '')? 0 : rowEditZona.fltRecComMax,
      fltMargenMax : (rowEditZona.fltMargenMax === '')? 0 : rowEditZona.fltMargenMax,
      fltMaquilaMin : (rowEditZona.fltMaquilaMin === '')? 0 : rowEditZona.fltMaquilaMin,
      fltMaquilaMax : (rowEditZona.fltMaquilaMax === '')? 0 : rowEditZona.fltMaquilaMax,
      fltConsumoMax : (rowEditZona.fltConsumoMax === '')? 0 : rowEditZona.fltConsumoMax,
      fltGastAdmMax : (rowEditZona.fltGastAdmMax === '')? 0 : rowEditZona.fltGastAdmMax,
      fltGastLabMax : (rowEditZona.fltGastLabMax === '')? 0 : rowEditZona.fltGastLabMax,
      fltPorcHumedMax : (rowEditZona.fltPorcHumedMax === '')? 0 : rowEditZona.fltPorcHumedMax,
      fltPesoMax : (rowEditZona.fltPesoMax === '')? 0 : rowEditZona.fltPesoMax,
      fltRecAgMax : (rowEditZona.fltRecAgMax === '')? 0 : rowEditZona.fltRecAgMax,
      fltLeyAgMax : (rowEditZona.fltLeyAgMax === '')? 0 : rowEditZona.fltLeyAgMax,
      fltLeyAgComMax : (rowEditZona.fltLeyAgComMax === '')? 0 : rowEditZona.fltLeyAgComMax,
      fltMargenAgMax : (rowEditZona.fltMargenAgMax === '')? 0 : rowEditZona.fltMargenAgMax,
      fltRecAgComMax : (rowEditZona.fltRecAgComMax === '')? 0 : rowEditZona.fltRecAgComMax,
      fltRecIntMax : (rowEditZona.fltRecIntMax === '')? 0 : rowEditZona.fltRecIntMax,
      fltMaquilaIntMax : (rowEditZona.fltMaquilaIntMax === '')? 0 : rowEditZona.fltMaquilaIntMax,
      fltConsumoIntMax : (rowEditZona.fltConsumoIntMax === '')? 0 : rowEditZona.fltConsumoIntMax,
      fltTmsLiqMax : (rowEditZona.fltTmsLiqMax === '')? 0 : rowEditZona.fltTmsLiqMax,
      fltTmsIntMax : (rowEditZona.fltTmsIntMax === '')? 0 : rowEditZona.fltTmsIntMax,
      strUsuarioModif : this.userDec.strUsuario
    }
    ParametroService.UpdateParametroProv(FormUpdate)
    .then(response =>{
      this.openMessage('Parametro actualizado correctamente.');
      this.getParametrosProveedor();
    }).catch(e =>{
      console.log(e);
      if(e.response.status === 401) this.redirectLogin(e.response.statusText+', Vuelva a Iniciar Sesion');
      else  this.openMessageError('Error al editar Parametro.');
    })
    this.dialogEditarPProVisible = false;
  }

  getPorcentage(value){
    var formatter = new Intl.NumberFormat('es-PE', { style: 'percent', minimumFractionDigits: 0, maximumFractionDigits: 0 });
    return formatter.format(value);
  }

  handleTabClick(tab, event) {
    if(tab.name=="first"){
      this.getParametrosZona();
    }
    if(tab.name=='second'){
      this.getParametrosProveedor();
    }
    if(tab.name=="third"){
      this.getPrecioInternacional();
    }
    if(tab.name =="fourth"){
      this.getFactorConversion();
    }
  }
  handleAgregarPZona(){
    this.LimpiarAgregarPZona();
    this.getZonas();
    this.dialogAgregarPZonaVisible = true;
  }
  handleAgregarPProv(){
    this.LimpiarAgregarPProv();
    this.getProveedores();
    this.dialogAgregarPProVisible = true;
  }
  handleEditZona2(){
    this.handleEditZona(this.seleccionarRows);
  }
  handleEditZona(row){
    this.rowEditZona = {};
    this.rowEditZona = row;
    this.getZonas();
    this.dialogEditarPZonaVisible = true;
  }
  handleEditProv2(){
    this.handleEditProv(this.seleccionarRowsProveedor);
  }
  handleEditProv(row){
    this.rowEditProveedor = {};
    this.rowEditProveedor = row;
    this.getProveedores();
    this.dialogEditarPProVisible = true;
  }
  LimpiarAgregarPZona(){
    this.FormAgregarPZona.intIdZona = '';
    this.FormAgregarPZona.fltRecComMax = '';
    this.FormAgregarPZona.fltMargenMax = '';
    this.FormAgregarPZona.fltMaquilaMin = '';
    this.FormAgregarPZona.fltMaquilaMax = '';
    this.FormAgregarPZona.fltConsumoMax = '';
    this.FormAgregarPZona.fltGastAdmMax = '';
    this.FormAgregarPZona.fltGastLabMax = '';
    this.FormAgregarPZona.fltPorcHumedMax = '';
    this.FormAgregarPZona.fltPesoMax = '';
    this.FormAgregarPZona.fltRecAgMax = '';
    this.FormAgregarPZona.fltLeyAgMax = '';
    this.FormAgregarPZona.fltLeyAgComMax = '';
    this.FormAgregarPZona.fltMargenAgMax = '';
    this.FormAgregarPZona.fltRecAgComMax = '';
    this.FormAgregarPZona.fltRecIntMax = '';
    this.FormAgregarPZona.fltMaquilaIntMax = '';
    this.FormAgregarPZona.fltConsumoIntMax = '';
    this.FormAgregarPZona.fltTmsLiqMax = '';
    this.FormAgregarPZona.fltTmsIntMax = '';
  }
  LimpiarAgregarPProv(){
    this.FormAgregarPProv.intIdProveedor = '';
    this.FormAgregarPProv.fltRecComMax = '';
    this.FormAgregarPProv.fltMargenMax = '';
    this.FormAgregarPProv.fltMaquilaMin = '';
    this.FormAgregarPProv.fltMaquilaMax = '';
    this.FormAgregarPProv.fltConsumoMax = '';
    this.FormAgregarPProv.fltGastAdmMax = '';
    this.FormAgregarPProv.fltGastLabMax = '';
    this.FormAgregarPProv.fltPorcHumedMax = '';
    this.FormAgregarPProv.fltPesoMax = '';
    this.FormAgregarPProv.fltRecAgMax = '';
    this.FormAgregarPProv.fltLeyAgMax = '';
    this.FormAgregarPProv.fltLeyAgComMax = '';
    this.FormAgregarPProv.fltMargenAgMax = '';
    this.FormAgregarPProv.fltRecAgComMax = '';
    this.FormAgregarPProv.fltRecIntMax = '';
    this.FormAgregarPProv.fltMaquilaIntMax = '';
    this.FormAgregarPProv.fltConsumoIntMax = '';
    this.FormAgregarPProv.fltTmsLiqMax = '';
    this.FormAgregarPProv.fltTmsIntMax = '';
  }

  //DUPLICAR PARAMETROS
  handleDuplicarParam(index, row){
    this.$confirm('Desea Duplicar el Parametro de: '+row.strZona+' ?', 'Duplicar', {
      confirmButtonText: 'Duplicar',
      cancelButtonText: 'Cancelar',
      type: 'success'
    }).then(() => {
      ParametroService.DuplicarParametro(row.intParamCom, this.userDec.strUsuario)
      .then(response =>{
        if(response === -1) this.openMessageError('SERVICE: Error al duplicar Parametro')
        else this.openMessage('Se duplicó correctamente');
        this.getParametrosZona();
      }).catch(e =>{
        console.log(e);
        this.openMessageError('Error al duplicar Parametro');
      })

    }).catch(() => { });
  }
  handleDuplicarParamProv(index, row){
    this.$confirm('Desea Duplicar el Parametro de: '+row.strProveedor+' ?', 'Duplicar', {
      confirmButtonText: 'Duplicar',
      cancelButtonText: 'Cancelar',
      type: 'success'
    }).then(() => {
      ParametroService.DuplicarParametroProv(row.intIdParamComProv, this.userDec.strUsuario)
      .then(response =>{
        if(response === -1) this.openMessageError('SERVICE: Error al duplicar Parametro')
        else this.openMessage('Se duplicó correctamente');
        this.getParametrosProveedor();
      }).catch(e =>{
        console.log(e);
        this.openMessageError('Error al duplicar Parametro');
      })

    }).catch(() => { });
  }

  getDateString(fecha:string){
    if(fecha === '' || fecha === null || fecha === undefined) return "";
    else{
      if(fecha.length === 10) fecha = fecha+'T00:00:00';
      var dateString = new Date(fecha).toLocaleDateString('es-PE', this.options)
      return dateString;
    }
  }
  openMessage(strMessage : string) {
    this.$message({
      showClose: true,
      message: strMessage,
      type: 'success'
    });
  }
  openMessageError(strMessage:string){
    this.$message({
      showClose: true,
      type: 'error',
      message: strMessage
    });
  }
  redirectLogin(msg){
    Notification.warning(msg)
    window.sessionStorage.clear();
    router.push('/')
  }
  seleccionarRow(row,index){
    this.seleccionarRows=row;
  }
  seleccionarRowProveedor(row,index){
    this.seleccionarRowsProveedor=row;
  }
  seleccionarRowInternacional(row,index){
    this.seleccionarRowsInternacional=row;
  }

  data() {
    return {
      FormPrecioInter:{
        strPrecioDia: '',
        strPrecioConv: ''
      },
      FormAgregarPZona:{
        intIdZona : '',
        fltRecComMax : '',
        fltMargenMax : '',
        fltMaquilaMin : '',
        fltMaquilaMax : '',
        fltConsumoMax : '',
        fltGastAdmMax : '',
        fltGastLabMax : '',
        fltPorcHumedMax : '',
        fltPesoMax : '',
        fltRecAgMax : '',
        fltLeyAgMax : '',
        fltLeyAgComMax : '',
        fltMargenAgMax : '',
        fltRecAgComMax : '',
        fltRecIntMax : '',
        fltMaquilaIntMax : '',
        fltConsumoIntMax : '',
        fltTmsLiqMax : '',
        fltTmsIntMax : '',
        strUsuarioCrea : ''
      },
      FormAgregarPProv:{
        intIdProveedor : '',
        fltRecComMax : '',
        fltMargenMax : '',
        fltMaquilaMin : '',
        fltMaquilaMax : '',
        fltConsumoMax : '',
        fltGastAdmMax : '',
        fltGastLabMax : '',
        fltPorcHumedMax : '',
        fltPesoMax : '',
        fltRecAgMax : '',
        fltLeyAgMax : '',
        fltLeyAgComMax : '',
        fltMargenAgMax : '',
        fltRecAgComMax : '',
        fltRecIntMax : '',
        fltMaquilaIntMax : '',
        fltConsumoIntMax : '',
        fltTmsLiqMax : '',
        fltTmsIntMax : '',
        strUsuarioCrea : ''
      },
      FormAgregarPI: {
        dtmFecha: '',
        strGoldAM: '',
        strGoldPM: '',
        strSilver: '',
        strUsuarioCrea: '',
      }
    };
  }
  created() {
    if(typeof window != 'undefined') {
      this.CheckAccess();
    }
  }

}
