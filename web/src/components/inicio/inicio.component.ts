import { Vue, Component } from 'vue-property-decorator'
import router from '@/router';
import { Notification } from 'element-ui';
import GLOBAL from '@/Global';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css';
@Component({
   name: 'inicio'
})
export default class InicioComponent extends Vue {
  msgname:string;
  constructor (){
    super()
  }
  checkAccesos(){
    var lista:any = window.sessionStorage.getItem('session_access');
    if(lista === null){
      this.$router.push('/');
      Notification.warning('No tiene permisos para acceder a esta página');
    }
    else{
      var listaAccesos:any = JSON.parse(GLOBAL.decodeString(lista));
      if (listaAccesos.length > 0){
        console.log('Bienvenido');
      }
      else{
        Notification.warning('No tiene permisos para acceder a esta página');
        this.$router.push('/');
      }
    }
  }

  created() {
    if(typeof window != 'undefined') {
      this.checkAccesos();
    }
  }

}
