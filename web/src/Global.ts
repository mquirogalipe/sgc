import * as CONFIG from '@/Config';


//var intCodUsuario;
export default {
  token:'',
  dataGuardar:null,
  component:'',
  isActive:false,
  filter:false,
  isCollapse:false,
  setComponent(data){
    this.component=data;
  },
  getComponent(){
    return this.component;
  },
  setToken(token){
    // this.token=token;
    window.sessionStorage.setItem('user_token',token);
  },
  getDataGuardar(){
    return this.dataGuardar;
  },
  getDataGuardarComponent(){
    return this.component;
  },
  setDataGuardar(component,dataGuardar){
    this.component=component;
    this.dataGuardar=dataGuardar;
  },
  getToken(){
    // if(this.token ===''){
      return window.sessionStorage.getItem('user_token');
    // }
    // else{
    //   return this.token;
    // }
  },
  getParseDate(fecha){
    var Datef=new Date(fecha);
    var dia = Datef.getDate();
    var mes = (Datef.getMonth()<12) ? Datef.getMonth()+1 : mes = Datef.getMonth();
    var yyyy = Datef.getFullYear();
    var dd = (dia<10) ? '0'+dia : dd=dia;
    var mm = (mes<10) ? '0'+mes : mm=mes;
    return yyyy+'-'+mm+'-'+dd;
  },
  getDateFormat(fecha){
    var Datef=new Date(fecha);
    var dia = Datef.getDate();
    var mes = (Datef.getMonth()<12) ? Datef.getMonth()+1 : mes = Datef.getMonth();
    var yyyy = Datef.getFullYear();
    var dd = (dia<10) ? '0'+dia : dd=dia;
    var mm = (mes<10) ? '0'+mes : mm=mes;
    return dd+'/'+mm+'/'+yyyy;
  },
  getPeriodoFormat(fecha){
    var Datef=new Date(fecha);
    var mes = (Datef.getMonth()<12) ? Datef.getMonth()+1 : mes = Datef.getMonth();
    var yyyy = Datef.getFullYear();
    var mm = (mes<10) ? '0'+mes : mm=mes;
    return mm+'/'+yyyy;
  },
  getDateTimeString(fecha){
    var Datef=new Date(fecha);
    var dia = Datef.getDate();
    var mes = (Datef.getMonth()<12) ? Datef.getMonth()+1 : mes = Datef.getMonth();
    var yyyy = Datef.getFullYear();
    var hour = Datef.getHours()
    var mins = Datef.getMinutes();
    var secs = Datef.getSeconds();

    var HH:string = '', MM:string = '', SS:string = '';
    HH = (hour < 10) ? '0'+hour : hour.toString();
    MM = (mins < 10) ? '0'+mins : mins.toString();
    SS = (secs < 10) ? '0'+secs : secs.toString();
    // return HH+':'+MM+':'+SS;
    var dd = (dia<10) ? '0'+dia : dd=dia;
    var mm = (mes<10) ? '0'+mes : mm=mes;
    return yyyy+'-'+mm+'-'+dd+'T'+HH+':'+MM+':'+SS;
  },
  encodeString(value:string):string{
    var encode = btoa(value);
    var key = CONFIG.key_value;
    var success = key.substring(0,2) + encode + key;
    return success;
  },
  decodeString(value:string):string{
    var key = CONFIG.key_value;
    var success = value.substring(2,(value.length - key.length));
    var decode = atob(success);
    return decode;
  },
  truncateNumber(value: number, fix: number){
    var nro = value.toString();
    nro = (nro.indexOf (".")> 0) ? nro.slice(0, (nro.indexOf("."))+(fix+1)) : value.toString();
    return Number(nro);
  },
  getActive(){
    return this.isActive;
  },
  getFilter(){
    return this.filter;
  },
  setActive(isActive){
    return this.isActive=isActive;
  },
  getCollapse(){
    return this.isCollapse;
  },
  setCollapse(isCollapse){
    this.isCollapse=isCollapse;
  },
}
