import InicioComponent from '@/components/inicio/inicio.vue';
import Barmenu from '@/components/barmenu/barmenu.vue';
import RolesComponent from '@/components/roles/roles.vue';
import UsuarioComponent from '@/components/usuario/usuario.vue';
import IngresoSistemaComponent from '@/components/ingresoSistema/ingresoSistema.vue';
import EditarPerfilComponent from '@/components/editarPerfil/editarPerfil.vue';
import BalanzaComponent from '@/components/balanza/balanza.vue';
import LaboratorioComponent from '@/components/laboratorio/laboratorio.vue';
import ComercialComponent from '@/components/comercial/comercial.vue';
import LeyReferencialBalComponent from "@/components/leyesReferenciales/leyReferencial_bal/leyReferencialBal.vue";
import LeyReferencialLabComponent from '@/components/leyesReferenciales/leyReferencial_Lab/leyReferencialLab.vue';
import LeyReferencialComComponent from '@/components/leyesReferenciales/leyReferencial_Com/leyReferencialCom.vue';
import AcopiadorComponent from '@/components/Acopiador/acopiador.vue';
import ZonaComponent from '@/components/zona/zona.vue';
import CuotasComponent from '@/components/cuotas/cuotas.vue';
import ReporteAcopioMineralComponent from '@/components/Reportes/ReporteAcopioMineral/reporteAcopioMineral.vue';
import ControlCuotasComponent from '@/components/Reportes/ReporteControlCuotas/controlCuotas.vue';
import ReportePruebaComponent from '@/components/Reportes/Reporte_Prueba/reportePrueba.vue';
import ReporteCuotasMesComponent from '@/components/Reportes/ReporteCuotasMes/reporteCuotasMes.vue';
import ReporteAcopiadorMesComponent from '@/components/Reportes/ReporteAcopiadorMes/reporteAcopiadorMes.vue';
import ReporteZonaMesComponent from '@/components/Reportes/ReporteZonaMes/reporteZonaMes.vue';
import ReportePonderadoMesComponent from '@/components/Reportes/ReportePonderadoMes/ReportePonderadoMes.vue';
import ReporteAcopiadorZonaComponent from '@/components/Reportes/ReporteAcopiadorZona/reporteAcopiadorZona.vue';
import LiquidacionComponent from '@/components/liquidacion/liquidacion.vue';
import VentasComponent from '@/components/ventas/ventas.vue';
import ParametrosComponent from '@/components/Parametros/parametros.vue';
import SecochaComponent from '@/components/Secocha/secocha.vue';
import TrujilloComponent from '@/components/Trujillo/trujillo.vue';
import LeyesComercialesComponent from '@/components/LeyesComerciales/leyesComerciales.vue';
import ArmadoRumasComponent from '@/components/planta/ArmadoRumas/armadoRumas.vue';
import TanquesComponent from '@/components/planta/Tanques/tanques.vue';
import GastoAdmComponent from '@/components/GastoAdm/gastoAdm.vue';
import ReporteMineralRobinComponent from '@/components/Reportes/ReporteMineralRobin/reporteMineralRobin.vue';
import AtencionClienteComponent from '@/components/AtencionCliente/atencionCliente.vue';
import CierreInventarioMineralComponent from '@/components/ProcesoCierre/CierreInventarioMineral/cierreInventarioMineral.vue';
import LotesTrujilloComponent from '@/components/LotesTrujillo/lotesTrujillo.vue';
import PersonaComponent from '@/components/Persona/persona.vue';

import MargenLiquidacionComponent from '@/components/Reportes/ReporteMargenes/MargenLiquidacion/margenLiquidacion.vue';
import MargenProductivoComponent from '@/components/Reportes/ReporteMargenes/MargenProductivo/margenProductivo.vue';
import DuplicarLoteComponent from '@/components/DuplicarLote/duplicarLote.vue';
import HabilitarEdicionComponent from '@/components/HabilitarEdicion/habilitarEdicion.vue';
import ProveedorComponent from '@/components/Proveedor/proveedor.vue';
import RegistrarLaboratorioComponent from '@/components/RegistrarLaboratorio/registrarLaboratorio.vue';
import MineralPlantaComponent from '@/components/Reportes/ReportesPlanta/MineralPlanta/mineralPlanta.vue';
import MineralProcesadoComponent from '@/components/Reportes/ReportesPlanta/MineralProcesado/mineralProcesado.vue';
import MargenDiarioComponent from '@/components/Reportes/ReporteMargenes/MargenDiario/margenDiario.vue';
import StockMineralComponent from '@/components/ProcesoCierre/StockMineral/stockMineral.vue';
import ReporteProvisionesComponent from '@/components/Reportes/ReporteProvisiones/reporteProvisiones.vue';
import ValorizacionBullonComponent from '@/components/planta/valorizacionBullon/valorizacionBullon.vue';
export default [
  {
    path: '/menu',
    component: Barmenu,
    redirect: '/',
    children:[
      {
        path: 'inicio',
        component: InicioComponent,
      },
      {
        path: 'balanza',
        component: BalanzaComponent
      },
      {
        path: 'laboratorio',
        component: LaboratorioComponent
      },
      {
        path: 'comercial',
        component: ComercialComponent
      },
      {
        path: 'cuotas',
        component: CuotasComponent
      },
      {
        path: 'ley_ref_balanza',
        component: LeyReferencialBalComponent
      },
      {
        path: 'ley_ref_laboratorio',
        component: LeyReferencialLabComponent
      },
      {
        path: 'ley_ref_comercial',
        component: LeyReferencialComComponent
      },
      {
        path:'roles',
        component: RolesComponent,
      },
      {
        path:'usuario',
        component: UsuarioComponent,
      },
      {
        path: 'acopiador',
        component: AcopiadorComponent,
      },
      {
        path: 'zona',
        component: ZonaComponent,
      },
      {
        path:'editar-perfil',
        component: EditarPerfilComponent
      },
      {
        path:'reporte_acopio',
        component: ReporteAcopioMineralComponent
      },
      {
        path:'control_cuotas',
        component: ControlCuotasComponent
      },
      {
        path: 'reporte_cuotas_mes',
        component: ReporteCuotasMesComponent
      },
      {
        path: 'reporte_acopiador_mes',
        component: ReporteAcopiadorMesComponent,
      },
      {
        path: 'reporte_zona_mes',
        component: ReporteZonaMesComponent
      },
      {
        path: 'reporte_ponderado_mes',
        component: ReportePonderadoMesComponent
      },
      {
        path: 'reporte_acopiador_zona',
        component: ReporteAcopiadorZonaComponent
      },
      {
        path: 'reporte_mineral',
        component: ReporteMineralRobinComponent
      },
      {
        path:'liquidacion',
        component: LiquidacionComponent
      },
      {
        path:'ventas',
        component: VentasComponent
      },
      {
        path:'parametros',
        component: ParametrosComponent
      },
      {
        path: 'secocha',
        component: SecochaComponent
      },
      {
        path: 'trujillo',
        component: TrujilloComponent
      },
      {
        path: 'ley_comercial',
        component: LeyesComercialesComponent
      },
      {
        path: 'armado_rumas',
        component: ArmadoRumasComponent
      },
      {
        path: 'tanques',
        component: TanquesComponent
      },
      {
        path: 'gasto_administrativo',
        component: GastoAdmComponent
      },
      {
        path: 'atencion_cliente',
        component: AtencionClienteComponent
      },
      {
        path: 'cierre_inventario',
        component: CierreInventarioMineralComponent
      },
      {
        path: 'lotes_trujillo',
        component: LotesTrujilloComponent
      },
      {
        path: 'reporte_margen',
        component: MargenLiquidacionComponent
      },
      {
        path: 'reporte_margen_productivo',
        component: MargenProductivoComponent
      },
      {
        path: 'duplicar_lote',
        component: DuplicarLoteComponent
      },
      {
        path: 'habilitar_edicion',
        component: HabilitarEdicionComponent
      },
      {
        path: 'proveedor',
        component: ProveedorComponent
      },
      {
        path: 'registrar_laboratorio',
        component: RegistrarLaboratorioComponent
      },
      {
        path: 'mineral_planta',
        component: MineralPlantaComponent
      },
      {
        path: 'mineral_procesado',
        component: MineralProcesadoComponent
      },
      {
        path: 'margen_diario',
        component: MargenDiarioComponent
      },
      {
        path: 'stock_mineral',
        component: StockMineralComponent
      },
      {
        path: 'reporte_provisiones',
        component: ReporteProvisionesComponent
      },
      {
        path: 'valorizacion_bullon',
        component: ValorizacionBullonComponent
      },
      {
        path: 'persona',
        component: PersonaComponent
      },
      // {
      //   path: 'reporte_prueba',
      //   component: ReportePruebaComponent
      // },
      { path: '**', redirect: '/menu/inicio' }
    ]
  },
  {
    path: '/',
    component: IngresoSistemaComponent,
  },
  { path: '**', redirect: '/' }
];
