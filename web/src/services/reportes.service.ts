import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  GetReporteAcopioMineral(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/acopiomineral', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteControlCuotas(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/controlcuotas', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetDetalleReporteAcopio(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/detalle/acopio', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteAcopioMRL_Detallado(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/acopiomineral/detallado', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteAcopioMes(Anio, type){
    var Data = {strAnio : Anio, strType: type}
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/acopiomes', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteZonaMes(Anio){
    var Data = {strAnio : Anio}
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/zonames', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReportePonderadoMes(Anio){
    var Data = {strAnio : Anio}
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/ponderadomes', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteAcopiadorMes(Anio){
    var Data = {strAnio : Anio}
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/acopiadormes', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteAcopiadorZona(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'reportes/get/acopiadorzona', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteRobin(Anio, Zona:string){
    var Data = {strAnio : Anio, strZona: Zona}
    return axios.post(CONFIG.API_URL+'reportes/get/mineralrobin', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteDiarioRobin(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportediariorobin', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenes(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenDetallado(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/detallado', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenProductivo(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/productivo', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenProdDetallado(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/productivo/detallado', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMineralPlanta(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reporteplanta/mineralplanta', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMineralProcesado(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reporteplanta/mineralprocesado', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },

  GetReporteMargenDiario(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/margendiario', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenDiarioDet(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/margendiario/detallado', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteMargenDiarioZona(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/margendiario/zona', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReporteProvisiones(FormSearch){
    return axios.post(CONFIG.API_URL+'reportes/get/reportemargenes/provisiones', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
