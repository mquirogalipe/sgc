import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  searchMaestro(strNombre: string){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'maestro/search/'+strNombre) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  SearchLiquidaciones(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'liquidacion/search',FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetParametros(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'liquidacion/parametros', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  ActualizarLiquidacion(FormUpdate){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'liquidacion/put',FormUpdate) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetMaquila(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'liquidacion/maquila',Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetReintegro(Lote){
    var Data = {strLote: Lote};
    return axios.post(CONFIG.API_URL+'liquidacion/get/reintegro',Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  PutReintegro(FormUpdate){
    return axios.post(CONFIG.API_URL+'liquidacion/put/reintegro',FormUpdate) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetLaboratorios(){
    return axios.get(CONFIG.API_URL+'liquidacion/laboratorio/get')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
