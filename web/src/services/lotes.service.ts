import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  SearchLotes(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'lotes/search', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  SearchLotesComercial(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'lotes/search/comercial', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLote(FormAgregar){
    return axios.post(CONFIG.API_URL+'lotes/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  EditarLoteBalanza(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'lotes/update/balanza', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarLoteLaboratorio(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'lotes/update/laboratorio', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarLoteComercial(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'lotes/update/comercial', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  DeleteLote(idLote, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'lotes/delete/'+idLote+'/'+strUsuario) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  DuplicarLote(idLote, strLote){
    return axios.get(CONFIG.API_URL+'lotes/duplicate/'+idLote+'/'+strLote)
    .then(response =>{
      return response.data;
    })
  }
}
