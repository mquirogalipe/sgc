import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'cuota/get/all') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarCuota(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'cuota/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarCuota(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'cuota/update', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EliminarCuota(idCuota, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'cuota/delete/'+idCuota+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data;
    })
  },
  BuscarCuotas(Month){
    var Data = {strMes: Month};
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'cuota/search', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
