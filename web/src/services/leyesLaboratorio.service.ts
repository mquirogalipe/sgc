import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  SearchLeyesLaboratorio(strLote){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'leyes/laboratorio/get/'+strLote) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLeyesLaboratorio(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyes/laboratorio/put', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  AgregarLeyesLote(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyes/laboratorio/lote/put', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
}
