import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  GetVentas(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'ventas/search', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  PutVenta(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'ventas/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
}
