import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'leycomercial/get') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLeyComercial(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leycomercial/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarLeyComercial(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leycomercial/update', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EliminarLeyComercial(idLeyCom, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'leycomercial/delete/'+idLeyCom+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data;
    })
  }
}
