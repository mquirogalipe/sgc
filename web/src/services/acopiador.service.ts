import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'acopiador/get/all') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarAcopiador(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'acopiador/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarAcopiador(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'acopiador/update', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EliminarAcopiador(idAcopiador, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'acopiador/delete/'+idAcopiador+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data;
    })
  }
}
