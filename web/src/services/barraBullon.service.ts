import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  getBarrasBullon(FormSearch){
    return axios.post(CONFIG.API_URL+'barrabullon/get', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarBarraBullon(FormAgregar){
    return axios.post(CONFIG.API_URL+'barrabullon/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  EditarBarraBullon(rowEdit){
    return axios.post(CONFIG.API_URL+'barrabullon/update', rowEdit)
    .then(response =>{
      return response.data;
    })
  }
}
