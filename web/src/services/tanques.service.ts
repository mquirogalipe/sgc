import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  getTanques(){
    return axios.get(CONFIG.API_URL+'tanques/get')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  PutTanque(FormAgregar){
    return axios.post(CONFIG.API_URL+'tanques/put', FormAgregar)
    .then(response =>{
      return response.data
    })
  },
}
