import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchLaboratorios(Data){
    return axios.post(CONFIG.API_URL+'laboratorio/search', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLaboratorio(FormAgregar){
    return axios.post(CONFIG.API_URL+'laboratorio/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  EditarLaboratorio(rowEdit){
    return axios.post(CONFIG.API_URL+'laboratorio/update', rowEdit)
    .then(response =>{
      return response.data;
    })
  },
}
