import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchConsumos(strLote){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'consumo/get/'+strLote ) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarConsumo(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'consumo/put', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
}
