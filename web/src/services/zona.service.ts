import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'zona/get/all') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarZona(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'zona/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarZona(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'zona/update', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EliminarZona(idZona, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'zona/delete/'+idZona+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data;
    })
  },
  SearchZona(strZona){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'zona/search/'+strZona) //, {headers: header})
    .then(response => {
      return response.data;
    })
  }
}
