import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchLotesAS(FormSearch){
    return axios.post(CONFIG.API_URL+'lotestrujillo/search', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLotesAS(FormAgregar){
    return axios.post(CONFIG.API_URL+'lotestrujillo/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  AgregarAnyLotesAS(Lista){
    return axios.post(CONFIG.API_URL+'lotestrujillo/put/any', Lista)
    .then(response =>{
      return response.data;
    })
  },
  UpdateLotesAS(rowEdit){
    return axios.post(CONFIG.API_URL+'lotestrujillo/update', rowEdit)
    .then(response =>{
      return response.data;
    })
  },
  SearchLotesLJ(FormSearch){
    return axios.post(CONFIG.API_URL+'lotestrujillo/search/loteslj', FormSearch)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
