import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'usuario/get/all') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  getRoles(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'rol/get/all') //, {headers: header})
    .then(response => {
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarUsuario(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'usuario/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data
    })
  },
  EliminarUsuario(idUsuario, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'usuario/delete/'+idUsuario+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data
    })
  },
  EditarUsuario(rowSelectedEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'usuario/update', rowSelectedEdit) //, {headers: header})
    .then(response =>{
      return response.data
    })
  },
  ConsultarUsuarios(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'usuario/search', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetUsuarioAccesos(strCargo, header){
    // var head = {'Authorization': 'Bearer '+header};
    return axios.get(CONFIG.API_URL+'usuario/get/accesos/'+strCargo) //, {headers: head})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
