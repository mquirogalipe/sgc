import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingPersona(strCodPersona: string){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'usuario/get/persona/'+strCodPersona) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  UpdatePersona(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'usuario/update/persona', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  ChangeUsuario(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'usuario/change', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  }
}
