import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchGastoAdm(Mes){
    var Data:any = {strMes: Mes}
    return axios.post(CONFIG.API_URL+'gastoadm/search', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetGastoAdm(Data){
    return axios.post(CONFIG.API_URL+'gastoadm/get', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetProveedores(Zona){
    var Data:any = {strZona : Zona }
    return axios.post(CONFIG.API_URL+'gastoadm/get/proveedor', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetAcopiadores(){
    return axios.get(CONFIG.API_URL+'gastoadm/get/acopiador')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarGastoAdm(FormAgregar){
    return axios.post(CONFIG.API_URL+'gastoadm/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  EditarGastoAdm(FormEdit){
    return axios.post(CONFIG.API_URL+'gastoadm/update', FormEdit)
    .then(response =>{
      return response.data;
    })
  },
  EliminarGastoAdm(idGasto, strUsuario){
    return axios.delete(CONFIG.API_URL+'gastoadm/delete/'+idGasto+'/'+strUsuario)
    .then(response => {
      return response.data;
    })
  },
  ObtenerGastoAdmPorMes(anio){
    var Data = {strAnio: anio};
    return axios.post(CONFIG.API_URL+'gastoadm/get/mes', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
