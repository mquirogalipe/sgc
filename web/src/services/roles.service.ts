import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  loadingData(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'rol/get/all') //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  getAccesos(){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'accesos/get/all') //, {headers: header})
    .then(response => {
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  getRolAcceso(intRol){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.get(CONFIG.API_URL+'rolacceso/get/'+intRol) //, {headers: header})
    .then(response => {
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarRol(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'rol/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EditarRol(rowSelectedEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'rol/update', rowSelectedEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  EliminarRol(idRol, strUsuario){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.delete(CONFIG.API_URL+'rol/delete/'+idRol+'/'+strUsuario) //, {headers: header})
    .then(response => {
      return response.data;
    })
  }
}
