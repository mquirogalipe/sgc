import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  loadingData(){
    return axios.get(CONFIG.API_URL+'proveedor/get/all')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarProveedor(FormAgregar){
    return axios.post(CONFIG.API_URL+'proveedor/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },
  EditarProveedor(rowEdit){
    return axios.post(CONFIG.API_URL+'proveedor/update', rowEdit)
    .then(response =>{
      return response.data;
    })
  },
  EliminarProveedor(idZona, strUsuario){
    return axios.delete(CONFIG.API_URL+'proveedor/delete/'+idZona+'/'+strUsuario)
    .then(response => {
      return response.data;
    })
  },
  SearchProveedor(Data){
    return axios.post(CONFIG.API_URL+'proveedor/search', Data)
    .then(response => {
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
