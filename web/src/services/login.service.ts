import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  headers : {'Access-Control-Allow-Origin':'*'},
  LoginUser(FormLogin){
    return axios.post(CONFIG.API_URL+'membership/login', FormLogin, {headers: this.headers})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  Authentification(usuario){
    return axios.post(CONFIG.API_URL+'membership/authentication', usuario, {headers: this.headers})
    .then(response =>{
      GLOBAL.setToken(response.data);
      return JSON.parse(JSON.stringify(response.data));
    })
  }
}
