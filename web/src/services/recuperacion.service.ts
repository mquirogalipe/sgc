import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchRecuperacion(strLote, strTipoProceso){
    var Data = {strLote: strLote, strTipoProceso: strTipoProceso}
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'recuperacion/get', Data) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarRecuperacion(Data){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'recuperacion/put', Data) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  }
}
