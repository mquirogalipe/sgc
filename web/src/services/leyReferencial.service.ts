import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  // headers : {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')},
  SearchLeyesReferenciales(FormSearch){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyreferencial/search', FormSearch) //, {headers: header})
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarLeyReferencial(FormAgregar){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyreferencial/put', FormAgregar) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  UpdateLeyReferencialBal(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyreferencial/update/balanza', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  UpdateLeyReferencialLab(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyreferencial/update/laboratorio', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
  UpdateLeyReferencialCom(rowEdit){
    // var header = {'Authorization': 'Bearer '+window.sessionStorage.getItem('user_token')};
    return axios.post(CONFIG.API_URL+'leyreferencial/update/comercial', rowEdit) //, {headers: header})
    .then(response =>{
      return response.data;
    })
  },
}
