import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  loadingData(){
    return axios.get(CONFIG.API_URL+'cierreinventario/get')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  getRumasInventario(){
    return axios.get(CONFIG.API_URL+'cierreinventario/get/rumas')
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  getPesoInventario(fecha){
    var Data = {strFechaFin: fecha};
    return axios.post(CONFIG.API_URL+'cierreinventario/get/pesos', Data)
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarCierreInventario(FormAgregar){
    return axios.post(CONFIG.API_URL+'cierreinventario/put', FormAgregar)
    .then(response =>{
      return response.data;
    })
  },

}
