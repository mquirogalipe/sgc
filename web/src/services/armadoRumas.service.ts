import axios from 'axios';
import * as CONFIG from '../Config';
import GLOBAL from '../Global';
export default {
  SearchRumas(Data){
    return axios.post(CONFIG.API_URL+'rumas/get', Data )
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  AgregarRuma(Data){
    return axios.post(CONFIG.API_URL+'rumas/put', Data)
    .then(response =>{
      return response.data;
    })
  },
  BuscarRumas(Data){
    return axios.post(CONFIG.API_URL+'rumas/search', Data )
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetAllRumas(Data){
    return axios.post(CONFIG.API_URL+'rumas/get/all', Data )
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
  GetStockMineral(Data){
    return axios.post(CONFIG.API_URL+'rumas/get/stock/mineral', Data )
    .then(response =>{
      return JSON.parse(JSON.stringify(response.data));
    })
  },
}
